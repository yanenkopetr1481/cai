//
//  File.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/16/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import KeychainSwift
import AlamofireImage
import Localize_Swift

enum ApiMethods {
    case getVerificationCode
    case checkVerificationCode
    case getUserData
    case changeUserData
    case logout
    case getUserDeals
    case saveUserPhoto
    case saveDealwithNewProduct
    case getUserProduct
    case getUserPayment
    case anulateDeal
    case getOtherDeal
    case buyDeal
    case getSellers
    case addReplenish
    case getRequisite
    case saveUserDocument
    case checkPhone
    case updateToken
    case getContacts
    case searchContact
    case getExistedContact
    case addContact
    case removeContact
    case payForPurchase
    case addReplenishReceipt
    case getCountries
    case removeReplenish
    case editUser
    case termsAccept
    case updateNotificationToken
    case deleteUserPhoto
    case additionalServices
    case getCategories
    case getFilteredDeals
    case addContactsV2
    case getMyCategories
    case setAddressDelivery
    case createMyCategory
    case editMyCategories
    case deleteMyCategories
    
    case findStoreProductWithFilter
    case createStoreProduct
    case deleteStoreProduct
    case getStoreProducts
    case editStoreProduct
    case setProductsAvgPerGramSum
    case setProductsPriceSum
    case saveDealWithExistProduct
    case getMyFilteredDeals
}

enum DealDataType {
    case purchase
    case sell
}

class APIManager: NSObject {
    
    static let sharedInstance = APIManager()
    
    private static let sendPhoneCode = "/api/auth/code/"
    private static let authenticate = "/api/auth/authenticate"
    private static let userData = "/api/user"
    private static let changeUserData = "/api/user"
    private static let logout = "/api/user/logout"
    private static let getUserDeals = "/api/deal"
    private static let saveUserPhoto = "/api/photos/save"
    private static let deleteUserPhoto = "/api/photos"
    private static let saveDealwithNewProduct = "/api/deal/create/withNewProduct"
    private static let getUserProduct = "/api/product/active"
    private static let getUserPaymnet = "/api/user/payments/me"
    private static let anulateDeal = "/api/deal/anulate"
    private static let otherDeal = "/api/deal/other"
    private static let buyDeal = "/api/deal/accept"
    private static let contacts = "/api/user/contacts"
    private static let getRequisite = "/api/requisite"
    private static let saveUserDocument = "/api/document"
    private static let checkPhone = "/api/user/phone"
    private static let updateToken = "/api/auth/refreshToken"
    private static let getContacts = "/api/contactInfo/support"
    private static let addReplenish = "/api/user/payments/replenish"
    private static let payForPurchase = "/api/user/payments/purchase"
    private static let addReplenishReceipt = "/api/user/payments/replenish/receipt"
    private static let getCountries = "/api/location/countries"
    private static let removeReplenish = "/api/user/payments/replenish/"
    private static let termsAccept = "/api/deal/accept/newTerms"
    private static let updateNotificationToken = "/api/auth/notificationToken"
    private static let additionalServices = "/api/admin/additionalServices"
    private static let getCategories = "/api/deal/categories"
    private static let getExistedContacts = "/api/user/contacts/inApp"
    private static let addContactsV2 = "/api/user/contacts/v2"
    private static let setAddressDelivery = "/api/deal/deliveryAddress"
    
    private static let getMyCategories = "/api/product/categories"
    private static let editMyCategories = "/api/product/categories/title"
    
    private static let findStoreProductWithFilter = "/api/product/find"
    private static let storeProduct = "/api/product"
    private static let saveDealWithExistProduct = "/api/deal/create/withExistedProduct"
    private static let getMyFilteredDeals = "/api/deal/my"
        
    static var headers = [
        "Content-Type": "application/json"
    ]
    
    let defaults = UserDefaults.standard
    var isTokenUpdating = false
    
    private static let path = Bundle.main.path(forResource: "ServerApi", ofType: "plist")
    private static let config = NSDictionary(contentsOfFile: path!)
    static let baseURLString = config!["url"] as! String
    
    func callToApi(url:String, method: HTTPMethod,  params: [String : Any], onSuccess: @escaping([String:JSON]) -> Void, apiMethod:ApiMethods,viewController:UIViewController) {
        
        APIManager.headers["Accept-Language"] = Localize.currentLanguage()
         
        switch apiMethod {
        case .payForPurchase, .checkVerificationCode, .logout, .getVerificationCode,.addReplenish, .addContact, .changeUserData, .anulateDeal, .buyDeal, .updateToken , .removeContact, .removeReplenish, .editUser, .termsAccept, .updateNotificationToken, .deleteUserPhoto, .getFilteredDeals, .getExistedContact, .addContactsV2, .setAddressDelivery, .createMyCategory, .editMyCategories, .deleteMyCategories, .editStoreProduct, .setProductsPriceSum, .setProductsAvgPerGramSum, .deleteStoreProduct, .findStoreProductWithFilter, .saveDealWithExistProduct, .getMyFilteredDeals:
            AF.request(url, method: method, parameters: params, encoding: JSONEncoding.default, headers: HTTPHeaders(APIManager.headers))
                .validate()
                .responseJSON(queue: .main, options: JSONSerialization.ReadingOptions.allowFragments, completionHandler: { (response) in
                    switch response.result {
                    case .success:
                        if let result = response.data {
                            let data = JSON(result).dictionary!
                            
                            if (viewController is AddProfileData) {
                                onSuccess(data)
                            }
                            else {
                                if let status = data["status"]?.number, status == 1001, apiMethod != .updateToken {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "mainRouter") as! RouterNavigation
                                    vc.providesPresentationContextTransitionStyle = true
                                    vc.definesPresentationContext = true
                                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                                    vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                                    
                                    UserDefaults.standard.set(false, forKey: "isUserVerified")
                                    
                                    viewController.present(vc, animated: true, completion: nil)
                                    return
                                }
                                else {
                                    onSuccess(data)
                                }
                            }
                        }
                    case .failure(let error):
                        let error = error.errorDescription
                        print(error!)
                        onSuccess([:])
                    }
                })
        case  .searchContact,.getContacts, .getUserPayment, .getSellers, .getUserDeals, .getUserProduct, .getOtherDeal, .getRequisite, .checkPhone, .getUserData, .getCountries, .additionalServices, .getCategories, .getMyCategories, .getStoreProducts:
            AF.request(url, method: method, parameters: params, encoding: URLEncoding.default, headers: HTTPHeaders(APIManager.headers))
                .validate()
                .response(queue: .main, completionHandler: { (response) in
                    switch response.result {
                    case .success:
                        if let result = response.data {
                            let data = JSON(result).dictionary!
                            
                            if (viewController is AddProfileData) {
                                onSuccess(data)
                            }
                            else {
                                if let status = data["status"]?.number, status == 1001 {
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "mainRouter") as! RouterNavigation
                                    vc.providesPresentationContextTransitionStyle = true
                                    vc.definesPresentationContext = true
                                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                                    vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                                    
                                    UserDefaults.standard.set(false, forKey: "isUserVerified")
                                    
                                    viewController.present(vc, animated: true, completion: nil)
                                    return
                                }
                                else {
                                    onSuccess(data)
                                }
                            }
                        }
                    case .failure(let error):
                        let error = error.errorDescription
                        print(error!)
                        onSuccess([:])
                    }
                })
        case .saveUserPhoto, .saveDealwithNewProduct, .saveUserDocument, .addReplenishReceipt, .createStoreProduct:
            break
        }
    }
    
    func removeReplenish(id: Int, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController){
        let urlString : String = APIManager.baseURLString + APIManager.removeReplenish + "\(id)"
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .delete, params: [:], onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .removeReplenish, viewController: viewController)
    }
    
    func removeContact(id: Int, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.contacts + "/\(id)"
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .delete, params: ["id" : id], onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .removeContact, viewController: viewController)
    }
    
    func searchContact(value: String, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.contacts
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: ["criteria" : value], onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .searchContact, viewController: viewController)
    }
    
    func searchUsersContact(name: String, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.contacts
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: ["criteria" : name], onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .searchContact, viewController: viewController)
    }
    
    func editContact(id: Int, value: String, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.contacts
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .patch, params: ["id" : id, "name": value], onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .editUser, viewController: viewController)
    }
    
    func addContact(id: Int, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.contacts
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .post, params: ["id" : id], onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .addContact, viewController: viewController)
    }
    
    func addContactsV2(ids: [Int], completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.addContactsV2
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .post, params: ["ids" : ids], onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .addContactsV2, viewController: viewController)
    }
    
    func getExistedContacts(phones: [String], completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.getExistedContacts
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .post, params: ["phones" : phones], onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .getExistedContact, viewController: viewController)
    }
    
    func termsAccept(id: Int, type:Bool, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.termsAccept
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .put, params: ["id" : id,"accept":type], onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .termsAccept, viewController: viewController)
    }
    
    func setAddressDelivery(data: [String:Any], completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.setAddressDelivery
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .patch, params: data, onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .setAddressDelivery, viewController: viewController)
    }
    
    func getMyCategories(parentID:Int?, all:Bool, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        var urlString : String = APIManager.baseURLString + APIManager.getMyCategories
        
        if parentID != nil {
            urlString = urlString + "?parentId=\(parentID!)"
            urlString = urlString + "&all=\(all)"
        }
        else {
            urlString = urlString + "?all=\(all)"
        }
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [String:Any](), onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .getMyCategories, viewController: viewController)
    }
    
    func createMyCategory(parentID:Int?, title:String, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.getMyCategories
        
        var data = [String:Any]()
        if parentID != nil {
            data["parentId"] = parentID
            data["title"] = title
        }
        else {
            data["title"] = title
        }
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .post, params: data, onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .createMyCategory, viewController: viewController)
    }
    
    func editMyCategory(id:Int, title:String, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.editMyCategories
        let data = ["id": id, "title":title] as [String : Any]
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .patch, params: data, onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .editMyCategories, viewController: viewController)
    }
    
    func deleteMyCategory(id:Int, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.getMyCategories + "/\(id)"
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .delete, params: [String:Any](), onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .deleteMyCategories, viewController: viewController)
    }
    
    func getCategories(parentID:Int?, completionHandler: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        var urlString : String = APIManager.baseURLString + APIManager.getCategories
        
        if parentID != nil {
            urlString = urlString + "?parentId=\(parentID!)"
        }
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [String:Any](), onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .getCategories, viewController: viewController)
    }
    
    func getCountries(completionHandler: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.getCountries
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [:], onSuccess: { data in
            completionHandler(data)
        }, apiMethod: .getCountries, viewController: viewController)
    }
    
    fileprivate func checkRefreshTokenIsExpired(completionHandler: @escaping(Bool) -> Void,viewController:UIViewController) {
        
        if !isTokenUpdating {
            
            let seconds =  ApiConfigurationData.expiresIn != "" ? (Double(ApiConfigurationData.expiresIn)! - Date().toMillis()) / 1000 : 20.0
            
            if (seconds < 10) {
                
                self.isTokenUpdating = true
                APIManager.headers.removeValue(forKey: "Authorization")
                
                self.callToApi(url: APIManager.baseURLString + APIManager.updateToken, method: .put, params: ["refreshToken" : ApiConfigurationData.refreshToken], onSuccess: { data in
                    
                    self.isTokenUpdating = false
                    
                    guard let accessToken = data["accessToken"]?.string, let refreshToken = data["refreshToken"]?.string, let expiresIn = data["expiresIn"]?.int else {
                        completionHandler(false)
                        return
                    }
                    
                    ApiConfigurationData.expiresIn = "\(expiresIn)"
                    ApiConfigurationData.refreshToken = refreshToken
                    ApiConfigurationData.accessToken = accessToken
                    
                    let keychain = KeychainSwift()
                    keychain.accessGroup = "A5MXJN8KJZ.com.iPiterGroup.TestCaiTraidingApp"
                    keychain.set(refreshToken, forKey: "refresh")
                    
                    completionHandler(true)
                    
                }, apiMethod: .updateToken, viewController: viewController)
            }
            else {
                completionHandler(true)
            }
        }
    }
    
    private func sendRequest(url:String, method: HTTPMethod,  params: [String : Any], onSuccess: @escaping([String:JSON]) -> Void, apiMethod:ApiMethods,viewController:UIViewController) {
        
        checkRefreshTokenIsExpired(completionHandler: { status in
            
            if status {
                self.callToApi(url: url, method: method, params: params, onSuccess: { (data) in
                    self.isTokenUpdating = false
                    onSuccess(data)
                }, apiMethod: apiMethod, viewController: viewController)
            }
            else {
                onSuccess([:])
            }
        }, viewController: viewController)
    }
    
    private func sendPhotoRequest(data:[Data], url:URL, onSuccess: @escaping([String:JSON]) -> Void) {
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        
        var headers = APIManager.headers
        headers["Content-Type"] = "multipart/form-data"
        headers["Content-Disposition"] = "form-data"
        
        AF.upload(multipartFormData: { (multipartFormData) in
            data.forEach { (data) in
                multipartFormData.append(data, withName: "photo", fileName: ".jpeg", mimeType: "image/jpeg")
            }
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: HTTPHeaders(headers), interceptor: .none, fileManager: .default).responseJSON { response in
            //            debugPrint(response)
            switch response.result {
            case .success:
                if let result = response.data {
                    let data = JSON(result)
                    onSuccess(["url" : data])
                }
            case .failure(_):
                onSuccess([:])
            }
        }
    }
    
    private func prepareSendPhotoRequest(sendData:[Data], url:URL, onSuccess: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        
        
        checkRefreshTokenIsExpired(completionHandler: { (status) in
            if status {
                self.sendPhotoRequest(data: sendData, url: url) { (data) in
                    onSuccess(data)
                }
            }
            else {
                onSuccess([:])
            }
        }, viewController: viewController)
    }
    
    func updateToken(refreshToken:String, onSuccess: @escaping([String:JSON]) -> Void,viewController:UIViewController){
        
        let urlString : String = APIManager.baseURLString + APIManager.updateToken
        sendRequest(url: urlString, method: .put, params: [
            "deviceId": UIDevice.current.identifierForVendor!.uuidString,
            "refreshToken" : refreshToken
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .updateToken, viewController: viewController)
    }
    
    func updateNotificationToken(onSuccess: @escaping([String:JSON]) -> Void,viewController:UIViewController) {
        
        let parameters = [
            "deviceId": UIDevice.current.identifierForVendor!.uuidString,
            "notificationToken": self.defaults.string(forKey: "PushNotificationsToken") ?? ""
            ] as [String : Any]
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        
        let urlString : String = APIManager.baseURLString + APIManager.updateNotificationToken
        sendRequest(url: urlString, method: .put, params: parameters, onSuccess: { data in
            onSuccess(data)
        }, apiMethod: .updateNotificationToken, viewController: viewController)
    }
    
    func checkVerificationToken(phoneNumber: String, token: String,onSuccess: @escaping([String:JSON]) -> Void,viewController:UIViewController){
        
        let urlString : String = APIManager.baseURLString + APIManager.authenticate
        let parameters = [
            "phone": phoneNumber,
            "token": token,
            "deviceInfo": [
                "deviceType" : "DEVICE_TYPE_IOS",
                "deviceId": UIDevice.current.identifierForVendor!.uuidString,
                "notificationToken": self.defaults.string(forKey: "PushNotificationsToken")
            ]
            ] as [String : Any]
        
        sendRequest(url: urlString, method: .post, params: parameters, onSuccess: { data in
            onSuccess(data)
        }, apiMethod: .checkVerificationCode, viewController: viewController)
    }
    
    func getUserData(onSuccess: @escaping([String:JSON]) -> Void,viewController:UIViewController){
        
        let urlString : String = APIManager.baseURLString + APIManager.userData
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        
        sendRequest(url: urlString, method: .get, params: [:], onSuccess: { data in
            print(data)
            onSuccess(data)
        }, apiMethod: .getUserData, viewController: viewController)
    }
    
    func getVerificationCode(phoneNumber: String, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController){
        
        let urlString : String = APIManager.baseURLString + APIManager.sendPhoneCode
        let parameters = ["phone": phoneNumber] as [String : Any]
        
        sendRequest(url: urlString, method: .put, params: parameters, onSuccess: { data in
            onSuccess(data)
        }, apiMethod: .getVerificationCode, viewController: viewController)
    }
    
    func checkPhone(phoneNumber: String, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController){
        
        let urlString : String = APIManager.baseURLString + APIManager.checkPhone
        let parameters = ["phone": phoneNumber] as [String : Any]
        
        sendRequest(url: urlString, method: .get, params: parameters, onSuccess: { data in
            onSuccess(data)
        }, apiMethod: .checkPhone, viewController: viewController)
    }
    
    func getContacts(onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        APIManager.headers.removeValue(forKey: "Authorization")
        let urlString : String = APIManager.baseURLString + APIManager.getContacts
        
        sendRequest(url: urlString, method: .get, params: [:], onSuccess: { data in
            onSuccess(data)
        }, apiMethod: .getContacts, viewController: viewController)
    }
    
    func changeUserData(data: [String:String], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController){
        
        let urlString : String = APIManager.baseURLString + APIManager.changeUserData
        let parameters = data as [String : Any]
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .put, params: parameters, onSuccess: { data in
            onSuccess(data)
        }, apiMethod: .changeUserData, viewController: viewController)
    }
    
    func payForpurchase(id:NSNumber, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController){
        
        let urlString : String = APIManager.baseURLString + APIManager.payForPurchase
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        
        sendRequest(url: urlString, method: .post, params: [ "id": id ], onSuccess: { data in
            onSuccess(data)
        }, apiMethod: .payForPurchase, viewController: viewController)
    }
    
    func logout(onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController){
        
        let urlString : String = APIManager.baseURLString + APIManager.logout
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .post, params: [
            "deviceId": UIDevice.current.identifierForVendor!.uuidString,
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .logout, viewController: viewController)
    }
    
    func getRequisite(onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.getRequisite
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [:], onSuccess: { data in
            onSuccess(data)
        }, apiMethod: .getRequisite, viewController: viewController)
    }
    
    func getUserDeals(active: Bool, type:DealDataType, page:Int,  onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.getUserDeals
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [
            "page" : page,
            "size" : 7,
            "active" : active,
            "action" : type == .sell ? "SELL" : "PURCHASE"
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .getUserDeals, viewController: viewController)
    }
    
    func getUserProducts(page:Int, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.getUserProduct
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [
            "page" : page,
            "size" : 7
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .getUserProduct, viewController: viewController)
    }
    
    func getSellers(page:Int, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.contacts
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [
            "page" : page,
            "size" : 15
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .getSellers, viewController: viewController)
    }
    
    func getUserPayment(page:Int, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.getUserPaymnet
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [
            "page" : page,
            "size" : 10
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .getUserPayment, viewController: viewController)
    }
    
    func saveUserDocument(data:[UIImage], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.saveUserDocument
        let url = URL(string:urlString)!
        
        var images = [Data]()
        data.forEach { (image) in
            images.append(image.jpegData(compressionQuality: 0.3)!)
        }
        prepareSendPhotoRequest(sendData: images, url: url, onSuccess:{ (data) in
            onSuccess(data)
        }, viewController: viewController)
    }
    
    func saveUserPhoto(data:UIImage, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.saveUserPhoto
        let image = data.jpegData(compressionQuality: 0.3)
        let url = URL(string:urlString)!
        
        prepareSendPhotoRequest(sendData: [image!], url: url, onSuccess:{ (data) in
            onSuccess(data)
        }, viewController: viewController)
    }
    
    func deleteUserPhoto(url:String, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.deleteUserPhoto
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"

        sendRequest(url: urlString, method: .delete, params: ["imageUrl":url], onSuccess: { data in
            onSuccess(data)
        }, apiMethod: .deleteUserPhoto, viewController: viewController)
    }
    
    func addReplenishReceipt(data:[String:String], images:[UIImage], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.addReplenishReceipt
        let url = URL(string: urlString)!
        
        prepareSetDataWithMultipart(url: url, imageName: "photos", sendData: data, images: images, onSuccess: { (data) in
            onSuccess(data)
        }, method: .put, viewController: viewController)
    }
    
    func addReplenish(data:[String:String], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.addReplenish
        
        sendRequest(url: urlString, method: .post, params: data, onSuccess: { data in
            onSuccess(data)
        }, apiMethod: .addReplenish, viewController: viewController)
    }
    
    func saveDealwithNewProduct(data:[String:String], images:[UIImage], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.saveDealwithNewProduct
        let url = URL(string: urlString)!
        
        prepareSetDataWithMultipart(url: url, imageName: "images", sendData: data, images: images, onSuccess: { (data) in
            onSuccess(data)
        }, method: .post, viewController: viewController)
    }
        
    func saveDealWithExistProduct(data:[String:Any], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.saveDealWithExistProduct
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .post, params: data, onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .saveDealWithExistProduct, viewController: viewController)
    }
    
    func buyDeal(id:NSNumber, additionalServices:[Int], accept:Bool, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.buyDeal
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .post, params: [
            "id":id,
            "accept": accept,
            "additionalServices": additionalServices
        ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .buyDeal, viewController: viewController)
    }
    
    func anulateDeal(id:NSNumber,reason:String,anulate:Bool, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.anulateDeal
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .put, params: [
            "id":id,"anulationReason":"","anulate": anulate
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .anulateDeal, viewController: viewController)
    }
    
    func getOtherDeals(page:Int, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.otherDeal
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [
            "page" : page,
            "size" : 7
    
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .getOtherDeal, viewController: viewController)
    }
    
    func getFiltersDeals(page:Int, data:[String:Any], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.otherDeal
        
        var configuredData = data
        
        configuredData["page"]  = page
        configuredData["size"]  = 7

        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .post, params: configuredData, onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .getFilteredDeals, viewController: viewController)
    }
    
    func getMyFiltersDeals(page:Int, data:[String:Any], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.getMyFilteredDeals
        
        var configuredData = data
        
        configuredData["page"]  = page
        configuredData["size"]  = 7

        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .post, params: configuredData, onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .getMyFilteredDeals, viewController: viewController)
    }
    
    func getAdditionalServices(onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.additionalServices
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [String:Any](), onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .additionalServices, viewController: viewController)
    }
        
    private func prepareSetDataWithMultipart(url:URL,imageName:String, sendData:[String:String], images:[UIImage], onSuccess: @escaping([String:JSON]) -> Void, method:HTTPMethod = .post, viewController: UIViewController) {
        
        checkRefreshTokenIsExpired(completionHandler:  { status in
            
            if status {
                
                self.setDataWithMultipart(url: url, imageName: imageName, data: sendData, images: images, onSuccess: { (data) in
                    onSuccess(data)
                }, method: method)
            }
            else {
                onSuccess([:])
            }
        }, viewController: viewController)
    }
    
    private func setDataWithMultipart(url:URL,imageName:String, data:[String:String], images:[UIImage], onSuccess: @escaping([String:JSON]) -> Void , method: HTTPMethod = .post) {
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        
        var headers = APIManager.headers
        headers["Content-Type"] = "multipart/form-data"
        headers["Content-Disposition"] = "form-data"
        
        AF.upload(multipartFormData: { (multipart) in
            
            images.forEach { (image) in
                multipart.append(image.jpegData(compressionQuality: 0.3)!, withName: imageName, fileName: "photo.jpeg", mimeType: "image/jpeg")
            }
            
            data.forEach { data in
                multipart.append(data.value.data(using: .utf8)!, withName: data.key)
            }
        }, to: url, usingThreshold: UInt64.init(), method: method, headers: HTTPHeaders(headers), interceptor: .none, fileManager: .default).responseJSON { response in
            switch response.result {
            case .success:
                if let result = response.data {
                    let data = JSON(result)
                    onSuccess(["url" : data])
                }
            case .failure(_):
                onSuccess([:])
            }
        }
    }
    
    
    // Store
    
    func getFilterStoreProducts(page:Int, data:[String:Any], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.findStoreProductWithFilter
        
        var configuredData = data
        
        configuredData["page"]  = page
        configuredData["size"]  = 10

        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .post, params: configuredData, onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .findStoreProductWithFilter, viewController: viewController)
    }
    
    func createStoreProduct(data:[String:String], images:[UIImage], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.storeProduct
        let url = URL(string: urlString)!
        
        prepareSetDataWithMultipart(url: url, imageName: "images", sendData: data, images: images, onSuccess: { (data) in
            onSuccess(data)
        }, method: .post, viewController: viewController)
    }
    
    func deleteStoreProducts(id:Int, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        let urlString : String = APIManager.baseURLString + APIManager.storeProduct + "/\(id)"

        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .delete, params: [String:Any](), onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .deleteStoreProduct, viewController: viewController)
    }
    
    func getStoreUserProducts(page:Int, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.storeProduct
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .get, params: [
            "page" : page,
            "size" : 7,
            "active" : true
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .getStoreProducts, viewController: viewController)
    }
    
    func setProductAvgPerGramm(ids:[Int], avgPerGramm:Double, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.storeProduct + "/prices"
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .patch, params: [
            "productIds" : ids,
            "avgPerGramm" : avgPerGramm
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .setProductsAvgPerGramSum, viewController: viewController)
    }
    
    func setProductPrices(ids:[Int], price:Double, onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.storeProduct + "/prices"
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .patch, params: [
            "productIds" : ids,
            "price" : price
            ], onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .setProductsPriceSum, viewController: viewController)
    }
    
    func editStoreProduct(data:[String:Any], onSuccess: @escaping([String:JSON]) -> Void, viewController: UIViewController) {
        
        let urlString : String = APIManager.baseURLString + APIManager.storeProduct
        
        APIManager.headers["Authorization"] = "Bearer \(ApiConfigurationData.accessToken)"
        sendRequest(url: urlString, method: .put, params: data, onSuccess: { data in
                onSuccess(data)
        }, apiMethod: .editStoreProduct, viewController: viewController)
    }
}
