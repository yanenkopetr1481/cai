//
//  AppDelegate.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/12/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseCore
import FirebaseInstanceID
import FirebaseMessaging
import KeychainSwift
import Reachability
import SwiftyJSON
import MFSDK
import CoreLocation


@UIApplicationMain class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    let defaults = UserDefaults.standard
    let keychain = KeychainSwift()
    var reachability: Reachability?
    
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    var locationStatus : NSString = "Not Started"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        initLocationManager()
        
        reachability = Reachability()
        try? reachability?.startNotifier()
        
        getContactData()
        
        UNUserNotificationCenter.current().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        
        //        defaults.removeObject(forKey: "AppLocalBadges")
        if let badges = defaults.array(forKey: "AppLocalBadges") {
            
            if (badges.indices.contains(0) && (badges[0] as? Int) != nil) {
                AppLocalData.BadgesCount[.deal] = badges[0] as? Int
            }
            
            if (badges.indices.contains(1) && (badges[1] as? Int) != nil) {
                AppLocalData.BadgesCount[.payment] = badges[1] as? Int
            }
            
            if (badges.indices.contains(2) && (badges[2] as? Int) != nil) {
                AppLocalData.BadgesCount[.purchase] = badges[2] as? Int
            }
            
            if (badges.indices.contains(3) && (badges[3] as? Int) != nil) {
                AppLocalData.BadgesCount[.credit] = badges[3] as? Int
            }
            
            UIApplication.shared.applicationIconBadgeNumber = AppLocalData.BadgesCount[.deal]! + AppLocalData.BadgesCount[.payment]! + AppLocalData.BadgesCount[.purchase]! + AppLocalData.BadgesCount[.credit]!
        }
        
        
        Messaging.messaging().delegate = self
        
        
        InstanceID.instanceID().instanceID { (result, error) in
            if error != nil {
            } else if let result = result {
                self.defaults.set(result.token, forKey: "PushNotificationsToken")
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        UIApplication.shared.windows.forEach { window in
            
            if #available(iOS 13.0, *) {
                window.overrideUserInterfaceStyle = .light
            }
        }
        
        setupPayment()
        
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Light", size: 18.0)!], for: .normal)
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(5)
        UserDefaults.standard.set(true, forKey: "isUserVerified")
        
        return true;
    }
    
    // Location Manager helper stuff
    func initLocationManager() {
        seenError = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func setupPayment() {
        
        //        let token = "7Fs7eBv21F5xAocdPvvJ-sCqEyNHq4cygJrQUFvFiWEexBUPs4AkeLQxH4pzsUrY3Rays7GVA6SojFCz2DMLXSJVqk8NG-plK-cZJetwWjgwLPub_9tQQohWLgJ0q2invJ5C5Imt2ket_-JAlBYLLcnqp_WmOfZkBEWuURsBVirpNQecvpedgeCx4VaFae4qWDI_uKRV1829KCBEH84u6LYUxh8W_BYqkzXJYt99OlHTXHegd91PLT-tawBwuIly46nwbAs5Nt7HFOozxkyPp8BW9URlQW1fE4R_40BXzEuVkzK3WAOdpR92IkV94K_rDZCPltGSvWXtqJbnCpUB6iUIn1V-Ki15FAwh_nsfSmt_NQZ3rQuvyQ9B3yLCQ1ZO_MGSYDYVO26dyXbElspKxQwuNRot9hi3FIbXylV3iN40-nCPH4YQzKjo5p_fuaKhvRh7H8oFjRXtPtLQQUIDxk-jMbOp7gXIsdz02DrCfQIihT4evZuWA6YShl6g8fnAqCy8qRBf_eLDnA9w-nBh4Bq53b1kdhnExz0CMyUjQ43UO3uhMkBomJTXbmfAAHP8dZZao6W8a34OktNQmPTbOHXrtxf6DS-oKOu3l79uX_ihbL8ELT40VjIW3MJeZ_-auCPOjpE3Ax4dzUkSDLCljitmzMagH2X8jN8-AYLl46KcfkBV";
        
        let token = "4Rxj5l7rdMUlXwhGeZikX4v-q53cw26woMeJFpNRIUttEi8Y_wuQHyNVAfp1hByWgQXmxBQwYH30dFyMbbuALTAhUjPQ77rZzEFRSYv4lMVLi6-ThTL30ms1zUjkk_gUJ8g-285jbkXsYjqMaS_ZjKi6rgQETWlcZcWez7pJMEfcxzIzzVGC97QOeBqtSBRqT4rFa9uyUJyv-poUrt1TF_tIPAOHjAuSYBnR4y0c4Kd8pmMd0R9BcCiZhZTWgBLK8BNIYOz5bNPVzWl-EKKZpPzF8bdD5L2mNm9Uo5dsrVbVBgIgkh3mXPFIP2LasJ12CTpeLc1ODtTwoeG-TVb_7ARg-f3QUXTy36uDn___t8jjF7re-qd0QWGYvFXzW1bLAlsDPsmYCD7qxtKxVMwbRcJYNkrsA3bdpcjl18kOKdg-ndVkMnqFNXo_-BZdAEabv-_WKbCKT3ZICWGeD49qNL69UiQ3bUgE6gZNkxWYb0gdrFjBitl1xv2uiT06NcONNNDaW-8xE0i16Oiu3Y3Z4IvqgSOUjIoRK1HQDmj0rquwEhTXJaZPR1hIinuuDe8d_ybms5BeNisq6SXq-hT9OLYB6dcsJHJxBTyeFztrb9AfLL_hH00huwEjTV8jXzHVdjWAovtvd8QHryqBMvYsC0N79I7fjtlNiGKrBIE9lLKfoJrUOILQQrBSqalsFw6wfHemyw";
        
        MFSettings.shared.configure(token: token, baseURL: "https://api.myfatoorah.com")
        
        // You can change title and color of navigation bar, also title and color of dismiss button
        let them = MFTheme(navigationTintColor: .white, navigationBarTintColor: .darkGray, navigationTitle: "payment_replenishBalance".localized(), cancelButtonTitle: "Cancel".localized())
        
        MFSettings.shared.setTheme(theme: them)
    }
    
    func saveDictionary(dict: Dictionary<Int, String>, key: String){
        let preferences = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: dict)
        preferences.set(encodedData, forKey: key)
        // Checking the preference is saved or not
    }
    
    func getDictionary(key: String) -> Dictionary<Int, String> {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: key) != nil{
            let decoded = preferences.object(forKey: key)  as! Data
            let decodedDict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! Dictionary<Int, String>
            
            return decodedDict
        } else {
            let emptyDict = Dictionary<Int, String>()
            return emptyDict
        }
    }
    
    func getContactData() {
        APIManager.sharedInstance.getContacts(onSuccess: { (data) in
            if let status = data["status"]?.number, status == 1, let response = data["data"]?.dictionary {
                
                response["contacts"]?.array?.each({ (index, contact) in
                    if contact["contactType"].string == "PHONE" {
                        ApiConfigurationData.contacNumber.append(contact["info"].string ?? "")
                    }
                    
                    if contact["contactType"].string == "TELEGRAM" {
                        ApiConfigurationData.telegram = contact["info"].string ?? ""
                    }
                })
                
                ApiConfigurationData.supportWorkingAdditionalInfo = response["supportWorkingAdditionalInfo"]?.string ?? String()
                ApiConfigurationData.supportWorkingHours = response["supportWorkingHours"]?.string ?? String()
                ApiConfigurationData.supportWorkingDays = response["supportWorkingDays"]?.string ?? String()
            }
        }, viewController: UIViewController())
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let paymentId = url[MFConstants.paymentId] {
            NotificationCenter.default.post(name: .applePayCheck, object: paymentId)
        }
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        var id = userInfo["type"] as? NSString
        id = id != nil ? id : "5"
        let type = Int(id! as String)
        
        switch type {
        case 0:
            AppLocalData.BadgesCount[.payment]! += 1
        case 1:
            AppLocalData.BadgesCount[.purchase]! += 1
        case 2:
            AppLocalData.BadgesCount[.deal]! += 1
        case 6:
            AppLocalData.BadgesCount[.credit]! += 1
        default:
            break
        }
        
        let data = [AppLocalData.BadgesCount[.deal],AppLocalData.BadgesCount[.payment],AppLocalData.BadgesCount[.purchase],AppLocalData.BadgesCount[.credit]]
        defaults.setValue(data, forKey: "AppLocalBadges")
        let countBadge = AppLocalData.BadgesCount[.deal]! + AppLocalData.BadgesCount[.payment]! + AppLocalData.BadgesCount[.purchase]! + AppLocalData.BadgesCount[.credit]!
        UIApplication.shared.applicationIconBadgeNumber = countBadge
        NotificationCenter.default.post(name: Notification.Name("ReceiveData"), object: nil)
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CAI_Traiding")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if !defaults.bool(forKey: "ISPushNotificationsEnable") {
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        
        var id = userInfo["type"] as? NSString
        id = id != nil ? id : "5"
        let type = Int(id! as String)
        
        switch type {
        case 0:
            PageData.page = .Payments
        case 1:
            PageData.page = .Purchases
        case 2:
            PageData.page = .Deals
        case 6:
            PageData.page = .Home
        default:
            break
        }
        
        if !(UserProfileData.name?.isEmpty ?? true), var topController = UIApplication.shared.keyWindow?.rootViewController  {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            guard let vc = topController.storyboard!.instantiateViewController(withIdentifier: "routerNavigator") as? RouterNavigation else {
                return
            }
            
            vc.providesPresentationContextTransitionStyle = true
            vc.definesPresentationContext = true
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            topController.present(vc, animated: true, completion: nil)
        }
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.hexString
        self.defaults.set(deviceTokenString, forKey: "PushNotificationsToken")
    }
}

extension AppDelegate:MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let token = fcmToken else {
            return
        }
        
        let dataDict:[String: String] = ["token": token]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}


extension AppDelegate:CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        if ((error) != nil) {
            if (seenError == false) {
                seenError = true
                print(error)
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocation = manager.location else { return }
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            UserProfileData.countryCode = placemarks?.first?.isoCountryCode
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        var shouldIAllow = false
        
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.denied:
            locationStatus = "User denied access to location"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "Status not determined"
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LabelHasbeenUpdated"), object: nil)
        if (shouldIAllow == true) {
            NSLog("Location to Allowed")
            // Start location services
            locationManager.startUpdatingLocation()
        } else {
            NSLog("Denied access: \(locationStatus)")
        }
    }
}
