//
//  PreviewReplenishNavigator.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 14.07.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit

class PreviewReplenishNavigator: UINavigationController {

    var paymentData:PaymentData?

    override func viewDidLoad() {
        super.viewDidLoad()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "previewPlenish") as! ReplenishConfirm
        vc.paymentData = paymentData
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let vc = segue.destination as? ReplenishConfirm {
//            vc.paymentData = self.paymentData
//        }
//    }
}
