//
//  Protocols.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/29/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

protocol HomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: MenuOption?)
}

protocol MenuControllerDelegate {
    func menuResponseToggle(responsedMenuOption menuOption: MenuOption?)
}
