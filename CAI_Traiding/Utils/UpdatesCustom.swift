//
//  UpdatesUI.swift
//  Updates
//
//

import Foundation
import StoreKit
import SafariServices
import Updates

public class UpdatesUICustom: NSObject {
    
    private let animated: Bool
    private let completion: (() -> Void)?
    
    public init(animated: Bool = true, completion: (() -> Void)? = nil) {
        self.animated = animated
        self.completion = completion
    }
    
    /// Button title displayed by the `UIAlertController`.
    private func buttonTitle(_ key: String) -> String {
        let updateLocalizationKey = "updates.\(key)-button-title".lowercased()
        let updateButtonTitle = NSLocalizedString(updateLocalizationKey, comment: key)
        if updateButtonTitle != updateLocalizationKey {
            return updateButtonTitle
        } else {
            return key
        }
    }
    
    /// Presents SKStoreProductViewController modally.
    public func presentAppStore(animated: Bool = true, delegate: SKStoreProductViewControllerDelegate,
                                presentingViewController: UIViewController) {
        guard let appStoreId = Updates.appStoreId, let appStoreIdentifierInt = UInt(appStoreId) else {
            return
        }
        let animated = self.animated
        let appStoreIdentifier: NSNumber = NSNumber(value: appStoreIdentifierInt)
        let parameters = [SKStoreProductParameterITunesItemIdentifier: appStoreIdentifier]
        let viewController = SKStoreProductViewController()
        viewController.delegate = delegate
        viewController.loadProduct(withParameters: parameters) { [weak self] (loadedSuccessfully, error) in
            if !loadedSuccessfully, let appStoreURL = Updates.appStoreURL {
                print(error as Any)
                self?.presentSafariViewController(animated: animated,
                                                  presentingViewController: presentingViewController,
                                                  url: appStoreURL)
            }
        }
        DispatchQueue.main.async {
            presentingViewController.present(viewController, animated: animated, completion: nil)
        }
    }
    
    /// Prompt the user to update to the latest version
    func promptToUpdate(_ result: UpdatesResult, animated: Bool = true, completion: (() -> Void)? = nil,
                                      presentingViewController: UIViewController,
                                      title: String? = nil,
                                      message: String? = nil) {
        
        switch result {
        case .available(let update):
            let alertTitle: String
            if let title = title {
                alertTitle = title
            } else if let productName = Updates.productName {
                alertTitle = "\(productName) v\(update.newVersionString) Available"
            } else {
                alertTitle = "Version \(update.newVersionString) Available"
            }
            let alertMessage: String? = message ?? update.releaseNotes
            let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
            let updateButtonTitle = buttonTitle("Update")
            let updateAction = UIAlertAction(title: updateButtonTitle, style: .default) { _ in
                self.presentAppStore(animated: animated, delegate: self, presentingViewController: presentingViewController)
            }
            
            let cancelButtonTitle = buttonTitle("Cancel")
            let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { _ in
                alert.dismiss(animated: animated, completion: self.completion)
            }
            alert.addAction(updateAction)
            alert.addAction(cancelAction)
            presentingViewController.present(alert, animated: animated, completion: nil)
        case .none:
            self.completion!()
            break;
        }
        
        //        guard let .available(update) = result else {  return }
    }
}

extension UpdatesUICustom: SKStoreProductViewControllerDelegate {
    
    /// Invoked when the user elects to close the SKStoreProductViewController.
    /// - Parameters:
    ///     - viewController: The SKStoreProductViewController to be dismissed.
    @objc public func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: animated, completion: completion)
    }
}

// Private API
private extension UpdatesUICustom {
    
    /// Presents the specified URL
    func presentSafariViewController(animated: Bool, presentingViewController: UIViewController, url: URL) {
        let safariViewController = SFSafariViewController(url: url)
        presentingViewController.present(safariViewController, animated: animated, completion: nil)
    }
    
}
