//
//  ViewController.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/12/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import Localize_Swift

class ViewController: RootViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = UserDefaults.standard.string(forKey: "AppLanguageLocal") ?? "en"
        Localize.setCurrentLanguage(lang)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
