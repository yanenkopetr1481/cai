//
//  MenuOptions.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/29/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit

enum MenuOption: Int, CustomStringConvertible {
    
    case None
    case Home
    case Purchases
    case Payments
    case Sellers
    case Deals
    case NewOffers
    case Settings
    case Support
    case Store
    
    var description: String {
        switch self {
        case .None: return ""
        case .Home: return "menu_1".localized()
        case .Purchases: return "menu_3".localized()
        case .Payments: return "menu_4".localized()
        case .Sellers: return "menu_5".localized()
        case .Deals: return "menu_6".localized()
        case .NewOffers: return "menu_9".localized()
        case .Settings: return "menu_7".localized()
        case .Support: return "menu_8".localized()
        case .Store: return "menu_10".localized()
        }
    }
    
    var image: UIImage {
        switch self {
        case .Home: return UIImage(named: "menu_home") ?? UIImage()
        case .Purchases: return UIImage(named: "menu_purchases") ?? UIImage()
        case .Payments: return UIImage(named: "menu_payment") ?? UIImage()
        case .Sellers: return UIImage(named: "menu_contact") ?? UIImage()
        case .Deals: return UIImage(named: "menu_deals") ?? UIImage()
        case .NewOffers: return UIImage(named: "menu_deals") ?? UIImage()
        case .Settings: return UIImage(named: "menu_setting") ?? UIImage()
        case .Support: return UIImage(named: "menu_support") ?? UIImage()
        case .Store: return UIImage(named: "menu_store") ?? UIImage()
        case .None: return UIImage()
        }
    }
}
