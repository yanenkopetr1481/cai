//
//  PaymentData.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/21/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import Foundation
import SwiftyJSON

struct PaymentData {
    var paymentStatus:String
    var id:NSNumber
    var sum:NSNumber
    var invoiceId:NSNumber
    var description:String
    var title:String
    var payerName:String
    var approver:String
    var dealData:DealData?
    var type:String?
    var reason:String?
    var created:Double?
    var updated:Double?
    var photos:[JSON]?
    var adminPhotos:[JSON]?

}
