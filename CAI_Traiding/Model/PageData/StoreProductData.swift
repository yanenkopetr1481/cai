//
//  StoreProductData.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 05.04.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct StoreProductData:Equatable {
    static func == (lhs: StoreProductData, rhs: StoreProductData) -> Bool {
        return (lhs.id == rhs.id)
    }
    
    var id:NSNumber
    var productTitle:String
    var productWeight:Double
    var productGramSum:Double
    var productSum:Double
    var productImagesURLs:[JSON]
    var category:JSON?
    var description:String
}
