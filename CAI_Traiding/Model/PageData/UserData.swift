//
//  User.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/16/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct ApiConfigurationData {
    static var expiresIn = ""
    static var refreshToken = ""
    static var accessToken = ""
    static var contacNumber = [String()]
    static var telegram = ""
    static var supportWorkingAdditionalInfo = ""
    static var supportWorkingHours = ""
    static var supportWorkingDays = ""
    static var appImages = NSCache<AnyObject, AnyObject>()
}

struct UserProfileData {
    static var id:Int?
    static var creditCard:String?
    static var cardHolderName:String?
    static var commissionForSum:Double?
    static var commissionForGramm:Double?
    static var name:String?
    static var phone:String?
    static var image:UIImage?
    static var wallet:walletType?
    static var address:String?
    static var language:String?
    static var imageUrl:URL?    
    static var nickname:String?
    static var isVerified:Bool = false
    static var countryName:String?
    static var countryID:Int?
    static var countryCode:String?

    struct walletType {
         var creditLimit:Double?
         var debt:Double?
         var debtCreated:String?
         var debtExpired:String?
         var balance:Double?
    }
}

struct SellerData {
    var id:Int
    var name:String
    var phone:String
    var nickName:String
}
