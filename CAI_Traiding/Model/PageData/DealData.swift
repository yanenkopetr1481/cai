//
//  DealData.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/16/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct DealData:Equatable {
    static func == (lhs: DealData, rhs: DealData) -> Bool {
        return (lhs.id == rhs.id)
    }
    
    var id:NSNumber
    var productTitle:String
    var productWeight:String
    var productSeller:String
    var productCustomer:String
    var productGramSum:String
    var productSum:String
    var productImages:[UIImage]
    var productPayStatus:String
    var shipingData:[JSON]
    var otherStatus:String
    var type:DealType
    var productImagesURLs:[JSON]
    var customerAccept:Bool
    var sellerAccept:Bool
    var anulateData:DealAnulateData
    var isPrivate:Bool
    var commision_included:Bool
    var commissionForGramm:String
    var commissionForSum:String
    var termsDaata:TermsData?
    var productCreated:Double?
    var productUpdated:Double?
    var services:Array<JSON>?
    var category:JSON?
}

//needsToAcceptNewTerms
//newTerms
//customerAcceptNewTerms
//
struct TermsData {
    var needsToAcceptNewTerms:Bool?
    var newTerms:[String : SwiftyJSON.JSON]?
    var customerAcceptNewTerms:Bool?
    var sellerAcceptNewTerms:Bool?
}

enum DealType {
    case exist
    case new
    case preview
    case purchase
    case other
}

struct DealAnulateData {
    var isSellerAnulate:Bool
    var isCustomerAnulate:Bool
    var anulateReson:String
}
