//
//  ViewController.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 26.01.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func performSegueToReturnBack()  {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
