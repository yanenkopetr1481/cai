//
//  Image.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/10/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectiveC
import AlamofireImage

extension UIImage {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

extension UIImageView {
    
    func downloadImageFrom(url:URL, contentMode: UIView.ContentMode, setNewImage:Bool = true, defaultImageName:String = "empty_product") {
        
        self.showSkeleton()
        self.showActivityIndicator()
        
        AF.request(url).response {
            result in
            DispatchQueue.main.async {
                if let data = result.data {
                    let image = UIImage(data: data)
                    ApiConfigurationData.appImages.setObject(image!, forKey: url.absoluteString as AnyObject)

                    if setNewImage {
                        self.image = UIImage(data: data)
                        self.hideSkeleton(transition: .crossDissolve(0.15))
                        self.contentMode =  contentMode
                        
                        for view in self.subviews {
                            view.removeFromSuperview()
                        }
                    }
                }
                else {

                    if (defaultImageName == "profile") {
                        UserProfileData.image = UIImage(named: defaultImageName)
                    }
                    
                    for view in self.subviews {
                        view.removeFromSuperview()
                    }
                    
                    self.image = UIImage(named: defaultImageName)
                }
            }
        }
    }
}

private var activityIndicatorAssociationKey: UInt8 = 0

extension UIImageView {
    var activityIndicator: UIActivityIndicatorView! {
        get {
            return objc_getAssociatedObject(self, &activityIndicatorAssociationKey) as? UIActivityIndicatorView
        }
        set(newValue) {
            objc_setAssociatedObject(self, &activityIndicatorAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func showActivityIndicator() {
        self.activityIndicator = UIActivityIndicatorView(style: .gray)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.frame = CGRect(x:0.0, y:0.0, width:40.0, height:40.0);
        self.activityIndicator.style = .gray
        self.activityIndicator.center = CGPoint(x:self.frame.size.width / 2, y:self.frame.size.height / 2);
        self.activityIndicator.autoresizingMask = .flexibleLeftMargin
        self.activityIndicator.isUserInteractionEnabled = false
        
        self.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
    }
    
    
    func hideActivityIndicator() {
        self.activityIndicator.stopAnimating()
    }
}

extension UIImage {
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
}
