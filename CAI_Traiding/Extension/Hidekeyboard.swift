//
//  Hidekeyboard.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/22/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import Foundation
import  UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
