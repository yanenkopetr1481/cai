//
//  Global.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 04.03.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import Foundation

extension Date {
    func days(to secondDate: Date, calendar: Calendar = Calendar.current) -> Int {
        return calendar.dateComponents([.day], from: self, to: secondDate).day!
    }
}

extension Notification.Name {
    public static let appDidInstall = Notification.Name("app-did-install")
    public static let appVersionDidChange = Notification.Name("app-version-did-change")
}
