//
//  Data.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/5/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import Foundation

extension Date {
    func isInSameDayOf(date: Date) -> Bool {
        return Calendar.current.isDate(self, inSameDayAs:date)
    }
}
