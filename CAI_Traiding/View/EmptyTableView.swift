//
//  EmptyTableView.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/30/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit

protocol EmptyTableViewDelegate {
    func refreshData()
}

class EmptyTableView: UIView {
    
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var refreshBtn:UIButton!
    
    var delegate:EmptyTableViewDelegate?
    
    @IBAction func refreshData(_ sender:UIButton) {
        self.delegate?.refreshData()
    }
}
