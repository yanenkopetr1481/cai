//
//  AddressAlert.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 27.03.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyJSON
import RxAnimated

protocol AddressAlertDelegate {
    func setAdress(id:Int, address:String, countryID:String, phone:String, comment:String, contactName:String)
}

class AddressAlert: UIViewController {

    @IBOutlet weak var alertView:UIView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var addressTextField:UITextField!
    @IBOutlet weak var btn:UIButton!
    
    @IBOutlet weak var phoneTextField:UITextField!
    @IBOutlet weak var commentTextField:UITextField!
    @IBOutlet weak var contactNameField:UITextField!

    @IBOutlet weak var addressLabel:UILabel!
    @IBOutlet weak var contactNameLabel:UILabel!
    @IBOutlet weak var phoneLabel:UILabel!
    @IBOutlet weak var commentLabel:UILabel!

    @IBOutlet weak var selectedCountry:UITextField!
    @IBOutlet weak var selectCountryBtn:UIButton!
    
    @IBOutlet weak var textFieldTrailingConstraint:NSLayoutConstraint!
    
    var delegate:AddressAlertDelegate?
    var address = ""
    var disposed = DisposeBag()
    var id:Int!
    
    var languages = [JSON]()
    var country:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewConfiguration()
    }
    
    @IBAction func showLanguagePicker (_ sender:UIButton) {
        
        if languages.count > 0 {
            DispatchQueue.main.async {
                let alert = UIAlertController(style: .actionSheet, title: "", message: "")

                let pickerViewValues: [[String]] = [self.languages.map { $0["name"].string! }]
                
                var id = self.languages.firstIndex { (data) -> Bool in
                    return data["id"].int == self.country
                }
                
                id = id != nil ? (id! + 1) : 0
                
                let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: id!)

                alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
                    self.country = index.row
                    self.selectedCountry.rx.text.onNext(self.languages[self.country!]["name"].string!)

                    self.selectedCountry.isHidden = false
                    self.selectCountryBtn.setTitle("replenish_changeCountry".localized(), for: .normal)
                }
                
                alert.addAction(title: "Cancel".localized(), style: .cancel)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func setupViewConfiguration() {
        
        APIManager.sharedInstance.getCountries(completionHandler: { (data) in
            if let data = data["data"]?.array, !data.isEmpty {
                self.languages = data
                self.country = 0
                self.selectedCountry.isHidden = false
                self.selectedCountry.rx.text.onNext(self.languages[self.country!]["name"].string!)
            }
        }, viewController: self)
        
        titleLabel.text = "address_title".localized()
        btn.setTitle("address_btn".localized(), for: .normal)
        
        btn.rx.tap.subscribe(onNext: {
            _ in
            
            let selectedCountryJson = self.languages.first { (datas) -> Bool in
                return datas.dictionary?["name"]?.string == self.selectedCountry.text
            }
            
            if !(selectedCountryJson?.isEmpty ?? true) {
                
                let id = selectedCountryJson?.dictionary?["id"]?.number
                let countryId = "\(id ?? 1)"
                
                self.dismiss(animated: true) {
                    self.delegate?.setAdress(id: self.id, address: self.addressTextField.text!, countryID: countryId, phone: self.phoneTextField.text!, comment: self.commentTextField.text!, contactName: self.contactNameField.text!)
                }
            }
            
        }).disposed(by: self.disposed)
        
        let textFields = [addressTextField, phoneTextField, commentTextField, contactNameField]
        
        Observable.combineLatest(textFields.map {  $0!.rx.text.orEmpty })
            .map { $0.map { !$0.isEmpty } }
            .map { $0.contains(false) }
            .bind(to: btn.rx.isHidden)
            .disposed(by: self.disposed)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        addressTextField.becomeFirstResponder()
    }
    
    @objc
    dynamic func keyboardWillShow(
        _ notification: NSNotification
    ) {
        animateWithKeyboard(notification: notification) {
            (keyboardFrame) in
            let constant = 20 + keyboardFrame.height
            self.textFieldTrailingConstraint?.constant = constant
        }
    }
    
    @objc
    dynamic func keyboardWillHide(
        _ notification: NSNotification
    ) {
        animateWithKeyboard(notification: notification) {
            (keyboardFrame) in
            self.textFieldTrailingConstraint?.constant = 20
        }
    }
}

extension UIViewController {
    func animateWithKeyboard(
        notification: NSNotification,
        animations: ((_ keyboardFrame: CGRect) -> Void)?
    ) {
        // Extract the duration of the keyboard animation
        let durationKey = UIResponder.keyboardAnimationDurationUserInfoKey
        let duration = notification.userInfo![durationKey] as! Double
        
        // Extract the final frame of the keyboard
        let frameKey = UIResponder.keyboardFrameEndUserInfoKey
        let keyboardFrameValue = notification.userInfo![frameKey] as! NSValue
        
        // Extract the curve of the iOS keyboard animation
        let curveKey = UIResponder.keyboardAnimationCurveUserInfoKey
        let curveValue = notification.userInfo![curveKey] as! Int
        let curve = UIView.AnimationCurve(rawValue: curveValue)!

        // Create a property animator to manage the animation
        let animator = UIViewPropertyAnimator(
            duration: duration,
            curve: curve
        ) {
            // Perform the necessary animation layout updates
            animations?(keyboardFrameValue.cgRectValue)
            
            // Required to trigger NSLayoutConstraint changes
            // to animate
            self.view?.layoutIfNeeded()
        }
        
        // Start the animation
        animator.startAnimation()
    }
}
