//
//  StoreProductCell.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 07.04.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import UIKit

class StoreProductCell: UITableViewCell {
    
    @IBOutlet weak var productImage:UIImageView!
    @IBOutlet weak var productTitle:UILabel!
    @IBOutlet weak var productWeight:UILabel!
    @IBOutlet weak var productSum:UILabel!
    @IBOutlet weak var productCategory:UILabel!
    @IBOutlet weak var productCategoryLoc:UILabel!

    @IBOutlet weak var productWeightLoc:UILabel!
    @IBOutlet weak var productSumLoc:UILabel!
    @IBOutlet weak var totalStack:UIStackView!
    @IBOutlet weak var productID:UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        productImage.layer.cornerRadius = productImage.frame.width / 2
        productWeightLoc.text = "deal_weight".localized()
        productSumLoc.text = "deal_sum".localized()
        productCategoryLoc.text = "deal_category".localized()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let backgroundView = UIView()
        backgroundView.layer.cornerRadius = 10
        backgroundView.isHidden = true
        self.selectedBackgroundView = backgroundView
    }
    
    func setupViewConfiguration(data:StoreProductData) {
                
        self.productTitle.text = data.productTitle
        self.productCategory.text = data.category?["title"].string
        self.productSum.text = "\(data.productSum)$"
        self.productWeight.text = "\(data.productWeight)g"
        
        self.productID.setTitle("ID:\(data.id)", for: .normal)
    }
}
