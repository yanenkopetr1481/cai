//
//  PurchasesCell.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/21/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit

class PurchasesCell: UITableViewCell {
    
    @IBOutlet weak var productImage:UIImageView!
    @IBOutlet weak var productTitle:UILabel!
    @IBOutlet weak var productWeight:UILabel!
    @IBOutlet weak var productSum:UILabel!
    @IBOutlet weak var payBtn:UIButton!
    @IBOutlet weak var isPrivate:UIButton!
    @IBOutlet weak var productWeightLoc:UILabel!
    @IBOutlet weak var productSumLoc:UILabel!
    @IBOutlet weak var totalStack:UIStackView!
    @IBOutlet weak var productID:UIButton!
    @IBOutlet weak var declineBtn:UIButton!
    
    @IBOutlet weak var statusStackView:UIStackView!
    @IBOutlet var statusesImageView:[UIImageView]!
    @IBOutlet var statusesSerpantin:[UILabel]!
    @IBOutlet weak var btnsStackView: UIStackView!
    
    var statusForAppearStatus = ["in_office", "sent_to_second_office", "in_seconf_office", "sent_to_customer"]

    override func awakeFromNib() {
        super.awakeFromNib()
        
        productImage.layer.cornerRadius = productImage.frame.width / 2
        isPrivate.setTitle("deal_private".localized(), for: .normal)
        productWeightLoc.text = "deal_weight".localized()
        
        self.statusStackView.isHidden = true
        self.declineBtn.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let backgroundView = UIView()
        backgroundView.layer.cornerRadius = 10
        backgroundView.isHidden = true
        self.selectedBackgroundView = backgroundView
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.statusStackView.isHidden = true
        self.btnsStackView.isHidden = false
    }
    
    fileprivate func configureStatusView(_ data: DealData) {

        statusesSerpantin.forEach { (label) in
            label.textColor = UIColor.init(hex: "#d6d6d6")
        }
        
        self.statusStackView.isHidden = true

        if !data.productPayStatus.isEmpty {
            self.payBtn.isHidden = false
            self.payBtn.backgroundColor = (data.productPayStatus == "PAID" || data.productPayStatus == "PAID_TO_SELLER") ? UIColor.init(hex: "#087100") : UIColor.init(hex: "#C80101")
            let status = data.productPayStatus.uppercased()
            self.payBtn.setTitle(status.localized(), for: .normal)
        }
        else {
            self.payBtn.isHidden = true
        }
        
        if !data.shipingData.isEmpty {
            
            var index = 0
            self.statusesImageView.forEach({ (imageView) in
                imageView.image = UIImage(named: "status_\(index)")
                imageView.cornerRadius = imageView.width / 2
                
                index = index + 1
            })
            var statuses = [String]()
            
            data.shipingData.forEach { (shippingData) in
                statuses.append((shippingData.dictionary?["type"]?.string?.replacingOccurrences(of: " ", with: "_"))!)
            }
            
            index = 0
            
            var count = 0
            
            self.statusForAppearStatus.forEach { (status) in
                if statuses.contains(status) {
                    count = count + 1
                }
                
                index += 1
            }

            for i in 0..<count {
                statusesImageView[i].cornerRadius = 10
                if (i == count - 1) {
                    statusesImageView[i].image = UIImage(named: "status_\(i)_filled")
                }
                
                
                if i > 0 && i <= statusesSerpantin.count {
                    let serpantinIndex = i - 1
                    statusesSerpantin[serpantinIndex].textColor = UIColor(hex: "#D32F2F")
                }
            }
            
            self.btnsStackView.isHidden = count > 0
            self.statusStackView.isHidden = count > 0 ? false : true
        }
        else {
            self.statusStackView.isHidden = true
        }
    }
    
    fileprivate func otherStatus(_ data: String, _ isTwoBtn: Bool = false) {
        self.payBtn.isHidden = false
        self.payBtn.backgroundColor = UIColor.init(hex: "#C80101")
        self.payBtn.setTitle(data.uppercased(), for: .normal)
        
        self.declineBtn.isHidden = !isTwoBtn
        
        if isTwoBtn {
            self.payBtn.backgroundColor = UIColor.init(hex: "#087100")
            self.declineBtn.backgroundColor = UIColor.init(hex: "#C80101")
            self.declineBtn.setTitle("Decline".localized(), for: .normal)
        }
    }
    
    fileprivate func hiddenStatusBtn() {
        self.payBtn.isHidden = true
        self.declineBtn.isHidden = true
    }
    
    func setupViewConfiguration(data:DealData) {
        
        hiddenStatusBtn()
        
        self.isPrivate.alpha = data.isPrivate ? 1 : 0
        self.productTitle.text = data.productTitle
        self.productWeight.text = data.productWeight + "g"
        self.productSum.text = data.productSum + "$"
        
        self.productID.setTitle("ID:\(data.id)", for: .normal)
        
        if (data.type != .other) {
            if data.otherStatus != "ACTIVE" {
                if data.otherStatus == "ANULATED" {                    
                    otherStatus(data.type == .preview ? "RETURNED_TO_YOU".localized() : "RETURNED_TO_SELLER".localized())
                }
                else {
                    otherStatus(data.otherStatus.localized())
                }
            }
            else if data.anulateData.isCustomerAnulate &&  data.anulateData.isSellerAnulate {
                otherStatus(data.type == .preview ? "RETURNED_TO_YOU".localized() : "RETURNED_TO_SELLER".localized())
            }
            else if data.anulateData.isCustomerAnulate {
                otherStatus(data.type != .purchase ? "customer_anulate".localized() : "deal_you_anulate".localized())
            }
            else if data.anulateData.isSellerAnulate {
                otherStatus(data.type == .purchase ? "seller_anulate".localized() : "deal_you_anulate".localized())
            }
            else if data.sellerAccept && data.customerAccept {
                configureStatusView(data)
            }
            else if data.sellerAccept && !data.customerAccept {
                otherStatus(data.type != .purchase ? "customer_accept".localized() : "Accept_single".localized(), data.type == .purchase)
            }
            else if !data.sellerAccept && data.customerAccept {
                otherStatus(data.type == .purchase ? "seller_accept".localized() : "Accept_single".localized(), data.type != .purchase)
            }
        }
        else {
            hiddenStatusBtn()
        }        
    }
}
