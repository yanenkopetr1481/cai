//
//  MenuCell.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/28/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import BadgeControl

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var submenuTitile:UILabel!
    @IBOutlet weak var submenuIcon:UIImageView!
    @IBOutlet weak var badgeBtn:UIButton!
    
    private var notificationBadge: BadgeController!

    override func awakeFromNib() {
        notificationBadge = BadgeController(for: badgeBtn)
        notificationBadge.animation = BadgeAnimations.rightLeft
        notificationBadge.badgeHeight = 20
        notificationBadge.badgeTextFont = UIFont(name: "Helvetica Neue", size: 12)!
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        let _isSelected = selected
        
        notificationBadge.badgeBackgroundColor = _isSelected ? .white : .red
        notificationBadge.badgeTextColor = _isSelected ? .red : .white
        
        super.setSelected(selected, animated: animated)
    }
    
    func setupViewCinfiguration(_title:String, _tag:Int, _iconName:UIImage) {
        self.submenuTitile.text = _title
        self.tag = _tag
        self.submenuIcon.image = _iconName
    }
    
    func setBadge(_notificationCount: Int, _isSelected: Bool) {

        if _notificationCount > 0 {
            notificationBadge.badgeBackgroundColor = _isSelected ? .white : .red
            notificationBadge.badgeTextColor = _isSelected ? .red : .white

            notificationBadge.addOrReplaceCurrent(with: "\(_notificationCount)", animated: true)
        }
        else {
            notificationBadge.remove(animated: false)
        }
    }
}
