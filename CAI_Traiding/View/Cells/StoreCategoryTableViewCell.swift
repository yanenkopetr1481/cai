//
//  StoreCategoryTableViewCell.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 03.04.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import UIKit

class StoreCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryName:UILabel!
    @IBOutlet weak var showMorebtn:UIButton!
    @IBOutlet weak var leftAnchorConstratint:NSLayoutConstraint!
    
    @IBOutlet weak var editBtn:UIButton!
    @IBOutlet weak var deleteBtn:UIButton!
    @IBOutlet weak var addSubcategoryBtn:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
