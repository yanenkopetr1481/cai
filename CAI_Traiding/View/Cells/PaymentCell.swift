//
//  Payment.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/14/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit

class PaymentCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var paymentDescription: UILabel!
    @IBOutlet weak var paymentStatus: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var imageBlock: UIImageView!
    @IBOutlet weak var paymentCreated: UILabel!
    @IBOutlet weak var midLine: UILabel!
    @IBOutlet weak var roundView:UIView!
    
    var isStatusHide = false

    let backgroundColors = [
        "APPROVED": UIColor.init(hex: "#388E3C"),
        "ANULATED": UIColor.init(hex: "#D32F2F"),
        "AWAITING_FOR_APPROVE": UIColor.init(hex: "#1976D2"),
        "AWAITING_FOR_RECEIPT": UIColor.init(hex: "#1976D2"),
        "PAID": UIColor.init(hex: "#388E3C"),
        "UNPAID": UIColor.init(hex: "#D32F2F"),
        "PAID_TO_SELLER": UIColor.init(hex: "#388E3C"),
        "PAID_OUT": UIColor.init(hex: "#388E3C"),
        "RETURNED_BACK":UIColor.init(hex: "#388E3C")
    ]
    
    let backgroundViewColors = [
//        "APPROVED": UIColor.init(hex: "#2ba644"),
        "APPROVED": UIColor.white,
        "AWAITING_FOR_APPROVE": UIColor.init(hex: "#d6d6d6"),
        "AWAITING_FOR_RECEIPT": UIColor.init(hex: "#d6d6d6"),
    ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        title.linesCornerRadius = 5
        paymentDescription.linesCornerRadius = 5
        price.linesCornerRadius = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let backgroundView = UIView()
        backgroundView.layer.cornerRadius = 10
        backgroundView.backgroundColor = .white
        self.selectedBackgroundView = backgroundView
    }
    
    func setupView(data:PaymentData) {
        self.title.text = data.type?.localized() ?? data.title
        self.paymentDescription.isHidden = false
        self.paymentDescription.text = data.description
        
        self.paymentStatus.isHidden = true
        self.midLine.isHidden = true
        var price = ""
        var sign = ""
        
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm"
        
        if ((data.reason) != nil && data.reason != "") {
            self.title.text = data.reason!
        }
        
        let date = Date(timeIntervalSince1970: data.created!)
        paymentCreated.text = format.string(from: date)
        
        self.roundView.backgroundColor = .white

        switch data.type {
            case "INVOICE_PAYMENT":
                                

                if (data.paymentStatus == "APPROVED") {
                    self.price.textColor = "\(data.sum)".contains("-") ? UIColor.init(hex: "#D32F2F") : UIColor.init(hex: "#388E3C")
                    price = "\(data.sum)".contains("-") ? "\(data.sum)" : "\(data.sum)"
                    sign = "\(data.sum)".contains("-") ? "" : "+"
                }
                else {
                    self.price.textColor = .black
                    price = "\(data.sum)"
                    sign = ""
                    self.roundView.backgroundColor = self.isStatusHide ? .lightGray : self.backgroundViewColors[data.paymentStatus]
                }

                self.imageBlock.image = UIImage(named: "icon_replenish")
                self.paymentDescription.isHidden = true
                break
            case "CARD_PAYMENT":
                self.price.textColor = "\(data.sum)".contains("-") ? UIColor.init(hex: "#D32F2F") : UIColor.init(hex: "#388E3C")
                price = "\(data.sum)".contains("-") ? "\(data.sum)" : "\(data.sum)"
                sign = "\(data.sum)".contains("-") ? "" : "+"
                self.imageBlock.image = UIImage(named: "icon_visa")
                self.paymentDescription.isHidden = true
                break
            case "DEAL_PAYMENT_WITHDRAW":
                price = "\(data.sum)"
                sign = "\(data.sum)".contains("-") ? "" : "-"
                self.price.textColor = UIColor.init(hex: "#D32F2F")
                self.imageBlock.image = UIImage(named: "icon_deal")
                break
            case "DEAL_PAYMENT_REPLENISH":
                self.price.textColor = "\(data.sum)".contains("-") ? UIColor.init(hex: "#D32F2F") : UIColor.init(hex: "#388E3C")
                price = "\(data.sum)".contains("-") ? "\(data.sum)" : "\(data.sum)"
                sign = "\(data.sum)".contains("-") ? "" : "+"
                self.imageBlock.image = UIImage(named: "icon_deal")
                break
            case "DEAL_ANULATED_WITHDRAW":
                price = "\(data.sum)"
                sign = "\(data.sum)".contains("-") ? "" : "-"
                self.price.textColor = UIColor.init(hex: "#D32F2F")
                self.imageBlock.image = UIImage(named: "icon_deal")
                break
            case "DEAL_ANNULATED_REPLENISH":
                self.price.textColor = "\(data.sum)".contains("-") ? UIColor.init(hex: "#D32F2F") : UIColor.init(hex: "#388E3C")
                price = "\(data.sum)".contains("-") ? "\(data.sum)" : "\(data.sum)"
                sign = "\(data.sum)".contains("-") ? "" : "+"
                self.imageBlock.image = UIImage(named: "icon_deal")
                break
            case "BALANCE_WITHDRAW":
                price = "\(data.sum)"
                sign = "\(data.sum)".contains("-") ? "" : "-"
                self.price.textColor = UIColor.init(hex: "#D32F2F")
                self.imageBlock.image = UIImage(named: "icon_replenish")
                break
            case "DEAL_NEW_TERMS_REPLENISH":
                self.price.textColor = "\(data.sum)".contains("-") ? UIColor.init(hex: "#D32F2F") : UIColor.init(hex: "#388E3C")
                price = "\(data.sum)".contains("-") ? "\(data.sum)" : "\(data.sum)"
                sign = "\(data.sum)".contains("-") ? "" : "+"
                self.imageBlock.image = UIImage(named: "icon_replenish")
                break
            case "DEAL_NEW_TERMS_WITHDRAW":
                self.price.textColor = UIColor.init(hex: "#D32F2F")
                price = "\(data.sum)".contains("-") ? "\(data.sum)" : "\(data.sum)"
                sign = "\(data.sum)".contains("-") ? "" : "-"
                self.imageBlock.image = UIImage(named: "icon_replenish")
                break
            case "ADMIN_BALANCE_REPLENISH":
                self.price.textColor = "\(data.sum)".contains("-") ? UIColor.init(hex: "#D32F2F") : UIColor.init(hex: "#388E3C")
                price = "\(data.sum)".contains("-") ? "\(data.sum)" : "\(data.sum)"
                sign = "\(data.sum)".contains("-") ? "" : "+"
                self.imageBlock.image = UIImage(named: "icon_replenish")
                break
            case "ADMIN_BALANCE_WITHDRAW":
                self.price.textColor = UIColor.init(hex: "#D32F2F")
                price = "\(data.sum)".contains("-") ? "\(data.sum)" : "\(data.sum)"
                sign = "\(data.sum)".contains("-") ? "" : "-"
                self.imageBlock.image = UIImage(named: "icon_replenish")
                break
            default:
                price = data.paymentStatus != "" ? "\(data.sum)" : "\(data.sum)"
                sign = data.paymentStatus != "" ? "+" : "-"
                self.price.textColor = data.paymentStatus != "" ? UIColor.init(hex: "#388E3C") : UIColor.init(hex: "#D32F2F")
                self.imageBlock.image = UIImage(named: "icon_deal")
        }
        

        self.price.text = sign + "\(price.doubleValue)" + "$"
        
        if (!data.paymentStatus.isEmpty) {
            self.paymentStatus.backgroundColor = backgroundColors[data.paymentStatus]
            self.paymentStatus.setTitle(data.paymentStatus.localized(), for: .normal)
            self.paymentStatus.isHidden = false

            if data.paymentStatus == "ANULATED" {
                self.midLine.isHidden = false
            }
            
            if (data.paymentStatus != "APPROVED") {
                self.price.text = ""
                self.title.text = "\(data.type?.localized() ?? data.title ) \(price.doubleValue) ($)"
            }
        }
        else if (!(data.dealData?.productPayStatus.isEmpty ?? true)) {

            if let status = data.dealData?.productPayStatus {
                self.paymentStatus.backgroundColor = backgroundColors[status.replacingOccurrences(of: " ", with: "")]
            }
            else {
                self.paymentStatus.backgroundColor = .lightGray
            }
            
            self.paymentStatus.setTitle(data.dealData?.productPayStatus.localized(), for: .normal)
            self.paymentStatus.isHidden = false
        }
        else {
            self.paymentStatus.isHidden = true
        }
        
        self.title.setContentHuggingPriority(. required, for: .horizontal)
        self.price.setContentHuggingPriority(. defaultLow, for: .horizontal)
        
        if (isStatusHide) {
            self.paymentStatus.isHidden = isStatusHide
        }
    }
}

