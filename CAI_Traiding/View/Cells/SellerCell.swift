//
//  SellerCell.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/17/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit

class SellerCell: UITableViewCell {

    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var sellePhone:UILabel!
    @IBOutlet weak var sellerId:UILabel!
    @IBOutlet weak var removeBtn:UIButton!
    @IBOutlet weak var editBtn:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let backgroundView = UIView()
        backgroundView.layer.cornerRadius = 10
        backgroundView.backgroundColor = .white
        self.selectedBackgroundView = backgroundView
    }
    
    func setupView(data:SellerData) {
        self.name.text = data.name
        self.sellePhone.text = data.phone
        self.sellerId.text = "contact_user_id".localized() + data.nickName
    }
}
