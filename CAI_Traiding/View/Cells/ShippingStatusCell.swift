//
//  ShippingStatusCell.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 09.07.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit

class ShippingStatusCell: UITableViewCell {
    
    @IBOutlet weak var s_imageBtn:UIButton!
    @IBOutlet weak var s_imageS:UIImageView!
    @IBOutlet weak var s_type:UILabel!
    
    @IBOutlet weak var s_price:UILabel!
    @IBOutlet weak var s_weight:UILabel!
    @IBOutlet weak var s_price_weight_stack:UIStackView!

    @IBOutlet weak var s_description:UILabel!
    @IBOutlet weak var s_description_stack:UIStackView!

    @IBOutlet weak var s_del_title:UILabel!
    @IBOutlet weak var s_del_stack:UIStackView!
    @IBOutlet weak var s_del_time:UILabel!

    @IBOutlet weak var s_crea_stack:UIStackView!
    @IBOutlet weak var s_creat:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_type: String, _del_time: Double, _creat: Double, _description: String, _price: Double, _weight: Double) {
        
//        s_crea_title.text = "deal_created".localized()
        s_del_title.text = "deal_delivery".localized()
        
        self.s_weight.isHidden = false
        self.s_price.isHidden = false
        self.s_description_stack.isHidden = false
        self.s_price_weight_stack.isHidden = false
        self.s_del_stack.isHidden = false
        self.s_crea_stack.isHidden = false

        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm"
        
        s_type.text = _type.replacingOccurrences(of: " ", with: "_").uppercased().localized()
        
        s_description.text = _description
        if (_description.isEmpty) {
            self.s_description_stack.isHidden = true
        }
        
        var date = Date()
        if (_del_time != 0) {
            date = Date(timeIntervalSince1970: _del_time)
            s_del_time.text = format.string(from: date)
        }
        else {
            self.s_del_stack.isHidden = true
        }
        
        if (_creat != 0) {
            date = Date(timeIntervalSince1970: _creat)
            s_creat.text = format.string(from: date)
        }
        else {
            self.s_crea_stack.isHidden = true
        }
        
        if (_price != 0) {
            self.s_price.text = "\(_price)$"

        }
        else {
            self.s_price.isHidden = true
        }
        
        
        if (_weight != 0) {
            self.s_weight.text = "\(_weight)g"

        }
        else {
            self.s_weight.isHidden = true
        }
        
        
        if self.s_weight.isHidden && self.s_price.isHidden {
            self.s_price_weight_stack.isHidden = true
        }
    }
}
