//
//  Cell.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/21/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import Foundation

import UIKit

class Cell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var label1: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
