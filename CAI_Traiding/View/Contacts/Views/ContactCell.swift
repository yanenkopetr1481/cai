import UIKit
import RxSwift

final class ContactTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    static let identifier = String(describing: ContactTableViewCell.self)
    static let size: CGSize = CGSize(width: 80, height: 80)
    
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    @IBOutlet weak var inviteBtn: UIButton!
    @IBOutlet weak var inviteView: UIView!
    
    var contact: Contact?
    var disposed = DisposeBag()
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    // MARK: Configure Selection
    override func setSelected(_ selected: Bool, animated: Bool) {
        if contact?.isExist == true {
            super.setSelected(selected, animated: animated)
            accessoryType = selected ? .checkmark : .none
        }
    }
    
    func configure(with contact: Contact) {
        
        selectionStyle = .none
        backgroundColor = nil
        contentView.backgroundColor = nil
        
        self.contact = contact
        DispatchQueue.main.async {
            self.update()
        }
    }
    
    func update() {
        guard let contact = contact else { return }
        
        if let ava = contact.image {
            userPhoto?.image = ava
        } else {
            userPhoto?.setImageForName(string: contact.displayName, circular: true, gradient: true)
        }
        
        userName?.text = contact.displayName
        
        if contact.phones.count >= 1  {
            userPhone?.text = "\(contact.phones[0].number)"
        } else {
            userPhone?.text = "No phone numbers available"
        }
        
        inviteView.isHidden = contact.isExist == true
        self.selectionStyle = contact.isExist == true ? .none : .default
    }
}
