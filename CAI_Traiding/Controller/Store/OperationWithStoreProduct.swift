//
//  CreateStoreProduct.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 05.04.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import UIKit
import Gallery
import Lightbox
import JGProgressHUD
import NotificationBannerSwift
import SwiftyJSON
import LGButton
import RxSwift
import Alamofire

enum ProductPreviewStyle {
    case create
    case preview
}

class OperationWithStoreProduct: RootViewController {
    
    @IBOutlet weak var imagesStack: UIStackView!
    @IBOutlet weak var imagesCountLabel: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnChooseImage: LoadingButton!
    @IBOutlet weak var imgProfileHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var yourTextView:UIScrollView!
    @IBOutlet weak var productSumByGramLoc:UILabel!
    @IBOutlet weak var productTitleLoc:UILabel!
    @IBOutlet weak var productWeightLoc:UILabel!
    @IBOutlet weak var productSumLoc:UILabel!
    @IBOutlet weak var nextBtn:UIButton!
    @IBOutlet weak var addPhotoLoc:UILabel!
    @IBOutlet weak var libraryCount: UILabel!
    @IBOutlet weak var libraryStack: UIStackView!
    @IBOutlet weak var categoriesStackViews:UIStackView!
    @IBOutlet weak var categoriesParentStackViews:UIStackView!
    @IBOutlet weak var categoriesTitleLGBtn:LGButton!
    @IBOutlet weak var editBtn:UIButton!
    @IBOutlet weak var removeProduct:UIButton!
    @IBOutlet weak var saveEditingBtn:UIButton!
    @IBOutlet var textFields: [UITextField]!

    var library:[UIImage] = [UIImage]()
    var gallery: GalleryController!
    var lightbox :LightboxController!
    var selectedImageLightBox:Int = 0
    var productData:StoreProductData?
    var isSelectDealType:Bool = true
    var lightBoxType:Bool = false
    var isRemoveImage:Bool = false
    var alertTitle:String = "deal_alertTitle".localized()
    var alertTrueButton:String = "deal_alertTrueButton".localized()
    var alertFalseButton:String = "deal_alertFalseButton".localized()
    var productPriceForGram:String = ""
    var spaceLeading:CGFloat = 10
    
    var categories:[JSON]?
    var disposedBag = DisposeBag()
    var selectedCategoryBtn:LGButton?
    var selectedCategoryData:JSON?
    
    var category:JSON?
    var previewStyle:ProductPreviewStyle!
    var isEditedEnabled:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewConfiguration()
        hideKeyboardWhenTappedAround()
        setTransparentNavigationBar()
        setupGalleryConfiguration()
        setupLocalization()
    }
    
    func createCategory(parentStack:UIStackView, categories:JSON, spacingLeading:CGFloat = 0){
        
        let lgBtn = LGButton()
        lgBtn.titleFontSize = 20
        lgBtn.titleFontName = "Helvetica Neue Light"
        lgBtn.spacingLeading = spacingLeading
        lgBtn.titleNumOfLines = 10
        lgBtn.bgColor = .clear
        lgBtn.titleString = "\(categories["title"].string!)"
        lgBtn.titleColor = .black
        lgBtn.layer.cornerRadius = 5
        lgBtn.leftImageSrc = UIImage(named: "check_unfill")
        
        lgBtn.leftAligned = true
        lgBtn.tag = categories["id"].int!
        
        let childStack = UIStackView()
        childStack.isHidden = true
        
        var haveChild = false
        if (categories["hasChilds"].bool ?? false) {
            lgBtn.rightImageSrc = UIImage(named: "chevron_down")
            childStack.axis = .vertical
            haveChild = true
        }
        
        var isChildLoaded = false
        
        lgBtn.rx.controlEvent(.touchUpInside).bind { (_) in
            
            if (categories["hasChilds"].bool ?? false) {
                
                if (!isChildLoaded) {
                    
                    APIManager.sharedInstance.getMyCategories(parentID: lgBtn.tag, all: false, completionHandler: { (data) in
                        
                        guard let status = data["status"]?.number, status == 1 else {
                            return
                        }
                        
                        data["data"]?.array?.forEach({ (childCategory) in
                            
                            let stack = UIStackView()
                            stack.axis = .vertical
                            self.createCategory(parentStack: stack, categories: childCategory, spacingLeading: self.spaceLeading)
                            childStack.addArrangedSubview(stack)
                        })
                        
                        self.spaceLeading += 10
                        
                        isChildLoaded = true
                        lgBtn.rightImageSrc = UIImage(named: "chevron_up")
                        childStack.isHidden = false
                        return
                    }, viewController: self)
                }
                else {
                    lgBtn.rightImageSrc = !childStack.isHidden ? UIImage(named: "chevron_down") : UIImage(named: "chevron_up")
                    childStack.isHidden = !childStack.isHidden
                }
            }
            
            lgBtn.leftImageSrc = lgBtn.isSelected ? UIImage(named: "check_unfill") : UIImage(named: "check_fill")
            
            self.selectedCategoryBtn?.leftImageSrc = UIImage(named: "check_unfill")
            self.selectedCategoryBtn?.isSelected = false
            self.selectedCategoryBtn = lgBtn.isSelected ? nil : lgBtn
            self.category = lgBtn.isSelected ? nil : categories
            self.selectedCategoryData = lgBtn.isSelected ? nil : categories
            
            lgBtn.isSelected = !lgBtn.isSelected
            
        }.disposed(by: self.disposedBag)
        
        parentStack.addArrangedSubview(lgBtn)
        
        if haveChild {
            parentStack.addArrangedSubview(childStack)
        }
    }
    
    func getCategories() {
        
        self.categoriesStackViews.removeArrangedSubviews()
        self.categoriesStackViews.removeSubviews()
        
        APIManager.sharedInstance.getMyCategories(parentID: nil, all: false, completionHandler: { (data) in
            
            guard let status = data["status"]?.number, status == 1 else {
                return
            }
            
            self.categories?.removeAll()
            self.categories = data["data"]?.array
            
            if (self.categories != nil) {
                
                self.categories!.forEach({ (categories) in
                    let stack = UIStackView()
                    stack.axis = .vertical
                    self.createCategory(parentStack: stack, categories: categories)
                    self.categoriesStackViews.addArrangedSubview(stack)
                })
            }
            
        }, viewController: self)
    }
    
    func setupViewConfiguration() {
        
        removeProduct.isHidden = true
        removeProduct.setTitle("category_deleteCategoryBtn".localized(), for: .normal)
        libraryStack.isHidden = true
        editBtn.isHidden = true
        saveEditingBtn.isHidden = true
        
        self.categoriesTitleLGBtn.titleFontSize = 20
        self.categoriesTitleLGBtn.titleFontName = "Helvetica Neue Light"
        self.categoriesTitleLGBtn.spacingLeading = 0
        self.categoriesTitleLGBtn.titleNumOfLines = 10
        self.categoriesTitleLGBtn.bgColor = UIColor(hex: "#E50100")
        self.categoriesTitleLGBtn.titleString = "Select_category".localized()
        self.categoriesTitleLGBtn.titleColor = .white
        self.categoriesTitleLGBtn.layer.cornerRadius = 5
        self.categoriesTitleLGBtn.isSelected = false
        
        self.categoriesTitleLGBtn.rx.controlEvent(.touchUpInside).bind { (_) in
            self.categoriesStackViews.isHidden = self.categoriesTitleLGBtn.isSelected
            self.categoriesTitleLGBtn.isSelected = !self.categoriesTitleLGBtn.isSelected
            
            if self.categories == nil {
                self.getCategories()
            }
            
        }.disposed(by: self.disposeBag)
        
        editBtn.rx.tap.subscribe(onNext: {
            _ in
            
            self.saveEditingBtn.isHidden = self.isEditedEnabled
            self.categoriesStackViews.isHidden = self.isEditedEnabled
            self.categoriesParentStackViews.isHidden = self.isEditedEnabled
            
            self.textFields.forEach { (textField) in
                textField.isEnabled = !self.isEditedEnabled
                
                if (self.isEditedEnabled) {
                    if textField.tag == 0 {
                        textField.text = self.productData!.productTitle
                    }
                    
                    if textField.tag == 1 {
                        textField.text = "\(self.productData!.productWeight)"
                    }
                    
                    if textField.tag == 2 {
                        textField.text = "\(self.productData!.productSum)"
                    }
                    
                    if textField.tag == 3 {
                        textField.text = "\(self.productData!.productGramSum)"
                    }
                }
            }
            
            self.isEditedEnabled = !self.isEditedEnabled
            
        }).disposed(by: self.disposedBag)
        
        saveEditingBtn.rx.tap.subscribe(onNext: {
            _ in
            
            var isError = 0
            var title:String?
            var productSum:String?
            var productWeight:String?
            
            self.textFields.forEach { (textField) in
                if (textField.text!.isEmpty) {
                    isError = textField.tag == 4 ? 0 : textField.tag
                    UIView.animate(withDuration: 0.3) {
                        textField.layer.borderWidth = 0.5
                        textField.layer.cornerRadius = 5
                        textField.layer.borderColor = UIColor.red.cgColor
                    }
                }
                else {
                    switch textField.tag {
                    case 0:
                        title = textField.text!
                    case 1:
                        productWeight = textField.text!
                    case 2:
                        productSum = textField.text!
                    default:
                        return
                    }
                }
            }
            
            guard let newTitle = title, let newProductWeight = productWeight, let newProductSum = productSum else {
                let banner = NotificationBanner(title:  "Missing".localized(), subtitle: "BannerMissingField".localized(), leftView: nil, rightView: nil, style: .danger, colors: .none)
                banner.show()
                return
            }
            
            if isError == 0 {
                
                let hud = JGProgressHUD(style: .dark)
                           hud.textLabel.text = "Sending".localized()
                           hud.vibrancyEnabled = true
                           hud.show(in: self.view)
                
                APIManager.sharedInstance.editStoreProduct(data: [
                    "id": "\(self.productData!.id)",
                    "title": newTitle,
                    "weight": newProductWeight,
                    "description": "",
                    "price": newProductSum,
                    "categoryId": "\(self.category!["id"].int!)",
                                        
                ], onSuccess: { (data) in
                    
                    guard let status = data["status"]?.number, status == 1  else {
                        UIView.animate(withDuration: 0.1, animations: {
                            hud.textLabel.text = "Error".localized()
                            hud.detailTextLabel.text = "ServerError"
                            hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        })
                        
                        hud.dismiss(afterDelay: 1.0, animated: true)
                        return
                    }
                    
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Success".localized()
                        hud.detailTextLabel.text = nil
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                        self.back(self)
                    }
                }, viewController: self)
            }
            
        }).disposed(by: self.disposedBag)
        
        removeProduct.rx.tap.subscribe(onNext: {
            _ in
            
            let hud = JGProgressHUD(style: .dark)
                       hud.textLabel.text = "Sending".localized()
                       hud.vibrancyEnabled = true
                       hud.show(in: self.view)
            
            APIManager.sharedInstance.deleteStoreProducts(id: Int(truncating: self.productData!.id), onSuccess: { (data) in
                guard let status = data["status"]?.number, status == 1 else {
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Error".localized()
                        hud.detailTextLabel.text = "ServerError"
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    return
                }
                
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Success".localized()
                    hud.detailTextLabel.text = nil
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                    self.back(self)
                }
            }, viewController: self)
        }).disposed(by: self.disposedBag)
        
        textFields.forEach { (textField) in
            textField.addTarget(self, action: #selector(calculateCommisionAndGramPrice), for: UIControl.Event.editingChanged)
            textField.delegate = self
            textField.autocapitalizationType = .none
            if (textField.tag != 0 && textField.tag != 4) {
                textField.keyboardType = .decimalPad
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.categoriesStackViews.isHidden = true
        self.categoriesParentStackViews.isHidden = true
        
        if self.productData != nil {
            
            self.getCategories()
            removeProduct.isHidden = false
            addPhotoLoc.isHidden = true
            editBtn.isHidden = false
            
            btnChooseImage.setImage(nil, for: .normal)
            
            textFields.forEach { (textField) in
                
                textField.isEnabled = false
                
                if textField.tag == 0 {
                    textField.text = productData!.productTitle
                }
                
                if textField.tag == 1 {
                    textField.text = "\(productData!.productWeight)"
                }
                
                if textField.tag == 2 {
                    textField.text = "\(productData!.productSum)"
                }
                
                if textField.tag == 3 {
                    textField.text = "\(productData!.productGramSum)"
                }
            }
            
            nextBtn.isHidden = true

            productData!.productImagesURLs.forEach { urlJson in
                
                self.btnChooseImage.showLoading()
                
                let url = URL(string: urlJson.string!)!
                
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    self.library.append(image)
                    self.btnChooseImage.hideLoading()
                    
                    if (self.library.count > 0) {
                        self.imgProfile.image = self.library.first
                        self.libraryStack.isHidden = false
                        self.libraryCount.text = "\(self.library.count)+"
                    }
                }
                else {
                    AF.request(url).response {
                        result in
                        DispatchQueue.main.async {
                            if let data = result.data {
                                let image = UIImage(data: data)!
                                self.library.append(image)
                                ApiConfigurationData.appImages.setObject(image, forKey: url.absoluteString as AnyObject)
                                self.btnChooseImage.hideLoading()
                                if (self.library.count > 0) {
                                    self.imgProfile.image = self.library.first
                                    self.libraryStack.isHidden = false
                                    self.libraryCount.text = "\(self.library.count)+"
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            if previewStyle == .create {
                if self.category == nil {
                    self.getCategories()
                    self.categoriesParentStackViews.isHidden = false
                }
            }
        }
    }
    
    func setupLocalization() {
        addPhotoLoc.text = "addPhoto".localized()
        productTitleLoc.text = "deal_title".localized()
        productWeightLoc.text = "deal_weight".localized() + ",g"
        productSumByGramLoc.text = "deal_sum_gram".localized() + ",$"
        productSumLoc.text = "deal_sum".localized() + ",$"
        productTitleLoc.text = "deal_title".localized()
        nextBtn.setTitle("Next".localized(), for: .normal)
    }
    
    func setupGalleryConfiguration() {
        Config.Camera.imageLimit = 5
        Config.tabsToShow = [.imageTab, .cameraTab]
    }
    
    func setTransparentNavigationBar() {
        guard let _ = self.navigationController else {
            return
        }
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
        self.title = "newProduct".localized()
    }
    
    @objc func adjustForKeyboard(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
            
            let keyboardScreenEndFrame = keyboardValue.cgRectValue
            let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
            
            if notification.name == UIResponder.keyboardWillHideNotification {
                yourTextView.contentInset = .zero
                self.imgProfileHeightConstraint.constant = 270
                btnChooseImage.isHidden = false
                
                self.title = "newDeal".localized()
            } else {
                
                self.title = ""
                
                if #available(iOS 11.0, *) {
                    yourTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
                } else {
                    yourTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.alignmentRectInsets.bottom, right: 0)
                }
                
                if self.imgProfileHeightConstraint.constant == 270.0 {
                    UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
                        self.imgProfileHeightConstraint.constant -= keyboardSize.height - 100
                        self.btnChooseImage.isHidden = true
                    })
                }
            }
            
            yourTextView.scrollIndicatorInsets = yourTextView.contentInset
        }
    }
    
    @IBAction func back(_ sender:Any) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func showLibrary(_ sender: UIButton) {
        guard library.count > 0 else {
            return
        }
        
        let lightboxImages = library.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.footerView.isHidden = true
        lightbox.imageTouchDelegate = self
        lightbox.modalPresentationStyle = .fullScreen
        lightBoxType = true
        self.present(lightbox, animated: true)
    }
    
    @IBAction func btnChooseImageOnClick(_ sender: UIButton) {
        
        if previewStyle == .create {
            gallery = GalleryController()
            gallery.delegate = self
            gallery.modalPresentationStyle = .fullScreen
            
            present(gallery, animated: true, completion: {})
        }
    }
    
    @IBAction func showPreview(_ sender: UIButton) {
        
        var isError = 0
        var title:String?
        var productSum:String?
        var productWeight:String?
        
        textFields.forEach { (textField) in
            if (textField.text!.isEmpty) {
                isError = textField.tag == 4 ? 0 : textField.tag
                UIView.animate(withDuration: 0.3) {
                    textField.layer.borderWidth = 0.5
                    textField.layer.cornerRadius = 5
                    textField.layer.borderColor = UIColor.red.cgColor
                }
            }
            else {
                switch textField.tag {
                case 0:
                    title = textField.text!
                case 1:
                    productWeight = textField.text!
                case 2:
                    productSum = textField.text!
                default:
                    return
                }
            }
        }
        
        if !library.isEmpty && category != nil  {

            guard let title = title, let productWeight = productWeight, let productSum = productSum else {
                let banner = NotificationBanner(title:  "Missing".localized(), subtitle: "BannerMissingField".localized(), leftView: nil, rightView: nil, style: .danger, colors: .none)
                banner.show()
                return
            }
            
            if isError == 0 {
                
                let hud = JGProgressHUD(style: .dark)
                           hud.textLabel.text = "Sending".localized()
                           hud.vibrancyEnabled = true
                           hud.show(in: self.view)
                
                APIManager.sharedInstance.createStoreProduct(data: [
                    "title": title,
                    "weight": "\(productWeight.doubleValue)",
                    "description": "",
                    "price": "\(productSum.doubleValue)",
                    "categoryId": "\(category!["id"].int!)",
                                        
                ], images: library, onSuccess: { (data) in
                    
                    guard let response = data["url"]?.dictionary, let status = response["status"]?.number, status == 1, let responseData = response["data"]?.dictionary, let _ = responseData["id"]?.number  else {
                        UIView.animate(withDuration: 0.1, animations: {
                            hud.textLabel.text = "Error".localized()
                            hud.detailTextLabel.text = "ServerError"
                            hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        })
                        
                        hud.dismiss(afterDelay: 1.0, animated: true)
                        return
                    }
                    
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Success".localized()
                        hud.detailTextLabel.text = nil
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                        self.back(self)
                    }
                }, viewController: self)
            }
        }
        else {

            let banner = NotificationBanner(title:  "Missing".localized(), subtitle: "BannerMissing".localized(), leftView: nil, rightView: nil, style: .danger, colors: .none)
            banner.show()
        }
    }
    
    func onTapCustomAlertButton() {
        isRemoveImage = true
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.config = AlertCustomData(title: "RemoveImage".localized(), firstSegment: "Yes".localized(), secondSegment: "No".localized())
        lightbox.present(customAlert, animated: true, completion: nil)
    }
}

extension OperationWithStoreProduct: CustomAlertViewDelegate {
    
    func okButtonTapped(count: String) {
        
        lightbox.images.remove(at: selectedImageLightBox )
        
        if (lightBoxType) {
            library.remove(at: selectedImageLightBox )
            self.imagesCountLabel.text = "\(self.library.count)+"
            
            if (lightbox.images.count > 0) {
                imgProfile.image = library[library.count - 1]
                
                let index = selectedImageLightBox > 0 ? (selectedImageLightBox ) - 1 : 0
                lightbox.goTo(index, animated: true)
            }
            else {
                imgProfile.image = UIImage(named: "Background")
                self.imagesStack.isHidden = true
                DispatchQueue.main.async {
                    self.lightbox.dismiss(animated: true, completion: nil)
                }
            }
        }
        else {
            var images =  gallery.cart.images
            gallery.cart.remove(images[lightbox.currentPage])
            images.remove(at: lightbox.currentPage)
            gallery.cart.reload(images)
            if (lightbox.images.count == 0) {
                DispatchQueue.main.async {
                    self.lightbox.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func cancelButtonTapped() {}
    
    @objc func calculateCommisionAndGramPrice() {
        var sum:Double?
        var weight:Double?
        
        textFields.forEach { (textfield) in
            if (textfield.tag == 2) {
                sum = textfield.text?.doubleValue
            }
            
            if (textfield.tag == 1) {
                weight = textfield.text?.doubleValue
            }
            
            if (textfield.tag == 3) {
                if (sum != nil && weight != nil) {
                    productPriceForGram = String(format: "%.2f", Double(sum!/weight!))
                    textfield.text = productPriceForGram
                    textfield.layer.borderColor = UIColor.white.cgColor
                }
                else {
                    textfield.text = productPriceForGram
                    textfield.layer.borderColor = UIColor.white.cgColor
                }
            }
        }
    }
}

extension OperationWithStoreProduct:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) {
            textField.layer.borderWidth = 0.5
            textField.layer.cornerRadius = 5
            textField.layer.borderColor = UIColor.white.cgColor
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return !string.isSingleEmoji
    }
}

extension OperationWithStoreProduct: GalleryControllerDelegate {
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        
        DispatchQueue.main.async {
            Image.resolve(images: images, completion: { [weak self] resolvedImages in
                let images = resolvedImages.compactMap({ $0 })
                self!.imgProfile.image = images.last
                images.forEach { (image) in
                    self!.library.append(image)
                }
                self!.imagesCountLabel.text = "\(self!.library.count)+"
                self!.imagesStack.isHidden = false
            })
        }
        
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {}
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            self?.showLightbox(controller, images: resolvedImages.compactMap({ $0 }))
        })
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func showLightbox(_ controller: GalleryController,images: [UIImage]) {
        
        guard images.count > 0 else {
            return
        }
        
        lightBoxType = false
        
        let lightboxImages = images.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.footerView.isHidden = true
        lightbox.modalPresentationStyle = .fullScreen
        lightbox.imageTouchDelegate = self
        gallery.present(lightbox, animated: true)
    }
}

extension OperationWithStoreProduct:LightboxControllerTouchDelegate {
    func lightboxController(_ controller: LightboxController, didTouch image: LightboxImage, at index: Int) {
        
        selectedImageLightBox = index
        self.onTapCustomAlertButton()
    }
}
