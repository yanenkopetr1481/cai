//
//  Store.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 27.03.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import UIKit
import AARefreshControl
import SwiftyJSON
import JGProgressHUD
import RxSwift
import RxCocoa
import RxDataSources
import LGButton

class Store: RootViewController {
    
    @IBOutlet weak var createCategoryBtn:UIButton!
    @IBOutlet weak var categoriesTableView:UITableView!
    @IBOutlet weak var showAllProducts:UIButton!

    var disposedBag = DisposeBag()
    var refreshControl: AARefreshControl!

    var stackViewblocks:[UIStackView]!
    var spaceLeading:CGFloat = 10
    
    var categoriesData = [JSON]()
    let categoriesListRX: BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
    
    var parrentCategory:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewConfiguration()
        hideKeyboardWhenTappedAround()
    }
    
    func setupViewConfiguration(){
        
        categoriesTableView.register(UINib(nibName: "StoreCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "storeCategoryCell")
        
        configureRxTable()
        getMyCategories()

        createCategoryBtn.rx.tap.subscribe(onNext: {
            _ in
            
            self.showCustomAlert(titleName: "category_createCategoryTitle", btnName: "category_createCategoryBtn", style: .addCategory)
            
        }).disposed(by: self.disposedBag)
        
        showAllProducts.rx.tap.subscribe(onNext:{
            _ in
            self.showProducts(category: nil)
        }).disposed(by: self.disposedBag)
        
        showAllProducts.setTitle("store_show_all_products".localized(), for: .normal)
    }
    
    func showCustomAlert(titleName:String, btnName:String, style:CategoryAlertStyle) {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "createCategoryAlert") as! StoreAlert
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.config = CategoryAlertConfig(titleName: titleName, btnName: btnName, style: style)
        self.present(customAlert, animated: true, completion: nil)
    }
    
    @objc func refresh(_ sender: Any?) {
        self.getMyCategories()
    }
    
    func configureRxTable() {
        
        refreshControl = AARefreshControl(scrollView: categoriesTableView)
        refreshControl.tintColor = UIColor.black
        refreshControl.activityIndicatorViewColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        categoriesTableView.addSubview(refreshControl)
                
        categoriesTableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        categoriesListRX.asObservable().bind(to: categoriesTableView.rx.items(cellIdentifier: "storeCategoryCell")) { table, model, cell in
            
            let cell = cell as! StoreCategoryTableViewCell

            cell.categoryName.text = model["title"].string
            cell.tag = model["id"].int ?? 0
            cell.leftAnchorConstratint.constant = CGFloat(model["leadingConstraint"].float ?? 0)
            
            cell.editBtn.rx.tap.subscribe(onNext: {
                _ in
                self.parrentCategory = model["id"].int
                self.showCustomAlert(titleName: "category_editCategoryTitle", btnName: "category_editCategoryBtn", style: .edit)
                
            }).disposed(by: self.disposedBag)
            
            cell.deleteBtn.rx.tap.subscribe(onNext: {
                _ in
                self.parrentCategory = model["id"].int
                self.showCustomAlert(titleName: "category_deleteCategoryTitle", btnName: "category_deleteCategoryBtn", style: .delete)
                
            }).disposed(by: self.disposedBag)
            
            cell.addSubcategoryBtn.rx.tap.subscribe(onNext: {
                _ in
                
                self.parrentCategory = model["id"].int
                
                self.showCustomAlert(titleName: "category_addSubCategoryTitle", btnName: "category_addSubCategoryBtn", style: .addSubCategory)
                
            }).disposed(by: self.disposedBag)

            cell.selectionStyle = .none
            
        }.disposed(by: disposeBag)
        
        categoriesTableView.rx.modelSelected(JSON.self)
            .map{ $0 }
            .subscribe(onNext: { [weak self] data in
                self?.showProducts(category: data)
            }).disposed(by: disposeBag)
        
        categoriesTableView.rx
            .itemSelected
            .subscribe(onNext:  { [weak self] indexPath in
                self?.categoriesTableView.deselectRow(at: indexPath, animated: true)
            })
            .disposed(by: disposeBag)
    }
    
    func showProducts(category:JSON?) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "storeProduts") as! StoreProducts
        
        if category != nil {
            vc.category = category
        }
        
        vc.modalPresentationStyle = .fullScreen
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        
        self.view.window!.layer.add(transition, forKey: kCATransition)
        self.present(vc, animated: false, completion: nil)
    }
    
    func getMyCategories() {
        self.refreshControl.beginRefreshing()

        let api = APIManager.sharedInstance
        
        api.getMyCategories(parentID: nil, all: true, completionHandler: { (data) in
            
            if let categories = data["data"]?.array, !categories.isEmpty {
                
                self.categoriesData.removeAll()
                
                categories.forEach { (category) in
                    self.processCategoryData(category: category, leading: 0)
                }
                
                self.categoriesListRX.accept(self.categoriesData)
            }
            else {
                
//                self.categoriesTableView.isHidden = true
            }
            
            self.refreshControl.endRefreshing()
            
        }, viewController: self)
    }
    
    func processCategoryData(category:JSON, leading:Float) {
        
        var category = category
        category["leadingConstraint"].float = leading
        
        self.categoriesData.append(category)
        
        let childList = category["childList"].array
        if (!(childList?.isEmpty ?? true)) {
            childList?.forEach({ (subCategory) in
                self.processCategoryData(category: subCategory, leading: leading + 10)
            })
        }
    }
    
    func createCategory(parentID:Int?, title:String, completionHandler: @escaping(Bool) -> Void) {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        
        hud.show(in: self.view)
        
        APIManager.sharedInstance.createMyCategory(parentID: parentID, title: title, completionHandler: { (data) in
            guard let status = data["status"]?.number, status == 1, let _ = data["data"]?.dictionary?["id"]?.int else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["data"]?.string
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    
                    hud.dismiss(afterDelay: 1, animated: true)
                    completionHandler(false)
                })
                
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = data["data"]?.string
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            completionHandler(true)
            
            hud.dismiss(afterDelay: 1.0, animated: true)
        }, viewController: self)
    }
    
    func editCategory(id:Int, title:String, completionHandler: @escaping(Bool) -> Void) {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        
        hud.show(in: self.view)
        
        APIManager.sharedInstance.editMyCategory(id: id, title: title, completionHandler: { (data) in
            guard let status = data["status"]?.number, status == 1 else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["data"]?.string
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    
                    hud.dismiss(afterDelay: 1, animated: true)
                    completionHandler(false)
                })
                
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = data["data"]?.string
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            completionHandler(true)
            
            hud.dismiss(afterDelay: 1.0, animated: true)
        }, viewController: self)
    }
    
    func deleteCategory(id:Int, completionHandler: @escaping(Bool) -> Void) {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        
        hud.show(in: self.view)
        
        APIManager.sharedInstance.deleteMyCategory(id: id, completionHandler: { (data) in
            guard let status = data["status"]?.number, status == 1 else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["data"]?.string
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    
                    hud.dismiss(afterDelay: 1, animated: true)
                    completionHandler(false)
                })
                
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = data["data"]?.string
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            completionHandler(true)
            
            hud.dismiss(afterDelay: 1.0, animated: true)
        }, viewController: self)
    }
}

extension Store:CreateCategoryAlertDelegate {
    
    func actionWithCategory(vc:UIViewController, style: CategoryAlertStyle, title: String?) {
        
        switch style {
        case .edit:
            self.editCategory(id: self.parrentCategory!, title: title!) { (result) in
                if result {
                    vc.dismiss(animated: true, completion: nil)
                    self.getMyCategories()
                }
            }
        case .addSubCategory:
            self.createCategory(parentID: self.parrentCategory, title: title!) { (result) in
                if result {
                    vc.dismiss(animated: true, completion: nil)
                    self.getMyCategories()
                }
            }
        case .delete:
            vc.dismiss(animated: true, completion: nil)
            self.deleteCategory(id: self.parrentCategory!) { (result) in
                if result {
                    self.getMyCategories()
                }
            }
        case .addCategory:
            self.createCategory(parentID: nil, title: title!) { (result) in
                if result {
                    vc.dismiss(animated: true, completion: nil)
                    self.getMyCategories()
                }
            }
        case .setProductsAvg:
            break
        case .setProductsPrices:
            break
        }
    }
}

extension Store:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
