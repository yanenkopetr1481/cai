//
//  StoreProducts.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 04.04.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import UIKit
import AARefreshControl
import JGProgressHUD
import RxSwift
import SwiftyJSON
import NotificationBannerSwift
import RxDataSources
import RxCocoa
import RxSwift

protocol StoreProductsProtocol {
    func selectProduct(product:StoreProductData)
}

class StoreProducts: RootViewController {
    
    @IBOutlet weak var productsTable:UITableView!
    @IBOutlet weak var addProductBtn:UIButton!
    @IBOutlet weak var filterBtn:UIButton!
    @IBOutlet weak var productTitleLabel:UILabel!
    @IBOutlet weak var editBtn:UIButton!
    
    @IBOutlet weak var setSumBtn:UIButton!
    @IBOutlet weak var setAvgBtn:UIButton!
    
    @IBOutlet weak var configMultipleProductsView:UIView!

    var refreshControl: AARefreshControl!
    var category:JSON?
    var disposedBag = DisposeBag()
    var spaceLeading:CGFloat = 10
    var productsData = [StoreProductData]()
    var productPage:Int = 0
    var productID:[NSNumber] = [NSNumber]()
    var isMeResponseTry:Bool = false
    var fetchingMore:Bool = false
    var selectedFilters:Filters?

    let productsListRX: BehaviorRelay<[StoreProductData]> = BehaviorRelay(value: [])
    var isTableEditing:Bool = false
    
    var delegate:StoreProductsProtocol?
    var isNewDealPageDelegate:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupViewConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.productsData.removeAll()
        self.productID.removeAll()
        self.productsListRX.accept([])
        self.productPage = 0
        getMyProducts()
    }
    
    func showCustomAlert(titleName:String, btnName:String, style:CategoryAlertStyle) {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "createCategoryAlert") as! StoreAlert
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.config = CategoryAlertConfig(titleName: titleName, btnName: btnName, style: style)
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func setupViewConfiguration() {
        
        setSumBtn.setTitle("store_product_setSumBtn".localized(), for: .normal)
        setAvgBtn.setTitle("store_product_setAvgBtn".localized(), for: .normal)
        
        setSumBtn.rx.tap.subscribe(onNext: { _ in
            if let indexPaths = self.productsTable.indexPathsForSelectedRows, !indexPaths.isEmpty {
                self.showCustomAlert(titleName: "store_product_setPrice", btnName: "Set", style: .setProductsPrices)
            }
            else {
                let banner = NotificationBanner(title:  "Missing".localized(), subtitle: "MissingSelectProduct".localized(), leftView: nil, rightView: nil, style: .danger, colors: .none)
                banner.show()
            }
            
        }).disposed(by: self.disposedBag)
        
        
        setAvgBtn.rx.tap.subscribe(onNext: { _ in
            if let indexPaths = self.productsTable.indexPathsForSelectedRows, !indexPaths.isEmpty {
                self.showCustomAlert(titleName: "store_product_setAvg", btnName: "Set", style: .setProductsAvg)
            }
            else {
                let banner = NotificationBanner(title:"Missing".localized(), subtitle: "MissingSelectProduct".localized(), leftView: nil, rightView: nil, style: .danger, colors: .none)
                banner.show()
            }
            
        }).disposed(by: self.disposedBag)

        configMultipleProductsView.alpha = 0
        
        filterBtn.rx.tap.subscribe(onNext: { _ in
            self.showFilter()
        }).disposed(by: self.disposedBag)
        
        productsTable.register(UINib(nibName: "StoreProductCell", bundle: nil), forCellReuseIdentifier: "storeProductCell")
        productsTable.rowHeight = UITableView.automaticDimension
        productsTable.estimatedRowHeight = 120
        productsTable.allowsMultipleSelectionDuringEditing = true
        
        configureRxTable()
        productTitleLabel.text = "store_product_title".localized()
        
        editBtn.rx.tap.subscribe(onNext: { _ in
            self.isTableEditing = !self.isTableEditing
            self.productsTable.isEditing = self.isTableEditing
            
            UIView.animate(withDuration: 0.5) {
                self.configMultipleProductsView.alpha = self.productsTable.isEditing ? 1 : 0
            }
            
        }).disposed(by: self.disposedBag)
        
        if self.isNewDealPageDelegate {
            self.editBtn.isHidden = true
            self.filterBtn.isHidden = true
            self.addProductBtn.isHidden = true
        }
    }
    
    @objc func refresh(_ sender: Any?) {
        self.getMyProducts()
    }
    
    func configureRxTable() {
        
        refreshControl = AARefreshControl(scrollView: productsTable)
        refreshControl.tintColor = UIColor.black
        refreshControl.activityIndicatorViewColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        productsTable.addSubview(refreshControl)
        
        productsTable.rx.setDelegate(self).disposed(by: disposeBag)
        
        productsListRX.asObservable().bind(to: productsTable.rx.items(cellIdentifier: "storeProductCell")) { table, model, cell in
            
            let cell = cell as! StoreProductCell
            cell.awakeFromNib()
            
            cell.setupViewConfiguration(data: model)

            let imageUrl = model.productImagesURLs
            if let urlStr = imageUrl.last?.string, let url = URL(string: urlStr) {
                
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    cell.productImage.image = image
                }
                else {
                    cell.productImage.downloadImageFrom(url: url, contentMode: .scaleAspectFill)
                }
            }
            else {
                cell.productImage.image = UIImage(named: "empty_product")
            }
            
            cell.textLabel?.numberOfLines = 0
            
        }.disposed(by: disposeBag)
        
        productsTable.rx.modelSelected(StoreProductData.self)
            .map{ $0 }
            .subscribe(onNext: { [weak self] data in
                
                if let newDeal = self?.isNewDealPageDelegate, newDeal {
                    self?.delegate?.selectProduct(product: data)
                    self?.back()
                }
                else {
                    if self?.isTableEditing == false {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "createStoreProduct") as! OperationWithStoreProduct
                        vc.modalPresentationStyle = .fullScreen
                        vc.category = data.category
                        vc.previewStyle = .preview
                        vc.productData = data
                        let transition = CATransition()
                        transition.duration = 0.5
                        transition.type = CATransitionType.push
                        transition.subtype = CATransitionSubtype.fromRight
                        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                        
                        self?.view.window!.layer.add(transition, forKey: kCATransition)
                        self?.present(vc, animated: false, completion: nil)
                    }
                }
            }).disposed(by: disposeBag)
        
        productsTable.rx
            .itemSelected
            .subscribe(onNext:  { [weak self] indexPath in
                if self?.isTableEditing == false {
                    self?.productsTable.deselectRow(at: indexPath, animated: true)
                }
            })
            .disposed(by: disposeBag)
    }
    
    @IBAction func back() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func showCreateStoreproductVC(_ sender:UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "createStoreProduct") as! OperationWithStoreProduct
        vc.modalPresentationStyle = .fullScreen
        vc.category = self.category
        vc.previewStyle = .create
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        
        self.view.window!.layer.add(transition, forKey: kCATransition)
        self.present(vc, animated: false, completion: nil)
    }
    
    func getMyProducts(updateTable:Bool = false, beginRefresh: Bool = true) {
        
        let api = APIManager.sharedInstance
        
        if (self.category != nil) {
            api.getFilterStoreProducts(page: self.productPage, data: [
                "categoryIds" : [self.category!["id"].int!],
                "title": "",
                "weightLowLimit": 0.0,
                "weightHighLimit": "10000000.0",
                "priceLowLimit": 0.00,
                "priceUpLimit": 10000000,
                "createdFrom":0,
                "createdTo":1915840672,
                "active":true
            ], onSuccess: { (data) in
                self.processingResponceData(data: data, updateTable: updateTable, onRetry: {
                    self.getMyProducts(updateTable: updateTable, beginRefresh: beginRefresh)
                })
            }, viewController: self)
        }
        else {
            api.getStoreUserProducts(page: self.productPage, onSuccess: { (data) in
                self.processingResponceData(data: data, updateTable: updateTable, onRetry: {
                    self.getMyProducts(updateTable: updateTable, beginRefresh: beginRefresh)
                })
            }, viewController: self)
        }
    }
    
    func concat (lhs: Double, rhs: Double) -> String {
        let value = Double(exactly: rhs / lhs)
        return String(format: "%.2f", value!)
    }
    
    func processingResponceData(data:[String:JSON], updateTable:Bool = false, onRetry: @escaping() -> Void) {
        
        var isChanged = false
        var count = 0
        
        self.productsTable.backgroundView = UIView()
        self.refreshControl.beginRefreshing()

        if let response = data["data"]?.array, !response.isEmpty  {
            count = response.count
            
            response.forEach { value in
                guard let title = value["productTitle"].string,
                      let description = value["description"].string,
                      let weight = value["weight"].number,
                      let price = value["price"].number,
                      let photoUrl = value["photoUrl"].array,
                      let id = value.dictionary?["id"]?.number
                else { return }
                
                let category = value.dictionary?["productCategory"]
                let priceForGramm = self.concat(lhs: weight.doubleValue, rhs: price.doubleValue).doubleValue
                
                if !self.productID.contains(id) {
                    isChanged = true
                    
                    let newData = StoreProductData(id: id, productTitle: title, productWeight: weight.doubleValue, productGramSum: priceForGramm, productSum: price.doubleValue, productImagesURLs: photoUrl, category: category, description: description)
                    
                    if Int(truncating: newData.id) > Int(truncating: self.productsData.first?.id ?? 0) {
                        self.productsData.insert(newData, at: 0)
                    }
                    else {
                        self.productsData.append(newData)
                    }
                    
                    self.productID.append(id)
                }
            }
            
            if isChanged {
                self.productPage += 1
            }
        }
        
        if self.productsData.isEmpty {
            
            if (!self.isMeResponseTry) {
                self.isMeResponseTry = true
                onRetry()
            }
            else {
                self.isMeResponseTry = false
                self.setTableViewEmptyBackground()
                self.productsData.removeAll()
                self.productID.removeAll()
                self.productsListRX.accept([])
            }
        }
        else {
            if isChanged || updateTable {
                if count < 7 && self.productPage > 0 {
                    self.productPage -= 1
                }
                
                self.productsListRX.accept(self.productsData)
            }
            
            self.isMeResponseTry = false
        }
        
        self.productsTable.tableFooterView?.isHidden = true
        self.fetchingMore = false
        self.productsTable.isUserInteractionEnabled = true
        self.refreshControl.endRefreshing()
    }
    
    private func setTableViewEmptyBackground() {
        UIView.animate(withDuration: 0.5) {
            let view = Bundle.main.loadNibNamed("EmptyTableView", owner: self, options: nil)?.first as! EmptyTableView
            view.titleLabel.text = "EmptyRessult".localized()
            view.refreshBtn.setTitle("Refresh".localized(), for: .normal)
            view.delegate = self
            view.imageView.image = UIImage(named: "empty")
            self.productsTable.backgroundView = view
        }
    }
}

extension StoreProducts:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
                
        if (indexPath.row == self.productsData.count - 1) {
            if !fetchingMore {
                if (indexPath.row > 4) {
                    self.productsTable.tableFooterView?.isHidden = false
                    if selectedFilters != nil {
                        self.getFilterPageData()
                    }
                    else {
                        self.getMyProducts()
                    }
                }
            }
        }
    }
}

extension StoreProducts:EmptyTableViewDelegate {
    func refreshData() {
        if (!fetchingMore) {
            self.getMyProducts(updateTable: true, beginRefresh: true)
        }
    }
}

extension StoreProducts {
    func showFilter() {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "FIlterAlertView") as! FIlterAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.style = .product
        customAlert.config = self.selectedFilters
        self.present(customAlert, animated: true, completion: nil)
    }
}

extension StoreProducts:FilterAlertDelegate {
    
    func getFilterPageData() {
        
        guard let filters = self.selectedFilters else {
            return
        }
        
        let data = [
            "title":filters.productName,
            "weightLowLimit":filters.weightFrom,
            "weightHighLimit":filters.weightTo,
            "priceLowLimit":filters.priceFrom,
            "priceUpLimit":filters.priceTo,
            "categoryIds":filters.categories,
            "createdFrom": filters.dateFrom!,
            "createdTo": filters.dateTo!,
            "active":true
        ] as [String : Any]
        
        APIManager.sharedInstance.getFilterStoreProducts(page: self.productPage, data: data, onSuccess: { (data) in
            self.processingResponceData(data: data, updateTable: true, onRetry: {
                self.getFilterPageData()
            })
        }, viewController: self)
    }
    
    func setFilter(filters: Filters) {
        self.isMeResponseTry = false
        self.selectedFilters = filters
        self.productPage = 0
        self.productsData.removeAll()
        self.productID.removeAll()
        self.productsListRX.accept([])
        
        self.getFilterPageData()
    }
    
    func annulateFilters() {
        self.isMeResponseTry = false
        self.selectedFilters = nil
        self.productPage = 0
        self.productsData.removeAll()
        self.productID.removeAll()
        self.productsListRX.accept([])
        self.getMyProducts()
    }
}

extension StoreProducts:CreateCategoryAlertDelegate {
    
    func actionWithCategory(vc:UIViewController, style: CategoryAlertStyle, title: String?) {
        
        if style == .setProductsPrices {
            if let indexPaths = self.productsTable.indexPathsForSelectedRows, !indexPaths.isEmpty {
                                
                var ids = [Int]()
                
                indexPaths.forEach { (indexPath) in
                    ids.append(Int(truncating: self.productsData[indexPath.row].id))
                }
                
                if !ids.isEmpty {
                    
                    let hud = JGProgressHUD(style: .dark)
                    hud.textLabel.text = "Sending".localized()
                    hud.vibrancyEnabled = true
                    hud.show(in: self.view)
                    
                    APIManager.sharedInstance.setProductPrices(ids: ids, price: title!.doubleValue, onSuccess: { (data) in
                        guard let status = data["status"]?.number, status == 1  else {
                            UIView.animate(withDuration: 0.1, animations: {
                                hud.textLabel.text = "Error".localized()
                                hud.detailTextLabel.text = "ServerError"
                                hud.indicatorView = JGProgressHUDErrorIndicatorView()
                            })
                            
                            hud.dismiss(afterDelay: 1.0, animated: true)
                            return
                        }
                        
                        UIView.animate(withDuration: 0.1, animations: {
                            hud.textLabel.text = "Success".localized()
                            hud.detailTextLabel.text = nil
                            hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        })
                        
                        hud.dismiss(afterDelay: 1.0, animated: true)
                        
                        self.isTableEditing = !self.isTableEditing
                        self.productsTable.isEditing = self.isTableEditing
                        
                        UIView.animate(withDuration: 0.5) {
                            self.configMultipleProductsView.alpha = self.productsTable.isEditing ? 1 : 0
                        }
                        
                        vc.dismiss(animated: true, completion: nil)
                        self.annulateFilters()
                    }, viewController: self)
                }
            }
        }
        else if style == .setProductsAvg {
            if let indexPaths = self.productsTable.indexPathsForSelectedRows, !indexPaths.isEmpty {
                                
                var ids = [Int]()
                
                indexPaths.forEach { (indexPath) in
                    ids.append(Int(truncating: self.productsData[indexPath.row].id))
                }
                
                if !ids.isEmpty {
                    
                    let hud = JGProgressHUD(style: .dark)
                    hud.textLabel.text = "Sending".localized()
                    hud.vibrancyEnabled = true
                    hud.show(in: self.view)
                    
                    APIManager.sharedInstance.setProductAvgPerGramm(ids: ids, avgPerGramm: title!.doubleValue, onSuccess: { (data) in
                        guard let status = data["status"]?.number, status == 1  else {
                            UIView.animate(withDuration: 0.1, animations: {
                                hud.textLabel.text = "Error".localized()
                                hud.detailTextLabel.text = "ServerError"
                                hud.indicatorView = JGProgressHUDErrorIndicatorView()
                            })
                            
                            hud.dismiss(afterDelay: 1.0, animated: true)
                            return
                        }
                        
                        UIView.animate(withDuration: 0.1, animations: {
                            hud.textLabel.text = "Success".localized()
                            hud.detailTextLabel.text = nil
                            hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        })
                        
                        hud.dismiss(afterDelay: 1.0, animated: true)
                        
                        self.isTableEditing = !self.isTableEditing
                        self.productsTable.isEditing = self.isTableEditing
                        
                        UIView.animate(withDuration: 0.5) {
                            self.configMultipleProductsView.alpha = self.productsTable.isEditing ? 1 : 0
                        }
                        
                        vc.dismiss(animated: true, completion: nil)
                        self.annulateFilters()
                    }, viewController: self)
                }
            }
        }
    }
}
