//
//  RootViewController.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 26.01.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit
import Reachability
import RxReachability
import RxSwift

struct NetworkStatus {
    static var isReachable:Bool = false
    static var isLaunchUsed:Bool = false
}

class RootViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    var del:InternetLostDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Reachability.rx.isConnected
            .subscribe(onNext: {
                if (NetworkStatus.isReachable) {
                    NetworkStatus.isReachable = false
                    
                    if NetworkStatus.isLaunchUsed {
                        self.del?.hideView()
                    }
                    else {
                        if var topController = UIApplication.shared.keyWindow?.rootViewController {
                            while let presentedViewController = topController.presentedViewController {
                                topController = presentedViewController
                            }
                            
                            let vc = topController.storyboard!.instantiateViewController(withIdentifier: "launchPage") as! LaunchViewController
                            vc.providesPresentationContextTransitionStyle = true
                            vc.definesPresentationContext = true
                            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            
                            topController.present(vc, animated: true, completion: nil)
                        }
                    }
                }
            })
            .disposed(by: disposeBag)
        
        Reachability.rx.isDisconnected
            .subscribe(onNext: {
                if (!NetworkStatus.isReachable) {
                    NetworkStatus.isReachable = true
                    
                    if var topController = UIApplication.shared.keyWindow?.rootViewController {
                        while let presentedViewController = topController.presentedViewController {
                            topController = presentedViewController
                        }
                        
                        let vc = topController.storyboard!.instantiateViewController(withIdentifier: "internetLost") as! InternetLost
                        vc.providesPresentationContextTransitionStyle = true
                        vc.definesPresentationContext = true
                        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        self.del = vc
                        
                        topController.present(vc, animated: true, completion: nil)
                    }
                }
                
            })
            .disposed(by: disposeBag)
    }
}
