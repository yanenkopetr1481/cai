//
//  ROuterNavigation.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/3/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit

class RouterNavigation: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setTransparentNavigationBar()
    }
    
    func setTransparentNavigationBar() {
        let image = UIImage()
        navigationBar.setBackgroundImage(image, for: .default)
        navigationBar.shadowImage = image
        navigationBar.isTranslucent = true
    }
}
