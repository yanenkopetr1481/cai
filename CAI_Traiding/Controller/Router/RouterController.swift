//
//  RouterController.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/29/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import BadgeControl

protocol UserSettingsDelegate {
    func editUserData(button:UIBarButtonItem)
    func candelEditUserData(button:UIBarButtonItem)
}

protocol PurchaseDelegate {
    func editPurchaseData(button:UIBarButtonItem)
    func candelEditPurchaseData(button:UIBarButtonItem)
}

protocol SellsDelegate {
    func editSellsData(button:UIBarButtonItem)
    func cancelEditSellsData(button:UIBarButtonItem)
}

protocol NewOffersDelegate {
    func showFilter(button:UIBarButtonItem)
}

struct PageData {
    static var page:MenuOption = .Home
}

class RouterController: RootViewController {
    
    @IBOutlet weak var changedContent:UIView!
    @IBOutlet weak var menuView:UIView!
    
    var menuController: Menu!
    var isExpanded = true
    var previusVC:Int = 0
    var viewControllers:[UIViewController]?
    var purchaseDelegate:PurchaseDelegate?
    var userDelegate:UserSettingsDelegate?
    var sellsDelegate:SellsDelegate?
    var newOffersDelegate:NewOffersDelegate?

    let defaults = UserDefaults.standard
    var isFirstResponse:Bool = true
    
    private var notificationBadge: BadgeController!

    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewControllers = [firstViewController,secondViewController,thirdViewController,foursViewController,fivesViewController,eightViewController,sixsViewController,seventhViewController, nineViewController]
        configureMenuController()
        menuController.reloadBadgeCell()
        self.didSelectMenuOption(menuOption: PageData.page)
        self.handleMenuToggle(forMenuOption: PageData.page)

        self.menuController.tableView.selectRow(at: IndexPath(row: previusVC + 1, section: 0), animated: true, scrollPosition: .none)
        setTransparentNavigationBar()
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleMenu))
        swipeLeft.direction = .right
        self.changedContent.addGestureRecognizer(swipeLeft)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appBackFromBackground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReceiveData"), object: nil)
    }
    
    @objc func appBackFromBackground() {
        onReceiveData()
    }
    
    @objc func appMovedToBackground() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.animatePanel(shouldExpand: false, menuOption: nil)
        }
    }
    
    func setTransparentNavigationBar() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
    }
    
    @objc func handleMenu() {
        self.handleMenuToggle(forMenuOption: nil)
    }
    
    func configureNavigationBar() {
//        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenu))
        
        let filterBtn = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 30))
        filterBtn.setImage( #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysOriginal), for: .normal)
        filterBtn.addTarget(self, action: #selector(handleMenu), for: .touchUpInside)

        notificationBadge = BadgeController(for: filterBtn)
        notificationBadge.animation = BadgeAnimations.rightLeft
        notificationBadge.badgeHeight = 20
        notificationBadge.badgeTextFont = UIFont(name: "Helvetica Neue", size: 12)!
        notificationBadge.badgeBackgroundColor = .red
        notificationBadge.badgeTextColor = .white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: filterBtn)
        NotificationCenter.default.addObserver(self, selector: #selector(onReceiveData), name: NSNotification.Name(rawValue: "ReceiveData"), object: nil)
        onReceiveData()
    }

    //_ notification:Notification)
    @objc func onReceiveData() {
        if notificationBadge != nil {
            let count = AppLocalData.BadgesCount[.deal]! + AppLocalData.BadgesCount[.payment]! + AppLocalData.BadgesCount[.purchase]! + AppLocalData.BadgesCount[.credit]!
            
            
            if (count > 0) {
                notificationBadge.addOrReplaceCurrent(with: "\(count)", animated: true)
                if menuController != nil {
                    menuController.reloadBadgeCell()
                }
            }
            else {
                notificationBadge.remove(animated: true)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self.view)
        
        if isExpanded && location.x >= self.view.frame.width - 80{
            self.handleMenuToggle(forMenuOption: nil)
        }
    }
    
    func configureMenuController() {
        if menuController == nil {
            menuController = Menu()
            menuController.delegate = self
            
            addChild(menuController)
            
            // Add Child View as Subview
            self.menuView.addSubview(menuController.view)
            
            // Configure Child View
            menuController.view.frame = self.menuView.bounds
            menuController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            // Notify Child View Controller
            menuController.didMove(toParent: self)
        }
    }
    
    func animatePanel(shouldExpand: Bool, menuOption: MenuOption?) {
        
        if shouldExpand {
            isFirstResponse = false
            menuController.reloadBadgeCell()

            if UIView.userInterfaceLayoutDirection(
                for: self.view.semanticContentAttribute) == .rightToLeft {
                UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                    self.navigationController?.navigationBar.frame.origin.x = -self.view.frame.width
                    self.changedContent.frame.origin.x = -self.view.frame.width
                    
                    UIView.animate(withDuration: 0.1, delay: 0.4,usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                        self.changedContent.alpha = 0
                    })
                })
            }
            else {
                UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                    self.navigationController?.navigationBar.frame.origin.x = self.view.frame.width - 80
                    self.changedContent.frame.origin.x = self.view.frame.width - 80
                    
                    UIView.animate(withDuration: 0.1, delay: 0.4,usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                        self.changedContent.alpha = 0
                    })
                })
            }
            
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.changedContent.alpha = 1
                self.navigationController?.navigationBar.frame.origin.x = 0
                self.changedContent.frame.origin.x = 0
            }) { (_) in
                guard let menuOption = menuOption else { return }
                self.didSelectMenuOption(menuOption: menuOption)
            }
        }
    }
    
    private lazy var firstViewController: Home = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "home_vc") as! Home
        return viewController
    }()
    
    private lazy var secondViewController: PaymentList = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "payment_vc") as! PaymentList
        return viewController
    }()
    
    private lazy var thirdViewController: PurchaseList = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "purchases_vc") as! PurchaseList
        purchaseDelegate = viewController
        return viewController
    }()
    
    private lazy var foursViewController: Seller = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "sellers_vc") as! Seller
        return viewController
    }()
    
    private lazy var fivesViewController: DealsList = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "deals_vc") as! DealsList
        sellsDelegate = viewController
        return viewController
    }()
    
    private lazy var sixsViewController: Support = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "support_vc") as! Support
        return viewController
    }()
    
    private lazy var seventhViewController: Settings = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "settings_vc") as! Settings
        userDelegate = viewController
        viewController.delegate = self
        return viewController
    }()
    
    private lazy var eightViewController: NewOffers = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "newOffers_vc") as! NewOffers
        newOffersDelegate = viewController
        return viewController
    }()
    
    private lazy var nineViewController: Store = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "store_vc") as! Store
        return viewController
    }()
    
    func didSelectMenuOption(menuOption: MenuOption) {
        remove(asChildViewController: self.viewControllers![previusVC])
        
        if (previusVC == 2 && !isFirstResponse) {
            AppLocalData.BadgesCount[.payment] = 0
        }
        else if (previusVC == 1 && !isFirstResponse) {
            AppLocalData.BadgesCount[.purchase] = 0
        }
        else if (previusVC == 4 && !isFirstResponse) {
            AppLocalData.BadgesCount[.deal] = 0
        }
        else if (previusVC == 0 && !isFirstResponse) {
            AppLocalData.BadgesCount[.credit] = 0
        }
                
        let data = [AppLocalData.BadgesCount[.deal],AppLocalData.BadgesCount[.payment],AppLocalData.BadgesCount[.purchase],AppLocalData.BadgesCount[.credit]]
        defaults.setValue(data, forKey: "AppLocalBadges")
        let count = AppLocalData.BadgesCount[.deal]! + AppLocalData.BadgesCount[.payment]! + AppLocalData.BadgesCount[.purchase]! + AppLocalData.BadgesCount[.credit]!
        
        UIApplication.shared.applicationIconBadgeNumber = count
        onReceiveData()
        
        self.title = menuOption.description

        switch menuOption {
        case .Home:
            add(asChildViewController: self.viewControllers![0])
            previusVC = 0
        case .Payments:
            add(asChildViewController: self.viewControllers![1])
            previusVC = 2
        case .Purchases:
            add(asChildViewController: self.viewControllers![2])
            previusVC = 1
        case .Sellers:
            add(asChildViewController: self.viewControllers![3])
            previusVC = 3
        case .Deals:
            add(asChildViewController: self.viewControllers![4])
            previusVC = 4
        case .NewOffers:
            add(asChildViewController: self.viewControllers![5])
            previusVC = 5
        case .Support:
            add(asChildViewController: self.viewControllers![6])
            previusVC = 6
        case .Settings:
            add(asChildViewController: self.viewControllers![7])
            previusVC = 7
        case .Store:
            add(asChildViewController: self.viewControllers![8])
            previusVC = 8
        case .None:
            print("Show None")
        }
    }
    
    @objc private func candelEditUserData(sender: UIBarButtonItem) {
        userDelegate?.candelEditUserData(button: navigationItem.rightBarButtonItem!)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenu))
        navigationItem.rightBarButtonItem?.tag = 0
    }
    
    @objc private func candelEditPurchaseData(sender: UIBarButtonItem) {
        purchaseDelegate?.candelEditPurchaseData(button: navigationItem.leftBarButtonItem!)
    }
    
    @objc private func candelEditDealData(sender: UIBarButtonItem) {
        sellsDelegate?.cancelEditSellsData(button: navigationItem.leftBarButtonItem!)
    }
    
    @objc private func editUserData(sender: UIBarButtonItem) {
        userDelegate?.editUserData(button: sender)
        if sender.tag == 0 {
            sender.tag = 1
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized(), style: .done, target: self, action: #selector(candelEditUserData(sender:)))
        }
        else {
            sender.tag = 0
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenu))
        }
    }
    
    @objc private func editPurchaseData(sender: UIBarButtonItem) {
        purchaseDelegate?.editPurchaseData(button: sender)
        if sender.tag == 0 {
            sender.tag = 1
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Select_all".localized(), style: .done, target: self, action: #selector(candelEditPurchaseData(sender:)))
        }
        else {
            sender.tag = 0
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenu))
        }
    }
    
    @objc private func editDealData(sender: UIBarButtonItem) {
        sellsDelegate?.editSellsData(button: sender)
        if sender.tag == 0 {
            sender.tag = 1
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Select_all".localized(), style: .done, target: self, action: #selector(candelEditDealData(sender:)))
        }
        else {
            sender.tag = 0
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenu))
        }
    }
    
    @objc private func showFilter(sender:UIBarButtonItem) {
        newOffersDelegate?.showFilter(button: sender)
    }
        
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        if viewController is Settings {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "settings_edit".localized(), style: .done, target: self, action: #selector(editUserData(sender:)))
            navigationItem.leftBarButtonItem?.tintColor = .white
        }
        else if viewController is PurchaseList {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "settings_edit".localized(), style: .done, target: self, action: #selector(editPurchaseData(sender:)))
            navigationItem.leftBarButtonItem?.tintColor = .white
        }
        else if viewController is DealsList {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "settings_edit".localized(), style: .done, target: self, action: #selector(editDealData(sender:)))
            navigationItem.leftBarButtonItem?.tintColor = .white
        }
        else if viewController is NewOffers {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "filter-white"), style: .plain, target: self, action: #selector(showFilter(sender:)))
            
            navigationItem.leftBarButtonItem?.tintColor = .white
        }
        else {
            navigationItem.rightBarButtonItem = UIBarButtonItem()
        }
        
        // Add Child View as Subview
        self.changedContent.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.changedContent.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
}

extension RouterController: HomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: MenuOption?) {
        
         if !isExpanded {
            configureMenuController()
        }
        
        isExpanded = !isExpanded
        animatePanel(shouldExpand: isExpanded, menuOption: menuOption)
    }
}


extension RouterController:SettingDelegate {
    func changeLanguage() {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "mainRouter") as? RouterNavigation
        AppLocalData.Countries = nil
        vc!.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
    }
}
