//
//  Login.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/13/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import FlagPhoneNumber
import JGProgressHUD
import Alamofire
import KeychainSwift
import FirebaseAuth
import CountryPickerView
import Localize_Swift
import CoreLocation

class Login: ViewController  {
    
    @IBOutlet weak var phoneNumberTextField:UITextField!
    @IBOutlet weak var nextButton:UIButton!
    @IBOutlet weak var errorMessage:UIStackView!
    @IBOutlet weak var bottomTetView: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet var codeTextFields: [UITextField]!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var clearBtn: UIButton!
    private lazy var phoneUtil: NBPhoneNumberUtil = NBPhoneNumberUtil()
    
    var cpvInternal = CountryPickerView()
    
    fileprivate var alertStyle: UIAlertController.Style = .actionSheet
    fileprivate var dialCode:String = ""
    fileprivate var phoneNumber:String = ""
    fileprivate let term = "login_page_1_bottom_text_terms".localized()
    fileprivate let policy = "login_page_1_bottom_text_policy".localized()
    fileprivate let sms = "login_page_3_bottom_text_sms".localized()
    
    var countCode = 0
    let keychain = KeychainSwift()
    var isSMSTimerOn = false
    var code = ""
    var timer:Timer?
    var timeLeft = 120
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewconfiguration()
        configureTextView()
        configuteCodeTextFields()
        setupLocalization()

        if self.phoneNumberTextField != nil && self.code.isEmpty{
            self.code = UserProfileData.countryCode  ?? "RU"
        }
        
        if phoneNumberTextField != nil {
            setupPhonePickerView()
        }
                
        UserDefaults.standard.removeObject(forKey: "AppLocalBadges")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    func setupPhonePickerView(code:String = "") {
        cpvInternal = CountryPickerView()
        cpvInternal.flagSpacingInView = 2
        cpvInternal.showCountryCodeInView = false
        cpvInternal.countryDetailsLabel.frame = CGRect(x: 10, y: 10, width: 10, height: 10)
        cpvInternal.addConstraint(NSLayoutConstraint(item: cpvInternal, attribute: .leading, relatedBy: .equal, toItem: cpvInternal.flagImageView, attribute: .leading, multiplier: 1, constant: -10))
        cpvInternal.showCountryNameInView = false
        
        if let code = UserProfileData.countryCode {
            cpvInternal.setCountryByCode(code)
        }

        phoneNumberTextField.leftView = cpvInternal
        phoneNumberTextField.leftViewMode = .always
        phoneNumberTextField.delegate = self
        phoneNumberTextField.textColor = .black
        phoneNumberTextField.keyboardType = .numberPad
        phoneNumberTextField.addTarget(self, action: #selector(textChanged), for: .editingChanged)

        [  cpvInternal].forEach {
            $0?.dataSource = self
        }
        
        cpvInternal.delegate = self
        phoneNumberTextField.showDoneButtonOnKeyboard()
    }
    
    private func getValidNumber(phoneNumber: String) -> NBPhoneNumber? {
        let countryCode = cpvInternal.selectedCountry.code
        
        do {
            let parsedPhoneNumber: NBPhoneNumber = try phoneUtil.parse(phoneNumber, defaultRegion: countryCode)
            let isValid = phoneUtil.isValidNumber(parsedPhoneNumber)
            
            return isValid ? parsedPhoneNumber : nil
        } catch _ {
            return nil
        }
    }

    
    func showTextView () {
        DispatchQueue.main.async {
            let alert = UIAlertController(style: self.alertStyle)
            let text: [AttributedTextBlock] = [
                .header1("U.S. Returns & Refunds Policy."),
                .header2("Standard Return Policy."),
                .normal("There are a few important things to keep in mind when returning a product you purchased online from Apple:"),
                .list("You have 14 calendar days to return an item from the date you received it."),
                .list("Only items that have been purchased directly from Apple, either online or at an Apple Retail Store, can be returned to Apple. Apple products purchased through other retailers must be returned in accordance with their respective returns and refunds policy."),
                .list("Please ensure that the item you're returning is repackaged with all the cords, adapters and documentation that were included when you received it."),
                .normal("There are some items, however, that are ineligible for return, including:"),
                .list("Opened software"),
                .list("Electronic Software Downloads"),
                .list("Software Up-to-Date Program Products (software upgrades)"),
                .list("Apple Store Gift Cards"),
                .list("Apple Developer products (membership, technical support incidents, WWDC tickets)"),
                .list("Apple Print Products"),
                .normal("*You can return software, provided that it has not been installed on any computer. Software that contains a printed software license may not be returned if the seal or sticker on the software media packaging is broken.")]
            
            alert.addTextViewer(text: .attributedText(text))
            alert.addAction(title: "OK", style: .cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setupLocalization() {
        
        switch nextButton.tag {
        case 0:
            descriptionTextView.adjustsFontForContentSizeCategory = true
            let quote = "login_page_1_description_text".localized()
            let font = UIFont.systemFont(ofSize: 18)
            let style = NSMutableParagraphStyle()
            style.alignment = .center
            let attributes = [NSAttributedString.Key.font: font,NSAttributedString.Key.foregroundColor : UIColor.white,NSAttributedString.Key.paragraphStyle:style]
            let attributedQuote = NSAttributedString(string: quote, attributes: attributes)
            
            descriptionTextView.textAlignment = .center
            descriptionTextView.attributedText = attributedQuote
            descriptionTextView.isSelectable = false
            
            nextButton.setTitle("login_page_1_next_tile".localized(), for: .normal)
        case 1:
            nextButton.setTitle("Confirm".localized(), for: .normal)
            break
        case 2:
            nextButton.setTitle("Confirm".localized(), for: .normal)
            break
        default:
            break
        }
    }
    
    func setupViewconfiguration() {
        self.hideKeyboardWhenTappedAround()
        
        if timeLabel != nil {
            let smsTimerTo = UserDefaults.standard.integer(forKey: "smsTimerTo")

            let diff = smsTimerTo - Int(NSDate().timeIntervalSince1970)
            
            if (diff > 0) {
                self.timeLeft = diff
                self.startSMSTimer()
                self.isSMSTimerOn = true
            }
            else {
                timeLabel.isHidden = true
            }
        }

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func configuteCodeTextFields() {
        if (codeTextFields != nil) {
            codeTextFields.forEach { (textField) in
                textField.addTarget(self, action: #selector(textChanged), for: .editingChanged)
                if #available(iOS 12.0, *) {
                    textField.textContentType = .oneTimeCode
                }
            }
        }
    }
    
    @IBAction func clearInutCode(_ sender:UIButton) {
        codeTextFields.forEach { (textField) in
            textField.text = ""
        }
        
        UIView.animate(withDuration: 1) {
            sender.alpha = 0
        }
    }
    
    func checkRange(_ range: NSRange, contain index: Int) -> Bool {
        return index > range.location && index < range.location + range.length
    }
    
    @objc func handleTermTapped(gesture: UITapGestureRecognizer) {
        let termString = bottomTetView.text! as NSString
        let termRange = termString.range(of: term)
        let policyRange = termString.range(of: policy)
        let smsRange = termString.range(of: sms)
        
        let tapLocation = gesture.location(in: bottomTetView)
        let index = bottomTetView.indexOfAttributedTextCharacterAtPoint(point: tapLocation)
        
        if checkRange(termRange, contain: index) == true {
            showTextView()
            return
        }
        
        if checkRange(policyRange, contain: index) {
            showTextView()
            return
        }
        
        if checkRange(smsRange, contain: index) {
            sendSMS(isRetry: true)
            return
        }
    }
    
    func configureTextView() {
        if (bottomTetView != nil) {
            
            var attributedString:NSMutableAttributedString?
            
            if (bottomTetView.tag == 0) {
                attributedString = String.format(strings: [term, policy],
                                                 boldFont: UIFont.boldSystemFont(ofSize: 15),
                                                 boldColor: UIColor.blue,
                                                 inString: "login_page_1_bottom_text".localized(),
                                                 font: UIFont.systemFont(ofSize: 15),
                                                 color: UIColor.black) as? NSMutableAttributedString
                
            }
            else {
                attributedString = String.format(strings: [sms],
                                                 boldFont: UIFont.boldSystemFont(ofSize: 15),
                                                 boldColor: UIColor.blue,
                                                 inString: "login_page_3_bottom_text".localized(),
                                                 font: UIFont.systemFont(ofSize: 15),
                                                 color: UIColor.black) as? NSMutableAttributedString
                
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTermTapped))
            bottomTetView.addGestureRecognizer(tap)
            self.bottomTetView.attributedText = attributedString
            self.bottomTetView.textColor = .white
            self.bottomTetView.textAlignment = .center
            self.bottomTetView.isUserInteractionEnabled = true
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func sendSMS(isRetry:Bool = false) {
                
        if !isSMSTimerOn {
            
            let hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Sending".localized()
            hud.vibrancyEnabled = true
            hud.show(in: self.view)
            
            self.verifyNumber { (error) in
                
                if error != nil {
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Error".localized()
                        hud.detailTextLabel.text = error ?? ""
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    
                    return
                }
                
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Success".localized()
                    hud.detailTextLabel.text = nil
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                
                let smsTimerTo = UserDefaults.standard.integer(forKey: "smsTimerTo")

                let diff = smsTimerTo - Int(NSDate().timeIntervalSince1970)
                
                if (!isRetry) {
                    self.isSMSTimerOn = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                        self.segueToNewView(id: "third")
                    })
                }
                else {
                    
                    if diff > 0 {
                        self.timeLeft = diff
                    }
                    else {
                        self.timeLeft = 120
                        UserDefaults.standard.set(Int(NSDate().timeIntervalSince1970) + 120, forKey: "smsTimerTo")
                    }
                    
                    self.startSMSTimer()
                    self.isSMSTimerOn = true
                }
            }
        }
        else {
            let hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "seconds_left".localized() + " \(timeLeft) " + "sec".localized()
            hud.vibrancyEnabled = true
            hud.show(in: self.view)
            hud.dismiss(afterDelay: 1.0, animated: true)
        }
    }
    
    func startSMSTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
        
        if (timeLabel != nil) {
            timeLabel.isHidden = false
        }
    }
    
    @objc func onTimerFires()
    {
        timeLeft -= 1
        
        if (timeLabel != nil) {
            timeLabel.text = "seconds_left".localized() + " \(timeLeft) " + "sec".localized()
        }

        if timeLeft <= 0 {
            timer?.invalidate()
            isSMSTimerOn = false
            timer = nil
            if (timeLabel != nil) {
                timeLabel.isHidden = true
            }
            
            UserDefaults.standard.set(Int(NSDate().timeIntervalSince1970), forKey: "smsTimerTo")
            self.timeLeft = 120
        }
    }
    
    @IBAction func openController(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            self.segueToNewView(id: "second")
        case 1:
            if self.phoneNumber != ""  {
                sendSMS()
            }
        default:
            
            var code = String()
            codeTextFields.forEach { (textfields) in
                code += textfields.text!
            }
            
            if (code.count != 6) {
                UIView.animate(withDuration: 2, animations: {
                    self.errorMessage.isHidden = false
                    self.codeTextFields.forEach { (textFields) in
                        textFields.layer.borderColor = UIColor.red.cgColor
                        textFields.layer.borderWidth = 1
                        textFields.layer.cornerRadius = 5
                        textFields.textColor = .red
                    }
                })
            }
            else {
                let hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Sending".localized()
                hud.vibrancyEnabled = true
                hud.show(in: self.view)
                
                self.verifyCode(verificationCode: code) { (token, error) in
                    
                    if error != nil || token == nil {
                        UIView.animate(withDuration: 0.1, animations: {
                            hud.textLabel.text = "Error".localized()
                            hud.detailTextLabel.text = error?.localizedDescription ?? ""
                            hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        })
                        
                        hud.dismiss(afterDelay: 1.0, animated: true)
                        return
                    }
                    
                    APIManager.sharedInstance.checkVerificationToken(phoneNumber: self.phoneNumber, token: token!, onSuccess: { data in
                        if !data.isEmpty {
                            guard let expiresIn = (data["expiresIn"]?.int),  let refreshToken = (data["refreshToken"]?.string),
                                let accessToken = (data["accessToken"]?.string),
                                let userData = (data["userDetails"]?.dictionary) else {
                                    UIView.animate(withDuration: 0.1, animations: {
                                        hud.textLabel.text = "Error".localized()
                                        hud.detailTextLabel.text = nil
                                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                    })
                                    
                                    hud.dismiss(afterDelay: 1.0, animated: true)
                                    return
                            }

                            ApiConfigurationData.expiresIn = "\(expiresIn)"
                            ApiConfigurationData.refreshToken = refreshToken
                            ApiConfigurationData.accessToken = accessToken
                            
                            self.keychain.accessGroup = "A5MXJN8KJZ.com.iPiterGroup.TestCaiTraidingApp"
                            self.keychain.set(refreshToken, forKey: "refresh")
                            
                            guard
                                let phone = userData["phone"]?.string,
                                let wallet = userData["wallet"]?.dictionary, let commissionForSum = userData["commissionForSum"]?.double, let commissionForGramm = userData["commissionForGramm"]?.double  else {
                                    UIView.animate(withDuration: 0.1, animations: {
                                        hud.textLabel.text = "Error".localized()
                                        hud.detailTextLabel.text = nil
                                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                    })
                                    
                                    hud.dismiss(afterDelay: 1.0, animated: true)
                                    return
                            }
                            
                            let creditCard = userData["cardNumber"]?.string ?? ""
                            let cardHolder = userData["cardHolderName"]?.string ?? ""
                            
                            if let country = userData["country"]?.dictionary {
                                let countryName = country["name"]?.string
                                let countryID = country["id"]?.number
                                UserProfileData.countryName = countryName
                                UserProfileData.countryID = Int(truncating: countryID!)
                            }
                            
                            let walletObj = UserProfileData.walletType(creditLimit: wallet["creditLimit"]?.double ?? 0, debt: wallet["debt"]?.double ?? 0, debtCreated: wallet["debtCreated"]?.string ?? "", debtExpired: wallet["debtExpired"]?.string ?? "", balance: userData["balance"]?.double ?? 0)
                            
                            if let urlStr = userData["photoUrl"]?.array?.last?.string, let url = URL(string: urlStr) {
                                UserProfileData.imageUrl = url
                            }
                            
                            UIView.animate(withDuration: 0.1, animations: {
                                hud.textLabel.text = "Success".localized()
                                hud.detailTextLabel.text = nil
                                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                            })
                            
                            UserProfileData.nickname = userData["nickName"]?.string ?? ""
                            UserDefaults.standard.set(userData["verified"]?.bool ?? false, forKey: "isUserVerified")

                            guard let name = userData["name"]?.string, name != "", let address = userData["address"]?.string, address != ""  else {
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "profileData") as? AddProfileData
                                self.navigationController?.pushViewController(vc!, animated: true)
                                return
                            }
                            
                            if !(userData["verified"]?.bool ?? false)  {
                                self.navigationController?.popToRootViewController(animated: true)
                                return
                            }
                            
                            hud.dismiss(afterDelay: 0.5, animated: true)
                            
                            guard let id = userData["userId"]?.int else {
                                self.navigationController?.popToRootViewController(animated: true)
                                return
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                
                                UserProfileData.id = id
                                UserProfileData.creditCard = creditCard
                                UserProfileData.cardHolderName = cardHolder
                                
                                UserProfileData.commissionForSum = commissionForSum
                                UserProfileData.commissionForGramm = commissionForGramm
                                
                                UserProfileData.wallet = walletObj
                                
                                UserProfileData.phone = phone
                                
                                UserProfileData.name = name
                                UserProfileData.address = address
                                
                                self.isSMSTimerOn = false
                                self.timer = nil
                                
                                UserDefaults.standard.set(Int(NSDate().timeIntervalSince1970), forKey: "smsTimerTo")
                                self.timeLeft = 120
                                
                                self.segueToNewView(id: "home")
                            })
                        }
                        else {
                            UIView.animate(withDuration: 0.1, animations: {
                                hud.textLabel.text = "Error".localized()
                                hud.detailTextLabel.text = nil
                                hud.indicatorView = JGProgressHUDErrorIndicatorView()
                            })
                            
                            hud.dismiss(afterDelay: 1.0, animated: true)
                        }
                    }, viewController: self)
                }
            }
        }
    }
    
    @IBAction func _back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func segueToNewView(id:String) {
        if (id != "home") {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: id) as? Login
            if self.phoneNumber != "" {
                vc!.phoneNumber = self.phoneNumber
            }
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "routerNavigator") as? RouterNavigation
                self.present(vc!, animated: true, completion: nil)
            })
        }
    }
}

extension Login: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let identifier = URL.absoluteString
        
        if (identifier == "sms") {
            if self.phoneNumber != ""  {
                sendSMS(isRetry: true)
            }
        }
        else {
            showTextView()
        }
        return false
    }
}

extension Login: UITextFieldDelegate {
    
    @objc func textChanged(_ textField: UITextField) {
        
        if (textField.tag != 20) {
            countCode = textField.text!.isEmpty ? countCode - 1 : countCode + 1
            
            UIView.animate(withDuration: 1) {
                self.clearBtn.alpha = self.countCode > 0 ? 1 : 0
            }
            
            if textField.tag != 0  && textField.text?.count == 1{
                if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
                    nextField.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
            }
        }
        else {
            if let validNumber = getValidNumber(phoneNumber: textField.text!) {
                nextButton.isEnabled = true
                nextButton.alpha = 1
                self.phoneNumber = "+\(validNumber.countryCode!)" + "\(validNumber.nationalNumber!)"
                textField.resignFirstResponder()
                sendSMS()
            } else {
                nextButton.isEnabled = false
                nextButton.alpha = 0.5
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.tag != 20) {
            
            if self.errorMessage != nil && !self.errorMessage.isHidden {
                UIView.animate(withDuration: 2, animations: {
                    self.errorMessage.isHidden = true
                    self.codeTextFields.forEach { (textFields) in
                        textFields.layer.borderColor = UIColor.clear.cgColor
                        textFields.textColor = .black
                    }
                })
            }
            
            if textField.tag != 0 {
                guard let textFieldText = textField.text,
                    let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                        return false
                }
                let substringToReplace = textFieldText[rangeOfTextToReplace]
                let count = textFieldText.count - substringToReplace.count + string.count
                
                return count <= 1
            }
            
        }
        
        return true
    }
}

extension Login {
    
    func verifyNumber(completionHandler: @escaping(String?) -> Void) {
        PhoneAuthProvider.provider().verifyPhoneNumber(self.phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                completionHandler(error.localizedDescription)
                return
            }
            
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            completionHandler(nil)
        }
    }
    
    func verifyCode(verificationCode:String, completionHandler:@escaping(String?, Error?) -> Void) {
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID ?? "",
            verificationCode: verificationCode)
        
        auth(credential: credential) { token, error  in
            completionHandler(token, error)
        }
    }
    
    func auth(credential:AuthCredential, completionHandler: @escaping (String?, Error?) -> Void){
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if (error != nil) {
                completionHandler(nil, error)
                return
            }
            
            authResult?.user.getIDToken(completion: { (token, error) in
                if error != nil {
                    completionHandler(nil, error)
                }
                
                completionHandler(token, nil)
            })
        }
    }
}


extension Login:CountryPickerViewDelegate, CountryPickerViewDataSource {
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
    }    
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country"
    }
    
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .tableViewHeader
    }
    
    func localeForCountryNameInList(in countryPickerView: CountryPickerView) -> Locale {
        return Locale(identifier: Localize.currentLanguage())
    }
}
