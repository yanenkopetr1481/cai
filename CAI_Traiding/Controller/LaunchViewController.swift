//
//  LaunchViewController.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/22/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import KeychainSwift
import Updates


enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}


class LaunchViewController: RootViewController {
    
    @IBOutlet weak var callBtn:UIButton!
    @IBOutlet weak var message:UILabel!
    @IBOutlet weak var logoutBtn:UIButton!
    
    var isVerified:Bool = true
    var isPresentedNewView:Bool = false
    
    let throbberView = ThrobberView()
    let keychain = KeychainSwift()
    var isResponseRefreshCount:Int = 0
    var isResponseUserCount:Int = 0
    
    var timer: Timer?
    
    var isFireEnable = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        isFireEnable = true
        isVerified = true
        isResponseRefreshCount = 0
        isResponseUserCount = 0
        timer = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        DispatchQueue.main.async {
            self.isVerified = UserDefaults.standard.bool(forKey: "isUserVerified")
            self.setupViewConfiguration()
        }
    }
    
    func isUpdateAvailable(completion: @escaping (Bool?, Error?) -> Void) throws -> URLSessionDataTask {
        if  let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=com.iPiterGroup.CaiTraidingApp") {
        
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                if let error = error { throw error }
                guard let data = data else { return }
                let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
                guard let result = (json?["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String else {
                    throw VersionError.invalidResponse
                }
                            
                Updates.newVersionString = "2"
                completion(version != currentVersion, nil)
            } catch {
                completion(nil, error)
            }
        }
            task.resume()
            return task
        }
        else {
            throw VersionError.invalidBundleInfo
        }
    }
    
    
    func setupViewConfiguration() {
        callBtn.isHidden = isVerified
        message.isHidden = isVerified
        
        logoutBtn.setTitle("settings_logout".localized(), for: .normal)
        logoutBtn.isHidden = isVerified
        
        message.text = "Verified".localized()
        keychain.accessGroup = "A5MXJN8KJZ.com.iPiterGroup.TestCaiTraidingApp"
        
        throbberView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(throbberView)
        throbberView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        throbberView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        throbberView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        throbberView.widthAnchor.constraint(equalToConstant: 90).isActive = true
        
        throbberView.startAnimating()
        observeAppVersionDidChange()
        Updates.updatingMode = .manually
        Updates.notifying = .always
        Updates.appStoreId =  "1494215590"
        Updates.comparingVersions =  .major
        Updates.minimumOSVersion = "12.0.0"
        Updates.countryCode = "us"

        do {
           _ =  try isUpdateAvailable { (update, error) in
                if error != nil {
                    self.startMainProcess()
                } else if let update = update {
                    if (update) {
                        Updates.checkForUpdates { result in
                            let vc = UpdatesUICustom(animated: true) {
                                self.startMainProcess()
                            }
                            vc.promptToUpdate(result, presentingViewController: self)
                        }
                    }
                    else {
                        self.startMainProcess()
                    }
                }
                else {
                    self.startMainProcess()
                }
            }
        }
        catch {
            self.startMainProcess()
        }
    }
    
    func startMainProcess() {
        if (self.isFireEnable) {
            self.isFireEnable = false
            Timer.scheduledTimer(timeInterval: 0.0, target: self, selector: #selector(self.fire), userInfo: nil, repeats: false)
        }
    }
    
    private func observeAppVersionDidChange() {
        NotificationCenter.default.addObserver(self, selector: #selector(appDidInstall),
        name: .appDidInstall, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appVersionDidChange),
                                               name: .appVersionDidChange, object: nil)
    }
    
    @objc func appDidInstall(_ notification: Notification) {
        print("App installed.")
    }
    
    @objc func appVersionDidChange(_ notification: Notification) {
        print("App version changed.")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
    }
    
    @IBAction func logout(_ sender:UIButton) {
        ApiConfigurationData.accessToken = ""
        ApiConfigurationData.expiresIn = ""
        ApiConfigurationData.refreshToken = ""
        ApiConfigurationData.contacNumber = [String]()
        
        UserProfileData.creditCard = nil
        UserProfileData.commissionForSum = nil
        UserProfileData.commissionForGramm = nil
        UserProfileData.name = nil
        UserProfileData.phone = nil
        UserProfileData.image = nil
        UserProfileData.wallet = nil
        UserProfileData.address = nil
        UserProfileData.language = nil
        UserProfileData.imageUrl = nil
        UserProfileData.nickname = nil
        UserProfileData.id = nil
        
        AppLocalData.Countries = nil
        self.keychain.accessGroup = "A5MXJN8KJZ.com.iPiterGroup.TestCaiTraidingApp"
        if let _ = self.keychain.get("refresh") {
            self.keychain.delete("refresh")
        }
        
        UserDefaults.standard.set(true, forKey: "isUserVerified")
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "language") as? SetLanguage
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    fileprivate func goHome() {
        DispatchQueue.main.async {
            self.throbberView.stopAnimating()
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "language") as? SetLanguage
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    @objc func fire()
    {
    
        if isVerified {
            
            if let refresh = keychain.get("refresh") {
                
                if isResponseRefreshCount < 2 && self.isResponseUserCount < 2 {
                    
                    ApiConfigurationData.refreshToken = refresh
                    
                    APIManager.sharedInstance.updateToken(refreshToken: refresh, onSuccess: { (data) in
                        
                        guard let accessToken = data["accessToken"]?.string, let refreshToken = data["refreshToken"]?.string, let expiresIn = data["expiresIn"]?.int else {
                            
                            if data["status"]?.number == 1001 || data["status"]?.number == 1002 {
                                self.isVerified = false
                                self.setupViewConfiguration()
                            }
                            else {
                                self.isResponseRefreshCount += 1
                            }
                            
                            self.fire()
                            return
                        }
                        

                        ApiConfigurationData.expiresIn = "\(expiresIn)"
                        ApiConfigurationData.refreshToken = refreshToken
                        ApiConfigurationData.accessToken = accessToken

                        let keychain = KeychainSwift() 
                        keychain.accessGroup = "A5MXJN8KJZ.com.iPiterGroup.TestCaiTraidingApp"
                        keychain.set(refreshToken, forKey: "refresh")
                        
                        APIManager.sharedInstance.updateNotificationToken(onSuccess: { (data) in
                            
                            if data["status"]?.number == 1 {
                                self.getUserData()
                            }
                            else {
                                self.goHome()
                            }
                            
                        }, viewController: self)
                    }, viewController: self)
                }
                else {
                    goHome()
                }
            }
            else {
                goHome()
            }
        }
        else {
            if timer == nil {
                timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(getUserData), userInfo: nil, repeats: true)
            }
        }
    }
    
    @objc func getUserData() {
        
        if (ApiConfigurationData.accessToken == "") {
            if let refresh = keychain.get("refresh") {
                
                ApiConfigurationData.refreshToken = refresh
                
                APIManager.sharedInstance.updateToken(refreshToken: refresh, onSuccess: { (data) in
                    
                    guard let accessToken = data["accessToken"]?.string, let refreshToken = data["refreshToken"]?.string, let expiresIn = data["expiresIn"]?.int else {
                        return
                    }

                    ApiConfigurationData.expiresIn = "\(expiresIn)"
                    ApiConfigurationData.refreshToken = refreshToken
                    ApiConfigurationData.accessToken = accessToken
                    
                    let keychain = KeychainSwift()
                    keychain.accessGroup = "A5MXJN8KJZ.com.iPiterGroup.TestCaiTraidingApp"
                    keychain.set(refreshToken, forKey: "refresh")
                    
                    APIManager.sharedInstance.updateNotificationToken(onSuccess: { (data) in
                        
                        if data["status"]?.number == 1 {
                            self.getUserData()
                        }
                        else {
                            self.goHome()
                        }
                        
                    }, viewController: self)
                    
                }, viewController: self)
            }
            else {
                goHome()
            }
        }
        else {
            APIManager.sharedInstance.getUserData(onSuccess: { (userData) in
                                
                guard let id = userData["userId"]?.int else {
                    self.goHome()
                    return
                }
                
                guard let verified = userData["verified"]?.bool, verified == true else  {
                    self.isResponseUserCount += 1
                    self.fire()
                    return
                }
                
                guard let phone = userData["phone"]?.string, let wallet = userData["wallet"]?.dictionary,
                    let commissionForSum = userData["commissionForSum"]?.double, let commissionForGramm = userData["commissionForGramm"]?.double  else {
                        self.goHome()
                        return
                }
                
                let creditCard = userData["cardNumber"]?.string ?? ""
                let cardHolder = userData["cardHolderName"]?.string ?? ""
                
                if let country = userData["country"]?.dictionary {
                    let countryName = country["name"]?.string
                    let countryID = country["id"]?.number
                    UserProfileData.countryName = countryName
                    UserProfileData.countryID = Int(truncating: countryID!)
                }
                
                
                let walletObj = UserProfileData.walletType(creditLimit: wallet["creditLimit"]?.double ?? 0, debt: wallet["debt"]?.double ?? 0, debtCreated: wallet["debtCreated"]?.string ?? "", debtExpired: wallet["debtExpired"]?.string ?? "", balance: userData["balance"]?.double ?? 0)
                
                if let urlStr = userData["photoUrl"]?.array?.last?.string, let url = URL(string: urlStr) {
                    UserProfileData.imageUrl = url
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    UserProfileData.id = id

                    UserProfileData.creditCard = creditCard
                    UserProfileData.cardHolderName = cardHolder
                    
                    UserProfileData.commissionForSum = commissionForSum
                    UserProfileData.commissionForGramm = commissionForGramm
                    
                    UserProfileData.wallet = walletObj
                    UserProfileData.phone = phone
                    
                    let nickname = userData["nickName"]?.string
                    UserProfileData.nickname = nickname
                    
                    guard let name = userData["name"]?.string, !name.isEmpty else {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "profileData") as? AddProfileData
                        self.navigationController?.pushViewController(vc!, animated: true)
                        return
                    }
                    
                    UserProfileData.name = name
                    
                    guard let address = userData["address"]?.string, !address.isEmpty else {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "profileData") as? AddProfileData
                        self.navigationController?.pushViewController(vc!, animated: true)
                        return
                    }
                    
                    UserProfileData.name = name
                    UserProfileData.address = address
                    
                    UserDefaults.standard.set(true, forKey: "isUserVerified")
                    
                    if self.timer != nil {
                        self.timer!.invalidate()
                        self.timer = nil
                    }
                    
                    if !self.isPresentedNewView {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "routerNavigator") as? RouterNavigation
                        self.show(vc!, sender: self)
                        self.isPresentedNewView = true
                    }
                })
            }, viewController: self)
        }
    }
}

