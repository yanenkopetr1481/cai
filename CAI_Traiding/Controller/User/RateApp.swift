//
//  RateApp.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/20/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit

class RateApp: RootViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        configureNavigationBar()
    }
    
//    override var prefersStatusBarHidden: Bool {
//        return true
//    }
    
    func configureNavigationBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(back))
        navigationItem.leftBarButtonItem?.tintColor = .white
    }
    
    @objc func back(_ sender:Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
