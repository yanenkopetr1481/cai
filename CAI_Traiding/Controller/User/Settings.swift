//
//  Settings.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/30/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import Gallery
import JGProgressHUD
import Lightbox
import Localize_Swift
import NotificationBannerSwift
import KeychainSwift
import Firebase
import SwiftyJSON
import FirebaseInstanceID

protocol SettingDelegate {
    func changeLanguage()
}

class Settings: RootViewController {
    
    @IBOutlet weak var userNameTitle: UILabel!
    @IBOutlet weak var userAddressTitle: UILabel!
    @IBOutlet weak var userLanguageTitle: UILabel!
    @IBOutlet weak var userCountryTitle: UILabel!
    @IBOutlet weak var userNotificationsTitle: UILabel!
    @IBOutlet weak var userNickName: UILabel!
    @IBOutlet weak var userCardNumberTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userIsPhoneEnablee: UISwitch!
    @IBOutlet weak var imageBlock: UIView!
    @IBOutlet weak var btnChooseImage: UIButton!
    @IBOutlet weak var languageStackView: UIStackView!
    @IBOutlet weak var countryStackView: UIStackView!
    @IBOutlet weak var phoneStackView: UIStackView!
    @IBOutlet weak var bottomLine: UIView!
    @IBOutlet weak var topLine: UIView!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var changeLang: UIButton!
    @IBOutlet weak var userCardHolderNameTitle: UILabel!
    @IBOutlet weak var userCardHolderName: UILabel!
    @IBOutlet weak var userCardHolderStackView: UIStackView!
    @IBOutlet weak var userCardNumberStackView: UIStackView!
    @IBOutlet weak var userCardNumber: UILabel!
    
    @IBOutlet var fieldTextLabels: [UITextField]!
    @IBOutlet var labels: [UILabel]!
    
    let thePicker = UIPickerView()
    var isTextFieldModified = false
    var actionSheet: UIAlertController!
    var availableLanguages = Localize.availableLanguages()
    var isUserDataEditing:Bool = false
    var gallery: GalleryController!
    var lightbox: LightboxController!
    var userLocalDate = [String?]()
    var customAlert:CustomAlertViewPurhcases?
    var longgesture:UILongPressGestureRecognizer!
    let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    let defaults = UserDefaults.standard
    let keychain = KeychainSwift()
    var languages = [JSON]()
    var country = Int()
    var contryName = String()
    var isCameraOpen = false
    var delegate:SettingDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        userIsPhoneEnablee.setOn(!defaults.bool(forKey: "ISPushNotificationsEnable"), animated: true)
        longgesture = UILongPressGestureRecognizer(target: self, action: #selector(removePhoto))
        longgesture.minimumPressDuration = 0.5
        btnChooseImage.addGestureRecognizer(longgesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        hideKeyboardWhenTappedAround()
        setupImageBlockConfiguration()
        setupGalleryConfiguration()
        initCoreData()
        setupLocalization()
        
        thePicker.delegate = self
        
        if let countries = AppLocalData.Countries {
            self.languages = countries
            self.setCountryPicker()
        }
        else {
            getLanguages(handler: {
                self.setCountryPicker()
            })
        }
        
        self.availableLanguages.removeAll { (string) -> Bool in
            return string == "Base" ? true : false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !isCameraOpen {
            initCoreData()
            preventDefaultLabel()
        }
    }
    
    func setCountryPicker() {
        if !self.languages.isEmpty {
            AppLocalData.Countries = self.languages
            
            self.fieldTextLabels.forEach { (textfied) in
                if textfied.tag == 4 {
                    textfied.inputView = self.thePicker
                }
            }
            
            let index = self.languages.firstIndex { (data) -> Bool in
                return data.dictionary!["name"]?.string == UserProfileData.countryName
                } ?? 0
            
            country = Int(truncating: self.languages[index]["id"].number!)
            
            self.thePicker.selectRow(index, inComponent: 0, animated: true)
        }
    }
    
    func getLanguages(handler:@escaping()->Void) {
        APIManager.sharedInstance.getCountries(completionHandler: { (data) in
            if let data = data["data"]?.array, !data.isEmpty {
                self.languages = data
                handler()
            }
        }, viewController: self)
    }
    
    @IBAction func changePushNotification(_ sender: UISwitch) {
        defaults.set(!sender.isOn, forKey: "ISPushNotificationsEnable")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isCameraOpen {

            NotificationCenter.default.addObserver(self, selector: #selector(changeLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
            showOrHideCardData()
            initCoreData()
            setCountryPicker()
        }
        
        isCameraOpen = false
    }
    
    func showOrHideCardData() {
        if UserProfileData.cardHolderName == nil || UserProfileData.cardHolderName!.isEmpty {
            userCardHolderStackView.isHidden = true
        }
        else {
            userCardHolderStackView.isHidden = false
        }
        
        if UserProfileData.creditCard == nil || UserProfileData.creditCard!.isEmpty {
            userCardNumberStackView.isHidden = true
        }
        else {
            userCardHolderStackView.isHidden = false
        }
    }
    
    @objc func changeLanguage(){
        self.languages = [JSON]()
        delegate?.changeLanguage()
    }
    
    func setupLocalization() {
        userNameTitle.text = "settings_name".localized()
        userAddressTitle.text = "settings_address".localized()
        userLanguageTitle.text = "settings_language".localized()
        userNotificationsTitle.text = "settings_notifications".localized()
        logoutButton.setTitle("settings_logout".localized(), for: .normal)
        userCardHolderNameTitle.text = "settings_cardHolder".localized()
        userCardNumberTitle.text = "settings_cardNumber".localized()
        changeLang.setTitle(Localize.displayNameForLanguage(Localize.currentLanguage()), for: .normal)
        userCountryTitle.text = "settings_contry".localized()
    }
    
    func checkCreditData(){
        if UserProfileData.creditCard?.isEmpty ?? true {
            userCardNumberTitle.isHidden = true
            userCardNumber.isHidden = true
        }
        else {
            userCardNumberTitle.isHidden = false
            userCardNumber.isHidden = false
        }
        
        if UserProfileData.cardHolderName?.isEmpty ?? true {
            userCardHolderNameTitle.isHidden = true
            userCardHolderName.isHidden = true
        }
        else {
            userCardHolderNameTitle.isHidden = false
            userCardHolderName.isHidden = false
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height - 160
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func initCoreData() {
        userLocalDate = [UserProfileData.name, UserProfileData.address, UserProfileData.creditCard, UserProfileData.cardHolderName, UserProfileData.countryName]
        country = Int()
        contryName = String()
        isUserDataEditing = false
        isTextFieldModified = false
        
        fieldTextLabels.forEach { (textField) in
            textField.text = userLocalDate[textField.tag] ?? ""
            textField.autocapitalizationType = .none
            textField.isHidden = true
        }
        
        labels.forEach { (label) in
            label.text = userLocalDate[label.tag] ?? label.text
            label.isHidden = false
        }
        
        userName.text = UserProfileData.nickname
        userNickName.text = UserProfileData.phone
        
        showOrHideCardData()
        btnChooseImage.isHidden = true
    }
    
    func setupGalleryConfiguration() {
        Config.Camera.imageLimit = 1
        Config.tabsToShow = [.imageTab, .cameraTab]
    }
    
    func setupImageBlockConfiguration() {
        
        self.imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        self.btnChooseImage.layer.cornerRadius = btnChooseImage.bounds.width/2
        
        guard let image = ApiConfigurationData.appImages.object(forKey: (UserProfileData.imageUrl?.absoluteString ?? "profile") as AnyObject) as? UIImage else {
            
            ApiConfigurationData.appImages.setObject(UIImage(named: "profile")!, forKey: (UserProfileData.imageUrl?.absoluteString ?? "profile") as AnyObject)
            imgProfile.image = UIImage(named: "profile")
            return
        }
        
        imgProfile.image = image
    }
    
    @IBAction func btnChooseImageOnClick(_ sender: UIButton) {
        isCameraOpen = true
        gallery = GalleryController()
        gallery.delegate = self
        gallery.modalPresentationStyle = .fullScreen
        present(gallery, animated: true, completion: nil)
    }
    
    @IBAction func logout(_ sender: UIButton) {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Wait".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        APIManager.sharedInstance.logout(onSuccess: { (data) in
            
            let instance = InstanceID.instanceID()
            instance.deleteID { (error) in
                
                instance.instanceID { (result, error) in
                    if let error = error {
                        print("Error fetching remote instange ID: \(error)")
                    } else {
                        self.defaults.set(result?.token, forKey: "PushNotificationsToken")
                        print("Remote instance ID token: \(String(describing: result?.token))")
                    }
                }
            }
            
            UIView.animate(withDuration: 0.2, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = nil
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                
                ApiConfigurationData.accessToken = ""
                ApiConfigurationData.expiresIn = ""
                ApiConfigurationData.refreshToken = ""
                ApiConfigurationData.contacNumber = [String]()
                
                UserProfileData.creditCard = nil
                UserProfileData.commissionForSum = nil
                UserProfileData.commissionForGramm = nil
                UserProfileData.name = nil
                UserProfileData.phone = nil
                UserProfileData.image = nil
                UserProfileData.wallet = nil
                UserProfileData.address = nil
                UserProfileData.language = nil
                UserProfileData.imageUrl = nil
                UserProfileData.nickname = nil
                UserProfileData.countryName = nil
                UserProfileData.countryID = nil
                UserProfileData.cardHolderName = nil
                
                AppLocalData.Countries = nil
                AppLocalData.BadgesCount = [.deal:Int(),.payment:Int(),.purchase:Int(),.credit:Int()]
                self.languages = [JSON]()
                UIApplication.shared.applicationIconBadgeNumber = 0
                
                self.defaults.removeObject(forKey: "AppLocalBadges")
                
                self.keychain.accessGroup = "A5MXJN8KJZ.com.iPiterGroup.TestCaiTraidingApp"
                if let _ = self.keychain.get("refresh") {
                    self.keychain.delete("refresh")
                }
                
                self.delegate?.changeLanguage()
            }
        }, viewController: self)
    }
    
    @objc func removePhoto() {
        customAlert = (self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases)
        customAlert!.providesPresentationContextTransitionStyle = true
        customAlert!.definesPresentationContext = true
        customAlert!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert!.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert!.delegate = self
        customAlert!.config = AlertCustomData(title: "removePhoto".localized(), firstSegment: "Yes".localized(), secondSegment: "No".localized())
        self.present(customAlert!, animated: true, completion: nil)
    }
    
    @IBAction func doChangeLanguage(_ sender: AnyObject) {
        actionSheet = UIAlertController(title: nil, message: "SwitchLanguage".localized(), preferredStyle: UIAlertController.Style.actionSheet)
        for language in availableLanguages {
            let displayName = Localize.displayNameForLanguage(language)
            let languageAction = UIAlertAction(title: displayName, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                Localize.setCurrentLanguage(language)
                UserDefaults.standard.set([language], forKey: "AppleLanguages")
                UserDefaults.standard.set(language, forKey: "AppLanguageLocal")
                UIView.appearance().semanticContentAttribute = language == "ar" ? .forceRightToLeft : .forceLeftToRight
                UserDefaults.standard.synchronize()
            })
            actionSheet.addAction(languageAction)
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.cancel, handler: {
            (alert: UIAlertAction) -> Void in
        })
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
}

extension Settings: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        self.isTextFieldModified = true
        
        if textField.tag == 2 {
            
            //range.length will be greater than 0 if user is deleting text - allow it to replace
            if range.length > 0
            {
                return true
            }
            
            //Don't allow empty strings
            if string == " "
            {
                return false
            }
            
            //Check for max length including the spacers we added
            if range.location == 19
            {
                return false
            }
            
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: "-", with: "")
            
            //Verify entered text is a numeric value
            let digits = NSCharacterSet.decimalDigits
            for char in replacementText.unicodeScalars
            {
                if !digits.contains(char)
                {
                    return false
                }
            }
            
            //Put an empty space after every 4 places
            if range.location == 4 || range.location == 9 || range.location == 14
            {
                originalText?.append("-")
                textField.text = originalText
            }
            return true
        }
        else {
            return !string.isSingleEmoji
        }
    }
}

extension Settings: CustomAlertViewDelegate {
    func cancelButtonTapped() {
        self.customAlert!.dismiss(animated: true, completion: nil)
    }
    
    func okButtonTapped(count: String) {
        
        let urlString = UserProfileData.imageUrl?.absoluteString ?? "profile"
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        APIManager.sharedInstance.deleteUserPhoto(url: urlString, onSuccess: { (data) in
            
            guard  let status = data["status"]?.number, status == 1  else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["data"]?.string
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.5, animated: true)
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = nil
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            hud.dismiss(afterDelay: 1.0, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                
                ApiConfigurationData.appImages.removeObject(forKey: UserProfileData.nickname! as AnyObject)
                ApiConfigurationData.appImages.setObject(UIImage(named:"profile")!, forKey: "profile" as AnyObject)
                self.imgProfile.image = UIImage(named:"profile")
                UserProfileData.image = UIImage(named:"profile")
                self.customAlert!.dismiss(animated: true, completion: nil)
            })
            
        }, viewController: self)
    }
}

extension Settings: GalleryControllerDelegate {
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        
        DispatchQueue.main.async {
            Image.resolve(images: images, completion: { [weak self] resolvedImages in
                let images = resolvedImages.compactMap({ $0 })
                
                self!.imgProfile.isSkeletonable = true
                //                self!.btnChooseImage.showLoading()
                self!.imgProfile.showAnimatedGradientSkeleton()
                
                APIManager.sharedInstance.saveUserPhoto(data: images[0], onSuccess: { (data) in
                    if let url = data["url"]?.array?.first?.string{
                        self!.imgProfile.image = images[0]
                        UserProfileData.imageUrl = URL(string: url)
                        ApiConfigurationData.appImages.setObject(images[0], forKey: (UserProfileData.imageUrl?.absoluteString ?? "profile") as AnyObject)

                        UserProfileData.image = images[0]
                    }
                    self!.imgProfile.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                }, viewController: self!)
            })
        }
        
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {}
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        LightboxConfig.DeleteButton.enabled = true
        
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            self?.showLightbox(controller, images: resolvedImages.compactMap({ $0 }))
        })
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func showLightbox(_ controller: GalleryController,images: [UIImage]) {
        guard images.count > 0 else {
            return
        }
        
        let lightboxImages = images.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.modalPresentationStyle = .fullScreen
        lightbox.headerView.deleteButton.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
        gallery.present(lightbox, animated: true)
    }
    
    @objc func deleteImage() {
        var images =  gallery.cart.images
        gallery.cart.remove(images[lightbox.currentPage])
        images.remove(at: lightbox.currentPage)
        gallery.cart.reload(images)
    }
    
    func preventDefaultLabel() {
        fieldTextLabels.forEach { (textField) in
            textField.borderStyle = .none
            textField.backgroundColor = UIColor.white
            textField.textColor = .black
            textField.isUserInteractionEnabled = false
            textField.isHidden = true
            self.labels[textField.tag].isHidden = false
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.languageStackView.alpha = 1
            self.phoneStackView.alpha = 1
            self.bottomLine.alpha = 1
            self.topLine.alpha = 1
            self.logoutButton.alpha = 1
        })
        
    }
}

extension Settings:UserSettingsDelegate {
    
    func candelEditUserData(button: UIBarButtonItem) {
        button.title = "settings_edit".localized()
        preventDefaultLabel()
        initCoreData()
    }
    
    
    func editUserData(button: UIBarButtonItem) {
        
        if !isUserDataEditing {
            button.title = "Save".localized()
            
            userCardNumberStackView.isHidden = false
            userCardHolderStackView.isHidden = false
            btnChooseImage.isHidden = false
            
            fieldTextLabels.forEach { (textField) in
                textField.borderStyle = .roundedRect
                textField.layer.borderColor = UIColor(red: 0.922, green: 0.922, blue: 0.922, alpha: 1).cgColor
                textField.backgroundColor = UIColor(red: 0.922, green: 0.922, blue: 0.922, alpha: 1)
                textField.textColor = .black
                textField.isUserInteractionEnabled = true
                textField.delegate = self
                textField.isHidden = false
                self.labels[textField.tag].isHidden = true
            }
            
            UIView.animate(withDuration: 0.2, animations: {
                self.languageStackView.alpha = 0
                self.phoneStackView.alpha = 0
                self.bottomLine.alpha = 0
                self.topLine.alpha = 0
                self.logoutButton.alpha = 0
            })
        }
        else {
            if isTextFieldModified {
                button.isEnabled = !button.isEnabled
                
                isTextFieldModified = false
                
                var user_data = [String:String]()
                var isError = false
                fieldTextLabels.forEach { (textfield) in
                    
                    guard let value = textfield.text, value != "" else {
                        isError = (textfield.tag == 2 || textfield.tag == 3) ? false : true
                        return
                    }
                    
                    switch textfield.tag {
                    case 0:
                        user_data["name"] = value
                    case 1:
                        user_data["address"] = value
                    case 2:
                        user_data["cardNumber"] = value
                    case 3:
                        user_data["cardHolderName"] = value
                    case 4:
                        user_data["countryId"] = "\(self.country)"
                    default:
                        break
                    }
                }
                
                if isError {
                    let banner = NotificationBanner(title: "Missing".localized(), subtitle: "checkFields".localized(), style: .danger)
                    banner.show()
                }
                else {
                    APIManager.sharedInstance.changeUserData(data: user_data, onSuccess: { (data) in
                        
                        button.title = "settings_edit".localized()
                        
                        self.fieldTextLabels.forEach { (textField) in
                            textField.borderStyle = .none
                            textField.backgroundColor = UIColor.white
                            textField.textColor = .black
                            textField.isUserInteractionEnabled = false
                            textField.isHidden = true
                            
                            self.labels[textField.tag].isHidden = false
                            self.labels[textField.tag].text = textField.text
                        }
                        
                        UIView.animate(withDuration: 0.2, animations: {
                            self.languageStackView.alpha = 1
                            self.phoneStackView.alpha = 1
                            self.bottomLine.alpha = 1
                            self.topLine.alpha = 1
                            self.logoutButton.alpha = 1
                            
                        })
                        
                        self.userName.text = UserProfileData.nickname
                        UserProfileData.name = user_data["name"]
                        UserProfileData.address = user_data["address"]
                        UserProfileData.creditCard = user_data["cardNumber"]
                        UserProfileData.cardHolderName = user_data["cardHolderName"]
                        
                        button.isEnabled = !button.isEnabled
                        
                        guard let id = user_data["countryId"] else {
                            return
                        }
                        guard let name = self.contryName as String? else {
                            return
                        }
                        
                        UserProfileData.countryName = name
                        UserProfileData.countryID = Int(id)
                        
                        self.showOrHideCardData()
                        self.btnChooseImage.isHidden = true
                        
                    }, viewController: self)
                }
            }
            else {
                button.title = "settings_edit".localized()
                
                showOrHideCardData()
                btnChooseImage.isHidden = true
                
                fieldTextLabels.forEach { (textField) in
                    textField.borderStyle = .none
                    textField.backgroundColor = UIColor.white
                    textField.textColor = .black
                    textField.isUserInteractionEnabled = false
                    textField.isHidden = true
                    self.labels[textField.tag].isHidden = false
                }
                
                UIView.animate(withDuration: 0.2, animations: {
                    self.languageStackView.alpha = 1
                    self.phoneStackView.alpha = 1
                    self.bottomLine.alpha = 1
                    self.topLine.alpha = 1
                    self.logoutButton.alpha = 1
                })
            }
        }
        
        isUserDataEditing = !isUserDataEditing
    }
}

extension Settings:UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.languages.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.languages[row]["name"].string!
        
    }
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        country = Int(truncating: self.languages[row]["id"].number!)
        contryName = self.languages[row]["name"].string!
        isTextFieldModified = true
        
        self.labels.forEach { (label) in
            if label.tag == 4 {
                label.text = self.languages[row]["name"].string!
            }
        }
        
        self.fieldTextLabels.forEach { (textfied) in
            if textfied.tag == 4 {
                textfied.text = self.languages[row]["name"].string!
            }
        }
    }
}


