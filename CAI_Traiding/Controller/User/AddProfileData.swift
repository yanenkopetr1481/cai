//
//  AddProfileData.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 12/2/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import Gallery
import JGProgressHUD
import Lightbox
import NotificationBannerSwift
import KeychainSwift
import SwiftyJSON

class AddProfileData: RootViewController {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnChooseImage: UIButton!
    @IBOutlet weak var btnChooseImageDocument: LoadingButton!
    @IBOutlet weak var nextBtn:UIButton!
    @IBOutlet weak var name:UITextField!
    @IBOutlet weak var nameLoc:UILabel!
    @IBOutlet weak var addressLoc:UILabel!
    @IBOutlet weak var cardLoc:UILabel!
    @IBOutlet weak var docLoc:UILabel!
    @IBOutlet weak var userImageLoc:UILabel!
    @IBOutlet weak var address:UITextField!
    @IBOutlet weak var userCardHolderName: UILabel!
    @IBOutlet var fieldTextLabels:[UITextField]!
    
    var customAlert:CustomAlertViewPurhcases?
    var gallery: GalleryController!
    var lightbox: LightboxController!
    var longgesture:UILongPressGestureRecognizer!
    var isTextFieldModified = false
    var isDocument = false
    let keychain = KeychainSwift()
    var languages = [JSON]()
    var country = Int()
    var contryName = String()
    let thePicker = UIPickerView()
    var timer:Timer?
    var isPresentedNew:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setupViewConfiguration()
        setupGalleryConfiguration()
        hideKeyboardWhenTappedAround()
        //        setTransparentNavigationBar()
    }
    
    func setupViewConfiguration() {
        
        country = UserProfileData.countryID ?? Int()
        contryName = UserProfileData.countryName ?? String()
        
        longgesture = UILongPressGestureRecognizer(target: self, action: #selector(removePhoto))
        longgesture.minimumPressDuration = 0.5
        btnChooseImage.addGestureRecognizer(longgesture)
        thePicker.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        fieldTextLabels.forEach { (textField) in
            
            var data:String?
            var placeHolder:String?
            
            switch textField.tag {
            case 0: data = UserProfileData.name
            case 1: data = UserProfileData.address
            case 2:
                data = UserProfileData.creditCard
                placeHolder = "not_require".localized()
            case 3:
                data = UserProfileData.cardHolderName
                placeHolder = "not_require".localized()
            case 4: data = UserProfileData.countryName
            default: break
            }
            
            textField.text = data
            textField.attributedPlaceholder = NSAttributedString(string: placeHolder ?? "",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            
            textField.autocorrectionType = .no
        }
        
        if let countries = AppLocalData.Countries {
            self.languages = countries
            self.setCountryPicker()
        }
        else {
            getLanguages()
        }
    }
    
    func setCountryPicker() {
        if !self.languages.isEmpty {
            AppLocalData.Countries = self.languages
            self.fieldTextLabels.forEach { (textfied) in
                if textfied.tag == 4 {
                    textfied.inputView = self.thePicker
                }
            }
        }
    }
    
    @objc func getLanguages() {
        APIManager.sharedInstance.getCountries(completionHandler: { (data) in
            if let data = data["data"]?.array, !data.isEmpty {
                self.timer?.invalidate()
                self.languages = data
                self.setCountryPicker()
            }
            else {
                if self.timer == nil {
                    self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.getLanguages), userInfo: nil, repeats: true)
                }
            }
        }, viewController: self)
    }
    
    //    override func viewWillDisappear(_ animated: Bool) {
    //    }
    //
    //    func setTransparentNavigationBar() {
    //        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
    //        self.navigationController!.navigationBar.shadowImage = UIImage()
    //        self.navigationController!.navigationBar.isTranslucent = true
    //    }
    
    func setupGalleryConfiguration() {
        Config.tabsToShow = [.imageTab, .cameraTab]
    }
    
    @objc func removePhoto() {
        customAlert = (self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases)
        customAlert!.providesPresentationContextTransitionStyle = true
        customAlert!.definesPresentationContext = true
        customAlert!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert!.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert!.delegate = self
        customAlert!.config = AlertCustomData(title: "removePhoto".localized(), firstSegment: "Yes".localized(), secondSegment: "No".localized())
        self.present(customAlert!, animated: true, completion: nil)
    }
    
    
    func setLocalization() {
        userImageLoc.text = "profile_photo".localized()
        docLoc.text = "profile_addDoc".localized()
        cardLoc.text = "profile_card".localized()
        nameLoc.text = "profile_name".localized()
        addressLoc.text = "profile_address".localized()
        userCardHolderName.text = "settings_cardHolder".localized()
        nextBtn.setTitle("Confirm".localized(), for: .normal)
        self.title = "profileData".localized()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func saveUserData(_ sender: UIButton) {
        
        var userData = [String:String]()
        var isError = false
        
        fieldTextLabels.forEach { (textfield) in
            guard let value = textfield.text, value != "" else {
                isError = (textfield.tag == 2 || textfield.tag == 3) ? false : true
                return
            }
            
            switch textfield.tag {
            case 0:
                userData["name"] = value
            case 1:
                userData["address"] = value
            case 2:
                userData["cardNumber"] = value
            case 3:
                userData["cardHolderName"] = value
            case 4:
                userData["countryId"] = "\(self.country)"
            default:
                break
            }
        }
        
        if isError {
            let banner = NotificationBanner(title: "BannerMissingField".localized(), subtitle: "checkFields".localized(), style: .danger)
            banner.show()
        }
        else {
            
            let hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Sending".localized()
            hud.vibrancyEnabled = true
            hud.show(in: self.view)
            
            APIManager.sharedInstance.changeUserData(data: userData, onSuccess: { (data) in
                
                guard  let status = data["status"]?.number, status == 1  else {
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Error".localized()
                        hud.detailTextLabel.text = data["data"]?.string
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.5, animated: true)
                    return
                }
                
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Success".localized()
                    hud.detailTextLabel.text = nil
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    
                    if !(UserDefaults.standard.bool(forKey: "isUserVerified"))  {
                        self.navigationController?.popToRootViewController(animated: true)
                        return
                    }
                    if !self.isPresentedNew {
                        
                        UserProfileData.name = userData["name"]
                        UserProfileData.address = userData["address"]
                        UserProfileData.creditCard = userData["cardNumber"] ?? "..."
                        UserProfileData.cardHolderName = userData["cardHolderName"] ??  "..."
                        
                        guard let id = userData["countryId"] else {
                            return
                        }
                        guard let name = self.contryName as String? else {
                            return
                        }
                        
                        UserProfileData.countryName = name
                        UserProfileData.countryID = Int(id)
                        
                        let keychain = KeychainSwift()
                        keychain.accessGroup = "A5MXJN8KJZ.com.iPiterGroup.TestCaiTraidingApp"
                        keychain.set(ApiConfigurationData.refreshToken, forKey: "refresh")
                        self.performSegue(withIdentifier: "home", sender: self)
                        self.isPresentedNew = true
                    }
                })
            }, viewController: self)
        }
    }
    
    @IBAction func btnChooseImageOnClick(_ sender: UIButton) {
        isDocument = sender.tag == 1
        if isDocument {
            Config.Camera.imageLimit = 5
        }
        else {
            Config.Camera.imageLimit = 1
        }
        
        gallery = GalleryController()
        gallery.delegate = self
        gallery.modalPresentationStyle = .fullScreen
        present(gallery, animated: true, completion: nil)
    }
}


extension AddProfileData: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        self.isTextFieldModified = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.tag == 2 {
            
            //range.length will be greater than 0 if user is deleting text - allow it to replace
            if range.length > 0
            {
                return true
            }
            
            //Don't allow empty strings
            if string == " "
            {
                return false
            }
            
            //Check for max length including the spacers we added
            if range.location == 19
            {
                return false
            }
            
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: "-", with: "")
            
            //Verify entered text is a numeric value
            let digits = NSCharacterSet.decimalDigits
            for char in replacementText.unicodeScalars
            {
                if !digits.contains(char)
                {
                    return false
                }
            }
            
            //Put an empty space after every 4 places
            if range.location == 4 || range.location == 9 || range.location == 14
            {
                originalText?.append("-")
                textField.text = originalText
            }
            return true
        }
        else {
            return !string.isSingleEmoji
        }
    }
}

extension AddProfileData: CustomAlertViewDelegate {
    func cancelButtonTapped() {
        self.customAlert!.dismiss(animated: true, completion: nil)
    }
    
    func okButtonTapped(count: String) {
        
        let urlString = UserProfileData.imageUrl?.absoluteString ?? "profile"
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        APIManager.sharedInstance.deleteUserPhoto(url: urlString, onSuccess: { (data) in
            
            guard  let status = data["status"]?.number, status == 1  else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["data"]?.string
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.5, animated: true)
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = nil
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            hud.dismiss(afterDelay: 1.0, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                
                self.imgProfile.image = UIImage(named:"profile")
                self.imgProfile.contentMode = .center
                UserProfileData.image = nil
                
                
                self.userImageLoc.isHidden = false
                self.customAlert!.dismiss(animated: true, completion: nil)
                ApiConfigurationData.appImages.removeObject(forKey: urlString as AnyObject)
            })
            
            
        }, viewController: self)
    }
}

extension AddProfileData: GalleryControllerDelegate {
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        
        DispatchQueue.main.async {
            Image.resolve(images: images, completion: { [weak self] resolvedImages in
                let images = resolvedImages.compactMap({ $0 })
                
                if self!.isDocument {
                    
                    self!.btnChooseImageDocument.showLoading()
                    APIManager.sharedInstance.saveUserDocument(data: images, onSuccess: { (data) in
                        self!.btnChooseImageDocument.hideLoading()
                        
                        if let _ = data["url"]?.rawString()  {
                            self!.docLoc.text = "profile_docAdded".localized()
                            self!.btnChooseImageDocument.setImage(UIImage(named: "success_add_document"), for: .normal)
                            self!.btnChooseImageDocument.isUserInteractionEnabled = false
                        }
                    }, viewController: self!)
                }
                else {
                    self!.imgProfile.isSkeletonable = true
                    self!.imgProfile.showAnimatedGradientSkeleton()
                    self!.userImageLoc.isHidden = true
                    
                    APIManager.sharedInstance.saveUserPhoto(data: images[0], onSuccess:  { (data) in
                        if let url =  data["url"]?.rawString() {
                            self!.imgProfile.image = images[0]
                            self!.imgProfile.isHidden = false
                            self!.imgProfile.contentMode = .scaleAspectFill
                            UserProfileData.imageUrl = URL(string: url)
                            ApiConfigurationData.appImages.setObject(images[0], forKey: (UserProfileData.imageUrl?.absoluteString ?? "profile") as AnyObject)
                            
                            UserProfileData.image = images[0]
                        }
                        else {
                            self!.userImageLoc.isHidden = true
                        }
                        
                        self!.imgProfile.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                    }, viewController: self!)
                }
            })
        }
        
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {}
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        LightboxConfig.DeleteButton.enabled = true
        
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            self?.showLightbox(controller, images: resolvedImages.compactMap({ $0 }))
        })
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func showLightbox(_ controller: GalleryController,images: [UIImage]) {
        guard images.count > 0 else {
            return
        }
        
        let lightboxImages = images.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.modalPresentationStyle = .fullScreen
        lightbox.headerView.deleteButton.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
        gallery.present(lightbox, animated: true)
    }
    
    @objc func deleteImage() {
        var images =  gallery.cart.images
        gallery.cart.remove(images[lightbox.currentPage])
        images.remove(at: lightbox.currentPage)
        gallery.cart.reload(images)
        self.imgProfile.isHidden = true
    }
}

extension AddProfileData:UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.languages.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.languages[row]["name"].string!
        
    }
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        country = Int(truncating: self.languages[row]["id"].number!)
        contryName = self.languages[row]["name"].string!
        isTextFieldModified = true
        
        self.fieldTextLabels.forEach { (textfied) in
            if textfied.tag == 4 {
                textfied.text = self.languages[row]["name"].string!
            }
        }
    }
}


