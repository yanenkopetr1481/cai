//
//  SetLanguage.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 12/1/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import Localize_Swift

class SetLanguage: ViewController {
    
    @IBOutlet weak var pickerView:UIPickerView!
    @IBOutlet weak var pickerViewTitle:UILabel!
    
    var actionSheet: UIAlertController!
    var availableLanguages = ["ru","en","ar"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NetworkStatus.isLaunchUsed = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func setText(){
        pickerViewTitle.text = "LanguagePickerTitle".localized()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPageData()
        setText()
    }
    
    func setupPageData() {
        Localize.setCurrentLanguage("en")
        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func doResetLanguage(_ sender: UIButton) {
        UserDefaults.standard.set(availableLanguages[sender.tag], forKey: "AppLanguageLocal")
        Localize.setCurrentLanguage(availableLanguages[sender.tag])
        UserDefaults.standard.set([availableLanguages[sender.tag]], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        UIView.appearance().semanticContentAttribute = sender.tag == 2 ? .forceRightToLeft : .forceLeftToRight

        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "first") as? Login
            self.navigationController?.pushViewController(vc!, animated: true)
//        }
    }
}
