//
//  DealPreview.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/25/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import Lightbox
import JGProgressHUD
import Alamofire
import SwiftyJSON
import FlexibleSteppedProgressBar
import NotificationBannerSwift
import LGButton
import RxSwift

class DealPreview: RootViewController {
    
    @IBOutlet weak var libraryCount: UILabel!
    @IBOutlet weak var libraryStack: UIStackView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var productTitle:UILabel!
    @IBOutlet weak var productWeight:UILabel!
    @IBOutlet weak var productSumByGram:UILabel!
    @IBOutlet weak var productSum:UILabel!
    @IBOutlet weak var productSeller:UILabel!
    
    @IBOutlet weak var productSellerLoc:UILabel!
    @IBOutlet weak var productWeightLoc:UILabel!
    @IBOutlet weak var productSumByGramLoc:UILabel!
    @IBOutlet weak var productSumLoc:UILabel!
    @IBOutlet weak var commisionLabel:UILabel!
    @IBOutlet weak var commisionData:UILabel!
    @IBOutlet weak var customerData:UILabel!
    @IBOutlet weak var customerLabel:UILabel!
    @IBOutlet weak var loadingBtn:LoadingButton!
    @IBOutlet weak var dealIdTitle:UILabel!
    @IBOutlet weak var dealId:UILabel!
    
    @IBOutlet weak var anulationReasonTitle:UILabel!
    @IBOutlet weak var anulationReason:UILabel!
    
    @IBOutlet weak var actionBtn:UIButton!
    @IBOutlet weak var payBtn:UIButton!
    @IBOutlet weak var acceptBtn:UIButton!
    @IBOutlet weak var declineBtn:UIButton!
    
    @IBOutlet weak var shippingStatusTableView:UITableView!
    @IBOutlet weak var backBtn:UIButton!
    @IBOutlet weak var progressBarView: UIView!
    @IBOutlet weak var progressBarShippingTitle: UILabel!
    
    @IBOutlet weak var commisionStack: UIStackView!
    @IBOutlet var checkBox: CheckBox!
    @IBOutlet weak var productAcceptComissionLoc:UILabel!
    @IBOutlet weak var serviceTitle:UILabel!
    @IBOutlet var stackView:UIStackView!
    @IBOutlet var stackViewMain:UIView!
    
    @IBOutlet weak var categoryLabelTitle:UILabel!
    @IBOutlet weak var categoryLabelData:UILabel!
    @IBOutlet weak var categoryStackView:UIStackView!
    
    var library:[UIImage] = [UIImage]()
    var lightbox :LightboxController!
    var lightboxShipping :LightboxController!
    
    @IBOutlet weak var statusStackView:UIStackView!
    @IBOutlet var statusesImageView:[UIImageView]!
    @IBOutlet var statusesSerpantin:[UILabel]!
    
    var productData:DealData?
    var customer:SellerData?
    
    var isNewProductPage = false
    var isReplenishPage = false
    var isAnnulatePage = false
    var isComissionSelected = false
    var isAnnulateProduct = false
    var isTermsChanging = false
    var acceptDeal = false
    var disposedBag = DisposeBag()
    
    var commision_sum:Double = 0
    var shipingData = [JSON]()
    var shipingImages = [Int:[UIImage]]()
    
    var product:StoreProductData?
    
    var shipingStatuses = ["awaiting", "in office", "sent to second office", "in second office", "sent to customer", "received by customer"  , "returned to seller"]
    
    var servicePrice = 0.0
    var selectedServices = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTransparentNavigationBar()
        setupLocalization()
        setupViewConfiguration()
    }
    override func viewDidAppear(_ animated: Bool) {
//        let adressAlert = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addressAlert") as! AddressAlert
//
//        adressAlert.id = Int(self.productData!.id)
//        adressAlert.address = UserProfileData.address ?? ""
//        adressAlert.delegate = self
//        adressAlert.providesPresentationContextTransitionStyle = true
//        adressAlert.definesPresentationContext = true
//        adressAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        adressAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//        self.present(adressAlert, animated: true, completion: nil)
    }
    
    func configureDeliveryStatusBar() {
        progressBarShippingTitle.isHidden = false
        progressBarView.isHidden = false
        
        let progressBar = FlexibleSteppedProgressBar()
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        self.progressBarView.addSubview(progressBar)
        
        let horizontalConstraint = progressBar.centerXAnchor.constraint(equalTo: self.progressBarView.centerXAnchor)
        let verticalConstraint = progressBar.topAnchor.constraint(equalTo: self.progressBarView.topAnchor)
        
        let widthConstraint = progressBar.widthAnchor.constraint(equalToConstant: self.progressBarView.frame.width - 30)
        let heightConstraint = progressBar.heightAnchor.constraint(equalToConstant: self.progressBarView.frame.height - 30)
        
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
        var count = shipingData.count + 1
        var index = count - 2
        if shipingData.last?.dictionary?["type"]?.string?.replacingOccurrences(of: " ", with: "_").uppercased() == "RECEIVED_BY_CUSTOMER" || shipingData.last?.dictionary?["type"]?.string?.replacingOccurrences(of: " ", with: "_").uppercased() == "RETURNED_TO_SELLER" || productData?.otherStatus == "COMPLETED" || productData?.otherStatus == "ANULATED" {
            count -= 1
            index = count - 1
        }
        
        progressBar.numberOfPoints = count
        progressBar.lineHeight = 2
        progressBar.radius = 3
        progressBar.progressRadius = 5
        progressBar.progressLineHeight = 2
        progressBar.selectedBackgoundColor = UIColor.init(hex: "#2b8a3e")
        progressBar.selectedOuterCircleStrokeColor = UIColor.init(hex: "#2b8a3e")
        progressBar.lastStateCenterColor = UIColor.init(hex: "#2b8a3e")
        progressBar.backgroundShapeColor = .darkGray
        progressBar.currentIndex = index
        progressBar.delegate = self
        
        progressBarShippingTitle.text = shipingData.last?.dictionary?["type"]?.string?.replacingOccurrences(of: " ", with: "_").uppercased().localized()
    }
    
    func setupLocalization() {
        productWeightLoc.text = "deal_weight".localized()
        productSumByGramLoc.text = "deal_sum_gram".localized()
        productSumLoc.text = "deal_sum".localized()
        productSellerLoc.text = "deal_seller".localized()
        commisionLabel.text = "deal_product_total_sum".localized()
        customerLabel.text = "deal_product_customer".localized()
        payBtn.setTitle("AnnullatePayment".localized(), for: .normal)
        acceptBtn.setTitle(productData?.type == .purchase ?  "deal_accept_and_pay".localized() : "Accept".localized(), for: .normal)
        declineBtn.setTitle("Decline".localized(), for: .normal)
        dealIdTitle.text = "deal_product_id".localized()
        anulationReasonTitle.text = "deal_anulate_reason".localized()
        productAcceptComissionLoc.text = "deal_accept_comission".localized()
        categoryLabelTitle.text = "deal_category".localized()
    }
    
    @IBAction func addPaymentConfirmation(_ sender:UIButton) {
        
        self.presentAlertView(title: "Accept".localized())
        
        isNewProductPage = false
        isReplenishPage = false
        isAnnulatePage = false
        isTermsChanging = false
    }
    
    @objc func checkboxValueChanged(sender: CheckBox) {
        isComissionSelected = !isComissionSelected
        actionBtn.isEnabled = isComissionSelected
        actionBtn.alpha = isComissionSelected ? 1 : 0.7
        checkBox.isChecked = isComissionSelected
    }
    
    func setupServices(data:Array<JSON>, isPreview:Bool = true) {
        
        self.stackView.removeArrangedSubviews()
        self.stackView.removeSubviews()
        self.stackView.spacing = 5
        serviceTitle.text = isPreview ? "deal_service_preview".localized() : "deal_service".localized()
        
        data.forEach({ (service) in
            
            let stack = UIStackView()
            stack.axis = .horizontal
            
            let lgBtn = LGButton()
            lgBtn.titleFontSize = 20
            lgBtn.titleFontName = "Helvetica Neue Light"
            lgBtn.spacingLeading = 0
            lgBtn.titleNumOfLines = 10
            lgBtn.bgColor = .clear
            lgBtn.titleString = "\(service["title"].string!)"
            lgBtn.titleColor = .black
            lgBtn.layer.cornerRadius = 5
            lgBtn.leftImageSrc = !isPreview ? UIImage(named: "check_unfill") : UIImage(named: "check_fill")
            
            lgBtn.leftAligned = true
            lgBtn.tag = service["id"].int!
            
            if !isPreview {
                lgBtn.rx.controlEvent(.touchUpInside).bind { (_) in
                    
                    lgBtn.leftImageSrc = lgBtn.isSelected ? UIImage(named: "check_unfill") : UIImage(named: "check_fill")
                    lgBtn.isSelected = !lgBtn.isSelected
                    
                    if lgBtn.isSelected {
                        self.servicePrice += service["price"].double!
                        self.selectedServices.append(service["id"].int!)
                    }
                    else {
                        self.selectedServices.removeAll { (id) -> Bool in
                            return id == service["id"].int!
                        }
                        
                        self.servicePrice -= service["price"].double!
                    }
                    
                    if (self.servicePrice > 0) {
                        self.actionBtn.setTitle("Buy".localized() + " (+\(self.servicePrice)$)", for: .normal)
                    }
                    else {
                        self.selectedServices = [Int]()
                        self.servicePrice = 0
                        self.actionBtn.setTitle("Buy".localized(), for: .normal)
                    }
                    
                    
                }.disposed(by: self.disposedBag)
            }
            
            stack.addArrangedSubview(lgBtn)
            
            let price = UILabel()
            price.text = "\(service["price"].double!)$"
            price.textColor = .black
            price.backgroundColor = .clear
            price.font = UIFont(name: "Helvetica Neue Light", size: 20.0)
            price.widthAnchor.constraint(equalToConstant: 100).isActive = true
            price.textAlignment = .right
            
            stack.addArrangedSubview(price)
            
            self.stackView.addArrangedSubview(stack)
            stackViewMain.isHidden = false
        })
    }
    
    func setupViewConfiguration() {
        
        statusesSerpantin.forEach { (label) in
            label.textColor = UIColor.init(hex: "#d6d6d6")
        }
        
        stackViewMain.isHidden = true
        progressBarShippingTitle.isHidden = true
        progressBarView.isHidden = true
        categoryStackView.isHidden = true
        statusStackView.isHidden = true
        
        checkBox.style = .tick
        checkBox.borderStyle = .square
        checkBox.addTarget(self, action: #selector(checkboxValueChanged(sender:)), for: .valueChanged)
        
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(checkboxValueChanged))
        self.productAcceptComissionLoc.isUserInteractionEnabled = true
        self.productAcceptComissionLoc.addGestureRecognizer(labelTap)
        commisionStack.isHidden = true
        
        shippingStatusTableView.isHidden = true
        shippingStatusTableView.delegate = self
        shippingStatusTableView.dataSource = self
        shippingStatusTableView.estimatedRowHeight = 170
        shippingStatusTableView.register(UINib(nibName: "ShippingStatusCell", bundle: nil), forCellReuseIdentifier: "shipping_status_cell")
        
        if let shiping_data = productData?.shipingData, !shiping_data.isEmpty {
            shippingStatusTableView.isHidden = false
            shipingData = shiping_data
            shippingStatusTableView.reloadData()
            //            configureDeliveryStatusBar()
            
            var index = 0
            self.statusesImageView.forEach({ (imageView) in
                imageView.image = UIImage(named: "status_\(index)")
                imageView.cornerRadius = imageView.width / 2
                
                index = index + 1
            })
            var statuses = [String]()
            
            shipingData.forEach { (shippingData) in
                statuses.append((shippingData.dictionary?["type"]?.string?.replacingOccurrences(of: " ", with: "_"))!)
            }
            
            index = 0
            
            var count = 0
            
            let statusForAppearStatus = ["in_office", "sent_to_second_office", "in_seconf_office", "sent_to_customer"]
            
            statusForAppearStatus.forEach { (status) in
                if statuses.contains(status) {
                    count = count + 1
                }
                
                index += 1
            }
            
            for i in 0..<count {
                statusesImageView[i].cornerRadius = 10
                if (i == count - 1) {
                    statusesImageView[i].image = UIImage(named: "status_\(i)_filled")
                }
                
                
                if i > 0 && i <= statusesSerpantin.count {
                    let serpantinIndex = i - 1
                    statusesSerpantin[serpantinIndex].textColor = UIColor(hex: "#D32F2F")
                }
            }
            
            self.statusStackView.isHidden = count > 0 ? false : true
        }
        
        guard productData != nil else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        if let id = productData?.id, id != 0 {
            dealIdTitle.isHidden = false
            dealId.isHidden = false
            dealId.text = "\(id)"
            
        }
        else {
            dealIdTitle.isHidden = true
            dealId.isHidden = true
        }
        
        commisionData.isHidden = true
        commisionLabel.isHidden = true
        customerData.isHidden = true
        customerLabel.isHidden = true
        anulationReasonTitle.isHidden = true
        anulationReason.isHidden = true
        
        productTitle.text = productData!.productTitle
        productWeight.text = productData!.productWeight + "g"
        productSumByGram.text = productData!.productGramSum + " $"
        productSum.text = productData!.productSum + " $"
        productSeller.text = productData!.productSeller
        
        acceptBtn.isHidden = true
        declineBtn.isHidden = true
        
        if customer != nil {
            customerData.isHidden = false
            customerLabel.isHidden = false
            customerData.text = customer?.name ?? customer?.nickName ?? customer?.phone
        }
        else if !(productData?.productCustomer.isEmpty)! {
            customerData.isHidden = false
            customerLabel.isHidden = false
            customerData.text = productData?.productCustomer
        }
        
        if (productData!.type != .new) {
            
            let weight = Double(productData!.productWeight.replacingOccurrences(of: "g", with: ""))
            let sum = Double(productData!.productSum)
            let commissionForGramm = Double(productData!.commissionForGramm) == 0 ? UserProfileData.commissionForGramm : Double(productData!.commissionForGramm)
            let commissionForSum = Double(productData!.commissionForSum) == 0 ? UserProfileData.commissionForSum : Double(productData!.commissionForSum)
            
            commision_sum = (weight! * commissionForGramm!  +  sum! * commissionForSum!)
            commisionLabel.text = "deal_product_commision".localized()
            
            switch productData!.type {
            case .new,.exist:
                actionBtn.setTitle("deal_save_and_sent".localized(), for: .normal)
            case .preview:
                
                if productData!.commision_included {
                    commisionData.isHidden = false
                    commisionLabel.isHidden = false
                    commisionData.text = String(format: "%.2f", commision_sum) + " $"
                }
                else {
                    commisionData.isHidden = false
                    commisionLabel.isHidden = false
                    commisionData.text = String(format: "%.2f", 0) + " $"
                }
                
                if !(productData?.anulateData.isSellerAnulate)! && !(productData?.anulateData.isCustomerAnulate)!{
                    if (!productData!.sellerAccept && productData!.customerAccept) {
                        acceptBtn.isHidden = false
                        declineBtn.isHidden = false
                    }
                    
                    actionBtn.setTitle("Annullate".localized(), for: .normal)
                }
                else if (productData?.anulateData.isCustomerAnulate)! && !(productData?.anulateData.isSellerAnulate)! {
                    acceptBtn.setTitle("Annullate".localized(), for: .normal)
                    acceptBtn.isHidden = false
                    acceptBtn.tag = 2
                    declineBtn.isHidden = false
                    declineBtn.tag = 3
                    actionBtn.isHidden = true
                }
                else if !(productData?.anulateData.isCustomerAnulate)! && (productData?.anulateData.isSellerAnulate)! {
                    payBtn.isHidden = true
                    acceptBtn.isHidden = true
                    declineBtn.isHidden = true
                    actionBtn.isHidden = true
                }
                else {
                    payBtn.isHidden = true
                    acceptBtn.isHidden = true
                    declineBtn.isHidden = true
                    actionBtn.isHidden = true
                }
                
                if (productData?.productCustomer != "") {
                    customerData.isHidden = false
                    customerLabel.isHidden = false
                    customerData.text = productData?.productCustomer
                }
                
                if (productData?.services != nil && !(productData?.services!.isEmpty)!) {
                    self.setupServices(data: (productData?.services)!)
                }
                
            case .purchase:
                
                if productData!.commision_included {
                    commisionData.isHidden = false
                    commisionLabel.isHidden = false
                    commisionData.text = String(format: "%.2f", 0) + " $"
                }
                else {
                    commisionData.isHidden = false
                    commisionLabel.isHidden = false
                    commisionData.text = String(format: "%.2f", commision_sum) + " $"
                }
                
                if !(productData?.anulateData.isSellerAnulate)! && !(productData?.anulateData.isCustomerAnulate)!{
                    acceptBtn.isHidden = productData!.customerAccept
                    declineBtn.isHidden = productData!.customerAccept
                    
                    if productData!.sellerAccept && productData!.customerAccept {
                        payBtn.isHidden = productData!.productPayStatus == "UNPAID" ? false : true
                        actionBtn.setTitle("Annullate".localized(), for: .normal)
                    }
                    else {
                        actionBtn.isHidden = true
                    }
                }
                else if (productData?.anulateData.isSellerAnulate)! && !(productData?.anulateData.isCustomerAnulate)! {
                    acceptBtn.setTitle("Annullate".localized(), for: .normal)
                    acceptBtn.isHidden = false
                    acceptBtn.tag = 2
                    declineBtn.isHidden = false
                    declineBtn.tag = 3
                    actionBtn.isHidden = true
                }
                else {
                    acceptBtn.isHidden = true
                    declineBtn.isHidden = true
                    actionBtn.isHidden = true
                }
                
                if (productData?.services != nil && !(productData?.services!.isEmpty)!) {
                    self.setupServices(data: (productData?.services)!)
                }
            case .other:
                actionBtn.setTitle("Buy".localized(), for: .normal)
                
                if productData!.commision_included {
                    commisionData.isHidden = false
                    commisionLabel.isHidden = false
                    commisionData.text = String(format: "%.2f", 0) + " $"
                }
                else {
                    
                    actionBtn.isEnabled = isComissionSelected
                    actionBtn.alpha = isComissionSelected ? 1 : 0.7
                    checkBox.isChecked = isComissionSelected
                    
                    commisionData.isHidden = false
                    commisionLabel.isHidden = false
                    commisionData.text = String(format: "%.2f", commision_sum) + " $"
                    commisionStack.isHidden = false
                }
                
                if DealServices.service != nil && !DealServices.service!.isEmpty {
                    self.setupServices(data: DealServices.service!, isPreview: false)
                }
            }
            
            if productData?.shipingData.count > 1 {
                payBtn.isHidden = true
                acceptBtn.isHidden = true
                declineBtn.isHidden = true
                actionBtn.isHidden = true
            }
            
            productData!.productImagesURLs.forEach { urlJson in
                
                self.loadingBtn.showLoading()
                
                let url = URL(string: urlJson.string!)!
                
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    self.library.append(image)
                    self.loadingBtn.hideLoading()
                    
                    if (self.library.count > 0) {
                        self.imgProfile.image = self.library.first
                        self.libraryStack.isHidden = false
                        self.libraryCount.text = "\(self.library.count)+"
                    }
                }
                else {
                    AF.request(url).response {
                        result in
                        DispatchQueue.main.async {
                            if let data = result.data {
                                let image = UIImage(data: data)!
                                self.library.append(image)
                                ApiConfigurationData.appImages.setObject(image, forKey: url.absoluteString as AnyObject)
                                self.loadingBtn.hideLoading()
                                if (self.library.count > 0) {
                                    self.imgProfile.image = self.library.first
                                    self.libraryStack.isHidden = false
                                    self.libraryCount.text = "\(self.library.count)+"
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            actionBtn.setTitle("deal_save_and_sent".localized(), for: .normal)
            
            if commision_sum != 0 {
                commisionData.isHidden = false
                commisionLabel.isHidden = false
                commisionData.text = String(format: "%.2f", commision_sum) + " $"
            }
            
            library = productData?.productImages ?? []
            
            if (library.count > 0) {
                imgProfile.image = library.last
                libraryStack.isHidden = false
                libraryCount.text = "\(library.count)+"
            }
        }
        
        if ((productData?.termsDaata) != nil  && productData?.termsDaata?.needsToAcceptNewTerms == true) {
            let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
            customAlert.providesPresentationContextTransitionStyle = true
            customAlert.definesPresentationContext = true
            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            customAlert.delegate = self
            
            customAlert.config = AlertCustomData(title: "deal_changed_data".localized(), firstSegment: "Accept".localized(), secondSegment: "Decline".localized(), isTersData: true, termsData: productData?.termsDaata?.newTerms, isCloseByTap: true)
            
            self.present(customAlert, animated: true, completion: nil)
            isTermsChanging = true
        }
        
        if productData?.anulateData.anulateReson != nil && !(productData?.anulateData.anulateReson.isEmpty)! {
            anulationReasonTitle.isHidden = false
            anulationReason.isHidden = false
            
            anulationReason.text = productData?.anulateData.anulateReson
        }
        
        if productData?.category != nil && !(productData?.category?.array?.isEmpty ?? true)  {
            categoryStackView.isHidden = false
            categoryLabelData.text = productData?.category?["title"].string
        }
    }
    
    @IBAction func previus(_ sender:Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setTransparentNavigationBar() {
        backBtn.isHidden = true
        if (self.navigationController != nil) {
            self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController!.navigationBar.shadowImage = UIImage()
            self.navigationController!.navigationBar.isTranslucent = true
            
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(back(_:)))
            swipeLeft.direction = .right
            self.view.addGestureRecognizer(swipeLeft)
        }
        else {
            backBtn.isHidden = false
            
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(previus(_:)))
            swipeLeft.direction = .right
            self.view.addGestureRecognizer(swipeLeft)
        }
        
    }
    
    @IBAction func back(_ sender:Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func acceptOrDeclineDeal(_ sender:UIButton) {
        
        var accept = true
        
        switch sender.tag {
        case 0:
            accept = true
        case 1:
            accept = false
        default:
            break
        }
        
        if sender.tag == 2 || sender.tag == 3 {
            let btn = UIButton()
            btn.tag = sender.tag
            self.saveDeal(btn)
        }
        else {
            self.acceptDeal = accept
            isNewProductPage = true
            isReplenishPage = false
            isAnnulatePage = false
            isTermsChanging = false
            self.presentAlertView(title: accept ? "Accept".localized() : "deal_decline".localized())
        }
    }
    
    @IBAction func showLibrary(_ sender: UIButton) {
        guard library.count > 0 else {
            return
        }
        
        let lightboxImages = library.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.footerView.isHidden = true
        lightbox.modalPresentationStyle = .fullScreen
        self.present(lightbox, animated: true)
    }
    
    @IBAction func saveDeal(_ sender:UIButton) {
        
        var data = [
            "description" : "Some description",
            "price" : productData?.productSum.replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil),
            "title" : productData?.productTitle,
            "weight" : productData?.productWeight,
            "includeCommission": "\(isComissionSelected)"
        ] as! [String:String]
        
        if (customer != nil) {
            data["customerIdentifier"] = customer?.phone ?? customer?.nickName
        }
        
        if let id = productData?.category?["id"], !id.isEmpty {
            data["categoryId"] = "\(id.int!)"
        }
        
        let hud = JGProgressHUD(style: .dark)
        
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        
        switch productData?.type {
        case .exist:
            break
        case .new:
            hud.show(in: self.view)
            
            if product != nil {
                
                var newData = [
                    "includeCommission": "\(isComissionSelected)",
                    "productId": self.product!.id
                ] as [String:Any]
                
                if (customer != nil) {
                    newData["customerId"] = customer!.id
                }
                
                APIManager.sharedInstance.saveDealWithExistProduct(data: newData, onSuccess: { (data) in
                    
                    guard let status = data["status"]?.number, status == 1, let responseData = data["data"]?.dictionary, let _ = responseData["id"]?.number  else {
                        UIView.animate(withDuration: 0.1, animations: {
                            hud.textLabel.text = "Error".localized()
                            hud.detailTextLabel.text = data["url"]?.dictionary?["data"]?.string ?? ""
                            hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        })
                        
                        hud.dismiss(afterDelay: 1.5, animated: true)
                        
                        return
                    }
                    
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Success".localized()
                        hud.detailTextLabel.text = nil
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }, viewController: self)
            }
            else {
                APIManager.sharedInstance.saveDealwithNewProduct(data: data, images: library, onSuccess: { (data) in
                    
                    guard let response = data["url"]?.dictionary, let status = response["status"]?.number, status == 1, let responseData = response["data"]?.dictionary, let _ = responseData["id"]?.number  else {
                        UIView.animate(withDuration: 0.1, animations: {
                            hud.textLabel.text = "Error".localized()
                            hud.detailTextLabel.text = data["url"]?.dictionary?["data"]?.string ?? ""
                            hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        })
                        
                        hud.dismiss(afterDelay: 1.5, animated: true)
                        
                        return
                    }
                    
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Success".localized()
                        hud.detailTextLabel.text = nil
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }, viewController: self)
            }            
            break
        case .none:
            break
        case .other:
            isNewProductPage = true
            isReplenishPage = false
            isAnnulatePage = false
            isTermsChanging = false
            acceptDeal = true
            
            self.presentAlertView(title: "Confirm_payment".localized())
        case .preview, .purchase:
            
            var anulate = true
            
            switch sender.tag {
            case 2:
                anulate = true
            case 3:
                anulate = false
            default:
                break
            }
            
            isNewProductPage = false
            isReplenishPage = false
            isAnnulatePage = true
            isTermsChanging = false
            
            self.isAnnulateProduct = anulate
            self.presentAlertView(title: anulate ? "deal_set_annulate".localized() : "deal_decline_annulate".localized())
        }
    }
    
    func presentAlertView(title:String, withPrice:Bool = false) {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        
        var amount = Double(UserProfileData.wallet?.balance ?? 0) + Double(UserProfileData.wallet?.creditLimit ?? 0) - Double(UserProfileData.wallet?.debt ?? 0) - Double(self.productData!.productSum)!
        
        if (!productData!.commision_included) {
            amount -= commision_sum
        }
        
        amount = Double(String(format: "%.2f", abs(amount)))!
        
        var title = title
        if (withPrice) {
            title += " ( > \(amount)$ )"
        }
        
        customAlert.config = AlertCustomData(title: title, firstSegment: "Yes".localized(), secondSegment: "No".localized(), amount: 10)
        self.present(customAlert, animated: true, completion: nil)
    }
}

extension DealPreview: CustomAlertViewDelegate {
    
    func okButtonTapped(count: String) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        
        if isNewProductPage {
            
            hud.show(in: self.view)
            
            APIManager.sharedInstance.buyDeal(id: productData!.id, additionalServices: self.selectedServices, accept: self.acceptDeal, onSuccess: { (data) in
                
                guard let status = data["status"]?.number, status == 1 else {
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Error".localized()
                        hud.detailTextLabel.text = data["data"]?.string
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        
                        hud.dismiss(afterDelay: 1.5, animated: true)
                        
                        if data["status"]?.number == 50 {
                            self.presentAlertView(title: "payment_replenishBalance".localized(), withPrice: true)
                            self.isNewProductPage = false
                            self.isReplenishPage = true
                            self.isAnnulatePage = false
                            self.isTermsChanging = false
                        }
                    })
                    
                    return
                }
                
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Success".localized()
                    hud.detailTextLabel.text = data["data"]?.string
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 0.1, animated: true)
                
                
                let alert = UIAlertController(title: "Delivery address", message: "Select delivery address", preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: UserProfileData.address ?? "Profile address", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Set other address", style: .default, handler: {
                    _ in
                    
                    let adressAlert = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addressAlert") as! AddressAlert
                    
                    adressAlert.id = Int(self.productData!.id)
                    adressAlert.address = UserProfileData.address ?? ""
                    adressAlert.delegate = self
                    adressAlert.providesPresentationContextTransitionStyle = true
                    adressAlert.definesPresentationContext = true
                    adressAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    adressAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    self.present(adressAlert, animated: true, completion: nil)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }, viewController: self)
        }
        else if isAnnulatePage {
            hud.show(in: self.view)
            
            APIManager.sharedInstance.anulateDeal(id: productData!.id, reason: "?", anulate: isAnnulateProduct, onSuccess: { (data) in
                guard let status = data["status"]?.number, status == 1 else {
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Error".localized()
                        hud.detailTextLabel.text = data["data"]?.string
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    
                    return
                }
                
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Success".localized()
                    hud.detailTextLabel.text = data["data"]?.string
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }, viewController: self)
        }
        else if isReplenishPage {
            DispatchQueue.main.async {
                let newView = self.storyboard?.instantiateViewController(withIdentifier: "selectReplenishType_vc") as! SelectReplenishType
                var amount = Double(UserProfileData.wallet?.balance ?? 0) + Double(UserProfileData.wallet?.creditLimit ?? 0) - Double(UserProfileData.wallet?.debt ?? 0) - Double(self.productData!.productSum)!
                
                if (!self.productData!.commision_included) {
                    amount -= self.commision_sum
                }
                
                amount = Double(String(format: "%.2f", abs(amount)))!
                
                newView.amount = amount
                newView.modalPresentationStyle = UIModalPresentationStyle.popover
                self.navigationController?.pushViewController(newView, animated: true)
            }
        }
        else if isTermsChanging {
            hud.textLabel.text = "Sending".localized()
            hud.vibrancyEnabled = true
            hud.show(in: self.view)
            
            APIManager.sharedInstance.termsAccept(id: Int(truncating: productData!.id), type: true, completionHandler: { (data) in
                guard let status = data["status"]?.number, status == 1 else {
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Error".localized()
                        hud.detailTextLabel.text = data["data"]?.string ?? ""
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 2.0, animated: true)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    
                    return
                }
                
                UIView.animate(withDuration: 0.1, animations: {
                    self.acceptBtn.isHidden = true
                    self.declineBtn.isHidden = true
                    hud.textLabel.text = "Success".localized()
                    hud.detailTextLabel.text = nil
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }, viewController: self)
        }
        else {
            hud.textLabel.text = "Sending".localized()
            hud.vibrancyEnabled = true
            hud.show(in: self.view)
            APIManager.sharedInstance.payForpurchase(id: productData!.id, onSuccess: { (data) in
                guard let status = data["status"]?.number, status == 1 else {
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Error".localized()
                        hud.detailTextLabel.text = data["data"]?.string ?? ""
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 2.0, animated: true)
                    
                    return
                }
                
                UIView.animate(withDuration: 0.1, animations: {
                    self.acceptBtn.isHidden = true
                    self.declineBtn.isHidden = true
                    hud.textLabel.text = "Success".localized()
                    hud.detailTextLabel.text = nil
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }, viewController: self)
        }
    }
    
    func cancelButtonTapped() {
        if isTermsChanging {
            let hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Sending".localized()
            hud.vibrancyEnabled = true
            hud.show(in: self.view)
            
            APIManager.sharedInstance.termsAccept(id: Int(truncating: productData!.id), type: false, completionHandler: { (data) in
                guard let status = data["status"]?.number, status == 1 else {
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Error".localized()
                        hud.detailTextLabel.text = data["data"]?.string ?? ""
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 2.0, animated: true)
                    return
                }
                
                UIView.animate(withDuration: 0.1, animations: {
                    self.acceptBtn.isHidden = true
                    self.declineBtn.isHidden = true
                    hud.textLabel.text = "Success".localized()
                    hud.detailTextLabel.text = nil
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }, viewController: self)
        }
    }
}


extension DealPreview:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shipingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shipping_status_cell", for: indexPath) as! ShippingStatusCell
        
        let created = shipingData[indexPath.row].dictionary?["createdAt"]?.number! ?? 0
        let delivery = shipingData[indexPath.row].dictionary?["deliveryDate"]?.number ?? 0
        let weight = shipingData[indexPath.row].dictionary?["currentWeight"]?.number ?? 0
        
        let price = shipingData[indexPath.row].dictionary?["currentPrice"]?.number ?? 0
        cell.setData(_type: (shipingData[indexPath.row].dictionary?["type"]?.string)!, _del_time: Double(truncating: delivery), _creat: Double(truncating: created), _description: (shipingData[indexPath.row].dictionary?["description"]?.string)!, _price: Double(truncating: price), _weight: Double(truncating: weight))
        cell.s_imageBtn.addTarget(self, action: #selector(showStatuses(_:)), for: .touchUpInside)
        cell.s_imageBtn.tag = indexPath.row
        cell.s_imageS.image = UIImage()
        cell.s_imageS.contentMode = .scaleAspectFill
        
        var isFirst = true
        shipingData[indexPath.row].dictionary?["photosUrl"]?.array?.forEach({ (urlJson) in
            
            if let url = URL(string: urlJson.string!) {
                
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    cell.s_imageS.image = image
                    cell.s_imageS.contentMode = .scaleAspectFill
                }
                else {
                    cell.s_imageS.downloadImageFrom(url: url, contentMode: .scaleAspectFill,setNewImage: isFirst)
                    isFirst = false
                }
            }
            else {
                cell.s_imageS.image = UIImage()
                cell.s_imageS.contentMode = .scaleAspectFill
            }
        })
        
        cell.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "deal_shipping_data_title".localized()
    }
    
    @objc func showStatuses(_ sender:UIButton) {
        
        var images = [UIImage]()
        
        shipingData[sender.tag].dictionary?["photosUrl"]?.array?.forEach({ (urlJson) in
            
            if let url = URL(string: urlJson.string!) {
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    images.append(image)
                }
            }
        })
        
        if images.count > 0 {
            let lightboxImages = images.map { (image) -> LightboxImage in
                return LightboxImage(image: image)
            }
            
            if lightboxImages.count > 0 {
                
                let lightboxShipping = LightboxController(images: lightboxImages, startIndex: 0)
                lightboxShipping.footerView.isHidden = true
                lightboxShipping.modalPresentationStyle = .fullScreen
                
                DispatchQueue.main.async {
                    self.present(lightboxShipping, animated: true)
                }
            }
            else {
                let banner = NotificationBanner(title: "Missing".localized(), subtitle: "shipping_photo_empty".localized(), style: .info)
                banner.show()
            }
        }
        else {
            let banner = NotificationBanner(title: "Missing".localized(), subtitle: "shipping_photo_empty".localized(), style: .info)
            banner.show()
        }
    }
}

extension DealPreview:FlexibleSteppedProgressBarDelegate {
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     didSelectItemAtIndex index: Int) {
        print("Index selected!")
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     willSelectItemAtIndex index: Int) {
        print("Index selected!")
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     canSelectItemAtIndex index: Int) -> Bool {
        return false
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
        return ""
    }
}

extension DealPreview:AddressAlertDelegate {
    
    func setAdress(id: Int, address: String, countryID: String, phone: String, comment: String, contactName:String ) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        let data = [
            "id" : id,
            "deliveryAddress": address,
            "countryId": countryID,
            "contactName": contactName,
            "contactPhone":phone,
            "comments": comment
            
        ] as [String : Any]
        
        APIManager.sharedInstance.setAddressDelivery(data: data, completionHandler: { (data) in
            guard let status = data["status"]?.number, status == 1 else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["data"]?.string ?? ""
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                })
                
                hud.dismiss(afterDelay: 2.0, animated: true)
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                self.acceptBtn.isHidden = true
                self.declineBtn.isHidden = true
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = nil
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            hud.dismiss(afterDelay: 1.0, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                self.navigationController?.popToRootViewController(animated: true)
            }
            
        }, viewController: self)
    }
}
