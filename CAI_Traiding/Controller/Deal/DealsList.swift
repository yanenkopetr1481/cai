//
//  Deals.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/30/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import SwiftyJSON
import AARefreshControl
import RxSwift
import RxCocoa
import RxDataSources
import AlamofireImage
import Alamofire
import JGProgressHUD

struct AppLocalData {
    static var Countries:[JSON]?
    static var BadgesCount:[BadgeType:Int] = [.deal:Int(),.payment:Int(),.purchase:Int(), .credit:Int()]
}

enum type {
    case me
    case other
    case archive
}

enum LoadingState {
    case notLoading
    case loading
    case loaded(UIImage)
}

enum BadgeType:String {
    case purchase = "2"
    case deal = "5"
    case payment = "3"
    case credit = "1"
}

class DealsList: RootViewController {
    
    @IBOutlet weak var dealsTable:UITableView!
    @IBOutlet weak var segmentTitle:UISegmentedControl!
    @IBOutlet weak var acceptDeclineView:UIView!
    @IBOutlet weak var filterBtn:UIButton!

    var refreshControl: AARefreshControl!
    var dealsList:[DealData] = [DealData]()
    var archiveDealsList:[DealData] = [DealData]()
    
    let dealsListRX: BehaviorRelay<[DealData]> = BehaviorRelay(value: [])
    
    var productID:[NSNumber] = [NSNumber]()
    var archiveDealsID:[NSNumber] = [NSNumber]()
    
    var isPushPresent:Bool = false
    var dealType:type = .me
    var previewedDealAt:Int = Int()
    var isMeResponseTry:Bool = false
    var isArchiveResponseTry:Bool = false
    var fetchingMore:Bool = false
    
    var dealPage:Int = 0
    var archivePage:Int = 0
    let defaults = UserDefaults.standard
    var accept = Bool()
    var dealID = NSNumber()
    
    var selectedDeal:DealData?
    var isAcceptStep = Bool()
    var isPurchaseDataEditing = false
    var isMultipleSelected = false
    var selectedIndexPath:[IndexPath]?
    var isMultipleSelectedType:Bool = false
    var additionlDeals:[Int] = [Int]()
    var selectedFilters:Filters?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewConfiguration()
        setupLocalization()
        configureRxTable()
        configureFooter()
    }
    
    func configureFooter() {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: dealsTable.bounds.width, height: CGFloat(44))
        self.dealsTable.tableFooterView = spinner
        self.dealsTable.tableFooterView?.isHidden = true
    }
    
    @objc func acceptDeal(_ sender:UIButton) {
        dealID = NSNumber(value: sender.tag)
        accept = true
        isAcceptStep = true
        self.presentAlertView(title: accept ? "Accept".localized() : "deal_decline".localized())
    }
    
    @objc func declineDeal(_ sender:UIButton) {
        dealID = NSNumber(value: sender.tag)
        accept = false
        isAcceptStep = true
        self.presentAlertView(title: accept ? "deal_decline".localized() : "deal_decline".localized())
    }
    
    func presentAlertView(title:String) {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        
        customAlert.config = AlertCustomData(title: title, firstSegment: "Yes".localized(), secondSegment: "No".localized(), amount: 1)
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func acceptOrDecline(id:NSNumber, accept:Bool, isUpdatePage:Bool = true) {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        APIManager.sharedInstance.buyDeal(id: id, additionalServices: self.additionlDeals, accept: accept, onSuccess: { (data) in
            
            guard let status = data["status"]?.number, status == 1 else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["data"]?.string
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    
                    hud.dismiss(afterDelay: 1.5, animated: true)
                })
                
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = data["data"]?.string
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            if isUpdatePage {
                self.getPagaData()
            }
            
            hud.dismiss(afterDelay: 1.0, animated: true)
        }, viewController: self)
    }
    
    func showFilter() {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "FIlterAlertView") as! FIlterAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.style = .myDeals
        customAlert.config = self.selectedFilters
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func configureRxTable() {
        
        dealsTable.allowsMultipleSelectionDuringEditing = true
        dealsTable.delegate = self
        
        dealsListRX.asObservable().bind(to: dealsTable.rx.items(cellIdentifier: "purchases")) { table, model, cell in

            let cell = cell as! PurchasesCell
            cell.awakeFromNib()
            
            cell.payBtn.isHidden = true
            cell.setupViewConfiguration(data: model)
            
            if (model.sellerAccept && model.customerAccept || !model.sellerAccept && !model.customerAccept) {
                cell.selectionStyle = .none
            }
            
            let imageUrl = model.productImagesURLs
            if let urlStr = imageUrl.last?.string, let url = URL(string: urlStr) {
                
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    cell.productImage.image = image
                    cell.productImage.cornerRadius = cell.productImage.width / 2
                }
                else {
                    cell.productImage.downloadImageFrom(url: url, contentMode: .scaleAspectFill)
                }
            }
            else {
                cell.productImage.image = UIImage(named: "empty_product")
            }
            
            cell.payBtn.addTarget(self, action: #selector(self.acceptDeal(_:)), for: .touchUpInside)
            cell.payBtn.tag = Int(truncating: model.id)
            
            cell.declineBtn.addTarget(self, action: #selector(self.declineDeal(_:)), for: .touchUpInside)
            cell.declineBtn.tag = Int(truncating: model.id)
            
            cell.textLabel?.numberOfLines = 0
           
            if #available(iOS 14.0, *) {
                cell.automaticallyUpdatesContentConfiguration = true
            }            
        }.disposed(by: disposeBag)
    }
    
    func concat (lhs: NSNumber, rhs: NSNumber) -> String {
        let value = Double(exactly: rhs.doubleValue / lhs.doubleValue)
        return String(format: "%.2f", value!)
    }
    
    @IBAction func changeDealType(_ sender: UISegmentedControl) {
        
        switch  sender.selectedSegmentIndex{
        case 0:
            self.dealType = .me
            self.dealsListRX.accept(self.dealsList)
        default:
            self.dealType = .archive
            self.dealsListRX.accept(self.archiveDealsList)
        }
        
        self.dealsListRX.accept([])
        
        self.isMeResponseTry = false
        self.isArchiveResponseTry = false
        getPagaData(updateTable: true)
    }
    
    func setupLocalization() {
        segmentTitle.setTitle("deal_first_segment".localized(), forSegmentAt: 0)
        segmentTitle.setTitle("deal_third_product".localized(), forSegmentAt: 1)
    }
    
    private func setTableViewEmptyBackground() {
        UIView.animate(withDuration: 0.5) {
            let view = Bundle.main.loadNibNamed("EmptyTableView", owner: self, options: nil)?.first as! EmptyTableView
            view.titleLabel.text = "EmptyRessult".localized()
            view.refreshBtn.setTitle("Refresh".localized(), for: .normal)
            view.delegate = self
            view.imageView.image = UIImage(named: "empty")
            self.dealsTable.backgroundView = view
        }
    }
    
    func getPagaData(updateTable:Bool = false, beginRefresh: Bool = true) {
        fetchingMore = true
        
        if (beginRefresh) {
            self.refreshControl.beginRefreshing()
        }
        
        self.dealsTable.backgroundView = UIView()
        
        switch self.dealType {
        case .me:
            APIManager.sharedInstance.getUserDeals(active: true, type: .sell, page: self.dealPage, onSuccess: { (data) in
                
                var isChanged = false
                var count = 0
                
                if let response = data["data"]?.array, !response.isEmpty  {
                    count = response.count
                    
                    response.forEach { value in
                        guard let product = value.dictionary?["product"],
                              let title = product["productTitle"].string,
                              let weight = product["weight"].number,
                              let sum = product["price"].number,
                              let seller = product["seller"].dictionary,
                              let sellerName = seller["name"]?.string,
                              let imagesUrl = product["photoUrl"].array,
                              let id = value.dictionary?["id"]?.number,
                              let pay = value.dictionary?["paymentStatus"]?.string,
                              let customerAccept = value.dictionary?["customerAccept"]?.bool,
                              let other = value.dictionary?["dealStatus"]?.string,
                              let sellerAccept = value.dictionary?["sellerAccept"]?.bool,
                              let isPrivate = value.dictionary?["private"]?.bool,
                              let commission_included = value.dictionary?["commissionIncluded"]?.bool
                        else { return }
                        
                        let serviceses = value.dictionary?["additionalServices"]?.array ?? []
                        let commsion_for_sum = value.dictionary?["commissionForSum"]?.number ?? 0
                        let commsion_for_gram = value.dictionary?["commissionForGramm"]?.number ?? 0
                        let customer:String = value.dictionary?["customer"]?.dictionary?["name"]?.string ?? ""
                        let shipingData = value.dictionary?["shippingStatuses"]?.array ?? [JSON]()
                        let created = product["productCreatedAt"].number ?? 0
                        let updated = value.dictionary?["updatedAt"]?.number ?? 0
                        let termsData =  TermsData(needsToAcceptNewTerms: value.dictionary?["needsToAcceptNewTerms"]?.bool, newTerms: value.dictionary?["newTerms"]?.dictionary, customerAcceptNewTerms: value.dictionary?["customerAcceptNewTerms"]?.bool, sellerAcceptNewTerms: value.dictionary?["sellerAcceptNewTerms"]?.bool)
                        
                        let priceForGramm = self.concat(lhs: weight, rhs: sum)
                        let anulateData = DealAnulateData(isSellerAnulate: value.dictionary?["sellerAnnulate"]?.bool ?? false, isCustomerAnulate: value.dictionary?["customerAnnulate"]?.bool ?? false, anulateReson: value.dictionary?["annulationReason"]?.string ?? "")
                        
                        let category = value.dictionary?["productCategory"]
                        
                        if !self.productID.contains(id) {
                            isChanged = true
                            let sum = "\(sum)"
                            
                            let data = DealData(id: id, productTitle: title, productWeight: "\(weight)", productSeller: sellerName, productCustomer: customer, productGramSum: priceForGramm, productSum: sum, productImages: [], productPayStatus: pay, shipingData: shipingData, otherStatus: other, type: .exist, productImagesURLs: imagesUrl, customerAccept: customerAccept, sellerAccept: sellerAccept, anulateData: anulateData, isPrivate: isPrivate, commision_included: commission_included, commissionForGramm: "\(commsion_for_gram)", commissionForSum: "\(commsion_for_sum)",termsDaata: termsData,productCreated: created as? Double,productUpdated: updated as? Double, services: serviceses, category: category)
                            
                            if Int(truncating: data.id) > Int(truncating: self.dealsList.first?.id ?? 0) {
                                self.dealsList.insert(data, at: 0)
                            }
                            else {
                                self.dealsList.append(data)
                            }
                            
                            self.productID.append(id)
                        }
                    }
                    
                    if isChanged {
                        self.dealPage += 1
                    }
                }
                
                if self.dealsList.isEmpty {
                    
                    if (!self.isMeResponseTry) {
                        self.isMeResponseTry = true
                        self.getPagaData()
                    }
                    else {
                        self.isMeResponseTry = false
                        self.setTableViewEmptyBackground()
                        self.dealsList.removeAll()
                        self.productID.removeAll()
                        self.dealsListRX.accept([])
                    }
                }
                else {
                    if isChanged || updateTable {
                        if count < 7 && self.dealPage > 0 {
                            self.dealPage -= 1
                        }
                        
//                        self.dealsList = self.dealsList.sorted { $0.productUpdated > $1.productUpdated}
                        self.dealsListRX.accept(self.dealsList)
                    }
                    
                    self.isMeResponseTry = false
                }
                self.dealsTable.tableFooterView?.isHidden = true
                self.fetchingMore = false
                self.dealsTable.isUserInteractionEnabled = true
                self.refreshControl.endRefreshing()
                
            }, viewController: self)
            
        case .archive:
            APIManager.sharedInstance.getUserDeals(active: false, type: .sell, page: self.archivePage, onSuccess: { (data) in
                
                var count = 0
                var isChanged = false
                if let response = data["data"]?.array, !response.isEmpty  {
                    count = response.count
                    
                    response.forEach { value in
                        guard let product = value.dictionary?["product"],
                              let title = product["productTitle"].string,
                              let weight = product["weight"].number,
                              let sum = product["price"].number,
                              let seller = product["seller"].dictionary,
                              let sellerName = seller["name"]?.string,
                              let imagesUrl = product["photoUrl"].array,
                              let id = value.dictionary?["id"]?.number,
                              let pay = value.dictionary?["paymentStatus"]?.string,
                              let customerAccept = value.dictionary?["customerAccept"]?.bool,
                              let other = value.dictionary?["dealStatus"]?.string,
                              let sellerAccept = value.dictionary?["sellerAccept"]?.bool,
                              let isPrivate = value.dictionary?["private"]?.bool,
                              let commission_included = value.dictionary?["commissionIncluded"]?.bool
                        else { return }
                        
                        let serviceses =  value.dictionary?["additionalServices"]?.array ?? []
                        let shipingData = value.dictionary?["shippingStatuses"]?.array ?? [JSON]()
                        let created = product["productCreatedAt"].number ?? 0
                        let updated = value.dictionary?["updatedAt"]?.number ?? 0
                        let commsion_for_sum = value.dictionary?["commissionForSum"]?.number ?? 0
                        let commsion_for_gram = value.dictionary?["commissionForGramm"]?.number ?? 0
                        let customer:String = value.dictionary?["customer"]?.dictionary?["name"]?.string ?? ""
                        let priceForGramm = self.concat(lhs: weight, rhs: sum)
                        let anulateData = DealAnulateData(isSellerAnulate: value.dictionary?["sellerAnnulate"]?.bool ?? false, isCustomerAnulate: value.dictionary?["customerAnnulate"]?.bool ?? false, anulateReson: value.dictionary?["annulationReason"]?.string ?? "")
                        let category = value.dictionary?["productCategory"]

                        if !self.archiveDealsID.contains(id) {
                            isChanged = true
                            let sum = "\(sum)"
                            
                            let data = DealData(id: id, productTitle: title, productWeight: "\(weight)", productSeller: sellerName, productCustomer: customer, productGramSum: priceForGramm, productSum: sum, productImages: [], productPayStatus: pay, shipingData: shipingData, otherStatus: other, type: .exist, productImagesURLs: imagesUrl, customerAccept: customerAccept, sellerAccept: sellerAccept, anulateData: anulateData, isPrivate: isPrivate, commision_included: commission_included, commissionForGramm: "\(commsion_for_gram)", commissionForSum: "\(commsion_for_sum)",productCreated: created as? Double, productUpdated: updated as? Double, services: serviceses,category: category)
                            
                            if Int(truncating: data.id) > Int(truncating: self.archiveDealsList.first?.id ?? 0) {
                                self.archiveDealsList.insert(data, at: 0)
                            }
                            else {
                                self.archiveDealsList.append(data)
                            }
                            
                            self.archiveDealsID.append(id)
                        }
                    }
                    
                    if isChanged {
                        self.archivePage += 1
                    }
                    
                }
                
                if self.archiveDealsList.isEmpty {
                    
                    if (!self.isArchiveResponseTry) {
                        self.isArchiveResponseTry = true
                        self.getPagaData()
                    }
                    else {
                        self.isArchiveResponseTry = false
                        self.setTableViewEmptyBackground()
                        self.archiveDealsList.removeAll()
                        self.archiveDealsID.removeAll()
                        self.dealsListRX.accept([])
                    }
                }
                else {
                    if isChanged || updateTable {
                        
                        if count < 7 && self.archivePage > 0 {
                            self.archivePage -= 1
                        }
                        
//                        self.archiveDealsList = self.archiveDealsList.sorted { $0.productUpdated > $1.productUpdated}
                        self.dealsListRX.accept(self.archiveDealsList)
                    }
                    
                    self.isArchiveResponseTry = false
                }
                
                self.dealsTable.tableFooterView?.isHidden = true
                self.dealsTable.isUserInteractionEnabled = true
                self.refreshControl.endRefreshing()
                self.fetchingMore = false
            }, viewController: self)
        case .other:
            break
        }
    }
    
    func setupViewConfiguration() {
        dealsTable.register(UINib(nibName: "PurchasesCell", bundle: nil), forCellReuseIdentifier: "purchases")
        self.dealsTable.rowHeight = UITableView.automaticDimension
        self.dealsTable.estimatedRowHeight = 180
        self.dealsTable.layer.cornerRadius = 10
        acceptDeclineView.alpha = 0
        refreshControl = AARefreshControl(scrollView: dealsTable)
        refreshControl.tintColor = UIColor.black
        refreshControl.activityIndicatorViewColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        dealsTable.addSubview(refreshControl)
        
        let loadingNib = UINib(nibName: "LoadingCell", bundle: nil)
        dealsTable.register(loadingNib, forCellReuseIdentifier: "loadingCell")
        
        filterBtn.rx.tap.subscribe(onNext: { _ in
            self.showFilter()
        }).disposed(by: self.disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        switch segmentTitle.selectedSegmentIndex {
        case 0:
            if (dealsList.count > 0) {
                dealsTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
        case 1:
            if (archiveDealsList.count > 0) {
                dealsTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
            
        default:
            break
        }
        
        checkDeleted()

        self.isMeResponseTry = false
        self.setTableViewEmptyBackground()
        self.dealsList.removeAll()
        self.productID.removeAll()
        self.dealsListRX.accept([])
        
        getPagaData(updateTable: true,beginRefresh: true)
    }
    
    func checkDeleted() {
        
        switch segmentTitle.selectedSegmentIndex {
        case 0:
            dealsList = [DealData]()
            productID = [NSNumber]()
            dealPage = 0
        case 1:
            archiveDealsList = [DealData]()
            archiveDealsID = [NSNumber]()
            archivePage = 0
        default:
            break
        }
    }
    
    @objc func refresh(_ sender: Any?) {
        if !fetchingMore {
            
            fetchingMore = true
            
            switch segmentTitle.selectedSegmentIndex {
            case 0:
                dealsList = [DealData]()
                productID = [NSNumber]()
                dealPage = 0
            case 1:
                archiveDealsList = [DealData]()
                archiveDealsID = [NSNumber]()
                archivePage = 0
            default:
                break
            }
            
            self.getPagaData(updateTable: true, beginRefresh: true)
        }
        else {
            self.refreshControl.endRefreshing()
        }
    }
    
    @IBAction func onTapCustomAlertButton(_ sender:UIButton) {
        performSegue(withIdentifier: "newProduct", sender: self)
    }
    
    private func calculateIndexPathsToReload(dealsData:[DealData], from deals: Int) -> [IndexPath] {
        let startIndex = dealsData.count - deals
        let endIndex = dealsData.count
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
}

extension DealsList:  UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var data = [DealData]()
        switch  segmentTitle.selectedSegmentIndex{
        case 0:
            data = self.dealsList
        default:
            data = self.archiveDealsList
        }
        
        if (indexPath.row == data.count - 1) {
            if !fetchingMore {
                if (indexPath.row > 4) {
                    self.dealsTable.tableFooterView?.isHidden = false
                    
                    guard let _ = self.selectedFilters else {
                        getPagaData(updateTable: false, beginRefresh: false)
                        return
                    }
                    
                    getFilterPageData()
                        
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var data = [DealData]()
        switch  segmentTitle.selectedSegmentIndex{
        case 0:
            data = self.dealsList
        default:
            data = self.archiveDealsList
        }
        
        var height = 110
        
        if (data.count > indexPath.row) {
            
            if (data[indexPath.row].shipingData.isEmpty) {
                height = height - 10
            }

            if (data[indexPath.row].sellerAccept && data[indexPath.row].customerAccept || !data[indexPath.row].sellerAccept && !data[indexPath.row].customerAccept) {
                height = height - 10
            }
            
            return CGFloat(height)
        }

        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !isPurchaseDataEditing {
            let vc = storyboard?.instantiateViewController(withIdentifier: "previewDeal") as! DealPreview
            
            let data = self.dealType == .me ? dealsList : archiveDealsList
            
            let termsDaata = (data[indexPath.row].termsDaata?.needsToAcceptNewTerms ?? false && data[indexPath.row].termsDaata?.customerAcceptNewTerms == nil) ? data[indexPath.row].termsDaata : nil
            
            vc.productData = DealData(id: data[indexPath.row].id, productTitle: data[indexPath.row].productTitle, productWeight: data[indexPath.row].productWeight, productSeller: data[indexPath.row].productSeller, productCustomer: data[indexPath.row].productCustomer, productGramSum: data[indexPath.row].productGramSum.replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil), productSum: data[indexPath.row].productSum.replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil), productImages:data[indexPath.row].productImages, productPayStatus: data[indexPath.row].productPayStatus, shipingData: data[indexPath.row].shipingData, otherStatus: data[indexPath.row].otherStatus, type: .preview, productImagesURLs: data[indexPath.row].productImagesURLs, customerAccept: data[indexPath.row].customerAccept, sellerAccept: data[indexPath.row].sellerAccept, anulateData: data[indexPath.row].anulateData, isPrivate: data[indexPath.row].isPrivate, commision_included: data[indexPath.row].commision_included, commissionForGramm: data[indexPath.row].commissionForGramm, commissionForSum: data[indexPath.row].commissionForSum,termsDaata: termsDaata, services: data[indexPath.row].services,category: data[indexPath.row].category)
            
            vc.customer = SellerData(id: 0, name: data[indexPath.row].productCustomer, phone: "", nickName: "")
            
            navigationController?.pushViewController(vc, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
            
            isPushPresent = true
        }
        else {
            if (dealsList[indexPath.row].sellerAccept && dealsList[indexPath.row].customerAccept || !dealsList[indexPath.row].sellerAccept && !dealsList[indexPath.row].customerAccept) {
                tableView.deselectRow(at: indexPath, animated: false)
            }
            else {
                if tableView.indexPathsForSelectedRows?.count > 0 {
                    UIView.animate(withDuration: 0.5) {
                        self.acceptDeclineView.alpha = 1
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if tableView.indexPathsForSelectedRows?.count == nil {
            UIView.animate(withDuration: 0.5) {
                self.acceptDeclineView.alpha = 0
            }
        }
    }
    
    @IBAction func acceptOrDeclineMultipleDeal(_ sender:UIButton) {
        
        accept = sender.tag == 1
        selectedIndexPath = dealsTable.indexPathsForSelectedRows
        
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        
        customAlert.config = AlertCustomData(title: accept ? "Accept_single".localized() : "Decline".localized(), firstSegment: "Yes".localized(), secondSegment: "No".localized(), amount: 1)
        self.present(customAlert, animated: true, completion: nil)
        
        isMultipleSelectedType = true
        isAcceptStep = false
    }
}

extension DealsList:EmptyTableViewDelegate {
    func refreshData() {
        if (!fetchingMore) {
            self.getPagaData(updateTable: true)
        }
    }
}

extension DealsList:CustomAlertViewDelegate {
    func okButtonTapped(count: String) {
        
        if isAcceptStep {
            self.acceptOrDecline(id: self.dealID, accept: self.accept, isUpdatePage: true)
            isMultipleSelectedType = false
        }
        else if isMultipleSelectedType {
            var last_id:NSNumber?

            if let last_row = self.selectedIndexPath?.last?.row {
                last_id = dealsList[last_row].id
            }
            
            self.selectedIndexPath?.forEach({ (indexPath) in
                let id = dealsList[indexPath.row].id
                let isNeedUpdate = last_id != nil ? (last_id == id) : false
                self.acceptOrDecline(id: id, accept: self.accept, isUpdatePage: isNeedUpdate)
            })
        }
        else {
            isMultipleSelectedType = false
            
            guard let deal = self.selectedDeal else {
                return
            }
            
            var commision_sum = (deal.productWeight.doubleValue * deal.commissionForGramm.doubleValue  +  deal.productSum.doubleValue * deal.commissionForSum.doubleValue)
            
            if selectedDeal!.commision_included {
                commision_sum = 0
            }
            
            DispatchQueue.main.async {
                let newView = self.storyboard?.instantiateViewController(withIdentifier: "selectReplenishType_vc") as! SelectReplenishType
                var amount = Double(UserProfileData.wallet?.balance ?? 0) + Double(UserProfileData.wallet?.creditLimit ?? 0) - Double(UserProfileData.wallet?.debt ?? 0) - deal.productSum.doubleValue
                
                if (!deal.commision_included) {
                    amount -= commision_sum
                }
                
                amount = Double(String(format: "%.2f", abs(amount)))!
                
                newView.amount = amount
                newView.modalPresentationStyle = UIModalPresentationStyle.popover
                self.navigationController?.pushViewController(newView, animated: true)
            }
        }
    }
    
    func cancelButtonTapped() {
    }    
}

extension DealsList:SellsDelegate {
    func editSellsData(button: UIBarButtonItem) {
        if !isPurchaseDataEditing {
            button.title = "Cancel".localized()
            dealsTable.isEditing = true
            segmentTitle.isEnabled = false
        }
        else {
            button.title = "settings_edit".localized()
            dealsTable.isEditing = false
            UIView.animate(withDuration: 0.5) {
                self.acceptDeclineView.alpha = 0
            }
            segmentTitle.isEnabled = true
        }
        
        isPurchaseDataEditing = !isPurchaseDataEditing
    }
     
    func cancelEditSellsData(button: UIBarButtonItem) {
        if !isMultipleSelected {
            button.title = "Deselect_all".localized()
            
            for row in  0..<dealsList.count {
                let indexPath = IndexPath(row: row, section: 0)
                
                if !(dealsList[indexPath.row].sellerAccept && dealsList[indexPath.row].customerAccept) {
                    dealsTable.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                }
            }
            
            UIView.animate(withDuration: 0.5) {
                self.acceptDeclineView.alpha = 1
            }
            segmentTitle.isEnabled = false

        }
        else {
            button.title = "Select_all".localized()
            for row in  0..<dealsList.count {
                dealsTable.deselectRow(at: IndexPath(row: row, section: 0), animated: true)
            }
            
            UIView.animate(withDuration: 0.5) {
                self.acceptDeclineView.alpha = 0
            }
            segmentTitle.isEnabled = true

        }
        
        isMultipleSelected = !isMultipleSelected
    }
}

extension DealsList:FilterAlertDelegate {
    
    func getFilterPageData() {
        
        guard let filters = self.selectedFilters else {
            return
        }
        
        let  sort = ["price", "weight"]
        let  direction = ["ASK", "DESK"]

        let data = [
            "title":filters.productName,
            "weightLowLimit":filters.weightFrom,
            "weightHighLimit":filters.weightTo,
            "priceLowLimit":filters.priceFrom,
            "priceUpLimit":filters.priceTo,
            "active":true,
            "sellerId": UserProfileData.id!,
            "sort": sort[filters.sort.rawValue],
            "sortDirection":direction[filters.sortDirection.rawValue],

        ] as [String : Any]
        
        APIManager.sharedInstance.getFiltersDeals(page: self.dealPage, data: data, onSuccess: { (data) in
           
            var isChanged = false
            var count = 0
            
            if let response = data["data"]?.array, !response.isEmpty  {
                count = response.count
                
                response.forEach { value in
                    guard let product = value.dictionary?["product"],
                          let title = product["productTitle"].string,
                          let weight = product["weight"].number,
                          let sum = product["price"].number,
                          let seller = product["seller"].dictionary,
                          let sellerName = seller["name"]?.string,
                          let imagesUrl = product["photoUrl"].array,
                          let id = value.dictionary?["id"]?.number,
                          let pay = value.dictionary?["paymentStatus"]?.string,
                          let customerAccept = value.dictionary?["customerAccept"]?.bool,
                          let other = value.dictionary?["dealStatus"]?.string,
                          let sellerAccept = value.dictionary?["sellerAccept"]?.bool,
                          let isPrivate = value.dictionary?["private"]?.bool,
                          let commission_included = value.dictionary?["commissionIncluded"]?.bool
                    else { return }
                    
                    let serviceses = value.dictionary?["additionalServices"]?.array ?? []
                    let commsion_for_sum = value.dictionary?["commissionForSum"]?.number ?? 0
                    let commsion_for_gram = value.dictionary?["commissionForGramm"]?.number ?? 0
                    let customer:String = value.dictionary?["customer"]?.dictionary?["name"]?.string ?? ""
                    let shipingData = value.dictionary?["shippingStatuses"]?.array ?? [JSON]()
                    let created = product["productCreatedAt"].number ?? 0
                    let updated = value.dictionary?["updatedAt"]?.number ?? 0
                    let termsData =  TermsData(needsToAcceptNewTerms: value.dictionary?["needsToAcceptNewTerms"]?.bool, newTerms: value.dictionary?["newTerms"]?.dictionary, customerAcceptNewTerms: value.dictionary?["customerAcceptNewTerms"]?.bool, sellerAcceptNewTerms: value.dictionary?["sellerAcceptNewTerms"]?.bool)
                    
                    let priceForGramm = self.concat(lhs: weight, rhs: sum)
                    let anulateData = DealAnulateData(isSellerAnulate: value.dictionary?["sellerAnnulate"]?.bool ?? false, isCustomerAnulate: value.dictionary?["customerAnnulate"]?.bool ?? false, anulateReson: value.dictionary?["annulationReason"]?.string ?? "")
                    
                    let category = value.dictionary?["productCategory"]
                    
                    if !self.productID.contains(id) {
                        isChanged = true
                        let sum = "\(sum)"
                        
                        let data = DealData(id: id, productTitle: title, productWeight: "\(weight)", productSeller: sellerName, productCustomer: customer, productGramSum: priceForGramm, productSum: sum, productImages: [], productPayStatus: pay, shipingData: shipingData, otherStatus: other, type: .exist, productImagesURLs: imagesUrl, customerAccept: customerAccept, sellerAccept: sellerAccept, anulateData: anulateData, isPrivate: isPrivate, commision_included: commission_included, commissionForGramm: "\(commsion_for_gram)", commissionForSum: "\(commsion_for_sum)",termsDaata: termsData,productCreated: created as? Double,productUpdated: updated as? Double, services: serviceses, category: category)
                        
                        if Int(truncating: data.id) > Int(truncating: self.dealsList.first?.id ?? 0) {
                            self.dealsList.insert(data, at: 0)
                        }
                        else {
                            self.dealsList.append(data)
                        }
                        
                        self.productID.append(id)
                    }
                }
                
                if isChanged {
                    self.dealPage += 1
                }
            }
            
            if self.dealsList.isEmpty {
                
                if (!self.isMeResponseTry) {
                    self.isMeResponseTry = true
                    self.getFilterPageData()
                }
                else {
                    self.isMeResponseTry = false
                    self.setTableViewEmptyBackground()
                    self.dealsList.removeAll()
                    self.productID.removeAll()
                    self.dealsListRX.accept([])
                }
            }
            else {
                if isChanged {
                    if count < 7 && self.dealPage > 0 {
                        self.dealPage -= 1
                    }
                    
//                    self.dealsList = self.dealsList.sorted { $0.productUpdated > $1.productUpdated}
                    self.dealsListRX.accept(self.dealsList)
                }
                
                self.isMeResponseTry = false
            }
            self.dealsTable.tableFooterView?.isHidden = true
            self.fetchingMore = false
            self.dealsTable.isUserInteractionEnabled = true
            self.refreshControl.endRefreshing()
        }, viewController: self)
    }
    
    func setFilter(filters: Filters) {
        self.isMeResponseTry = false
        self.selectedFilters = filters
        self.dealPage = 0
        self.dealsList.removeAll()
        self.productID.removeAll()
        self.dealsListRX.accept([])
        
        self.getFilterPageData()
    }
    
    func annulateFilters() {
        self.isMeResponseTry = false
        self.selectedFilters = nil
        self.dealsList.removeAll()
        self.productID.removeAll()
        self.dealsListRX.accept([])
        self.getPagaData()
    }
}
