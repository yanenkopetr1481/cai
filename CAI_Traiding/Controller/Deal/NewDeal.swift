//
//  NewDeal.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/3/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import Gallery
import Lightbox
import JGProgressHUD
import NotificationBannerSwift
import SwiftyJSON
import LGButton
import RxSwift
import Alamofire

protocol SelectContactDelegate {
    func setUser(user:SellerData)
}

class NewDeal: RootViewController {
    
    @IBOutlet weak var imagesStack: UIStackView!
    @IBOutlet weak var imagesCountLabel: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnChooseImage: LoadingButton!
    @IBOutlet weak var imgProfileHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var yourTextView:UIScrollView!
    @IBOutlet weak var productSumByGramLoc:UILabel!
    @IBOutlet weak var productTitleLoc:UILabel!
    @IBOutlet weak var productWeightLoc:UILabel!
    @IBOutlet weak var productSumLoc:UILabel!
    @IBOutlet weak var orLoc:UILabel!
    @IBOutlet weak var productAcceptComissionLoc:UILabel!
    @IBOutlet weak var productComissionLoc:UILabel!
    @IBOutlet weak var productComissionData:UILabel!
    @IBOutlet weak var nextBtn:UIButton!
    @IBOutlet weak var checkUserSelectType: UISegmentedControl!
    @IBOutlet weak var userPickerView: UIPickerView!
    @IBOutlet weak var userSelectStack: UIStackView!
    @IBOutlet weak var userPhone: UITextField!
    @IBOutlet weak var addPhotoLoc:UILabel!
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet var checkBox: CheckBox!

    @IBOutlet weak var categoriesStackViews:UIStackView!
    @IBOutlet weak var categoriesParentStackViews:UIStackView!
    @IBOutlet weak var categoriesTitleLGBtn:LGButton!
    
    @IBOutlet weak var categoriesFromProduct:UIStackView!
    @IBOutlet weak var orStackView:UIStackView!    
    @IBOutlet weak var categoriesFromProductLoca:UILabel!
    @IBOutlet weak var categoriesFromProductTitle:UITextField!
    
    @IBOutlet weak var createFromExistProduct:UIButton!
    
    var library:[UIImage] = [UIImage]()
    var gallery: GalleryController!
    var lightbox :LightboxController!
    var selectedImageLightBox:Int = 0
    var productData:DealData?
    var isSelectDealType:Bool = true
    var lightBoxType:Bool = false
    var isPrivate:Bool = false
    var isComissionSelected:Bool = true
    var isRemoveImage:Bool = false
    var alertTitle:String = "deal_alertTitle".localized()
    var alertTrueButton:String = "deal_alertTrueButton".localized()
    var alertFalseButton:String = "deal_alertFalseButton".localized()
    var productPriceForGram:String = ""
    var commision_sum:Double = 0
    var selectedUser:SellerData?
    var spaceLeading:CGFloat = 10
    
    var categories:[JSON]?
    var disposedBag = DisposeBag()
    var selectedCategoryBtn:LGButton?
    var selectedCategoryID:Int?
    var selectedCategoryData:JSON?
    
    var product:StoreProductData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewConfiguration()
        hideKeyboardWhenTappedAround()
        setTransparentNavigationBar()
        setupGalleryConfiguration()
        setupLocalization()
    }
    
    func createCategory(parentStack:UIStackView, categories:JSON, hud:JGProgressHUD, spacingLeading:CGFloat = 0){
        
        let lgBtn = LGButton()
        lgBtn.titleFontSize = 20
        lgBtn.titleFontName = "Helvetica Neue Light"
        lgBtn.spacingLeading = spacingLeading
        lgBtn.titleNumOfLines = 10
        lgBtn.bgColor = .clear
        lgBtn.titleString = "\(categories["title"].string!)"
        lgBtn.titleColor = .black
        lgBtn.layer.cornerRadius = 5
        lgBtn.leftImageSrc = UIImage(named: "check_unfill")
        
        lgBtn.leftAligned = true
        lgBtn.tag = categories["id"].int!
        
        let childStack = UIStackView()
        childStack.isHidden = true
        
        var haveChild = false
        if (categories["hasChilds"].bool ?? false) {
            lgBtn.rightImageSrc = UIImage(named: "chevron_down")
            childStack.axis = .vertical
            haveChild = true
        }
        
        var isChildLoaded = false
        
        lgBtn.rx.controlEvent(.touchUpInside).bind { (_) in
            
            if (categories["hasChilds"].bool ?? false) {
                
                if (!isChildLoaded) {
                    hud.textLabel.text = "deal_get_sub_category".localized()
                    hud.vibrancyEnabled = true
                    hud.show(in: self.view)
                    
                    APIManager.sharedInstance.getCategories(parentID: lgBtn.tag, completionHandler: { (data) in
                        
                        guard let status = data["status"]?.number, status == 1 else {
                            UIView.animate(withDuration: 0.1, animations: {
                                hud.textLabel.text = "Error".localized()
                                hud.detailTextLabel.text = data["data"]?.string ?? ""
                                hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                
                                hud.dismiss(afterDelay: 2.0, animated: true)
                            })
                            return
                        }
                        
                        data["data"]?.array?.forEach({ (childCategory) in
                            
                            let stack = UIStackView()
                            stack.axis = .vertical
                            self.createCategory(parentStack: stack, categories: childCategory, hud: hud, spacingLeading: self.spaceLeading)
                            childStack.addArrangedSubview(stack)
                        })
                        
                        self.spaceLeading += 10
                        
                        isChildLoaded = true
                        lgBtn.rightImageSrc = UIImage(named: "chevron_up")
                        childStack.isHidden = false
                        hud.dismiss(afterDelay: 2.0, animated: true)
                        return
                    }, viewController: self)
                }
                else {
                    lgBtn.rightImageSrc = !childStack.isHidden ? UIImage(named: "chevron_down") : UIImage(named: "chevron_up")
                    childStack.isHidden = !childStack.isHidden
                }
            }
            
            lgBtn.leftImageSrc = lgBtn.isSelected ? UIImage(named: "check_unfill") : UIImage(named: "check_fill")
            
            self.selectedCategoryBtn?.leftImageSrc = UIImage(named: "check_unfill")
            self.selectedCategoryBtn?.isSelected = false
            self.selectedCategoryBtn = lgBtn.isSelected ? nil : lgBtn
            self.selectedCategoryID = lgBtn.isSelected ? nil : lgBtn.tag
            self.selectedCategoryData = lgBtn.isSelected ? nil : categories

            lgBtn.isSelected = !lgBtn.isSelected
            
        }.disposed(by: self.disposedBag)
        
        parentStack.addArrangedSubview(lgBtn)
        
        if haveChild {
            parentStack.addArrangedSubview(childStack)
        }
    }
    
    func getCategories() {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "deal_get_category".localized()
        hud.vibrancyEnabled = true
        
        APIManager.sharedInstance.getCategories(parentID: nil, completionHandler: { (data) in
            
            guard let status = data["status"]?.number, status == 1 else {
                return
            }
            
            self.categories?.removeAll()
            self.categories = data["data"]?.array
            
            if (self.categories != nil) {
                
                self.categoriesTitleLGBtn.titleFontSize = 20
                self.categoriesTitleLGBtn.titleFontName = "Helvetica Neue Light"
                self.categoriesTitleLGBtn.spacingLeading = 0
                self.categoriesTitleLGBtn.titleNumOfLines = 10
                self.categoriesTitleLGBtn.bgColor = UIColor(hex: "#E50100")
                self.categoriesTitleLGBtn.titleString = "Select_category".localized()
                self.categoriesTitleLGBtn.titleColor = .white
                self.categoriesTitleLGBtn.layer.cornerRadius = 5
                self.categoriesTitleLGBtn.isSelected = false
                
                self.categoriesTitleLGBtn.rx.controlEvent(.touchUpInside).bind { (_) in
                    self.categoriesStackViews.isHidden = self.categoriesTitleLGBtn.isSelected
                    self.categoriesTitleLGBtn.isSelected = !self.categoriesTitleLGBtn.isSelected
                    
                }.disposed(by: self.disposeBag)
                
                self.categoriesStackViews.removeArrangedSubviews()
                self.categoriesStackViews.removeSubviews()
                
                self.categories!.forEach({ (categories) in
                    let stack = UIStackView()
                    stack.axis = .vertical
                    self.createCategory(parentStack: stack, categories: categories, hud: hud)
                    self.categoriesStackViews.addArrangedSubview(stack)
                    self.categoriesParentStackViews.isHidden = false
                })
            }
            
        }, viewController: self)
    }
    
    func setupViewConfiguration() {
        self.categoriesFromProduct.isHidden = true
        self.createFromExistProduct.setTitle("deal_product_title".localized(), for: .normal)
        self.categoriesFromProductLoca.text = "deal_category".localized()
            
        self.createFromExistProduct.rx.tap.subscribe(onNext: { _ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "storeProduts") as! StoreProducts
            
            vc.modalPresentationStyle = .fullScreen
            vc.delegate = self
            vc.isNewDealPageDelegate = true
            
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            
            self.view.window!.layer.add(transition, forKey: kCATransition)
            self.present(vc, animated: false, completion: nil)
        }).disposed(by: self.disposedBag)
        
        textFields.forEach { (textField) in
            textField.addTarget(self, action: #selector(calculateCommisionAndGramPrice), for: UIControl.Event.editingChanged)
            textField.delegate = self
            textField.autocapitalizationType = .none
            if (textField.tag != 0 && textField.tag != 4) {
                textField.keyboardType = .decimalPad
            }
        }
        
        checkBox.style = .tick
        checkBox.borderStyle = .square
        checkBox.addTarget(self, action: #selector(checkboxValueChanged(sender:)), for: .valueChanged)
        
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(checkboxValueChanged))
        self.productAcceptComissionLoc.isUserInteractionEnabled = true
        self.productAcceptComissionLoc.addGestureRecognizer(labelTap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        productComissionData.text = "deal_comission_receive_title".localized() + " ...$"
        productComissionData.isHidden = !isComissionSelected
        
        self.categoriesStackViews.isHidden = true
        self.categoriesParentStackViews.isHidden = true
    }
    
    @IBAction func changeUserSelectType(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            userPickerView.isHidden = true
            userPhone.isHidden = false
            productComissionLoc.isHidden = false
        }
        else {
            
            let newView = self.storyboard?.instantiateViewController(withIdentifier: "sellers_vc") as! Seller
            newView.isDelegate = true
            newView.delegate = self
            newView.modalPresentationStyle = UIModalPresentationStyle.popover
            self.present(newView, animated: true, completion: {
                self.checkUserSelectType.selectedSegmentIndex = 0
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showSelectDealType()
    }
    
    @objc func checkboxValueChanged(sender: CheckBox) {
        isComissionSelected = !isComissionSelected
        checkBox.isChecked = isComissionSelected
        calculateCommisionAndGramPrice()
    }
    
    func setupLocalization() {
        addPhotoLoc.text = "addPhoto".localized()
        checkUserSelectType.setTitle("deal_user_select_first".localized(), forSegmentAt: 0)
        checkUserSelectType.setTitle("deal_user_select_second".localized(), forSegmentAt: 1)
        productComissionLoc.text = "deal_comission".localized()
        productAcceptComissionLoc.text = "deal_accept_comission".localized()
        productTitleLoc.text = "deal_title".localized()
        productWeightLoc.text = "deal_weight".localized() + ",g"
        productSumByGramLoc.text = "deal_sum_gram".localized() + ",$"
        productSumLoc.text = "deal_sum".localized() + ",$"
        productTitleLoc.text = "deal_title".localized()
        orLoc.text = "OR".localized()

        nextBtn.setTitle("Next".localized(), for: .normal)
    }
    
    func setupGalleryConfiguration() {
        Config.Camera.imageLimit = 5
        Config.tabsToShow = [.imageTab, .cameraTab]
    }
    
    func setTransparentNavigationBar() {
        guard let _ = self.navigationController else {
            return
        }
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
        self.title = "newDeal".localized()
    }
    
    func showSelectDealType() {
        if (isSelectDealType) {
            let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
            customAlert.providesPresentationContextTransitionStyle = true
            customAlert.definesPresentationContext = true
            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            customAlert.delegate = self
            customAlert.config = AlertCustomData(title: alertTitle, firstSegment: alertTrueButton, secondSegment: alertFalseButton,amount: 1)
            self.present(customAlert, animated: true, completion: nil)
        }
    }
    
    @objc func adjustForKeyboard(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
            
            let keyboardScreenEndFrame = keyboardValue.cgRectValue
            let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
            
            if notification.name == UIResponder.keyboardWillHideNotification {
                yourTextView.contentInset = .zero
                self.imgProfileHeightConstraint.constant = 270
                btnChooseImage.isHidden = false
                
                self.title = "newDeal".localized()
            } else {
                
                self.title = ""
                
                if #available(iOS 11.0, *) {
                    yourTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
                } else {
                    yourTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.alignmentRectInsets.bottom, right: 0)
                }
                
                if self.imgProfileHeightConstraint.constant == 270.0 {
                    UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
                        self.imgProfileHeightConstraint.constant -= keyboardSize.height - 100
                        self.btnChooseImage.isHidden = true
                    })
                }
            }
            
            yourTextView.scrollIndicatorInsets = yourTextView.contentInset
        }
    }
    
    
    @IBAction func back(_ sender:Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showLibrary(_ sender: UIButton) {
        guard library.count > 0 else {
            return
        }
        
        let lightboxImages = library.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.footerView.isHidden = true
        lightbox.imageTouchDelegate = self
        lightbox.modalPresentationStyle = .fullScreen
        lightBoxType = true
        self.present(lightbox, animated: true)
    }
    
    @IBAction func btnChooseImageOnClick(_ sender: UIButton) {
        gallery = GalleryController()
        gallery.delegate = self
        gallery.modalPresentationStyle = .fullScreen
        
        
        present(gallery, animated: true, completion: {
            //            if self.gallery.children.count > 0{
            //                let viewControllers:[UIViewController] = self.gallery.children
            //                for viewContoller in viewControllers{
            //                    viewContoller.view.backgroundColor = .black
            //                }
            //            }
        })
    }
    
    @IBAction func showPreview(_ sender: UIButton) {
        
        var isError = 0
        var title:String?
        var productSum:Double?
        var productWeight:Double?
        
        textFields.forEach { (textField) in
            if (textField.text!.isEmpty) {
                isError = !isPrivate && textField.tag == 4 ? 0 : textField.tag
                UIView.animate(withDuration: 0.3) {
                    textField.layer.borderWidth = 0.5
                    textField.layer.cornerRadius = 5
                    textField.layer.borderColor = UIColor.red.cgColor
                }
            }
            else {
                switch textField.tag {
                case 0:
                    title = textField.text!
                case 1:
                    productWeight = textField.text?.doubleValue
                case 2:
                    productSum = textField.text?.doubleValue
                case 4:
                    if (textField.text != selectedUser?.name && textField.text != selectedUser?.nickName && textField.text != selectedUser?.phone) {
                        selectedUser = SellerData(id: 0, name: textField.text!.replacingOccurrences(of: " ", with: ""), phone: textField.text!, nickName: textField.text!)
                    }
                default:
                    return
                }
            }
        }
        
        if !library.isEmpty {
            
            guard let title = title, let productWeight = productWeight, let productSum = productSum else {
                let banner = NotificationBanner(title: "Missing".localized(), subtitle: "BannerMissingField".localized(), style: .danger)
                banner.show()
                return
            }
            
            if isError == 0 {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "previewDeal") as! DealPreview
                vc.productData = DealData(id: 0, productTitle: title, productWeight: "\(productWeight)", productSeller: UserProfileData.name ?? UserProfileData.phone!, productCustomer: "", productGramSum: productPriceForGram, productSum: "\(productSum)", productImages: self.library, productPayStatus: "", shipingData: [JSON](), otherStatus: "", type: .new, productImagesURLs: [], customerAccept: false, sellerAccept: false, anulateData: DealAnulateData(isSellerAnulate: false, isCustomerAnulate: false, anulateReson: ""), isPrivate: false, commision_included: isComissionSelected, commissionForGramm: "", commissionForSum: "", services: nil, category: self.selectedCategoryData)
                
                if (self.isPrivate) {
                    vc.customer =  selectedUser
                }
                
                vc.product = self.product
                
                vc.isComissionSelected = isComissionSelected
                
                if isComissionSelected {
                    vc.commision_sum = commision_sum
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else {
            
            let banner = NotificationBanner(title: "Missing".localized(), subtitle: "BannerMissing".localized(), style: .danger)
            banner.show()
        }
    }
    
    func onTapCustomAlertButton() {
        isRemoveImage = true
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.config = AlertCustomData(title: "RemoveImage".localized(), firstSegment: "Yes".localized(), secondSegment: "No".localized())
        lightbox.present(customAlert, animated: true, completion: nil)
    }
}

extension NewDeal: CustomAlertViewDelegate {
    
    func okButtonTapped(count: String) {
        if (isSelectDealType) {
            isSelectDealType = false
            isPrivate = true
            textFields.forEach { (textfield) in
                textfield.isHidden = false
            }
            
            productAcceptComissionLoc.isHidden = false
        }
        else {
            lightbox.images.remove(at: selectedImageLightBox )
            
            if (lightBoxType) {
                library.remove(at: selectedImageLightBox )
                self.imagesCountLabel.text = "\(self.library.count)+"
                
                if (lightbox.images.count > 0) {
                    imgProfile.image = library[library.count - 1]
                    
                    let index = selectedImageLightBox > 0 ? (selectedImageLightBox ) - 1 : 0
                    lightbox.goTo(index, animated: true)
                }
                else {
                    imgProfile.image = UIImage(named: "Background")
                    self.imagesStack.isHidden = true
                    DispatchQueue.main.async {
                        self.lightbox.dismiss(animated: true, completion: nil)
                    }
                }
            }
            else {
                var images =  gallery.cart.images
                gallery.cart.remove(images[lightbox.currentPage])
                images.remove(at: lightbox.currentPage)
                gallery.cart.reload(images)
                if (lightbox.images.count == 0) {
                    DispatchQueue.main.async {
                        self.lightbox.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func cancelButtonTapped() {
        if !isRemoveImage {
            isSelectDealType = false
            isPrivate = false
            
            userSelectStack.isHidden = true
        }
        
        getCategories()
    }
    
    @objc func calculateCommisionAndGramPrice() {
        var sum:Double?
        var weight:Double?
        
        textFields.forEach { (textfield) in
            if (textfield.tag == 2) {
                let value = NumberFormatter().number(from: textfield.text!.replacingOccurrences(of: ".", with: ","))
                
                if let value = value {
                    sum = Double(truncating: value)
                }
            }
            
            if (textfield.tag == 1) {
                let value = NumberFormatter().number(from: textfield.text!.replacingOccurrences(of: ".", with: ","))
                
                if let value = value {
                    weight = Double(truncating: value)
                }
            }
            
            if (textfield.tag == 3) {
                if (sum != nil && weight != nil) {
                    productComissionData.isHidden = !isComissionSelected
                    commision_sum = sum! - (weight! * UserProfileData.commissionForGramm! + sum! * UserProfileData.commissionForSum!)
                    productComissionData.text = "deal_comission_receive_title".localized() + " " + String(format: "%.2f", commision_sum) + "$"
                    productPriceForGram =  String(format: "%.2f", Double(sum!/weight!))
                    textfield.text = productPriceForGram
                    textfield.layer.borderColor = UIColor.white.cgColor
                }
                else {
                    productComissionData.isHidden = !isComissionSelected
                    productComissionData.text = "deal_comission_receive_title".localized() + " ...$"
                    textfield.text = productPriceForGram
                    textfield.layer.borderColor = UIColor.white.cgColor
                }
            }
        }
    }
}

extension NewDeal:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) {
            textField.layer.borderWidth = 0.5
            textField.layer.cornerRadius = 5
            textField.layer.borderColor = UIColor.white.cgColor
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return !string.isSingleEmoji
    }
}

extension NewDeal: GalleryControllerDelegate {
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        
        DispatchQueue.main.async {
            Image.resolve(images: images, completion: { [weak self] resolvedImages in
                let images = resolvedImages.compactMap({ $0 })
                self!.imgProfile.image = images.last
                images.forEach { (image) in
                    self!.library.append(image)
                }
                self!.imagesCountLabel.text = "\(self!.library.count)+"
                self!.imagesStack.isHidden = false
            })
        }
        
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {}
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            self?.showLightbox(controller, images: resolvedImages.compactMap({ $0 }))
        })
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func showLightbox(_ controller: GalleryController,images: [UIImage]) {
        
        guard images.count > 0 else {
            return
        }
        
        lightBoxType = false
        
        let lightboxImages = images.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.footerView.isHidden = true
        lightbox.modalPresentationStyle = .fullScreen
        lightbox.imageTouchDelegate = self
        gallery.present(lightbox, animated: true)
    }
}

extension NewDeal:LightboxControllerTouchDelegate {
    func lightboxController(_ controller: LightboxController, didTouch image: LightboxImage, at index: Int) {
        selectedImageLightBox = index
        self.onTapCustomAlertButton()
    }
}

extension NewDeal:SelectContactDelegate {
    func setUser(user: SellerData) {
        selectedUser = user
        userPhone.text = user.name
    }
}

extension NewDeal:StoreProductsProtocol {
    func selectProduct(product: StoreProductData) {
        
        textFields.forEach { (textfield) in
            textfield.isEnabled = false
            
            if (textfield.tag == 2) {
                textfield.text = "\(product.productSum)"
            }
            
            if (textfield.tag == 1) {
                textfield.text = "\(product.productWeight)"
            }
            
            
            if (textfield.tag == 0) {
                textfield.text = "\(product.productTitle)"
            }
        }
        
        self.calculateCommisionAndGramPrice()
        self.btnChooseImage.setImage(nil, for: .normal)

        product.productImagesURLs.forEach { urlJson in
            
            self.btnChooseImage.isHidden = false
            self.btnChooseImage.showLoading()
            
            let url = URL(string: urlJson.string!)!
            
            if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                self.library.append(image)
                self.btnChooseImage.hideLoading()
                
                if (self.library.count > 0) {
                    self.imgProfile.image = self.library.first
                    self.imagesStack.isHidden = false
                    self.imagesCountLabel.text = "\(self.library.count)+"
                }
            }
            else {
                AF.request(url).response {
                    result in
                    DispatchQueue.main.async {
                        if let data = result.data {
                            let image = UIImage(data: data)!
                            self.library.append(image)
                            ApiConfigurationData.appImages.setObject(image, forKey: url.absoluteString as AnyObject)
                            self.btnChooseImage.hideLoading()
                            if (self.library.count > 0) {
                                self.imgProfile.image = self.library.first
                                self.imagesStack.isHidden = false
                                self.imagesCountLabel.text = "\(self.library.count)+"
                            }
                            
                            self.btnChooseImage.isHidden = true
                        }
                    }
                }
            }
        }
        
        categoriesParentStackViews.isHidden = true
        categoriesFromProduct.isHidden = false
        categoriesFromProductTitle.text = product.category?["title"].string
        addPhotoLoc.isHidden = true
        orStackView.isHidden = true
        self.product = product
    }
}
