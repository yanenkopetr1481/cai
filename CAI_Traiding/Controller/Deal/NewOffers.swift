//
//  NewOffers.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 27.04.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit
import SwiftyJSON
import AARefreshControl
import RxSwift
import RxCocoa
import RxDataSources
import AlamofireImage
import Alamofire

struct DealServices {
    static var service:[JSON]?
}

class NewOffers: RootViewController {
    
    @IBOutlet weak var dealsTable:UITableView!
    
    var refreshControl: AARefreshControl!
    
    var dealsList:[DealData] = [DealData]()
    
    let dealsListRX: BehaviorRelay<[DealData]> = BehaviorRelay(value: [])
    
    var productID:[NSNumber] = [NSNumber]()
    
    var isPushPresent:Bool = false
    var dealType:type = .me
    var previewedDealAt:Int = Int()
    var isOtherResponseTry:Bool = false
    var fetchingMore:Bool = false
    
    var otherPage:Int = 0
    var selectedFilters:Filters?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewConfiguration()
        configureRxTable()
        configureFooter()
        hideKeyboardWhenTappedAround()
    }
    
    func configureFooter() {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: dealsTable.bounds.width, height: CGFloat(44))
        self.dealsTable.tableFooterView = spinner
        self.dealsTable.tableFooterView?.isHidden = true
    }
    
    func configureRxTable() {
        
        dealsTable.rx.setDelegate(self).disposed(by: disposeBag)
        
        dealsListRX.asObservable().bind(to: dealsTable.rx.items(cellIdentifier: "purchases")) { table, model, cell in
            
            let cell = cell as! PurchasesCell
            
            cell.payBtn.isHidden = true
            cell.setupViewConfiguration(data: model)
            
            let imageUrl = model.productImagesURLs
            if let urlStr = imageUrl.last?.string, let url = URL(string: urlStr) {
                
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    cell.productImage.image = image
                }
                else {
                    cell.productImage.downloadImageFrom(url: url, contentMode: .scaleAspectFill)
                }
            }
            else {
                cell.productImage.image = UIImage(named: "empty_product")
            }
            
            cell.textLabel?.numberOfLines = 0
            cell.selectionStyle = .none
            
        }.disposed(by: disposeBag)
        
        dealsTable.rx.modelSelected(DealData.self)
            .map{ $0 }
            .subscribe(onNext: { [weak self] data in
                
                self!.isPushPresent = true
                let vc = self?.storyboard?.instantiateViewController(withIdentifier: "previewDeal") as! DealPreview
                var type:DealType!
                
                type = .other
                
                vc.productData = DealData(id: data.id, productTitle: data.productTitle, productWeight: data.productWeight, productSeller: data.productSeller, productCustomer: data.productCustomer, productGramSum: data.productGramSum.replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil), productSum: data.productSum.replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil), productImages: data.productImages, productPayStatus: "", shipingData: [JSON](), otherStatus: data.otherStatus, type: type, productImagesURLs: data.productImagesURLs, customerAccept: data.customerAccept, sellerAccept:  data.sellerAccept, anulateData: data.anulateData, isPrivate: data.isPrivate, commision_included: data.commision_included, commissionForGramm: data.commissionForGramm, commissionForSum: data.commissionForSum,productCreated: data.productCreated, services: nil, category: data.category)
                
                self?.navigationController?.pushViewController(vc, animated: true)
                
            }).disposed(by: disposeBag)
        
        dealsTable.rx
            .itemSelected
            .subscribe(onNext:  { [weak self] indexPath in
                self?.dealsTable.deselectRow(at: indexPath, animated: true)
            })
            .disposed(by: disposeBag)
    }
    
    func concat (lhs: NSNumber, rhs: NSNumber) -> String {
        let value = Double(exactly: rhs.doubleValue / lhs.doubleValue)
        return String(format: "%.2f", value!)
    }
    
    private func setTableViewEmptyBackground() {
        UIView.animate(withDuration: 0.5) {
            let view = Bundle.main.loadNibNamed("EmptyTableView", owner: self, options: nil)?.first as! EmptyTableView
            view.titleLabel.text = "EmptyRessult".localized()
            view.refreshBtn.setTitle("Refresh".localized(), for: .normal)
            view.delegate = self
            view.imageView.image = UIImage(named: "empty")
            self.dealsTable.backgroundView = view
        }
    }
    
    func getPagaData(updateTable:Bool = false, beginRefresh: Bool = true) {
        fetchingMore = true
        
        if (beginRefresh) {
            self.refreshControl.beginRefreshing()
        }
        
        self.dealsTable.backgroundView = UIView()
        
        APIManager.sharedInstance.getAdditionalServices(onSuccess: { (data) in
            
            if let services = data["data"]?.array, !services.isEmpty {
                DealServices.service = nil
                DealServices.service = services
            }
            
            APIManager.sharedInstance.getOtherDeals(page: self.otherPage, onSuccess: { (data) in
                
                var isChanged = false
                var count = 0
                
                if let response = data["data"]?.array, !response.isEmpty  {
                    count = response.count
                    
                    response.forEach { value in
                        guard let product = value.dictionary?["product"],
                              let title = product["productTitle"].string,
                              let weight = product["weight"].number,
                              let sum = product["price"].number,
                              let seller = product["seller"].dictionary,
                              let sellerName = seller["name"]?.string,
                              let imagesUrl = product["photoUrl"].array,
                              let id = value.dictionary?["id"]?.number,
                              let sellerAccept = value.dictionary?["sellerAccept"]?.bool,
                              let commission_included = value.dictionary?["commissionIncluded"]?.bool
                        else { return }
                        
                        let priceForGramm = self.concat(lhs: weight, rhs: sum)
                        let created = product["productCreatedAt"].number ?? 0
                        let updated = value.dictionary?["updatedAt"]?.number ?? 0
                        let commsion_for_sum = value.dictionary?["commissionForSum"]?.number ?? 0
                        let commsion_for_gram = value.dictionary?["commissionForGramm"]?.number ?? 0
                        let category = value.dictionary?["productCategory"]

                        if !self.productID.contains(id) {
                            isChanged = true
                            let sum = "\(sum)"
                            
                            let data = DealData(id: id, productTitle: title, productWeight: "\(weight)", productSeller: sellerName, productCustomer: "", productGramSum: priceForGramm, productSum: sum, productImages: [], productPayStatus: "", shipingData: [JSON](), otherStatus: "", type: .other, productImagesURLs: imagesUrl, customerAccept: true, sellerAccept: sellerAccept, anulateData: DealAnulateData(isSellerAnulate: false, isCustomerAnulate: false, anulateReson: ""), isPrivate: false, commision_included: commission_included, commissionForGramm: "\(commsion_for_gram)", commissionForSum: "\(commsion_for_sum)",productCreated: created as? Double, productUpdated: updated as? Double, services: nil, category: category)
                            
                            if Int(truncating: data.id) > Int(truncating: self.dealsList.first?.id ?? 0) {
                                self.dealsList.insert(data, at: 0)
                            }
                            else {
                                self.dealsList.append(data)
                            }
                            
                            self.productID.append(id)
                        }
                    }
                    
                    if isChanged {
                        self.otherPage += 1
                    }
                    
                }
                
                if self.dealsList.isEmpty {
                    
                    if (!self.isOtherResponseTry) {
                        self.isOtherResponseTry = true
                        self.getPagaData()
                    }
                    else {
                        self.isOtherResponseTry = false
                        self.setTableViewEmptyBackground()
                        self.dealsList.removeAll()
                        self.productID.removeAll()
                        self.dealsListRX.accept([])
                    }
                    
                }
                else {
                    if isChanged || updateTable {
                        if count < 7 && self.otherPage > 0  {
                            self.otherPage -= 1
                        }
//                        self.dealsList = self.dealsList.sorted { $0.productUpdated > $1.productUpdated}
                        self.dealsListRX.accept(self.dealsList)
                    }
                    
                    self.isOtherResponseTry = false
                }
                
                self.dealsTable.tableFooterView?.isHidden = true
                self.dealsTable.isUserInteractionEnabled = true
                self.fetchingMore = false
                self.refreshControl.endRefreshing()
            }, viewController: self)
        }, viewController: self)
    }
    
    func setupViewConfiguration() {
        dealsTable.register(UINib(nibName: "PurchasesCell", bundle: nil), forCellReuseIdentifier: "purchases")
        self.dealsTable.rowHeight = UITableView.automaticDimension
        self.dealsTable.estimatedRowHeight = 120
        self.dealsTable.layer.cornerRadius = 10
        
        refreshControl = AARefreshControl(scrollView: dealsTable)
        refreshControl.tintColor = UIColor.black
        refreshControl.activityIndicatorViewColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        dealsTable.addSubview(refreshControl)
        
        let loadingNib = UINib(nibName: "LoadingCell", bundle: nil)
        dealsTable.register(loadingNib, forCellReuseIdentifier: "loadingCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (dealsList.count > 0) {
            dealsTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
        
        checkDeleted()
        getPagaData(updateTable: true,beginRefresh: true)
    }
    
    func checkDeleted() {
        dealsList = [DealData]()
        productID = [NSNumber]()
        otherPage = 0
    }
    
    @objc func refresh(_ sender: Any?) {
        if !fetchingMore {
            
            fetchingMore = true
            dealsList = [DealData]()
            productID = [NSNumber]()
            otherPage = 0
            
            if selectedFilters != nil {
                getFilterPageData()
            }
            else {
                self.getPagaData(updateTable: true, beginRefresh: true)
            }
        }
        else {
            self.refreshControl.endRefreshing()
        }
    }
    
    @IBAction func onTapCustomAlertButton(_ sender:UIButton) {
        performSegue(withIdentifier: "newProduct", sender: self)
    }
    
    private func calculateIndexPathsToReload(dealsData:[DealData], from deals: Int) -> [IndexPath] {
        let startIndex = dealsData.count - deals
        let endIndex = dealsData.count
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
}

extension NewOffers:  UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let data = self.dealsList
        
        if (indexPath.row == data.count - 1) {
            if !fetchingMore {
                if (indexPath.row > 4) {
                    self.dealsTable.tableFooterView?.isHidden = false
                    
                    if selectedFilters != nil {
                        getFilterPageData()
                    }
                    else {
                        getPagaData(updateTable: false, beginRefresh: false)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let data = self.dealsList
        
        var height = 110
        
        if (data.count > indexPath.row) {
            
            if (data[indexPath.row].shipingData.isEmpty) {
                height = height - 10
            }

            if (data[indexPath.row].sellerAccept && data[indexPath.row].customerAccept || !data[indexPath.row].sellerAccept && !data[indexPath.row].customerAccept) {
                height = height - 10
            }
            
            return CGFloat(height)
        }

        return 0
    }
}

extension NewOffers:EmptyTableViewDelegate {
    func refreshData() {
        if (!fetchingMore) {
            self.getPagaData(updateTable: true)
        }
    }
}

extension NewOffers:NewOffersDelegate {
    func showFilter(button: UIBarButtonItem) {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "FIlterAlertView") as! FIlterAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.config = self.selectedFilters
        self.present(customAlert, animated: true, completion: nil)
    }
}

extension NewOffers:FilterAlertDelegate {
    
    func getFilterPageData() {
        
        guard let filters = self.selectedFilters else {
            return
        }
        
        let data = [
            "title":filters.productName,
            "weightLowLimit":filters.weightFrom,
            "weightHighLimit":filters.weightTo,
            "priceLowLimit":filters.priceFrom,
            "priceUpLimit":filters.priceTo,
            "commissionIncluded":filters.commision,
            "categoryIds":filters.categories,
        ] as [String : Any]
        
        APIManager.sharedInstance.getFiltersDeals(page: self.otherPage, data: data, onSuccess: { (data) in
            
            var isChanged = false
            var count = 0
            
            if let response = data["data"]?.array, !response.isEmpty  {
                count = response.count
                
                response.forEach { value in
                    guard let product = value.dictionary?["product"],
                          let title = product["productTitle"].string,
                          let weight = product["weight"].number,
                          let sum = product["price"].number,
                          let seller = product["seller"].dictionary,
                          let sellerName = seller["name"]?.string,
                          let imagesUrl = product["photoUrl"].array,
                          let id = value.dictionary?["id"]?.number,
                          let sellerAccept = value.dictionary?["sellerAccept"]?.bool,
                          let commission_included = value.dictionary?["commissionIncluded"]?.bool
                    else { return }
                    
                    let priceForGramm = self.concat(lhs: weight, rhs: sum)
                    let created = product["productCreatedAt"].number ?? 0
                    let updated = value.dictionary?["updatedAt"]?.number ?? 0
                    let commsion_for_sum = value.dictionary?["commissionForSum"]?.number ?? 0
                    let commsion_for_gram = value.dictionary?["commissionForGramm"]?.number ?? 0
                    let category = value.dictionary?["productCategory"]

                    if !self.productID.contains(id) {
                        isChanged = true
                        let sum = "\(sum)"
                        
                        let data = DealData(id: id, productTitle: title, productWeight: "\(weight)", productSeller: sellerName, productCustomer: "", productGramSum: priceForGramm, productSum: sum, productImages: [], productPayStatus: "", shipingData: [JSON](), otherStatus: "", type: .other, productImagesURLs: imagesUrl, customerAccept: true, sellerAccept: sellerAccept, anulateData: DealAnulateData(isSellerAnulate: false, isCustomerAnulate: false, anulateReson: ""), isPrivate: false, commision_included: commission_included, commissionForGramm: "\(commsion_for_gram)", commissionForSum: "\(commsion_for_sum)",productCreated: created as? Double, productUpdated: updated as? Double, services: nil, category: category)
                        
                        if Int(truncating: data.id) > Int(truncating: self.dealsList.first?.id ?? 0) {
                            self.dealsList.insert(data, at: 0)
                        }
                        else {
                            self.dealsList.append(data)
                        }
                        
                        self.productID.append(id)
                    }
                }
                
                if isChanged {
                    self.otherPage += 1
                }
                
            }
            
            if self.dealsList.isEmpty {
                
                if (!self.isOtherResponseTry) {
                    self.isOtherResponseTry = true
                    self.getFilterPageData()
                }
                else {
                    self.isOtherResponseTry = false
                    self.setTableViewEmptyBackground()
                    self.dealsList.removeAll()
                    self.productID.removeAll()
                    self.dealsListRX.accept([])
                }
                
            }
            else {
                if isChanged || true {
                    if count < 7 && self.otherPage > 0  {
                        self.otherPage -= 1
                    }
//                    self.dealsList = self.dealsList.sorted { $0.productUpdated > $1.productUpdated}
                    self.dealsListRX.accept(self.dealsList)
                }
                
                self.isOtherResponseTry = false
            }

            self.dealsTable.tableFooterView?.isHidden = true
            self.dealsTable.isUserInteractionEnabled = true
            self.fetchingMore = false
            self.refreshControl.endRefreshing()
        }, viewController: self)
    }
    
    func setFilter(filters: Filters) {
        
        self.selectedFilters = filters
        self.otherPage = 0
        self.isOtherResponseTry = false
        self.dealsList.removeAll()
        self.productID.removeAll()
        self.dealsListRX.accept([])
        
        self.getFilterPageData()
    }
    
    func annulateFilters() {
        
        self.selectedFilters = nil
        self.otherPage = 0
        self.dealsList.removeAll()
        self.productID.removeAll()
        self.dealsListRX.accept([])
        self.getPagaData()
    }
}
