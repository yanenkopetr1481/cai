//
//  PurchaseList.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/3/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import AARefreshControl
import Lightbox
import Alamofire
import SwiftyJSON
import JGProgressHUD

class PurchaseList: RootViewController {
    
    @IBOutlet weak var paymentsTable:UITableView!
    @IBOutlet weak var segmentTitle:UISegmentedControl!
    @IBOutlet weak var acceptDeclineView:UIView!
    
    var library:[UIImage] = [UIImage]()
    var lightbox :LightboxController!
    var productListData:[DealData] = [DealData]()
    var archiveListData:[DealData] = [DealData]()
    var isPushPresent:Bool = false
    var productID:[NSNumber] = [NSNumber]()
    var archiveProductID:[NSNumber] = [NSNumber]()
    var refreshControl: AARefreshControl!
    var isResponseTry:Bool = false
    var isArchiveResponseTry:Bool = false
    var dealType:type = .me
    var page:Int = 0
    var archivePage:Int = 0
    var fetchingMore:Bool = false
    let defaults = UserDefaults.standard
    var selectedDeal:DealData?
    var accept = Bool()
    var isAcceptStep = Bool()
    var isPurchaseDataEditing = false
    var isMultipleSelected = false
    var selectedIndexPath:[IndexPath]?
    var isMultipleSelectedType:Bool = false
    var selectedFilters:Filters?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewConfiguration()
        configureFooter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        switch segmentTitle.selectedSegmentIndex {
        case 0:
            if (productListData.count > 0) {
                paymentsTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            }
        case 1:
            if (archiveListData.count > 0) {
                paymentsTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            }
        default:break
        }
        
        checkDeletedData()
        getPagaData(updateTable: true, beginRefresh: true)
    }
    
    func configureFooter() {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: paymentsTable.bounds.width, height: CGFloat(44))
        self.paymentsTable.tableFooterView = spinner
        self.paymentsTable.tableFooterView?.isHidden = true
    }
    
    func checkDeletedData() {
        
        switch segmentTitle.selectedSegmentIndex {
        case 0:
            productListData = [DealData]()
            productID = [NSNumber]()
            page = 0
        case 1:
            archiveListData = [DealData]()
            archiveProductID = [NSNumber]()
            archivePage = 0
        default:
            break
        }
    }
    
    @IBAction func changeDealType(_ sender: UISegmentedControl) {
        
        switch  sender.selectedSegmentIndex{
        case 0:
            self.dealType = .me
        case 1:
            self.dealType = .archive
        default:
            self.dealType = .other
        }
        
        isResponseTry = false
        isArchiveResponseTry = false
        getPagaData(updateTable: true, beginRefresh: true)
    }
    
    func concat (lhs: NSNumber, rhs: NSNumber) -> String {
        let value = Double(exactly: rhs.doubleValue / lhs.doubleValue)
        return String(format: "%.2f", value!)
    }
    
    private func setTableViewEmptyBackground() {
        UIView.animate(withDuration: 0.5) {
            let view = Bundle.main.loadNibNamed("EmptyTableView", owner: self, options: nil)?.first as! EmptyTableView
            view.titleLabel.text = "EmptyRessult".localized()
            view.refreshBtn.setTitle("Refresh".localized(), for: .normal)
            view.delegate = self
            view.imageView.image = UIImage(named: "empty")
            
            self.paymentsTable.backgroundView = view
        }
    }
    
    func presentAlertView(deal:DealData, title:String, withPrice:Bool = false) {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        
        var amount = Double(UserProfileData.wallet?.balance ?? 0) + Double(UserProfileData.wallet?.creditLimit ?? 0) - Double(UserProfileData.wallet?.debt ?? 0) - Double(deal.productSum)!
        
        var commision_sum = (deal.productWeight.doubleValue * deal.commissionForGramm.doubleValue  +  deal.productSum.doubleValue * deal.commissionForSum.doubleValue)
        
        if selectedDeal!.commision_included {
            commision_sum = 0
        }
        
        if (!deal.commision_included) {
            amount -= commision_sum
        }
        
        amount = Double(String(format: "%.2f", abs(amount)))!
        
        var title = title
        if (withPrice) {
            title += " ( > \(amount)$ )"
        }
        
        customAlert.config = AlertCustomData(title: title, firstSegment: "Yes".localized(), secondSegment: "No".localized(), amount: 10)
        
        self.selectedDeal = deal
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func acceptOrDecline(deal:DealData, accept:Bool, isNeedUpdate:Bool = true) {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        APIManager.sharedInstance.buyDeal(id: deal.id, additionalServices: [], accept: accept, onSuccess: { (data) in
            
            guard let status = data["status"]?.number, status == 1 else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["data"]?.string
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    
                    hud.dismiss(afterDelay: 1.5, animated: true)
                    
                    if data["status"]?.number == 50 {
                        self.isAcceptStep = false
                        self.presentAlertView(deal: deal, title: "payment_replenishBalance".localized(), withPrice: true)
                    }
                    else {
                        self.isAcceptStep = false
                    }
                })
                
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = data["data"]?.string
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            if isNeedUpdate {
                self.getPagaData()
            }
            
            hud.dismiss(afterDelay: 1.0, animated: true)
        }, viewController: self)
    }
    
    @objc func showLibrary(_ sender: UIButton) {
        self.library.removeAll()
        
        guard let urls = self.productListData[sender.tag].shipingData.first?["photosUrl"].array else {
            return
        }
        
        for url in urls {
            guard let photoUrl = URL(string: url.string!) else { return }
            guard let photoData = try? Data(contentsOf: photoUrl) else { return }
            guard let photoImage = UIImage(data: photoData) else { return }
            self.library.append(photoImage)
        }
        
        guard library.count > 0 else {
            return
        }
        
        let lightboxImages = library.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.footerView.isHidden = true
        lightbox.modalPresentationStyle = .fullScreen
        self.present(lightbox, animated: true)
    }
    
    
    func getPagaData(updateTable:Bool = false, beginRefresh: Bool = true) {
        
        fetchingMore = true
        
        if (beginRefresh) {
            self.refreshControl.beginRefreshing()
        }
        
        self.paymentsTable.backgroundView = UIView()
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            
            switch self.dealType {
            
            case .me:
                APIManager.sharedInstance.getUserDeals(active: true, type: .purchase, page: self.page, onSuccess:  { (data) in
                    
                    var count = 0
                    var isChanged = false
                    
                    if let response = data["data"]?.array, !response.isEmpty  {
                        count = response.count
                        
                        response.forEach { value in
                            guard let product = value.dictionary?["product"],
                                  let weight = product["weight"].number,
                                  let sum = product["price"].number,
                                  let seller = product["seller"].dictionary,
                                  let sellerName = seller["name"]?.string,
                                  let imagesUrl = product["photoUrl"].array,
                                  let id = value.dictionary?["id"]?.number,
                                  let paymentStatus = value.dictionary?["paymentStatus"]?.string,
                                  let customerAccept = value.dictionary?["customerAccept"]?.bool,
                                  let other = value.dictionary?["dealStatus"]?.string,
                                  let sellerAccept = value.dictionary?["sellerAccept"]?.bool,
                                  let isPrivate = value.dictionary?["private"]?.bool,
                                  let commission_included = value.dictionary?["commissionIncluded"]?.bool
                            else {
                                return
                            }
                            
                            let title = product["productTitle"].string
                            
                            let serviceses =  value.dictionary?["additionalServices"]?.array ?? []
                            let commsion_for_sum = value.dictionary?["commissionForSum"]?.number ?? 0
                            let commsion_for_gram = value.dictionary?["commissionForGramm"]?.number ?? 0
                            let customer:String = value.dictionary?["customer"]?.dictionary?["name"]?.string ?? ""
                            let priceForGramm = self.concat(lhs: weight, rhs: sum)
                            let anulateData = DealAnulateData(isSellerAnulate: value.dictionary?["sellerAnnulate"]?.bool ?? false, isCustomerAnulate: value.dictionary?["customerAnnulate"]?.bool ?? false, anulateReson: value.dictionary?["annulateReason"]?.string ?? "")
                            let shipingData = value.dictionary?["shippingStatuses"]?.array ?? [JSON]()
                            let created = product["productCreatedAt"].number ?? 0
                            let updated = value.dictionary?["updatedAt"]?.number ?? 0
                            
                            let termsData =  TermsData(needsToAcceptNewTerms: value.dictionary?["needsToAcceptNewTerms"]?.bool, newTerms: value.dictionary?["newTerms"]?.dictionary, customerAcceptNewTerms: value.dictionary?["customerAcceptNewTerms"]?.bool, sellerAcceptNewTerms: value.dictionary?["sellerAcceptNewTerms"]?.bool)
                            let category = value.dictionary?["productCategory"]

                            if !self.productID.contains(id) {
                                isChanged = true
                                let data = DealData(id: id, productTitle: title ?? "", productWeight: "\(weight)", productSeller: sellerName, productCustomer: customer, productGramSum: priceForGramm, productSum: "\(sum)", productImages: [], productPayStatus: paymentStatus, shipingData: shipingData, otherStatus: other, type: .purchase, productImagesURLs: imagesUrl, customerAccept: customerAccept, sellerAccept: sellerAccept, anulateData: anulateData, isPrivate: isPrivate, commision_included: commission_included, commissionForGramm: "\(commsion_for_gram)", commissionForSum: "\(commsion_for_sum)",termsDaata: termsData, productCreated: created as? Double, productUpdated: updated as? Double, services: serviceses, category: category)
                                
                                if Int(truncating: data.id) > Int(truncating: self.productListData.first?.id ?? 0) {
                                    self.productListData.insert(data, at: 0)
                                }
                                else {
                                    self.productListData.append(data)
                                }
                                
                                self.productID.append(id)
                            }
                        }
                        
                        if isChanged {
                            self.page += 1
                        }
                    }
                    
                    if self.productListData.isEmpty {
                        
                        if (!self.isResponseTry) {
                            self.isResponseTry = true
                            self.getPagaData()
                        }
                        else {
                            self.isResponseTry = false
                            self.setTableViewEmptyBackground()
                            self.productListData.removeAll()
                            self.productID.removeAll()
                            self.paymentsTable.reloadData()
                        }
                    }
                    else {
                        if isChanged || updateTable {
                            if count < 7 && self.page > 0  {
                                self.page -= 1
                            }
                            
//                            self.productListData = self.productListData.sorted { $0.productUpdated > $1.productUpdated}
                            self.paymentsTable.reloadData()
                        }
                        else {
                            if self.page > 0  {
                                self.page -= 1
                            }
                        }
                        
                        self.isResponseTry = false
                    }
                    
                    self.fetchingMore = false
                    

                    self.refreshControl.endRefreshing()
                }, viewController: self)
            case .other:
                break
            case .archive:
                APIManager.sharedInstance.getUserDeals(active: false, type: .purchase, page: self.archivePage, onSuccess: { (data) in
                    var count = 0
                    var isChanged = false
                    if let response = data["data"]?.array, !response.isEmpty  {
                        count = response.count
                        
                        response.forEach { value in
                            
                            guard let product = value.dictionary?["product"],
                                  let title = product["productTitle"].string,
                                  let weight = product["weight"].number,
                                  let sum = product["price"].number,
                                  let seller = product["seller"].dictionary,
                                  let sellerName = seller["name"]?.string,
                                  let imagesUrl = product["photoUrl"].array,
                                  let id = value.dictionary?["id"]?.number,
                                  let paymentStatus = value.dictionary?["paymentStatus"]?.string,
                                  let customerAccept = value.dictionary?["customerAccept"]?.bool,
                                  let other = value.dictionary?["dealStatus"]?.string,
                                  let sellerAccept = value.dictionary?["sellerAccept"]?.bool,
                                  let isPrivate = value.dictionary?["private"]?.bool,
                                  let commission_included = value.dictionary?["commissionIncluded"]?.bool
                            else {
                                return
                            }
                            
                            let serviceses =  value.dictionary?["additionalServices"]?.array ?? []
                            let commsion_for_sum = value.dictionary?["commissionForSum"]?.number ?? 0
                            let commsion_for_gram = value.dictionary?["commissionForGramm"]?.number ?? 0
                            let customer:String = value.dictionary?["customer"]?.dictionary?["name"]?.string ?? ""
                            let priceForGramm = self.concat(lhs: weight, rhs: sum)
                            let anulateData = DealAnulateData(isSellerAnulate: true, isCustomerAnulate: true, anulateReson: value.dictionary?["annulationReason"]?.string ?? "")
                            let shipingData = value.dictionary?["shippingStatuses"]?.array ?? [JSON]()
                            let created = product["productCreatedAt"].number ?? 0
                            let updated = value.dictionary?["updatedAt"]?.number ?? 0
                            let category = value.dictionary?["productCategory"]

                            if !self.archiveProductID.contains(id) {
                                isChanged = true
                                let data = DealData(id: id, productTitle: title, productWeight: "\(weight)", productSeller: sellerName, productCustomer: customer, productGramSum: priceForGramm, productSum: "\(sum)", productImages: [], productPayStatus: paymentStatus, shipingData: shipingData, otherStatus: other, type: .purchase, productImagesURLs: imagesUrl, customerAccept: customerAccept, sellerAccept: sellerAccept, anulateData: anulateData, isPrivate: isPrivate, commision_included: commission_included, commissionForGramm: "\(commsion_for_gram)", commissionForSum: "\(commsion_for_sum)",productCreated: created as? Double,productUpdated: updated as? Double, services: serviceses, category: category)
                                
                                if Int(truncating: data.id) > Int(truncating: self.archiveListData.first?.id ?? 0) {
                                    self.archiveListData.insert(data, at: 0)
                                }
                                else {
                                    self.archiveListData.append(data)
                                }
                                
                                self.archiveProductID.append(id)
                            }
                        }
                        
                        if isChanged {
                            self.archivePage += 1
                        }
                    }
                    
                    if self.archiveListData.isEmpty {
                        
                        if (!self.isArchiveResponseTry) {
                            self.isArchiveResponseTry = true
                            self.getPagaData()
                        }
                        else {
                            self.isArchiveResponseTry = false
                            self.setTableViewEmptyBackground()
                            self.archiveListData.removeAll()
                            self.archiveProductID.removeAll()
                            self.paymentsTable.reloadData()
                        }
                    }
                    else {
                        
                        if isChanged || updateTable {
                            if count < 7 && self.archivePage > 0  {
                                self.archivePage -= 1
                            }
                            
//                            self.archiveListData = self.archiveListData.sorted { $0.productUpdated > $1.productUpdated}
                            self.paymentsTable.reloadData()
                        }
                        else {
                            if self.archivePage > 0  {
                                self.archivePage -= 1
                            }
                        }
                        
                        self.isArchiveResponseTry = false
                    }
                    
                    self.fetchingMore = false                    
                    self.refreshControl.endRefreshing()
                }, viewController: self)
            }
        }
    }
    
    @objc func refresh(_ sender: Any?) {
        if !fetchingMore {
            
            fetchingMore = true
            
            if segmentTitle.selectedSegmentIndex == 0 {
                productListData = [DealData]()
                productID = [NSNumber]()
                page = 0
            }
            else if segmentTitle.selectedSegmentIndex == 1 {
                archiveListData = [DealData]()
                archiveProductID = [NSNumber]()
                archivePage = 0
            }
            self.getPagaData(updateTable: true, beginRefresh: true)
        }
        else {
            self.refreshControl.endRefreshing()
        }
    }
    
    
    func showFilter() {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "FIlterAlertView") as! FIlterAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.style = .myDeals
        customAlert.config = self.selectedFilters
        self.present(customAlert, animated: true, completion: nil)
    }
    
    @IBAction func showFilter(_ sender:UIButton) {
        self.showFilter()

    }
    
    func setupViewConfiguration() {
        
        paymentsTable.allowsMultipleSelectionDuringEditing = true
        acceptDeclineView.alpha = 0
        segmentTitle.setTitle("menu_3".localized(), forSegmentAt: 0)
        segmentTitle.setTitle("deal_third_product".localized(), forSegmentAt: 1)
        
        refreshControl = AARefreshControl(scrollView: paymentsTable)
        refreshControl.tintColor = UIColor.black
        refreshControl.activityIndicatorViewColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        paymentsTable.addSubview(refreshControl)
        
        paymentsTable.register(UINib(nibName: "PurchasesCell", bundle: nil), forCellReuseIdentifier: "purchases")
        self.paymentsTable.rowHeight = UITableView.automaticDimension
        self.paymentsTable.estimatedRowHeight = 250
        self.paymentsTable.layer.cornerRadius = 10
    }
    
    @objc func acceptDeal(_ sender:UIButton) {
        
        isAcceptStep = true
        selectedDeal = productListData[sender.tag]
        accept = true
        
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        
        customAlert.config = AlertCustomData(title: "Accept".localized(), firstSegment: "Yes".localized(), secondSegment: "No".localized(), amount: 1)
        self.present(customAlert, animated: true, completion: nil)
        
    }
    
    @objc func declineDeal(_ sender:UIButton) {
        selectedDeal = productListData[sender.tag]
        accept = false
        
        isAcceptStep = true
        
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        
        customAlert.config = AlertCustomData(title: "deal_decline".localized(), firstSegment: "Yes".localized(), secondSegment: "No".localized(), amount: 1)
        self.present(customAlert, animated: true, completion: nil)
    }
}

extension PurchaseList: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.dealType == .me ? productListData.count : archiveListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "purchases", for: indexPath) as! PurchasesCell
        
        let data = self.dealType == .me ? productListData : archiveListData
        
        if (data.count > indexPath.row) {
            cell.setupViewConfiguration(data: data[indexPath.row])
            
            cell.payBtn.addTarget(self, action: #selector(acceptDeal(_:)), for: .touchUpInside)
            cell.payBtn.tag = indexPath.row
            
            cell.declineBtn.addTarget(self, action: #selector(declineDeal(_:)), for: .touchUpInside)
            cell.declineBtn.tag = indexPath.row
            
            if (data[indexPath.row].sellerAccept && data[indexPath.row].customerAccept || self.dealType == .archive) {
                cell.selectionStyle = .none
            }
            
            let images = data[indexPath.row].productImagesURLs
            
            if let urlStr = images.last?.string, let url = URL(string: urlStr) {
                
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    cell.productImage.image = image
                }
                else {
                    cell.productImage.downloadImageFrom(url: url, contentMode: .scaleAspectFill)
                }
            }
            else {
                cell.productImage.image = UIImage(named: "empty_product")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var data = [DealData]()
        switch  segmentTitle.selectedSegmentIndex{
        case 0:
            data = self.productListData
            
        case 1:
            data = self.archiveListData
            
        default:
            break
        }
        
        if (indexPath.row == data.count - 1) {
            if !fetchingMore {
                if (indexPath.row > 4) {
                    self.paymentsTable.tableFooterView?.isHidden = false
                    getPagaData(updateTable: false, beginRefresh: false)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !isPurchaseDataEditing {
            let vc = storyboard?.instantiateViewController(withIdentifier: "previewDeal") as! DealPreview
            
            let data = self.dealType == .me ? productListData : archiveListData
            
            let termsDaata = (data[indexPath.row].termsDaata?.needsToAcceptNewTerms ?? false && data[indexPath.row].termsDaata?.customerAcceptNewTerms == nil) ? data[indexPath.row].termsDaata : nil
            
            vc.productData = DealData(id: data[indexPath.row].id, productTitle: data[indexPath.row].productTitle, productWeight: data[indexPath.row].productWeight, productSeller: data[indexPath.row].productSeller, productCustomer: data[indexPath.row].productCustomer, productGramSum: data[indexPath.row].productGramSum.replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil), productSum: data[indexPath.row].productSum.replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil), productImages:data[indexPath.row].productImages, productPayStatus: data[indexPath.row].productPayStatus, shipingData: data[indexPath.row].shipingData, otherStatus: data[indexPath.row].otherStatus, type: .purchase, productImagesURLs: data[indexPath.row].productImagesURLs, customerAccept: data[indexPath.row].customerAccept, sellerAccept: data[indexPath.row].sellerAccept, anulateData: data[indexPath.row].anulateData, isPrivate: data[indexPath.row].isPrivate, commision_included: data[indexPath.row].commision_included, commissionForGramm: data[indexPath.row].commissionForGramm, commissionForSum: data[indexPath.row].commissionForSum,termsDaata: termsDaata, services: data[indexPath.row].services, category: data[indexPath.row].category)
            
            vc.customer = SellerData(id: 0, name: data[indexPath.row].productCustomer, phone: "", nickName: "")
            
            navigationController?.pushViewController(vc, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
            
            isPushPresent = true
        }
        else {
            if tableView.indexPathsForSelectedRows?.count > 0 {
                if (productListData[indexPath.row].sellerAccept && productListData[indexPath.row].customerAccept) {
                    paymentsTable.deselectRow(at: indexPath, animated: false)
                }
                else {
                    if tableView.indexPathsForSelectedRows?.count > 0 {
                        UIView.animate(withDuration: 0.5) {
                            self.acceptDeclineView.alpha = 1
                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if tableView.indexPathsForSelectedRows?.count == nil {
            UIView.animate(withDuration: 0.5) {
                self.acceptDeclineView.alpha = 0
            }
        }
    }
    
    @IBAction func acceptOrDeclineMultipleDeal(_ sender:UIButton) {
        
        accept = sender.tag == 1
        selectedIndexPath = paymentsTable.indexPathsForSelectedRows
        
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        
        customAlert.config = AlertCustomData(title: isAcceptStep ? "Accept_single".localized() : "Decline".localized(), firstSegment: "Yes".localized(), secondSegment: "No".localized(), amount: 1)
        self.present(customAlert, animated: true, completion: nil)
        
        isMultipleSelectedType = true
        isAcceptStep = false
    }
}

extension PurchaseList:EmptyTableViewDelegate {
    func refreshData() {
        if !fetchingMore {
            self.getPagaData(updateTable: true,beginRefresh: true)
        }
        else {
            self.refreshControl.endRefreshing()
        }
    }
}

extension PurchaseList:CustomAlertViewDelegate {
    func cancelButtonTapped() {}
    
    func okButtonTapped(count: String) {
        
        if isAcceptStep {
            guard let deal = self.selectedDeal else {
                return
            }
            self.acceptOrDecline(deal: deal, accept: self.accept)
            isMultipleSelectedType = false
        }
        else if isMultipleSelectedType {
            var last_id:NSNumber?

            if let last_row = self.selectedIndexPath?.last?.row {
                last_id = productListData[last_row].id
            }
            
            self.selectedIndexPath?.forEach({ (indexPath) in
                let id = productListData[indexPath.row].id
                let isNeedUpdate = last_id != nil ? (last_id == id) : false
                self.acceptOrDecline(deal: productListData[indexPath.row], accept: self.accept,isNeedUpdate: isNeedUpdate)
            })
        }
        else {
            isMultipleSelectedType = false
            
            guard let deal = self.selectedDeal else {
                return
            }
            
            var commision_sum = (deal.productWeight.doubleValue * deal.commissionForGramm.doubleValue  +  deal.productSum.doubleValue * deal.commissionForSum.doubleValue)
            
            if selectedDeal!.commision_included {
                commision_sum = 0
            }
            
            DispatchQueue.main.async {
                let newView = self.storyboard?.instantiateViewController(withIdentifier: "selectReplenishType_vc") as! SelectReplenishType
                var amount = Double(UserProfileData.wallet?.balance ?? 0) + Double(UserProfileData.wallet?.creditLimit ?? 0) - Double(UserProfileData.wallet?.debt ?? 0) - deal.productSum.doubleValue
                
                if (!deal.commision_included) {
                    amount -= commision_sum
                }
                
                amount = Double(String(format: "%.2f", abs(amount)))!
                
                newView.amount = amount
                newView.modalPresentationStyle = UIModalPresentationStyle.popover
                self.navigationController?.pushViewController(newView, animated: true)
            }
        }
    }
}

extension PurchaseList:PurchaseDelegate {
    
    func editPurchaseData(button: UIBarButtonItem) {
        
        if !isPurchaseDataEditing {
            button.title = "Cancel".localized()
            paymentsTable.isEditing = true
            segmentTitle.isEnabled = false
        }
        else {
            button.title = "settings_edit".localized()
            paymentsTable.isEditing = false
            UIView.animate(withDuration: 0.5) {
                self.acceptDeclineView.alpha = 0
            }
            segmentTitle.isEnabled = true
        }
        
        isPurchaseDataEditing = !isPurchaseDataEditing
    }
    
    func candelEditPurchaseData(button: UIBarButtonItem) {
        
        if !isMultipleSelected {
            button.title = "Deselect_all".localized()
            
            for row in  0..<productListData.count {
                let indexPath = IndexPath(row: row, section: 0)
                
                if !(productListData[indexPath.row].sellerAccept && productListData[indexPath.row].customerAccept) {
                    paymentsTable.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                }                
            }
            
            UIView.animate(withDuration: 0.5) {
                self.acceptDeclineView.alpha = 1
            }
            segmentTitle.isEnabled = false

        }
        else {
            button.title = "Select_all".localized()
            for row in  0..<productListData.count {
                paymentsTable.deselectRow(at: IndexPath(row: row, section: 0), animated: true)
            }
            
            UIView.animate(withDuration: 0.5) {
                self.acceptDeclineView.alpha = 0
            }
            segmentTitle.isEnabled = true

        }
        
        isMultipleSelected = !isMultipleSelected
    }
}


extension PurchaseList:FilterAlertDelegate {
    
    func getFilterPageData() {
        
        guard let filters = self.selectedFilters else {
            return
        }
        
        let  sort = ["price", "weight"]
        let  direction = ["ASK", "DESK"]

        let data = [
            "title":filters.productName,
            "weightLowLimit":filters.weightFrom,
            "weightHighLimit":filters.weightTo,
            "priceLowLimit":filters.priceFrom,
            "priceUpLimit":filters.priceTo,
            "active":true,
            "customerId": UserProfileData.id!,
            "sort": sort[filters.sort.rawValue],
            "sortDirection":direction[filters.sortDirection.rawValue],

        ] as [String : Any]
        
        APIManager.sharedInstance.getFiltersDeals(page: self.page, data: data, onSuccess: { (data) in
           
            var isChanged = false
            var count = 0
            
            if let response = data["data"]?.array, !response.isEmpty  {
                count = response.count
                
                response.forEach { value in
                    guard let product = value.dictionary?["product"],
                          let title = product["productTitle"].string,
                          let weight = product["weight"].number,
                          let sum = product["price"].number,
                          let seller = product["seller"].dictionary,
                          let sellerName = seller["name"]?.string,
                          let imagesUrl = product["photoUrl"].array,
                          let id = value.dictionary?["id"]?.number,
                          let pay = value.dictionary?["paymentStatus"]?.string,
                          let customerAccept = value.dictionary?["customerAccept"]?.bool,
                          let other = value.dictionary?["dealStatus"]?.string,
                          let sellerAccept = value.dictionary?["sellerAccept"]?.bool,
                          let isPrivate = value.dictionary?["private"]?.bool,
                          let commission_included = value.dictionary?["commissionIncluded"]?.bool
                    else { return }
                    
                    let serviceses = value.dictionary?["additionalServices"]?.array ?? []
                    let commsion_for_sum = value.dictionary?["commissionForSum"]?.number ?? 0
                    let commsion_for_gram = value.dictionary?["commissionForGramm"]?.number ?? 0
                    let customer:String = value.dictionary?["customer"]?.dictionary?["name"]?.string ?? ""
                    let shipingData = value.dictionary?["shippingStatuses"]?.array ?? [JSON]()
                    let created = product["productCreatedAt"].number ?? 0
                    let updated = value.dictionary?["updatedAt"]?.number ?? 0
                    let termsData =  TermsData(needsToAcceptNewTerms: value.dictionary?["needsToAcceptNewTerms"]?.bool, newTerms: value.dictionary?["newTerms"]?.dictionary, customerAcceptNewTerms: value.dictionary?["customerAcceptNewTerms"]?.bool, sellerAcceptNewTerms: value.dictionary?["sellerAcceptNewTerms"]?.bool)
                    
                    let priceForGramm = self.concat(lhs: weight, rhs: sum)
                    let anulateData = DealAnulateData(isSellerAnulate: value.dictionary?["sellerAnnulate"]?.bool ?? false, isCustomerAnulate: value.dictionary?["customerAnnulate"]?.bool ?? false, anulateReson: value.dictionary?["annulationReason"]?.string ?? "")
                    
                    let category = value.dictionary?["productCategory"]
                    
                    if !self.productID.contains(id) {
                        isChanged = true
                        let sum = "\(sum)"
                        
                        let data = DealData(id: id, productTitle: title, productWeight: "\(weight)", productSeller: sellerName, productCustomer: customer, productGramSum: priceForGramm, productSum: sum, productImages: [], productPayStatus: pay, shipingData: shipingData, otherStatus: other, type: .exist, productImagesURLs: imagesUrl, customerAccept: customerAccept, sellerAccept: sellerAccept, anulateData: anulateData, isPrivate: isPrivate, commision_included: commission_included, commissionForGramm: "\(commsion_for_gram)", commissionForSum: "\(commsion_for_sum)",termsDaata: termsData,productCreated: created as? Double,productUpdated: updated as? Double, services: serviceses, category: category)
                        
                        if Int(truncating: data.id) > Int(truncating: self.productListData.first?.id ?? 0) {
                            self.productListData.insert(data, at: 0)
                        }
                        else {
                            self.productListData.append(data)
                        }
                        
                        self.productID.append(id)
                    }
                }
                
                if isChanged {
                    self.page += 1
                }
            }
            
            if self.productListData.isEmpty {
                
                if (!self.isResponseTry) {
                    self.isResponseTry = true
                    self.getFilterPageData()
                }
                else {
                    self.isResponseTry = false
                    self.setTableViewEmptyBackground()
                    self.productListData.removeAll()
                    self.productID.removeAll()
                }
            }
            else {
                if isChanged {
                    if count < 7 && self.page > 0 {
                        self.page -= 1
                    }
                    
//                    self.productListData = self.productListData.sorted { $0.productUpdated > $1.productUpdated}
                }
                
                self.isResponseTry = false
            }
            self.paymentsTable.tableFooterView?.isHidden = true
            self.fetchingMore = false
            self.paymentsTable.isUserInteractionEnabled = true
            self.refreshControl.endRefreshing()
        }, viewController: self)
    }
    
    func setFilter(filters: Filters) {
        self.isResponseTry = false
        self.selectedFilters = filters
        self.page = 0
        self.productListData.removeAll()
        self.productID.removeAll()
        
        self.getFilterPageData()
    }
    
    func annulateFilters() {
        self.isResponseTry = false
        self.selectedFilters = nil
        self.productListData.removeAll()
        self.productID.removeAll()
        self.getPagaData()
    }
}
