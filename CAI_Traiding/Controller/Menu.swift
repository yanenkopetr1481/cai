//
//  Menu.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/28/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit

protocol BadgeDelegate {
    func setBadgeCoun(count:Int)
}

class Menu: RootViewController {
    
    private let reuseIdentifer = "menu"
    
    var tableView: UITableView!
    var delegate: HomeControllerDelegate?
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func reloadBadgeCell() {
        let path = tableView.indexPathForSelectedRow
        tableView.reloadRows(at: [IndexPath(row: 2, section: 0),IndexPath(row: 3, section: 0),IndexPath(row: 5, section: 0),IndexPath(row: 1, section: 0)], with: .none)
        tableView.selectRow(at: path, animated: false, scrollPosition: .none)
    }
    
    func configureTableView() {
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: reuseIdentifer)
        tableView.backgroundColor = UIColor(red: 0.165, green: 0.165, blue: 0.165, alpha: 1)
        
        tableView.separatorStyle = .none
        tableView.rowHeight = 50
        
        view.addSubview(tableView)
        tableView.selectRow(at: IndexPath(row: 1, section: 0), animated: true, scrollPosition: .none)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor,constant: -30).isActive = true
        tableView.bottomAnchor.constraint(equalTo: super.view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension Menu:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifer, for: indexPath) as! MenuCell
        
        if (indexPath.row != 0) {
            let menuOption = MenuOption(rawValue: indexPath.row)
            cell.setupViewCinfiguration(_title:menuOption!.description, _tag: indexPath.row, _iconName: menuOption!.image)
            
            let backgroundView = UIView(frame: CGRect(x: 0, y: 5, width: cell.frame.width, height: cell.frame.height - 20))
            backgroundView.backgroundColor = UIColor.red
            backgroundView.layer.cornerRadius = 25
            cell.layer.cornerRadius = 25
            cell.selectedBackgroundView = backgroundView
        }
        else {
            cell.isUserInteractionEnabled = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuOption = MenuOption(rawValue: indexPath.row)
        delegate?.handleMenuToggle(forMenuOption: menuOption)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 1 {
            setBadge(cell as! MenuCell, indexPath: indexPath)
        }
    }
    
    func setBadge(_ cell: MenuCell, indexPath:IndexPath) {
        
        var selected = false
        if cell.isSelected {
            selected = true
        }
                
        var index = 0
        let type = BadgeType(rawValue: "\(indexPath.row)")
        
        if type != nil, let badgeCount = AppLocalData.BadgesCount[type!] {
            index = badgeCount
        }
        
        cell.setBadge(_notificationCount: index, _isSelected: selected)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 40 : 50
    }
}
