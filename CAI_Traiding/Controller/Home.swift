//
//  Home.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/28/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import SkeletonView
import Alamofire
import KeychainSwift
import BadgeControl
import SwiftyJSON
import SOPullUpView

class Home: RootViewController {
    
    @IBOutlet weak var helloLable: UILabel!
    @IBOutlet weak var usedCreditLimitTitle: UILabel!
    @IBOutlet weak var creditLimitTitle: UILabel!
    @IBOutlet weak var balanceTitle: UILabel!
    @IBOutlet weak var creditDaysLeft: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var imageBtn: LoadingButton!
    @IBOutlet weak var balanceBtn: UIButton!
    @IBOutlet weak var creditBtn: UIButton!
    @IBOutlet weak var debtExpired: UIButton!
    @IBOutlet weak var buttonStack: UIStackView!
    
    let defaults = UserDefaults.standard
    let keychain = KeychainSwift()
    let pullUpController = SOPullUpControl()
    private var notificationBadge: BadgeController!

    var isPullImploded:Bool = false
    var isLoadedView:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateUserInfo()
        changeDate()
        switchPageData(balanceBtn)
        NotificationCenter.default.addObserver(self, selector: #selector(onReceiveData), name: NSNotification.Name(rawValue: "ReceiveData"), object: nil)
        
        notificationBadge = BadgeController(for: creditBtn)
        notificationBadge.animation = BadgeAnimations.rightLeft
        notificationBadge.badgeHeight = 30
        notificationBadge.badgeTextFont = UIFont(name: "Helvetica Neue", size: 12)!
        notificationBadge.badgeBackgroundColor = .red
        notificationBadge.badgeTextColor = .white
        
        if AppLocalData.BadgesCount[.credit] > 0  {
            switchPageData(creditBtn)
            notificationBadge.addOrReplaceCurrent(with: "\(AppLocalData.BadgesCount[.credit]!)", animated: true)
        }
        
        setUserImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeBadge()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReceiveData"), object: nil)
        
    }
    
    func removeBadge() {
        if notificationBadge != nil {
            notificationBadge.remove(animated: true)
        }
    }
    
    //_ notification:Notification)
    @objc func onReceiveData() {
        updateUserInfo()
        if AppLocalData.BadgesCount[.credit] > 0  {
             notificationBadge.addOrReplaceCurrent(with: "\(AppLocalData.BadgesCount[.credit]!)", animated: true)
         }
    }
    
    // used to return bottom Padding of safe area.
    var bottomPadding: CGFloat {
        let window = UIApplication.shared.keyWindow
        return window?.safeAreaInsets.top ?? 0.0
    }
    
    func configureView() {
        
        if (!isPullImploded) {
            pullUpController.dataSource = self
            pullUpController.setupCard(from: view)
            isPullImploded = true
        }
        
        creditLimitTitle.text = "home_credit_limit".localized() + " \(UserProfileData.wallet?.creditLimit ?? 0) $"
        usedCreditLimitTitle.text = "home_user_credit".localized() + "\n \(UserProfileData.wallet?.debt ?? 0) $"
        balanceTitle.text = "home_user_balance".localized() + " \(UserProfileData.wallet?.balance ?? 0) $"
        helloLable.text = "home_hello".localized() + "\(UserProfileData.name ?? "")\n" + "home_wellcome".localized()
        creditDaysLeft.text = "home_credit_limit_not_used".localized()
                
        if (UserProfileData.name == nil) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "language") as! SetLanguage
            vc.modalPresentationStyle = .fullScreen
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                
                ApiConfigurationData.accessToken = ""
                ApiConfigurationData.expiresIn = ""
                ApiConfigurationData.refreshToken = ""
                ApiConfigurationData.contacNumber = [String]()
                
                UserProfileData.creditCard = nil
                UserProfileData.commissionForSum = nil
                UserProfileData.commissionForGramm = nil
                UserProfileData.name = nil
                UserProfileData.phone = nil
                UserProfileData.image = nil
                UserProfileData.wallet = nil
                UserProfileData.address = nil
                UserProfileData.language = nil
                UserProfileData.imageUrl = nil
                UserProfileData.nickname = nil
                UserProfileData.countryName = nil
                UserProfileData.countryID = nil
                AppLocalData.Countries = nil
                AppLocalData.BadgesCount = [.deal:Int(),.payment:Int(),.purchase:Int(),.credit:Int()]
                UIApplication.shared.applicationIconBadgeNumber = 0
                
                self.defaults.removeObject(forKey: "AppLocalBadges")
                
                self.keychain.accessGroup = "A5MXJN8KJZ.com.iPiterGroup.TestCaiTraidingApp"
                if let _ = self.keychain.get("refresh") {
                    self.keychain.delete("refresh")
                }
                
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func switchPageData(_ sender:UIButton) {
        
        switch sender.tag {
        case 0:
            removeBadge()
            balanceTitle.isHidden = false
            
            usedCreditLimitTitle.isHidden = true
            creditLimitTitle.isHidden = true
            creditDaysLeft.isHidden = true
            
            debtExpired.setImage(UIImage(named: "Group 1044-n"), for: .normal)
            creditBtn.setImage(UIImage(named: "Group 1043-n"), for: .normal)
            sender.setImage(UIImage(named: "Group 1042"), for: .normal)
        case 1:
            
            balanceTitle.isHidden = true
            
            usedCreditLimitTitle.isHidden = false
            creditLimitTitle.isHidden = false
            creditDaysLeft.isHidden = true
            
            balanceBtn.setImage(UIImage(named: "Group 1042-n"), for: .normal)
            debtExpired.setImage(UIImage(named: "Group 1044-n"), for: .normal)
            sender.setImage(UIImage(named: "Group 1043"), for: .normal)
        case 2:
            removeBadge()
            balanceTitle.isHidden = true
            
            usedCreditLimitTitle.isHidden = true
            creditLimitTitle.isHidden = true
            
            changeDate()
            creditDaysLeft.isHidden = false
            
            balanceBtn.setImage(UIImage(named: "Group 1042-n"), for: .normal)
            creditBtn.setImage(UIImage(named: "Group 1043-n"), for: .normal)
            sender.setImage(UIImage(named: "Group 1044"), for: .normal)
        default:
            break
        }
    }
    
    func changeDate() {
        let normalDate = UserProfileData.wallet?.debtExpired!.components(separatedBy: "T")
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let date1 = formatter.date(from: normalDate![0])
        
        if (date1 != nil) {
            creditDaysLeft.text = "home_debt_expired".localized() + "\n \(normalDate![0])"
        }
        else {
            creditDaysLeft.text = "home_credit_limit_not_used".localized()
        }
    }
    
    func updateUserInfo() {
        APIManager.sharedInstance.getUserData(onSuccess: { (userData) in
            
            guard let verified = userData["verified"]?.bool, verified == true else  {
                return
            }
            
            UserDefaults.standard.set(true, forKey: "isUserVerified")
            
            guard let phone = userData["phone"]?.string, let wallet = userData["wallet"]?.dictionary,
                let commissionForSum = userData["commissionForSum"]?.double, let commissionForGramm = userData["commissionForGramm"]?.double  else {
                    return
            }
            
            let creditCard = userData["cardNumber"]?.string ?? ""
            
            let walletObj = UserProfileData.walletType(creditLimit: wallet["creditLimit"]?.double ?? 00, debt: wallet["debt"]?.double ?? 0.0, debtCreated: wallet["debtCreated"]?.string ?? "", debtExpired: wallet["debtExpired"]?.string ?? "", balance: wallet["balance"]?.double ?? 0.0)
            
            if let urlStr = userData["photoUrl"]?.array?.last?.string, let url = URL(string: urlStr) {
                UserProfileData.imageUrl = url
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                
                UserProfileData.creditCard = creditCard
                UserProfileData.commissionForSum = commissionForSum
                UserProfileData.commissionForGramm = commissionForGramm
                
                UserProfileData.wallet = walletObj
                UserProfileData.phone = phone
                
                let nickname = userData["nickName"]?.string
                UserProfileData.nickname = nickname
                
                guard let name = userData["name"]?.string  else {
                    return
                }
                
                UserProfileData.name = name
                
                self.configureView()
            })
        }, viewController: self)
    }
    
    func setUserImage() {
        
        self.imageBtn.showLoading()
        
        let urlString = UserProfileData.imageUrl?.absoluteString ?? "profile"
        guard let image = ApiConfigurationData.appImages.object(forKey: urlString as AnyObject) else {
            
            self.imageBtn.hideLoading()
            
            guard let imageUrl:URL = UserProfileData.imageUrl  else {
                UserProfileData.image = UIImage(named: "profile")
                self.userPhoto.image = UIImage(named: "profile")
                return
            }
            
            self.userPhoto.downloadImageFrom(url: imageUrl, contentMode: .scaleAspectFill)
            return
        }
        
        self.userPhoto.image = image as? UIImage
        self.imageBtn.hideLoading()
    }
}

extension Home: SOPullUpViewDataSource {
    
    func pullUpViewCollapsedViewHeight() -> CGFloat {
        var height = bottomPadding + 35
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                height += 35
                
            case 1334:
                height += 35
                
            case 1920, 2208:
                height += 35
                
            case 2436:
                print("iPhone X/XS/11 Pro")
                
            case 2688:
                print("iPhone XS Max/11 Pro Max")
                
            case 1792:
                print("iPhone XR/ 11 ")
                
            default:
                print("Unknown")
            }
        }
        return height
    }
    
    func pullUpViewController() -> UIViewController {
        guard let vc = UIStoryboard(name: "PickedPull", bundle: nil).instantiateInitialViewController() as? PickedPullUpViewController else { return UIViewController() }
        vc.pullUpControl = self.pullUpController
        vc.parentVc = self
        return vc
    }
    
    func pullUpViewExpandedViewHeight() -> CGFloat {
        return 500
    }
}
