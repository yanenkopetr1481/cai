//
//  Payments.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/14/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import AARefreshControl
import SwiftyJSON
import MFSDK
import JGProgressHUD
import PDFReader
import Alamofire
import Lightbox

class PaymentList: RootViewController {
    
    @IBOutlet weak var paymentsTable:UITableView!
    @IBOutlet weak var usedCreditLimitTitle: UILabel!
    @IBOutlet weak var creditLimitTitle: UILabel!
    @IBOutlet weak var creditDaysLeft: UILabel!
    @IBOutlet weak var balanceTitle: UILabel!
    @IBOutlet weak var btnCash: UIButton!
    @IBOutlet weak var btnCard: UIButton!
    
    var paymentData:[PaymentData] = [PaymentData]()
    var spaymentsID:[NSNumber] = [NSNumber]()
    var refreshControl: AARefreshControl!
    var isResponseTry:Bool = false
    var fetchingMore:Bool = false
    var page:Int = 0
    var invoice_id:Int = 0
    
    var library:[UIImage] = [UIImage]()
    var lightbox :LightboxController!
    
    var isPaymentShow:Bool = false
    var isReplenishShow:Bool = false
    
    var paymentImages:[JSON]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupViewConfiguration()
        configureFooter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkDeletedData()
        updateUserInfo()
        getPageData(updateTable: true)
    }
    
    func configureFooter() {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: paymentsTable.bounds.width, height: CGFloat(44))
        self.paymentsTable.tableFooterView = spinner
        self.paymentsTable.tableFooterView?.isHidden = true
    }
    
    func checkDeletedData() {
        paymentData = [PaymentData]()
        spaymentsID = [NSNumber]()
        page = 0
    }
    
    @IBAction func showReplenish() {
        let newView = self.storyboard?.instantiateViewController(withIdentifier: "replenish_vc") as! Replenish
        newView.amount = nil
        self.navigationController?.pushViewController(newView, animated: true)
    }
    
    @objc func refresh(_ sender: Any?) {
        if !fetchingMore {
            
            fetchingMore = true
            paymentData = [PaymentData]()
            spaymentsID = [NSNumber]()
            page = 0
            
            self.getPageData(updateTable: true, beginRefresh: true)
        }
        else {
            self.refreshControl.endRefreshing()
        }
    }
    
    func configureView() {
        creditLimitTitle.text = "home_credit_limit".localized() + ": \(UserProfileData.wallet?.creditLimit ?? 0) $"
        usedCreditLimitTitle.text = "home_user_credit".localized() + ": \(UserProfileData.wallet?.debt ?? 0) $"
        balanceTitle.text = "home_user_balance".localized() + ": \(UserProfileData.wallet?.balance ?? 0) $"
        btnCash.setTitle("btn_cash".localized(), for: .normal)
        btnCard.setTitle("btn_card".localized(), for: .normal)
        changeDate()
    }

    func changeDate() {
        let normalDate = UserProfileData.wallet?.debtExpired!.components(separatedBy: "T")
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let date1 = formatter.date(from: normalDate![0])
        
        if (date1 != nil) {
            creditDaysLeft.text = "home_debt_expired".localized() + "\n \(normalDate![0])"
        }
        else {
            creditDaysLeft.text = "home_credit_limit_not_used".localized()
        }
    }
    
    func showSimpleActionSheet() {
                
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "PaymentAlert") as! PaymentAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.amount = nil
        
        self.present(customAlert, animated: true, completion: nil)
    }
    
    private func setTableViewEmptyBackground() {
        UIView.animate(withDuration: 0.5) {
            let view = Bundle.main.loadNibNamed("EmptyTableView", owner: self, options: nil)?.first as! EmptyTableView
            view.titleLabel.text = "EmptyRessult".localized()
            view.refreshBtn.setTitle("Refresh".localized(), for: .normal)
            view.delegate = self
            view.imageView.image = UIImage(named: "empty")
            self.paymentsTable.backgroundView = view
            self.paymentsTable.isUserInteractionEnabled = true
        }
    }
    
    func updateUserInfo() {
        APIManager.sharedInstance.getUserData(onSuccess:{ (userData) in
            
            guard let verified = userData["verified"]?.bool, verified == true else  {
                return
            }
            
            UserProfileData.isVerified = true
            
            guard let phone = userData["phone"]?.string, let wallet = userData["wallet"]?.dictionary,
                let commissionForSum = userData["commissionForSum"]?.double, let commissionForGramm = userData["commissionForGramm"]?.double  else {
                    return
            }
            
            let creditCard = userData["cardNumber"]?.string ?? ""
            
            let walletObj = UserProfileData.walletType(creditLimit: wallet["creditLimit"]?.double ?? 0, debt: wallet["debt"]?.double ?? 0, debtCreated: wallet["debtCreated"]?.string ?? "", debtExpired: wallet["debtExpired"]?.string ?? "", balance: wallet["balance"]?.double ?? 0)
            
            if let urlStr = userData["photoUrl"]?.array?.last?.string, let url = URL(string: urlStr) {
                UserProfileData.imageUrl = url
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                
                UserProfileData.creditCard = creditCard
                UserProfileData.commissionForSum = commissionForSum
                UserProfileData.commissionForGramm = commissionForGramm
                
                UserProfileData.wallet = walletObj
                UserProfileData.phone = phone
                
                let nickname = userData["nickName"]?.string
                UserProfileData.nickname = nickname
                
                guard let name = userData["name"]?.string, let address = userData["address"]?.string  else {
                    return
                }
                
                UserProfileData.name = name
                UserProfileData.address = address
                
                self.configureView()
            })
        }, viewController: self)
    }
    
    func concat (lhs: NSNumber, rhs: NSNumber) -> String {
        let value = Double(exactly: rhs.doubleValue / lhs.doubleValue)
        return String(format: "%.2f", value!)
    }
    
    func getPageData(updateTable:Bool = false, beginRefresh: Bool = true) {
        
        self.paymentsTable.isUserInteractionEnabled = false
        fetchingMore = true
        
        if (beginRefresh) {
            self.refreshControl.beginRefreshing()
        }
        
        self.paymentsTable.backgroundView = UIView()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            var count = 0
            var isChanged = false
            
            APIManager.sharedInstance.getUserPayment(page: self.page, onSuccess: { (data) in
                if let response = data["data"]?.array, !response.isEmpty  {
                    count = response.count
                    
                    response.forEach { value in
                        
                        print(value)
                        
                        guard let sum = value["sum"].number,
                            let id = value["id"].number,let created = value["created"].number
                            else { return }
                        let updated = value["updated"].number
                        let approver = value["approver"].string ?? ""
                        let description = value["description"].string ?? "payment_showDeal".localized()
                        let payerName = value["payerName"].string ?? ""
                        let invoiceId = value["invoiceId"].number ?? 0
                        let paymentStatus = value["paymentStatus"].string ?? ""
                        var dealData:DealData?
                        
                        let changeBalanceReason = value["changeBalanceReason"].dictionary?["title"]?.string ?? ""
                        let type = value["paymentType"].string
                        
                        if value["adminReceiptPhotos"] != nil {
                            let s = 10
                        }
                        
//                        sum = String(format: "%.2f", sum).doubleValue

                        if let deal = value["deal"].dictionary {
                            guard let product = deal["product"]?.dictionary,
                                let productTitle = product["productTitle"]?.string,
                                let weight = product["weight"]?.number,
                                let productSum = product["price"]?.number,
                                let seller = product["seller"]?.dictionary,
                                let sellerName = seller["name"]?.string,
                                let imagesUrl = product["photoUrl"]?.array,
                                let id = value.dictionary?["id"]?.number,
                                let customerAccept = deal["customerAccept"]?.bool,
                                let pay = deal["paymentStatus"]?.string,
                                let other = deal["dealStatus"]?.string,
                                let sellerAccept = deal["sellerAccept"]?.bool,
                                let isPrivate = deal["private"]?.bool,
                                let commision_included = deal["commissionIncluded"]?.bool
                                else {
                                    return
                            }
                            
                            let priceForGramm = self.concat(lhs: weight, rhs: sum)
                            let serviceses =  value.dictionary?["additionalServices"]?.array ?? []

                            let commsion_for_sum = deal["commissionForSum"]?.double ?? 0
                            let commsion_for_gram = deal["commissionForGramm"]?.double ?? 0
                            let customer:String = deal["customer"]?.dictionary?["name"]?.string ?? ""
                            let anulateData = DealAnulateData(isSellerAnulate: deal["sellerAnnulate"]?.bool ?? false, isCustomerAnulate: deal["customerAnnulate"]?.bool ?? false, anulateReson: deal["annulationReason"]?.string ?? "")
                            let shipingData = deal["shippingStatuses"]?.array ?? [JSON]()
                            let category = value.dictionary?["productCategory"]

                            let termsData =  TermsData(needsToAcceptNewTerms: value.dictionary?["needsToAcceptNewTerms"]?.bool, newTerms: value.dictionary?["newTerms"]?.dictionary, customerAcceptNewTerms: value.dictionary?["customerAcceptNewTerms"]?.bool, sellerAcceptNewTerms: value.dictionary?["sellerAcceptNewTerms"]?.bool)
                            
                            dealData = DealData(id: id, productTitle: productTitle, productWeight: "\(weight)", productSeller: sellerName, productCustomer: customer, productGramSum: priceForGramm, productSum: "\(productSum)", productImages: [], productPayStatus: pay, shipingData:shipingData, otherStatus: other, type: .purchase, productImagesURLs: imagesUrl, customerAccept: customerAccept, sellerAccept: sellerAccept, anulateData: anulateData, isPrivate: isPrivate, commision_included: commision_included, commissionForGramm: "\(commsion_for_gram)", commissionForSum: "\(commsion_for_sum)",termsDaata: termsData, services: serviceses, category: category)
                        }
                        
                        let title = dealData == nil ? "payment_replenishBalance".localized() : "payment_purchasePay".localized()
                        let photos = value["photos"].array
                        let adminPhotos = value["adminReceiptPhotos"].array 
                        
                        if !self.spaymentsID.contains(id) {
                            isChanged = true
                            let data = PaymentData(paymentStatus: paymentStatus, id: id, sum: sum, invoiceId: invoiceId, description: description, title: title, payerName: payerName, approver: approver, dealData: dealData,type: type, reason: changeBalanceReason, created: created as? Double,updated: updated as? Double, photos: photos, adminPhotos: adminPhotos)
                            
                            if Int(truncating: data.id) > Int(truncating: self.paymentData.first?.id ?? 0) {
                                self.paymentData.insert(data, at: 0)
                            }
                            else {
                                self.paymentData.append(data)
                            }
                            
                            self.spaymentsID.append(id)
                        }
                    }
                    
                    if isChanged {
                        self.page += 1
                    }
                }
                
                if self.paymentData.isEmpty {
                    
                    if (!self.isResponseTry) {
                        self.isResponseTry = true
                        self.getPageData()
                    }
                    else {
                        self.updateUserInfo()
                        self.isResponseTry = false
                        self.setTableViewEmptyBackground()
                        self.paymentData.removeAll()
                        self.spaymentsID.removeAll()
                        self.paymentsTable.reloadData()
                    }
                }
                else {
                    
                    if (count < 10 && self.page > 0) {
                        self.page -= 1
                    }
                    
                    self.updateUserInfo()
                    
                    if (isChanged) {
                        self.paymentData = self.paymentData.sorted { $0.updated > $1.updated}
                        self.paymentsTable.reloadData()
                    }
                    
                    self.isResponseTry = false
                }
                
                self.fetchingMore = false
                self.paymentsTable.isUserInteractionEnabled = true
                self.paymentsTable.tableFooterView?.isHidden = true
                self.refreshControl.endRefreshing()
            }, viewController: self)
        }
    }
    
    func setupViewConfiguration() {
        
        refreshControl = AARefreshControl(scrollView: paymentsTable)
        refreshControl.tintColor = UIColor.black
        refreshControl.activityIndicatorViewColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        paymentsTable.addSubview(refreshControl)
        
        paymentsTable.register(UINib(nibName: "Payment", bundle: nil), forCellReuseIdentifier: "payment")
        self.paymentsTable.rowHeight = UITableView.automaticDimension
        self.paymentsTable.estimatedRowHeight = 180
        self.paymentsTable.layer.cornerRadius = 10
    }
    
    @IBAction func creditCard(_ sender:UIButton) {
        showSimpleActionSheet()
    }
    
    func getInvoice(id:Int) {
        let url = URL(string: APIManager.baseURLString + "/api/user/payments/replenish/invoice?id=\(id)")
        
        var request = URLRequest(url: url!, timeoutInterval: Double.infinity)
        request.addValue("Bearer \(ApiConfigurationData.accessToken)", forHTTPHeaderField: "Authorization")
        
        request.httpMethod = "GET"
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Download".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        AF.request(request).response {
            result in
            DispatchQueue.main.async {
                if let data = result.data {
                    let document = PDFDocument(fileData: data, fileName: "Invoice")
                    let item = UIBarButtonItem(image: UIImage(named: "back_white"), style: .plain, target: self, action: #selector(self.back))
                    let readerController = PDFViewController.createNew(with: document!, title: "Invoice", actionButtonImage: nil, actionStyle: .activitySheet, backButton: item, isThumbnailsEnabled: true, startPageIndex: 0)
                    
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Success".localized()
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    
                    self.navigationController?.pushViewController(readerController, animated: true)
                }
            }
        }
    }
    
    @objc func back() {
        _ = navigationController?.popViewController(animated: true)
    }
}

extension PaymentList: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if paymentData[indexPath.row].dealData != nil {
            let vc = storyboard?.instantiateViewController(withIdentifier: "previewDeal") as! DealPreview
            vc.productData = paymentData[indexPath.row].dealData
            navigationController?.pushViewController(vc, animated: true)
        }
        else if (paymentData[indexPath.row].paymentStatus == "AWAITING_FOR_RECEIPT" || paymentData[indexPath.row].paymentStatus == "AWAITING_FOR_APPROVE") {
            let vc = storyboard?.instantiateViewController(withIdentifier: "previewPlenish") as! ReplenishConfirm
            vc.paymentData = paymentData[indexPath.row]
            self.navigationController?.show(vc, sender: self)
        }
        else if (paymentData[indexPath.row].paymentStatus == "APPROVED" ){
            let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
            customAlert.providesPresentationContextTransitionStyle = true
            customAlert.definesPresentationContext = true
            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            customAlert.delegate = self
            customAlert.config = AlertCustomData(title: "payment_data".localized(), firstSegment: "payment_invoice".localized(), secondSegment: "payment_check".localized(),isTextFieldHidden: true,amount: 1, isCloseByTap: true)
            
            self.present(customAlert, animated: true, completion: nil)
            self.invoice_id = Int(truncating: paymentData[indexPath.row].invoiceId)
            self.paymentImages = paymentData[indexPath.row].photos
            
            if paymentData[indexPath.row].adminPhotos != nil && self.paymentImages != nil {
                self.paymentImages = self.paymentImages! + paymentData[indexPath.row].adminPhotos!
            }
            
            isReplenishShow = false
            isPaymentShow = true
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (indexPath.row == paymentData.count - 1) {
            if !fetchingMore {
                if (indexPath.row > 4) {
                    self.paymentsTable.tableFooterView?.isHidden = false
                    getPageData(updateTable: false, beginRefresh: false)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "payment", for: indexPath) as! PaymentCell
        cell.awakeFromNib()

        if (self.paymentData.count > indexPath.row) {
            cell.setupView(data: self.paymentData[indexPath.row])
        }
        return cell
    }
}

extension PaymentList:EmptyTableViewDelegate {
    func refreshData() {
        self.getPageData(updateTable: true)
    }
}

extension PaymentList:CustomAlertViewDelegate {
    
    func okButtonTapped(count: String) {
        
        if isReplenishShow {

        }
        else if isPaymentShow {
            self.getInvoice(id: self.invoice_id)
        }
        
        isReplenishShow = false
        isPaymentShow = false
    }
    
    func cancelButtonTapped() {
        
        if (isPaymentShow) {
            isReplenishShow = false
            isPaymentShow = false
            
            library.removeAll()
            
            var count = 0
            
            paymentImages?.forEach { urlJson in
                
                let url = URL(string: urlJson.string!)!
                
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    self.library.append(image)
                    count += 1
                    
                    if count == paymentImages?.count {
                        self.showLibrary()
                    }
                }
                else {
                    AF.request(url).response {
                        result in
                        DispatchQueue.main.async {
                            if let data = result.data {
                                let image = UIImage(data: data)!
                                self.library.append(image)
                                ApiConfigurationData.appImages.setObject(image, forKey: url.absoluteString as AnyObject)
                                count += 1
                                
                                if count == self.paymentImages?.count {
                                    self.showLibrary()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func showLibrary() {
        let lightboxImages = library.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.footerView.isHidden = true
        lightbox.modalPresentationStyle = .fullScreen
        self.present(lightbox, animated: true)
    }
}


extension PaymentList:PaymentAlertViewDelegate {

    func okButtonTapped(paymentMethod: Int, count:Double) {
        
        let request = MFExecutePaymentRequest(invoiceValue: Decimal(count), paymentMethod: paymentMethod, callBackUrl: "http://31.131.31.129:8080/api/user/payments/callbackTest", errorUrl: "http://31.131.31.129:8080/api/user/payments/callbackTest")
        
        request.displayCurrencyIso = .unitedStates_USD
        request.customerName = UserProfileData.name ?? ""
        request.userDefinedField = UserProfileData.nickname ?? ""
        
        MFPaymentRequest.shared.executePayment(request: request, apiLanguage: .english, completion: { (response, invoiceId) in
            switch response {
            case .success(let data):
                _ = self.navigationController?.popViewController(animated: true)
                print(data)
                let hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Success".localized()
                hud.vibrancyEnabled = true
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                hud.textLabel.text = "Success with invoice id \(String(describing: invoiceId))"
                hud.show(in: self.view)
                hud.dismiss(afterDelay: 1.5, animated: true)
                
            case .failure(let failError):
                let hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Error".localized()
                hud.vibrancyEnabled = true
                hud.indicatorView = JGProgressHUDErrorIndicatorView()
                
                if let invoiceId = invoiceId {
                    hud.textLabel.text = "Fail: \(failError.statusCode) with invoice id \(invoiceId)"
                } else {
                    hud.textLabel.text = "Fail: \(failError.statusCode)"
                }
                
                hud.show(in: self.view)
                hud.dismiss(afterDelay: 1.5, animated: true)
                _ = self.navigationController?.popViewController(animated: true)
            }
        })
    }
}
