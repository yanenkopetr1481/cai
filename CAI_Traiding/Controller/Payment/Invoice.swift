//
//  Invoice.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 04.02.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit
import Alamofire
import WebKit
import PDFReader
import JGProgressHUD

class Invoice: UIViewController {
    
    @IBOutlet weak var titleLoc:UILabel!
    @IBOutlet weak var confirmBtn:UIButton!
    @IBOutlet weak var backBtn:UIButton!
    
    var invoideID:NSNumber?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setLanguageLoc()
        setTransparentNavigationBar()
    }
    
    func setupViweConfiguration() {
        if invoideID == nil {
            confirmBtn.isEnabled = false
        }
    }
    
    func setLanguageLoc() {
        titleLoc.text = "invoice_titleLoc".localized()
        confirmBtn.setTitle("invoice_title".localized(), for: .normal)
        self.title = "invoice_title".localized()
    }
    
    @IBAction func next(_ sender:UIButton) {
        let url = URL(string: APIManager.baseURLString + "/api/user/payments/replenish/invoice?id=\(self.invoideID!)")
        
        var request = URLRequest(url: url!, timeoutInterval: Double.infinity)
        request.addValue("Bearer \(ApiConfigurationData.accessToken)", forHTTPHeaderField: "Authorization")
        
        request.httpMethod = "GET"
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Download".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        AF.request(request).response {
            result in
            DispatchQueue.main.async {
                if let data = result.data {
                    guard let document = PDFDocument(fileData: data, fileName: "Invoice") else {
                        return
                    }
                    
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Success".localized()
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    
                    let item = UIBarButtonItem(image: UIImage(named: "back_white"), style: .plain, target: self, action: #selector(self.back))
                    let readerController = PDFViewController.createNew(with: document, title: "Invoice", actionButtonImage: nil, actionStyle: .activitySheet, backButton: item, isThumbnailsEnabled: true, startPageIndex: 0)
                    self.navigationController?.pushViewController(readerController, animated: true)
                }
            }
        }
    }
    
    @IBAction func back(_ sender:Any) {
        if self.navigationController != nil {
            var isSearched = false
            let viewControllers = self.navigationController!.viewControllers as Array
            for controller in viewControllers {
                if controller.isKind(of: DealPreview.self) {
                    isSearched = true
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
            
            for controller2 in viewControllers {
                if controller2.isKind(of: RouterController.self) {
                    isSearched = true
                    self.navigationController?.popToViewController(controller2, animated: true)
                    break
                }
            }
            
            if !isSearched {
                self.navigationController?.popViewController(animated: true)
            }
        }
        else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func backBtn(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func setTransparentNavigationBar() {
        let image = UIImage()
        self.backBtn.isHidden = true
        if (self.navigationController != nil) {
            self.navigationController!.navigationBar.setBackgroundImage(image, for: .default)
            self.navigationController!.navigationBar.shadowImage = image
            self.navigationController!.navigationBar.isTranslucent = true
        }
        else {
            self.backBtn.isHidden = false
        }
    }
}
