//
//  SelectReplenishType.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 09.07.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit
import MFSDK
import JGProgressHUD

class SelectReplenishType: UIViewController {
    
    @IBOutlet weak var backBtn:UIButton!
    @IBOutlet weak var btnCash: UIButton!
    @IBOutlet weak var btnCard: UIButton!
    @IBOutlet weak var titleLoc:UILabel!
    
    var amount:Double?
    
    @IBAction func previus(_ sender:Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewConfigure()
        setTransparentNavigationBar()
    }
    
    func setupViewConfigure() {
        btnCash.setTitle("btn_cash".localized(), for: .normal)
        btnCard.setTitle("btn_card".localized(), for: .normal)
        titleLoc.text = "payment_replenishBalance".localized()
    }
    
    func setTransparentNavigationBar() {
        if (self.navigationController != nil) {
            backBtn.isHidden = true
        }
        else {
            backBtn.isHidden = false
        }
    }
    
    @IBAction func back(_ sender:Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showCustomerAlert(_ sender:UIButton) {
        showSimpleActionSheet(controller: self)
    }
    
    @IBAction func showReplenish() {
        let newView = self.storyboard?.instantiateViewController(withIdentifier: "replenish_vc") as! Replenish
        newView.amount = amount
        self.navigationController?.pushViewController(newView, animated: true)
    }
    
    func showSimpleActionSheet(controller: UIViewController) {
                
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "PaymentAlert") as! PaymentAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        if (self.amount != nil) {
            customAlert.amount = self.amount!
        }
        else {
            customAlert.amount = nil
        }
        
        self.present(customAlert, animated: true, completion: nil)
    }
}

extension SelectReplenishType:PaymentAlertViewDelegate {
    func cancelButtonTapped() {}
    
    func okButtonTapped(paymentMethod: Int, count:Double) {
        
        let request = MFExecutePaymentRequest(invoiceValue: Decimal(count), paymentMethod: paymentMethod, callBackUrl: "http://31.131.31.129:8080/api/user/payments/callbackTest", errorUrl: "http://31.131.31.129:8080/api/user/payments/callbackTest")
        
        request.displayCurrencyIso = .unitedStates_USD
        request.customerName = UserProfileData.name ?? ""
        request.userDefinedField = UserProfileData.nickname ?? ""
        
        MFPaymentRequest.shared.executePayment(request: request, apiLanguage: .english, completion: { (response, invoiceId) in
            switch response {
            case .success(let data):
                _ = self.navigationController?.popViewController(animated: true)
                print(data)
                let hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Success".localized()
                hud.vibrancyEnabled = true
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                hud.textLabel.text = "Success with invoice id \(String(describing: invoiceId))"
                hud.show(in: self.view)
                hud.dismiss(afterDelay: 1.5, animated: true)
                
            case .failure(let failError):
                let hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Error".localized()
                hud.vibrancyEnabled = true
                hud.indicatorView = JGProgressHUDErrorIndicatorView()
                
                if let invoiceId = invoiceId {
                    hud.textLabel.text = "Fail: \(failError.statusCode) with invoice id \(invoiceId)"
                } else {
                    hud.textLabel.text = "Fail: \(failError.statusCode)"
                }
                
                hud.show(in: self.view)
                hud.dismiss(afterDelay: 1.5, animated: true)
                _ = self.navigationController?.popViewController(animated: true)
            }
        })
    }
}
