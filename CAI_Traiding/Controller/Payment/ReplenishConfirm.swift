//
//  PeplenishPreview.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 04.02.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit
import Gallery
import Lightbox
import NotificationBannerSwift
import JGProgressHUD
import Alamofire
import PDFReader

class ReplenishConfirm: UIViewController {
    
    @IBOutlet weak var titleLoc:UILabel!
    @IBOutlet weak var titleLb:UILabel!
    @IBOutlet weak var sumLb:UILabel!
    @IBOutlet weak var nameLb:UILabel!
    @IBOutlet weak var sumLoc:UILabel!
    @IBOutlet weak var nameLoc:UILabel!
    @IBOutlet weak var addPhotoLoc:UILabel!
    @IBOutlet weak var nextBtn:UIButton!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var getInvoiceBtn:UIButton!
    @IBOutlet weak var imagesCountLabel: UILabel!
    @IBOutlet weak var imagesStack: UIStackView!
    @IBOutlet weak var paymentShowCheck:UIButton!
    @IBOutlet weak var removeReplenish:UIButton!
    @IBOutlet weak var showadminPhotos:UIButton!

    @IBOutlet weak var backBtn:UIButton!

    var paymentData:PaymentData?
    
    var library:[UIImage] = [UIImage]()
    var libraryReciepts:[UIImage] = [UIImage]()
    var adminPhotos:[UIImage] = [UIImage]()
    var gallery: GalleryController!
    var lightbox :LightboxController!
    var selectedImageLightBox:Int = 0
    var alertTitle:String = "deal_alertTitle".localized()
    var alertTrueButton:String = "deal_alertTrueButton".localized()
    var alertFalseButton:String = "deal_alertFalseButton".localized()
    var isRemoveImage:Bool = false
    var lightBoxType:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLanguageLoc()
        setupGalleryConfiguration()
        setTransparentNavigationBar()
        setupViewConfiguration()
    }
    
    func setupViewConfiguration() {
        paymentShowCheck.isHidden = true
        
        titleLb.text = paymentData?.description
        nameLb.text = paymentData?.payerName
        sumLb.text = "\(String(describing: paymentData!.sum))"
        
        if !(paymentData?.photos?.isEmpty ?? true) {
            
            libraryReciepts.removeAll()
            
            var count = 0
            
            paymentData?.photos?.forEach { urlJson in
                
                let url = URL(string: urlJson.string!)!
                
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    self.libraryReciepts.append(image)
                    count += 1
                    
                    if count == paymentData?.photos?.count {
                        self.paymentShowCheck.isHidden = false
                    }
                }
                else {
                    AF.request(url).response {
                        result in
                        DispatchQueue.main.async {
                            if let data = result.data {
                                let image = UIImage(data: data)!
                                self.libraryReciepts.append(image)
                                ApiConfigurationData.appImages.setObject(image, forKey: url.absoluteString as AnyObject)
                                count += 1
                                
                                if count == self.paymentData?.photos?.count {
                                    self.paymentShowCheck.isHidden = false
                                }
                            }
                        }
                    }
                }
            }
        }
        
        self.showadminPhotos.isHidden = true
        self.showadminPhotos.setTitle("payment_showAdminPhotos".localized(), for: .normal)
        
        if !(paymentData?.adminPhotos?.isEmpty ?? true) {
            
            adminPhotos.removeAll()
            
            var count = 0
            
            paymentData?.photos?.forEach { urlJson in
                
                let url = URL(string: urlJson.string!)!
                
                if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                    self.adminPhotos.append(image)
                    count += 1
                    
                    if count == paymentData?.adminPhotos?.count {
                        self.showadminPhotos.isHidden = false
                    }
                }
                else {
                    AF.request(url).response {
                        result in
                        DispatchQueue.main.async {
                            if let data = result.data {
                                let image = UIImage(data: data)!
                                self.adminPhotos.append(image)
                                ApiConfigurationData.appImages.setObject(image, forKey: url.absoluteString as AnyObject)
                                count += 1
                                
                                if count == self.paymentData?.adminPhotos?.count {
                                    self.showadminPhotos.isHidden = false
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func showAdminPhotos(_ sender:UIButton) {
        
        if !adminPhotos.isEmpty {
            let lightboxImages = adminPhotos.map({ LightboxImage(image: $0) })
            lightbox = LightboxController(images: lightboxImages, startIndex: 0)
            lightbox.footerView.isHidden = true
            lightbox.imageTouchDelegate = self
            lightBoxType = true
            lightbox.modalPresentationStyle = .fullScreen
            self.present(lightbox, animated: true)
        }
    }
    
    func setLanguageLoc() {
        titleLoc.text = "payment_description".localized()
        sumLoc.text = "replenish_sumLoc".localized()
        nameLoc.text = "replenish_nameLoc".localized()
        addPhotoLoc.text = "replenish_addPhoto".localized()
        nextBtn.setTitle("replenish_confirmloc".localized(), for: .normal)
        getInvoiceBtn.setTitle("invoice_title".localized(), for: .normal)
        removeReplenish.setTitle("replenish_deleteloc".localized(), for: .normal)
        paymentShowCheck.setTitle("payment_show_check".localized(), for: .normal)
        
        self.title = "replenish_simple_title".localized()
    }
    
    @IBAction func previus(_ sender:Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showReciepts(_ sender:UIButton) {
        
        if !libraryReciepts.isEmpty {
            let lightboxImages = libraryReciepts.map({ LightboxImage(image: $0) })
            lightbox = LightboxController(images: lightboxImages, startIndex: 0)
            lightbox.footerView.isHidden = true
            lightbox.imageTouchDelegate = self
            lightBoxType = true
            lightbox.modalPresentationStyle = .fullScreen
            self.present(lightbox, animated: true)
        }
    }
    
    @IBAction func removeReplenish(_ sender:UIButton) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Wait".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        APIManager.sharedInstance.removeReplenish(id: Int(truncating: paymentData!.id), completionHandler: { (data) in
            
            guard let status = data["status"]?.number, status == 1  else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["data"]?.string ?? ""
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = data["data"]?.string ?? ""
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            hud.dismiss(afterDelay: 1.0, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }, viewController: self)
    }
    
    @IBAction func confirm(_ sender:UIButton) {
        
        if library.isEmpty {
            let banner = NotificationBanner(title: "Missing".localized(), subtitle: "BannerMissing".localized(), style: .danger)
            banner.show()
        }
        else {
            
            let hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Sending".localized()
            hud.vibrancyEnabled = true
            hud.show(in: self.view)
            
            APIManager.sharedInstance.addReplenishReceipt(data: ["paymentId": "\(paymentData!.id)"], images: library, onSuccess: { (data) in
                
                guard let response = data["url"]?.dictionary, let status = response["status"]?.number, status == 1  else {
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Error".localized()
                        hud.detailTextLabel.text = data["url"]?.dictionary?["message"]?.string ?? ""
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    
                    return
                }
                
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Success".localized()
                    hud.detailTextLabel.text = response["message"]?.string ?? ""
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }, viewController: self)
        }
    }
    
    @IBAction func back(_ sender:Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func setTransparentNavigationBar() {
        backBtn.isHidden = true
        if (self.navigationController != nil) {
            self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController!.navigationBar.shadowImage = UIImage()
            self.navigationController!.navigationBar.isTranslucent = true
        }
        else {
            backBtn.isHidden = false
        }
    }
    
    func setupGalleryConfiguration() {
        Config.Camera.imageLimit = 5
        Config.tabsToShow = [.imageTab, .cameraTab]
    }
    
    
    @IBAction func showLibrary(_ sender: UIButton) {
        guard library.count > 0 else {
            return
        }
        
        let lightboxImages = library.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.footerView.isHidden = true
        lightbox.imageTouchDelegate = self
        lightBoxType = true
        lightbox.modalPresentationStyle = .fullScreen
        self.present(lightbox, animated: true)
    }
    
    func onTapCustomAlertButton() {
        isRemoveImage = true
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.config = AlertCustomData(title: "RemoveImage".localized(), firstSegment: "Yes".localized(), secondSegment: "No".localized())
        lightbox.present(customAlert, animated: true, completion: nil)
    }
    
    @IBAction func btnChooseImageOnClick(_ sender: UIButton) {
        gallery = GalleryController()
        gallery.delegate = self
        gallery.modalPresentationStyle = .fullScreen
        present(gallery, animated: true, completion: nil)
    }
    
    @IBAction func getInvoice(_ sender:UIButton) {
        let urlStr = APIManager.baseURLString + "/api/user/payments/replenish/invoice?id=" + "\(self.paymentData!.invoiceId)"
        let url = URL(string: urlStr)!

        var request = URLRequest(url: url, timeoutInterval: Double.infinity)
        request.addValue("Bearer \(ApiConfigurationData.accessToken)", forHTTPHeaderField: "Authorization")
        
        request.httpMethod = "GET"
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Download".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        AF.request(request).response {
            result in
            DispatchQueue.main.async {
                if let data = result.data {
                    guard let document = PDFDocument(fileData: data, fileName: "Invoice") else {
                        return
                    }
                    
                    let item = UIBarButtonItem(image: UIImage(named: "back_white"), style: .plain, target: self, action: #selector(self.back))
                    let readerController = PDFViewController.createNew(with: document, title: "Invoice", actionButtonImage: nil, actionStyle: .activitySheet, backButton: item, isThumbnailsEnabled: true, startPageIndex: 0)
                    readerController.modalPresentationStyle = .fullScreen
                    if self.navigationController == nil {
                        if var topController = UIApplication.shared.keyWindow?.rootViewController {
                            while let presentedViewController = topController.presentedViewController {
                                topController = presentedViewController
                            }
                            
//                            let vc = topController.storyboard!.instantiateViewController(withIdentifier: "launchPage") as! LaunchViewController
                            readerController.providesPresentationContextTransitionStyle = true
                            readerController.definesPresentationContext = true
                            readerController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            readerController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            
                            UIView.animate(withDuration: 0.1, animations: {
                                hud.textLabel.text = "Success".localized()
                                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                            })
                            
                            hud.dismiss(afterDelay: 1.0, animated: true)
                            
                            topController.present(readerController, animated: true, completion: nil)
                        }
//                        self.present(readerController, animated: true, completion: nil)
                    }
                    else {
                        
                        UIView.animate(withDuration: 0.1, animations: {
                            hud.textLabel.text = "Success".localized()
                            hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        })
                        
                        hud.dismiss(afterDelay: 1.0, animated: true)
                        
                        self.navigationController?.pushViewController(readerController, animated: true)
                    }
                }
            }
        }
    }
}

extension ReplenishConfirm: CustomAlertViewDelegate {
    
    func okButtonTapped(count: String) {
        
        lightbox.images.remove(at: selectedImageLightBox )
        
        if (lightBoxType) {
            library.remove(at: selectedImageLightBox )
            self.imagesCountLabel.text = "\(self.library.count)+"
            
            if (lightbox.images.count > 0) {
                imageView.image = library[library.count - 1]
                
                let index = selectedImageLightBox > 0 ? (selectedImageLightBox ) - 1 : 0
                lightbox.goTo(index, animated: true)
            }
            else {
                imageView.image = UIImage(named: "Background")
                self.imagesStack.isHidden = true
                DispatchQueue.main.async {
                    self.lightbox.dismiss(animated: true, completion: nil)
                }
            }
        }
        else {
            var images =  gallery.cart.images
            gallery.cart.remove(images[lightbox.currentPage])
            images.remove(at: lightbox.currentPage)
            gallery.cart.reload(images)
            if (lightbox.images.count == 0) {
                DispatchQueue.main.async {
                    self.lightbox.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func cancelButtonTapped() {}
}


extension ReplenishConfirm: GalleryControllerDelegate {
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        
        DispatchQueue.main.async {
            Image.resolve(images: images, completion: { [weak self] resolvedImages in
                let images = resolvedImages.compactMap({ $0 })
                self!.imageView.image = images.last
                images.forEach { (image) in
                    self!.library.append(image)
                }
                self!.imagesCountLabel.text = "\(self!.library.count)+"
                self!.imagesStack.isHidden = false
            })
        }
        
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {}
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            self?.showLightbox(controller, images: resolvedImages.compactMap({ $0 }))
        })
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func showLightbox(_ controller: GalleryController,images: [UIImage]) {
        
        guard images.count > 0 else {
            return
        }
        
        lightBoxType = false
        
        let lightboxImages = images.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.footerView.isHidden = true
        lightbox.modalPresentationStyle = .fullScreen
        lightbox.imageTouchDelegate = self
        gallery.present(lightbox, animated: true)
    }
}

extension ReplenishConfirm:LightboxControllerTouchDelegate {
    func lightboxController(_ controller: LightboxController, didTouch image: LightboxImage, at index: Int) {
        
        selectedImageLightBox = index
        self.onTapCustomAlertButton()
    }
}
