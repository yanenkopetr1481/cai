//
//  PickedPullUpViewController.swift
//  SOPullUpView_Example
//
//  Created by Ahmad Sofi on 12/8/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import SOPullUpView
import AARefreshControl
import SwiftyJSON
import Alamofire
import Lightbox
import PDFReader
import JGProgressHUD

class PickedPullUpViewController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet weak var pickedTable: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var handleArea: UIView!
    @IBOutlet weak var arrowImage: UIImageView!
    
    var parentVc:UIViewController!
    // MARK: - Properties
    
    var pullUpControl: SOPullUpControl? {
        didSet {
            pullUpControl?.delegate = self
        }
    }
    
    var paymentData:[PaymentData] = [PaymentData]()
    var spaymentsID:[NSNumber] = [NSNumber]()
    var refreshControl: AARefreshControl!
    var isResponseTry:Bool = false
    var fetchingMore:Bool = false
    var page:Int = 0
    let defaults = UserDefaults.standard
    var invoice_id:Int = 0
    
    var library:[UIImage] = [UIImage]()
    var lightbox :LightboxController!
    
    var paymentImages:[JSON]?
    
    //      let keychain = KeychainSwift()
    
    // MARK: - Controller Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        checkDeletedData()
        getPageData(updateTable: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        titleLbl.alpha = 0
        titleLbl.text = "balance_history".localized()
        
        pickedTable.register(UINib(nibName: "Payment", bundle: nil), forCellReuseIdentifier: "payment")
        
        refreshControl = AARefreshControl(scrollView: pickedTable)
        refreshControl.tintColor = UIColor.black
        refreshControl.activityIndicatorViewColor = UIColor.black
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        pickedTable.addSubview(refreshControl)
        
        NetworkStatus.isLaunchUsed = true
    }
    
    
    func checkDeletedData() {
        paymentData = [PaymentData]()
        spaymentsID = [NSNumber]()
        page = 0
    }
    
    @objc func refresh(_ sender: Any?) {
        if !fetchingMore {
            
            fetchingMore = true
            paymentData = [PaymentData]()
            spaymentsID = [NSNumber]()
            page = 0
            
            self.getPageData(updateTable: true, beginRefresh: true)
        }
        else {
            //            self.refreshControl.endRefreshing()
        }
    }
    
    private func setTableViewEmptyBackground() {
        UIView.animate(withDuration: 0.5) {
            let view = Bundle.main.loadNibNamed("EmptyTableView", owner: self, options: nil)?.first as! EmptyTableView
            view.titleLabel.text = "EmptyRessult".localized()
            view.refreshBtn.setTitle("Refresh".localized(), for: .normal)
            view.delegate = self
            view.imageView.image = UIImage(named: "empty")
            self.pickedTable.backgroundView = view
            self.pickedTable.isUserInteractionEnabled = true
            self.pickedTable.rowHeight = UITableView.automaticDimension
            self.pickedTable.estimatedRowHeight = 180
            self.pickedTable.layer.cornerRadius = 10
        }
    }
    
    func concat (lhs: NSNumber, rhs: NSNumber) -> String {
        let value = Double(exactly: rhs.doubleValue / lhs.doubleValue)
        return String(format: "%.2f", value!)
    }
    
    func getPageData(updateTable:Bool = false, beginRefresh: Bool = true) {
        
        NotificationCenter.default.post(name: Notification.Name("ReceiveData"), object: nil)

        self.pickedTable.isUserInteractionEnabled = false
        fetchingMore = true
        
        if (beginRefresh) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.refreshControl.endRefreshing()
            }
        }
        
        self.pickedTable.backgroundView = UIView()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            //            var count = 0
            var isChanged = false
            
            APIManager.sharedInstance.getUserPayment(page: self.page, onSuccess: { (data) in
                if let response = data["data"]?.array, !response.isEmpty  {
                    //                    count = response.count
                    
                    response.forEach { value in
                        guard let sum = value["sum"].number,
                            let id = value["id"].number,let created = value["created"].number
                            else { return }
                        
                        let type = value["paymentType"].string
                        let updated = value["updated"].number
                        let approver = value["approver"].string ?? ""
                        let description = value["description"].string ?? "payment_showDeal".localized()
                        let payerName = value["payerName"].string ?? ""
                        let invoiceId = value["invoiceId"].number ?? 0
                        let paymentStatus = value["paymentStatus"].string ?? ""
                        let changeBalanceReason = value["changeBalanceReason"].dictionary?["title"]?.string ?? ""
                        var dealData:DealData?
                        
                        if let deal = value["deal"].dictionary {
                            guard let product = deal["product"]?.dictionary,
                                let productTitle = product["productTitle"]?.string,
                                let weight = product["weight"]?.number,
                                let productSum = product["price"]?.number,
                                let seller = product["seller"]?.dictionary,
                                let sellerName = seller["name"]?.string,
                                let imagesUrl = product["photoUrl"]?.array,
                                let id = value.dictionary?["id"]?.number,
                                let customerAccept = deal["customerAccept"]?.bool,
                                let pay = deal["paymentStatus"]?.string,
                                let other = deal["dealStatus"]?.string,
                                let sellerAccept = deal["sellerAccept"]?.bool,
                                let isPrivate = deal["private"]?.bool,
                                let commision_included = deal["commissionIncluded"]?.bool,
                                let commsion_for_sum = deal["commissionForSum"]?.double,
                                let commsion_for_gram = deal["commissionForGramm"]?.double
                                else {
                                    return
                            }
                            
                            let priceForGramm = self.concat(lhs: weight, rhs: sum)
                            let serviceses =  value.dictionary?["additionalServices"]?.array ?? []

                            let customer:String = deal["customer"]?.dictionary?["name"]?.string ?? ""
                            let anulateData = DealAnulateData(isSellerAnulate: deal["sellerAnnulate"]?.bool ?? false, isCustomerAnulate: deal["customerAnnulate"]?.bool ?? false, anulateReson: deal["annulationReason"]?.string ?? "")
                            let shipingData = deal["shippingStatuses"]?.array ?? [JSON]()
                            let category = value.dictionary?["productCategory"]

                            let termsData =  TermsData(needsToAcceptNewTerms: value.dictionary?["needsToAcceptNewTerms"]?.bool, newTerms: value.dictionary?["newTerms"]?.dictionary, customerAcceptNewTerms: value.dictionary?["customerAcceptNewTerms"]?.bool, sellerAcceptNewTerms: value.dictionary?["sellerAcceptNewTerms"]?.bool)
                            
                            dealData = DealData(id: id, productTitle: productTitle, productWeight: "\(weight)", productSeller: sellerName, productCustomer: customer, productGramSum: priceForGramm, productSum: "\(productSum)", productImages: [], productPayStatus: pay, shipingData:shipingData, otherStatus: other, type: .purchase, productImagesURLs: imagesUrl, customerAccept: customerAccept, sellerAccept: sellerAccept, anulateData: anulateData, isPrivate: isPrivate, commision_included: commision_included, commissionForGramm: "\(commsion_for_gram)", commissionForSum: "\(commsion_for_sum)",termsDaata: termsData, services: serviceses, category: category)
                        }
                        
                        let title = dealData == nil ? "payment_replenishBalance".localized() : "payment_purchasePay".localized()
                        let photos = value["photos"].array
                        
                        if !self.spaymentsID.contains(id) {
                            isChanged = true
                            let data = PaymentData(paymentStatus: paymentStatus, id: id, sum: sum, invoiceId: invoiceId, description: description, title: title, payerName: payerName, approver: approver, dealData: dealData,type: type,reason: changeBalanceReason, created: created as? Double,updated: updated as? Double, photos: photos)
                            
                            if Int(truncating: data.id) > Int(truncating: self.paymentData.first?.id ?? 0) {
                                self.paymentData.insert(data, at: 0)
                            }
                            else {
                                self.paymentData.append(data)
                            }
                            
                            self.spaymentsID.append(id)
                        }
                    }
                    
                    if isChanged && ((self.page + 1) * 10 == self.paymentData.count) {
                        self.page += 1
                    }
                }
                
                if self.paymentData.isEmpty {
                    
                    if (!self.isResponseTry) {
                        self.isResponseTry = true
                        self.getPageData()
                    }
                    else {
                        self.isResponseTry = false
                        self.setTableViewEmptyBackground()
                        self.paymentData.removeAll()
                        self.spaymentsID.removeAll()
                        self.pickedTable.reloadData()
                    }
                }
                else {
                    
                    if (isChanged) {
                        self.paymentData = self.paymentData.sorted { $0.updated > $1.updated}
                        self.pickedTable.reloadData()
                    }
                    
                    self.isResponseTry = false
                }
                
                self.fetchingMore = false
                self.pickedTable.isUserInteractionEnabled = true
                self.pickedTable.tableFooterView?.isHidden = true
            }, viewController: self)
        }
    }
    
}

// MARK: - SOPullUpViewDelegate

extension PickedPullUpViewController: SOPullUpViewDelegate {
    
    func pullUpViewStatus(_ sender: UIViewController, didChangeTo status: PullUpStatus) {
        switch status {
        case .collapsed:
            UIView.animate(withDuration: 0.6)  { [weak self] in
                //                self?.titleLbl.alpha = 0
                self?.arrowImage.image = self?.arrowImage.image?.rotate(radians: .pi)
            }
        case .expanded:
            UIView.animate(withDuration: 0.6) { [weak self] in
                //                self?.titleLbl.alpha = 1
                self?.arrowImage.image = self?.arrowImage.image?.rotate(radians: .pi)
            }
        }
    }
    
    func pullUpHandleArea(_ sender: UIViewController) -> UIView {
        return handleArea
    }
}

// MARK: - UITableViewDelegate , UITableViewDataSource

extension PickedPullUpViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "payment", for: indexPath) as! PaymentCell
        cell.isStatusHide = true
        if (self.paymentData.count > indexPath.row) {
            cell.setupView(data: self.paymentData[indexPath.row])
        }

        return cell
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return 120
    //    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (indexPath.row == paymentData.count - 1) {
            if !fetchingMore {
                if (indexPath.row > 7) {
                    self.pickedTable.tableFooterView?.isHidden = false
                    getPageData(updateTable: false, beginRefresh: false)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if paymentData[indexPath.row].dealData != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "previewDeal") as! DealPreview
            vc.productData = paymentData[indexPath.row].dealData
            self.parentVc.present(vc, animated: true, completion: nil)
            //            navigationController?.pushViewController(vc, animated: true)
        }
        else if (paymentData[indexPath.row].paymentStatus == "AWAITING_FOR_RECEIPT" || paymentData[indexPath.row].paymentStatus == "AWAITING_FOR_APPROVE") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "previewPlenishNavigator") as! PreviewReplenishNavigator
            let vc = storyboard.instantiateViewController(withIdentifier: "previewPlenish") as! ReplenishConfirm

            vc.paymentData = paymentData[indexPath.row]
//            self.parentVc.present(vc, animated: true, completion: nil)
            self.parentVc.navigationController?.show(vc, sender: self)
        }
        else if (paymentData[indexPath.row].paymentStatus == "APPROVED" ){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
            customAlert.providesPresentationContextTransitionStyle = true
            customAlert.definesPresentationContext = true
            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            customAlert.delegate = self
            customAlert.config = AlertCustomData(title: "payment_data".localized(), firstSegment: "payment_invoice".localized(), secondSegment: "payment_check".localized(),isTextFieldHidden: true, isCloseByTap: true)
            
            self.parentVc.present(customAlert, animated: true, completion: nil)
            self.invoice_id = Int(truncating: paymentData[indexPath.row].invoiceId)
            self.paymentImages = paymentData[indexPath.row].photos
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func getInvoice(id:Int) {
        let url = URL(string: APIManager.baseURLString + "/api/user/payments/replenish/invoice?id=\(id)")
        
        var request = URLRequest(url: url!, timeoutInterval: Double.infinity)
        request.addValue("Bearer \(ApiConfigurationData.accessToken)", forHTTPHeaderField: "Authorization")
        
        request.httpMethod = "GET"
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Download".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)

        
        AF.request(request).response {
            result in
            DispatchQueue.main.async {
                if let data = result.data {
                    let document = PDFDocument(fileData: data, fileName: "Invoice")
                    let item = UIBarButtonItem(image: UIImage(named: "back_white"), style: .plain, target: self, action: #selector(self.back))
                    let readerController = PDFViewController.createNew(with: document!, title: "Invoice", actionButtonImage: nil, actionStyle: .activitySheet, backButton: item, isThumbnailsEnabled: true, startPageIndex: 0)
                    
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Success".localized()
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    self.parentVc.navigationController?.pushViewController(readerController, animated: true)
                }
            }
        }
    }
    
    @objc func back() {
        _ = self.parentVc.navigationController?.popViewController(animated: true)
    }
}

extension PickedPullUpViewController:EmptyTableViewDelegate {
    func refreshData() {
        self.getPageData(updateTable: true)
    }
}


extension PickedPullUpViewController:CustomAlertViewDelegate {
    
    func okButtonTapped(count: String) {
        
        self.getInvoice(id: self.invoice_id)
    }
    
    func cancelButtonTapped() {
        
        library.removeAll()
        
        var count = 0
        
        paymentImages?.forEach { urlJson in
            
            let url = URL(string: urlJson.string!)!
            
            if let image = ApiConfigurationData.appImages.object(forKey: url.absoluteString as AnyObject) as? UIImage {
                self.library.append(image)
                count += 1
                
                if count == paymentImages?.count {
                    self.showLibrary()
                }
            }
            else {
                AF.request(url).response {
                    result in
                    DispatchQueue.main.async {
                        if let data = result.data {
                            let image = UIImage(data: data)!
                            self.library.append(image)
                            
                            ApiConfigurationData.appImages.setObject(image, forKey: url.absoluteString as AnyObject)                            
                            count += 1
                            
                            if count == self.paymentImages?.count {
                                self.showLibrary()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func showLibrary() {
        let lightboxImages = library.map({ LightboxImage(image: $0) })
        lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.footerView.isHidden = true
        lightbox.modalPresentationStyle = .fullScreen
        self.parentVc.present(lightbox, animated: true)
    }
}

