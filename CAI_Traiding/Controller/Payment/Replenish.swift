//
//  Replenish.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 04.02.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import Gallery
import Lightbox
import JGProgressHUD
import SwiftyJSON

class Replenish: UIViewController {
    
    @IBOutlet weak var titleLoc:UILabel!
    @IBOutlet weak var sumLoc:UILabel!
    @IBOutlet weak var nameLoc:UILabel!
    @IBOutlet weak var nextBtn:UIButton!
    @IBOutlet var textFields:[UITextField]!
    @IBOutlet weak var selectedCountry:UILabel!
    @IBOutlet weak var selectCountryBtn:UIButton!
    @IBOutlet weak var backBtn:UIButton!
    
    var library:[UIImage] = [UIImage]()
    var gallery: GalleryController!
    var lightbox :LightboxController!
    var selectedImageLightBox:Int = 0
    var alertTitle:String = "deal_alertTitle".localized()
    var alertTrueButton:String = "deal_alertTrueButton".localized()
    var alertFalseButton:String = "deal_alertFalseButton".localized()
    var isRemoveImage:Bool = false
    var lightBoxType:Bool = false
    var invoiceId:NSNumber?
    var country:Int?
    var amount:Double?
    
    var languages = [JSON]()

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setLanguageLoc()
        setupViewConfiguration()
        setTransparentNavigationBar()
    }

    func setupViewConfiguration() {
        selectedCountry.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        APIManager.sharedInstance.getCountries(completionHandler: { (data) in
            if let data = data["data"]?.array, !data.isEmpty {
                self.languages = data
            }
        }, viewController: self)
        
        let placeHolder = "not_require".localized()

        textFields.forEach { (textField) in
            switch textField.tag {
            case 0:
                textField.attributedPlaceholder = NSAttributedString(string: placeHolder ,
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            case 1:
                textField.text = amount == nil ? "" : "\(amount!)"
            case 2:
                textField.text = UserProfileData.name
            default:break
            }
        }
        
        if !(UserProfileData.countryName?.isEmpty ?? true) {
            country = UserProfileData.countryID
            self.selectedCountry.text = UserProfileData.countryName
            self.selectedCountry.isHidden = false
            self.selectCountryBtn.setTitle("replenish_changeCountry".localized(), for: .normal)
        }
    }
    
    @IBAction func showLanguagePicker (_ sender:UIButton) {
        
        if languages.count > 0 {
            DispatchQueue.main.async {
                let alert = UIAlertController(style: .actionSheet, title: "", message: "")

                let pickerViewValues: [[String]] = [self.languages.map { $0["name"].string! }]
                
                let id = self.languages.firstIndex { (data) -> Bool in
                    return data["id"].int == self.country
                }
                
                let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: id ?? 0 )

                alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
                    self.country = index.row
                    self.selectedCountry.text = self.languages[self.country!]["name"].string!
                    self.selectedCountry.isHidden = false
                    self.selectCountryBtn.setTitle("replenish_changeCountry".localized(), for: .normal)
                }
                
                alert.addAction(title: "Cancel".localized(), style: .cancel)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func setLanguageLoc() {
        titleLoc.text = "payment_description".localized()
        sumLoc.text = "replenish_sumLoc".localized()
        nameLoc.text = "replenish_nameLoc".localized()
        nextBtn.setTitle("replenish_nextloc".localized(), for: .normal)
        selectCountryBtn.setTitle("replenish_selectCountry".localized(), for: .normal)
        self.title = "payment_creating_invoice".localized()
    }
    
    @IBAction func next(_ sender:UIButton) {
        
        var data = [String:String]()
        
        var isEmpty = false
        
        guard let _ = self.country else {
            let banner = NotificationBanner(title: "Missing".localized(), subtitle: "BannerCheckFields".localized(), style: .warning)
            banner.show()
            return
        }
        
        let selectedCountryJson = self.languages.first { (datas) -> Bool in
            return datas.dictionary?["name"]?.string == self.selectedCountry.text
        }
        
        let id = selectedCountryJson?.dictionary?["id"]?.number
        data["countryId"] = "\(id ?? 1)"

        textFields.forEach { (textField) in
            
            if (textField.text!.isEmpty && textField.tag != 0) {
                isEmpty = true
            }
            else {
                switch textField.tag {
                case 0:
                    data["description"] = textField.text ?? ""
                    break
                case 1:
                    data["sum"] = "\(textField.text!.doubleValue)"
                case 2:
                    data["payerName"] = textField.text!
                default: break
                }
            }
        }
        
        if isEmpty {
            let banner = NotificationBanner(title: "Missing".localized(), subtitle: "BannerCheckFields".localized(), style: .warning)
            banner.show()
        }
        else {
            
             let hud = JGProgressHUD(style: .dark)
                        hud.textLabel.text = "Sending".localized()
                        hud.vibrancyEnabled = true
                        hud.show(in: self.view)
                        
            APIManager.sharedInstance.addReplenish(data: data, onSuccess: { (data) in
                
                guard let status = data["status"]?.number, status == 1, let invoiceId = data["data"]?.dictionary?["invoiceId"]?.number,  let _ = data["data"]?.dictionary?["paymentId"]?.number  else {
                    UIView.animate(withDuration: 0.1, animations: {
                        hud.textLabel.text = "Error".localized()
                        hud.detailTextLabel.text = "ServerError"
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    })
                    
                    hud.dismiss(afterDelay: 1.0, animated: true)
                    
                    return
                }
                
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Success".localized()
                    hud.detailTextLabel.text = nil
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                    
                  self.invoiceId = invoiceId
                    
                    let newView = self.storyboard?.instantiateViewController(withIdentifier: "invoice") as! Invoice
                    newView.invoideID = invoiceId
                    self.navigationController?.pushViewController(newView, animated: true)                    
                }
            }, viewController: self)
        }
    }
    
    @IBAction func back(_ sender:Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backBtn(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func setTransparentNavigationBar() {
        let image = UIImage()
        self.backBtn.isHidden = true
        
        if (self.navigationController != nil) {
            self.navigationController!.navigationBar.setBackgroundImage(image, for: .default)
            self.navigationController!.navigationBar.shadowImage = image
            self.navigationController!.navigationBar.isTranslucent = true
        }
        else {
            self.backBtn.isHidden = false
        }
    }
    
    @objc func adjustForKeyboard(notification: NSNotification) {
        if notification.name == UIResponder.keyboardWillHideNotification {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        } else {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0 {
                    self.view.frame.origin.y -= keyboardSize.height
                }
            }
        }
    }
}

extension Replenish:UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return !string.isSingleEmoji
    }
}
