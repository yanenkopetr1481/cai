//
//  Support.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/30/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import SwiftyJSON

class Support: RootViewController {
    
    @IBOutlet weak var segmentTitle: UILabel!
    @IBOutlet weak var phoneBtn: UIButton!
    @IBOutlet weak var messengerBtn: UIButton!
    @IBOutlet weak var workTitle: UILabel!
    @IBOutlet weak var workTime: UILabel!
    @IBOutlet weak var workDay: UILabel!
    
    var alert:UIAlertController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        setTransparentNavigationBar()
        setLocalization()
        getContactData()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func getContactData() {
        
        if ApiConfigurationData.contacNumber.isEmpty {
                    APIManager.sharedInstance.getContacts(onSuccess: { (data) in
                if let status = data["status"]?.number, status == 1, let response = data["data"]?.dictionary {
                    
                    response["contacts"]?.array?.each({ (index, contact) in
                        if contact["contactType"].string == "PHONE" {
                            ApiConfigurationData.contacNumber.append(contact["info"].string ?? "")
                        }
                        
                        if contact["contactType"].string == "TELEGRAM" {
                            ApiConfigurationData.telegram = contact["info"].string ?? ""
                        }
                    })
                        
                    ApiConfigurationData.supportWorkingAdditionalInfo = response["supportWorkingAdditionalInfo"]?.string ?? String()
                    ApiConfigurationData.supportWorkingHours = response["supportWorkingHours"]?.string ?? String()
                    ApiConfigurationData.supportWorkingDays = response["supportWorkingDays"]?.string ?? String()
                }
            }, viewController: UIViewController())
        }
        else {
            setupPhoneAlert()
        }
        
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
//        print(formattedDate)

        let calendar = Calendar.current
        
//        print(calendar.component(.weekday, from: date))
        
        let now = Date()

        let date_arr = ApiConfigurationData.supportWorkingHours.replacingOccurrences(of: " ", with: "").components(separatedBy:"-")
        let start_arr = date_arr[0].components(separatedBy:":")
        let end_arr = date_arr[1].components(separatedBy:":")

        let eight_today = now.dateAt(hours: Int(start_arr[0])!, minutes: Int(start_arr[1])!)
        let four_thirty_today = now.dateAt(hours: Int(end_arr[0])!, minutes: Int(end_arr[1])!)
        
        if now >= eight_today &&
            now <= four_thirty_today
        {
            phoneBtn.isEnabled = !Date().isDateInWeekend
            phoneBtn.alpha = Date().isDateInWeekend ? 0.7 : 1

        }
        else {
            phoneBtn.isEnabled = false
            phoneBtn.alpha = 0.7
        }
    }
    
    func setupPhoneAlert() {
        if !ApiConfigurationData.contacNumber.isEmpty {
            self.alert = UIAlertController(style: .actionSheet)
            ApiConfigurationData.contacNumber.forEach { (number) in
                self.alert.addAction(UIAlertAction(title: number, style: .default, handler: { (action) in
                    let number = "tel://\(action.title!))"
                    if let url = URL(string: number), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url)
                    }
                }))
            }
            
            self.alert.addAction(title: "Cancel".localized(), style: .cancel)
            self.setLocalization()
        }
    }
    
    func setLocalization() {
        segmentTitle.text = "support_page_title".localized()
        phoneBtn.setTitle("support_segment_first_title".localized(), for: .normal)
        messengerBtn.setTitle("support_segment_second_title".localized(), for: .normal)
        workTitle.text = "support_work_title".localized()
        //        workTime.text = "support_work_time".localized()
        workTime.text = ApiConfigurationData.supportWorkingHours
        
        //        workDay.text = "support_work_day".localized()
        workDay.text = ApiConfigurationData.supportWorkingDays
        
        self.title = "Support".localized()
    }
    
    func setTransparentNavigationBar() {
        let image = UIImage()
        self.navigationController!.navigationBar.setBackgroundImage(image, for: .default)
        self.navigationController!.navigationBar.shadowImage = image
        self.navigationController!.navigationBar.isTranslucent = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.isHidden = false
    }
    
    func configureNavigationBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_white"), style: .plain, target: self, action: #selector(back))
        navigationItem.leftBarButtonItem?.tintColor = .white
    }
    
    @objc func back(_ sender:Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func chooseContactType(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            if !ApiConfigurationData.contacNumber.isEmpty {
                if alert != nil {
                    self.present(alert, animated: true, completion: {
                        sender.isSelected = false
                    })
                }
            }
            else {
                let banner = NotificationBanner(title: "Missing".localized(), subtitle: "PhoneMissing".localized(), style: .info)
                banner.show()
            }
        default:
            let url = "https://t.me/" + ApiConfigurationData.telegram
            if let url = URL(string: "\(url)"), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            }
        }
    }
}
