//
//  InternetLost.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 26.01.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit

class InternetLost: UIViewController {

    @IBOutlet weak var message:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.message.text = "no_internet".localized()
    }
}

extension InternetLost:InternetLostDelegate {
    func hideView() {
        self.dismiss(animated: true, completion:  nil)
    }
}

protocol InternetLostDelegate {
    func hideView()
}
