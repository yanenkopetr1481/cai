//
//  PaymentAlertView.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 26.01.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import MFSDK

protocol PaymentAlertViewDelegate: class {
    func okButtonTapped(paymentMethod: Int, count:Double)
    func cancelButtonTapped()
}

class PaymentAlertView: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var grandTitle: UILabel!
    @IBOutlet weak var closeBtn: UIButton!

    @IBOutlet var paymentStacks: [UIStackView]!
    @IBOutlet var paymentImages: [UIImageView]!
    @IBOutlet var paymentTitle: [UILabel]!
    @IBOutlet var paymentPrice: [UILabel]!
    @IBOutlet var paymentBtns: [UIButton]!
    
    var paymentMethods: [MFPaymentMethod]?

    var delegate: PaymentAlertViewDelegate?
    var selectedOption = 0
    var config:AlertCustomData?
    var amount:Double?
    var customAlert:CustomAlertViewPurhcases!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as? CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.config = AlertCustomData(title: "payment_recharge".localized(), firstSegment: "Next".localized(), secondSegment: "Cancel".localized(),isTextFieldHidden: false, amount: amount)
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        alertView.alpha = 0;
        setupView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
    }
    
    func getPaymentMethods(value:Double) {
        
        let request = MFInitiatePaymentRequest(invoiceAmount: Decimal(value), currencyIso: .kuwait_KWD)

        self.amount = value
        
        MFPaymentRequest.shared.initiatePayment(request: request, apiLanguage: .english, completion: { [weak self] (result) in
            switch result {
            case .success(let initiatePaymentResponse):
                
                var index = 0
                
                self?.paymentStacks.forEach { (stack) in
                    stack.isHidden = true
                }
                
                initiatePaymentResponse.paymentMethods?.forEach({ (method) in
                    if (index < self?.paymentTitle.count) {
                        self?.paymentStacks[index].isHidden = false
                        self?.paymentBtns[index].tag = method.paymentMethodId
                        self?.paymentBtns[index].addTarget(self, action: #selector(self?.selectPaymentMethod(_:)), for: .touchUpInside)
                        self?.paymentTitle[index].text = method.paymentMethodEn
                        self?.paymentPrice[index].text = String(format: "%.2f", NSDecimalNumber(decimal:method.totalAmount).doubleValue)
                        self?.paymentImages[index].downloadImageFrom(url: URL(string: method.imageUrl!)!, contentMode: .scaleToFill)
                        
                        index += 1
                    }
                })
                
                self?.animateView()
                
            case .failure(let failError):
                print(failError.localizedDescription)
                break
            }
        })
    }
    
    @objc func selectPaymentMethod(_ sender:UIButton) {
        
        self.dismiss(animated: true) {
            self.delegate?.okButtonTapped(paymentMethod: sender.tag, count: self.amount!)
        }
    }
    
    @IBAction func hide(_ sender:UIButton) {
        self.dismiss(animated: true)
    }
    
    func setupView() {
        
        titleLable.text = "Payment_method".localized()
        grandTitle.text = "Grand_total".localized()
        closeBtn.setTitle("Close".localized(), for: .normal)

        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
}

extension PaymentAlertView:CustomAlertViewDelegate {
    
    func okButtonTapped(count: String) {
        self.getPaymentMethods(value: count.doubleValue)
    }
    
    func cancelButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}
