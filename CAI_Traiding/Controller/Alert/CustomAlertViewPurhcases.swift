//
//  CustomAlertViewPurhcases.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 11/11/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift

protocol CustomAlertViewDelegate: class {
    func okButtonTapped(count:String)
    func cancelButtonTapped()
}

struct AlertCustomData {
    var title:String
    var firstSegment:String
    var secondSegment:String
    var isTextFieldHidden:Bool = true
    var isTersData:Bool = false
    var termsData:[String:JSON]?
    var amount:Double?
    var isCloseByTap:Bool = false
}

class CustomAlertViewPurhcases: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var succBtn: UIButton!
    @IBOutlet weak var declBtn: UIButton!
    @IBOutlet weak var termsData: UIStackView!
    @IBOutlet weak var newWeight: UILabel!
    @IBOutlet weak var oldWeight: UILabel!
    @IBOutlet weak var oldPrice: UILabel!
    @IBOutlet weak var newPrice: UILabel!

    var delegate: CustomAlertViewDelegate?
    var selectedOption = 0
    var config:AlertCustomData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
//        dissmissTouch()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
    }
    
    func dissmissTouch() {
        alertView.superview?.isUserInteractionEnabled = true
        alertView.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dissmissView)))
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dissmissView))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dissmissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupView() {
        
        textField.isHidden = true
        textField.delegate = self
        
        if config != nil {
            titleLabel.text = config?.title
            segmentedControl.setTitle(config?.firstSegment, forSegmentAt: 0)
            segmentedControl.setTitle(config?.secondSegment, forSegmentAt: 1)
            textField.isHidden = config?.isTextFieldHidden ?? true
            
            textField.text = config?.amount != nil ? "\(config!.amount!)" : ""
            
            succBtn.isEnabled = config?.amount != nil
            
            succBtn.setTitle(config?.firstSegment, for: .normal)
            declBtn.setTitle(config?.secondSegment, for: .normal)
            
            if config!.isTersData {
                self.termsData.isHidden = false
                self.newWeight.isHidden = false
                self.oldWeight.isHidden = false
                self.oldPrice.isHidden = false
                self.newPrice.isHidden = false
                self.newWeight.text = "deal_new_weight".localized() + "\n\(String(describing: config?.termsData?["newWeight"]!.double ?? 0))g"
                self.oldWeight.text = "deal_old_weight".localized() + "\n\(String(describing: config?.termsData?["oldWeight"]!.double ?? 0))g"
                self.oldPrice.text = "deal_old_price".localized() + "\n\(String(describing: config?.termsData?["oldSumToPay"]!.double ?? 0))$"
                self.newPrice.text = "deal_new_price".localized() + "\n\(String(describing: config?.termsData?["newSumToPay"]!.double ?? 0))$"
            }
            
            if config!.isCloseByTap {
                dissmissTouch()
            }
        }
        
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    @IBAction func alertSelect(_ sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == 0) {
            
            if textField.isHidden {
                delegate?.okButtonTapped(count: textField.text ?? "")
                self.dismiss(animated: true, completion: nil)
            }
            else {
                if !(textField.text?.isEmpty ?? true) {
                    delegate?.okButtonTapped(count: textField.text!)
                    self.dismiss(animated: true, completion: nil)
                }
                else {
//                    self.textField.shake()
                }
            }
        }
        else {
            delegate?.cancelButtonTapped()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func successAlert(_ sender: UIButton) {
        if textField.isHidden {
            self.dismiss(animated: true, completion: {
                self.delegate?.okButtonTapped(count: self.textField.text ?? "")
            })
        }
        else {
            if !(textField.text?.isEmpty ?? true) {
                
                self.dismiss(animated: true, completion: {
                    self.delegate?.okButtonTapped(count: self.textField.text!)
                })
            }
            else {
//                self.textField.shake()
            }
        }
    }
    
    @IBAction func hideAlert(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: {
            self.delegate?.cancelButtonTapped()
        })
    }
}

extension CustomAlertViewPurhcases:UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        succBtn.isEnabled = !text.isEmpty
        
        return true
    }
}
