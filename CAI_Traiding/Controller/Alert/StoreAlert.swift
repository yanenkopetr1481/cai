//
//  CreateCategoryAlert.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 29.03.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import UIKit
import RxSwift
import JGProgressHUD

enum CategoryAlertStyle {
    case edit
    case addSubCategory
    case delete
    case addCategory
    case setProductsPrices
    case setProductsAvg
}

struct CategoryAlertConfig {
    var titleName:String
    var btnName:String
    var style:CategoryAlertStyle
}

protocol CreateCategoryAlertDelegate {
    func actionWithCategory(vc:UIViewController, style:CategoryAlertStyle, title:String?)
}

class StoreAlert: UIViewController {
    
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var categoryName:UITextField!
    @IBOutlet weak var createBtn:UIButton!
    @IBOutlet weak var closeBtn:UIButton!

    var selectedCategiry:Int?
    var disposed = DisposeBag()
    var delegate:CreateCategoryAlertDelegate?
    
    var config:CategoryAlertConfig!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewConfiguration()
    }
    
    func setupViewConfiguration() {
        titleLabel.text = config.titleName.localized()
        createBtn.setTitle(config.btnName.localized(), for: .normal)
        
        if (config.style == .setProductsAvg || config.style == .setProductsPrices) {
            categoryName.keyboardType = .decimalPad
        }
        
        categoryName.isHidden = true
        
        if self.config.style != .delete {
            self.createBtn.isEnabled = false
            self.createBtn.alpha = 0.7
            self.categoryName.isHidden = false
            
            categoryName.rx.controlEvent(.editingChanged).subscribe(onNext: {
                _ in
                
                self.createBtn.isEnabled = !self.categoryName.text!.isEmpty
                self.createBtn.alpha = self.categoryName.text!.isEmpty ? 0.7 : 1
                
            }).disposed(by: self.disposed)
        }

        
        createBtn.rx.tap.subscribe(onNext: {
            _ in
            
            self.delegate?.actionWithCategory(vc: self, style: self.config.style, title: self.categoryName.text!)
            
        }).disposed(by: self.disposed)
        
        closeBtn.rx.tap.subscribe(onNext: {
            _ in
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: self.disposed)
        
        closeBtn.setTitle("Close".localized(), for: .normal)
    }
}
