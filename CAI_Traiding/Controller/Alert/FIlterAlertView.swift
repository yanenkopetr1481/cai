//
//  FIlterAlertView.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 18.03.2021.
//  Copyright © 2021 Petr Yanenko. All rights reserved.
//

import UIKit
import LGButton
import SwiftyJSON
import JGProgressHUD
import RxSwift
import RangeSeekSlider

protocol FilterAlertDelegate {
    func setFilter(filters:Filters)
    func annulateFilters()
}

enum FilterStyle {
    case deal
    case product
    case myDeals
}

enum SortType:Int {
    case weight
    case price
}
enum SortDirectionType:Int {
    case DESK
    case ASK
}

struct Filters {
    var productName:String
    var weightFrom:CGFloat
    var weightTo:CGFloat
    var priceFrom:CGFloat
    var priceTo:CGFloat
    var categories:[Int]
    var commision:Bool?
    var dateFrom:Int?
    var dateTo:Int?
    var sort: SortType
    var sortDirection: SortDirectionType
}

class FIlterAlertView: UIViewController {
    
    @IBOutlet weak var checkBox:CheckBox!
    @IBOutlet weak var categoriesTitleLGBtn:LGButton!
    @IBOutlet weak var categoriesStackViews:UIStackView!
    @IBOutlet weak var categoriesParentStackViews:UIStackView!
    @IBOutlet weak var setCategoryBtn:UIButton!
    @IBOutlet weak var annulateFilters:UIButton!
    @IBOutlet weak var closeBtn:UIButton!
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var productName:UITextField!
    @IBOutlet weak var weightRangeSlider:RangeSeekSlider!
    @IBOutlet weak var priceRangeSlider:RangeSeekSlider!
    
    @IBOutlet weak var filterTitleLabel:UILabel!
    @IBOutlet weak var weightLabel:UILabel!
    @IBOutlet weak var priceLabel:UILabel!
    
    @IBOutlet weak var dateStackView:UIStackView!
    @IBOutlet weak var dateTitle:UILabel!
    @IBOutlet weak var dateFromLabel:UILabel!
    @IBOutlet weak var dateFromPicker:UIDatePicker!
    @IBOutlet weak var dateToLabel:UILabel!
    @IBOutlet weak var dateToPicker:UIDatePicker!
    @IBOutlet weak var commisionStackView: UIStackView!
    
    @IBOutlet weak var sortStack:UIStackView!
    @IBOutlet weak var sortLabel:UILabel!
    @IBOutlet weak var sortSegment:UISegmentedControl!
    
    @IBOutlet weak var sortDirectionStack:UIStackView!
    @IBOutlet weak var sortDirectionLabel:UILabel!
    @IBOutlet weak var sortDirectionSegment:UISegmentedControl!

    var disposedBag = DisposeBag()
    var spaceLeading = 10.0
    
    var selectedCategoriesBtns = [LGButton]()
    var selectedCategoriesIds = [Int]()
    var categories:[JSON]?
    var delegate:FilterAlertDelegate?
    
    var config:Filters?
    var style:FilterStyle = .deal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewConfiguration()
        
        if self.style != .myDeals {
            getCategories()
        }
        
        hideKeyboardWhenTappedAround()
    }
    
    func dissmissTouch() {
        alertView.superview?.isUserInteractionEnabled = true
        alertView.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dissmissView)))
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dissmissView))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dissmissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupViewConfiguration() {
        
        sortStack.isHidden = true
        sortDirectionStack.isHidden = true
        
        
        sortLabel.text = "Filter_sortLabel".localized()
        sortDirectionLabel.text = "Filter_sortDirectionLabel".localized()
        
        sortSegment.setTitle("Filter_sort_first_segment".localized(), forSegmentAt: 0)
        sortSegment.setTitle("Filter_sort_second_segment".localized(), forSegmentAt: 1)
        
        sortDirectionSegment.setTitle("Filter_sortDirection_first_segment".localized(), forSegmentAt: 0)
        sortDirectionSegment.setTitle("Filter_sortDirection_second_segment".localized(), forSegmentAt: 1)
                
        dateTitle.text = "Filter_select_date".localized()

        filterTitleLabel.text = "Filter_title".localized()
        weightLabel.text = "Filter_select_weight".localized()
        priceLabel.text = "Filter_select_price".localized()
        dateFromLabel.text = "Filter_select_date_from".localized()
        dateToLabel.text = "Filter_select_date_to".localized()
        setCategoryBtn.setTitle("Filter_set_filter".localized(), for: .normal)
        annulateFilters.setTitle("Filter_reset_filter".localized(), for: .normal)
        
        annulateFilters.isHidden = true
        checkBox.style = .tick
        checkBox.borderStyle = .square
        categoriesParentStackViews.isHidden = true
        
        setCategoryBtn.rx.tap.subscribe(onNext: { _ in
            
            var dateFrom = Int(self.dateFromPicker.date.timeIntervalSince1970)
            let dateTo = Int(self.dateToPicker.date.timeIntervalSince1970)
            
            if (dateFrom == dateTo) {
                dateFrom = dateFrom - (60*60*24)
            }

            let filters = Filters(productName: self.productName.text ?? "", weightFrom: self.weightRangeSlider.selectedMinValue, weightTo: self.weightRangeSlider.selectedMaxValue, priceFrom: self.priceRangeSlider.selectedMinValue, priceTo: self.priceRangeSlider.selectedMaxValue, categories: self.selectedCategoriesIds, commision: self.checkBox.isChecked, dateFrom: dateFrom, dateTo: dateTo, sort: SortType(rawValue: self.sortSegment.selectedSegmentIndex)!, sortDirection: SortDirectionType(rawValue: self.sortDirectionSegment.selectedSegmentIndex)!)
            self.delegate?.setFilter(filters: filters)
            self.dissmissView()
        }).disposed(by: self.disposedBag)
        
        annulateFilters.rx.tap.subscribe(onNext: { _ in
            self.delegate?.annulateFilters()
            self.dissmissView()
        }).disposed(by: self.disposedBag)
        
        closeBtn.rx.tap.subscribe(onNext: { _ in
            self.dissmissView()
        }).disposed(by: self.disposedBag)
        
        if config != nil {
            productName.text = config?.productName
            weightRangeSlider.selectedMinValue = config?.weightFrom ?? 0
            weightRangeSlider.selectedMaxValue = config?.weightTo ?? 10000
            priceRangeSlider.selectedMinValue = config?.priceFrom ?? 0
            priceRangeSlider.selectedMaxValue = config?.priceTo ?? 100000
            checkBox.isChecked = config?.commision ?? false
            annulateFilters.isHidden = false
            sortSegment.selectedSegmentIndex = config?.sort.hashValue ?? 0
            sortDirectionSegment.selectedSegmentIndex = config?.sortDirection.hashValue ?? 0
        }
        
        productName.attributedPlaceholder = NSAttributedString(string: "Filter_input".localized(),
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(0.7)])
        dateStackView.isHidden = true
        
        if self.style == .product || self.style == .myDeals {
            dateStackView.isHidden = false
            commisionStackView.isHidden = true
        }
        
        if self.style == .myDeals {
            sortStack.isHidden = false
            sortDirectionStack.isHidden = false
            categoriesStackViews.isHidden = true
            productName.isHidden = true
            dateStackView.isHidden = true
        }
    }
    
    func getCategories() {
        
        if self.style == .deal {
            
            APIManager.sharedInstance.getCategories(parentID: nil, completionHandler: { (data) in
                
                self.processingCategories(data: data)
                            
            }, viewController: self)
        }
        else {
            APIManager.sharedInstance.getMyCategories(parentID: nil, all: false, completionHandler: { (data) in
                
                self.processingCategories(data: data)
                            
            }, viewController: self)
        }
    }
    
    func processingCategories(data:[String:JSON]) {
        guard let status = data["status"]?.number, status == 1 else {
            return
        }
        
        self.categories?.removeAll()
        self.categories = data["data"]?.array
        
        if (self.categories != nil) {
            
            self.categoriesTitleLGBtn.titleFontSize = 20
            self.categoriesTitleLGBtn.titleFontName = "Helvetica Neue Light"
            self.categoriesTitleLGBtn.spacingLeading = 0
            self.categoriesTitleLGBtn.titleNumOfLines = 10
            self.categoriesTitleLGBtn.titleString = "Select_category".localized()
            self.categoriesTitleLGBtn.titleColor = .white
            self.categoriesTitleLGBtn.layer.cornerRadius = 5
            self.categoriesTitleLGBtn.isSelected = false
            
            self.categoriesTitleLGBtn.rx.controlEvent(.touchUpInside).bind { (_) in
                self.categoriesStackViews.isHidden = self.categoriesTitleLGBtn.isSelected
                self.categoriesTitleLGBtn.isSelected = !self.categoriesTitleLGBtn.isSelected
                
            }.disposed(by: self.disposedBag)
            
            self.categoriesStackViews.removeArrangedSubviews()
            self.categoriesStackViews.removeSubviews()
            
            self.categories!.forEach({ (categories) in
                let stack = UIStackView()
                stack.axis = .vertical
                self.createCategory(parentStack: stack, categories: categories)
                self.categoriesStackViews.addArrangedSubview(stack)
                self.categoriesParentStackViews.isHidden = false
            })
        }
    }
    
    func createCategory(parentStack:UIStackView, categories:JSON, spacingLeading:CGFloat = 0){
        
        let lgBtn = LGButton()
        lgBtn.titleFontSize = 20
        lgBtn.titleFontName = "Helvetica Neue Light"
        lgBtn.spacingLeading = spacingLeading
        lgBtn.titleNumOfLines = 10
        lgBtn.bgColor = .clear
        lgBtn.titleString = "\(categories["title"].string!)"
        lgBtn.titleColor = .black
        lgBtn.layer.cornerRadius = 5
        lgBtn.leftImageSrc = UIImage(named: "check_unfill")
        
        lgBtn.leftAligned = true
        lgBtn.tag = categories["id"].int!
        
        let childStack = UIStackView()
        childStack.isHidden = true
        
        var haveChild = false
        if (categories["hasChilds"].bool ?? false) {
            lgBtn.rightImageSrc = UIImage(named: "chevron_down")
            childStack.axis = .vertical
            haveChild = true
        }
        
        if config != nil {
            config?.categories.forEach({ (id) in
                if id == lgBtn.tag {
                    self.selectedCategoriesBtns.append(lgBtn)
                    self.selectedCategoriesIds.append(lgBtn.tag)
                    lgBtn.leftImageSrc = UIImage(named: "check_fill")
                }
            })
        }
        
        var isChildLoaded = false
        
        lgBtn.rx.controlEvent(.touchUpInside).bind { (_) in
            
            if (categories["hasChilds"].bool ?? false) {
                
                if (!isChildLoaded) {
                    
                    if self.style == .deal {
                        APIManager.sharedInstance.getCategories(parentID: lgBtn.tag, completionHandler: { (data) in
                            
                            guard let status = data["status"]?.number, status == 1 else {
                                UIView.animate(withDuration: 0.1, animations: {
                                })
                                return
                            }
                            
                            data["data"]?.array?.forEach({ (childCategory) in
                                
                                let stack = UIStackView()
                                stack.axis = .vertical
                                self.createCategory(parentStack: stack, categories: childCategory, spacingLeading: CGFloat(self.spaceLeading))
                                childStack.addArrangedSubview(stack)
                            })
                            
                            self.spaceLeading += 10
                            
                            isChildLoaded = true
                            lgBtn.rightImageSrc = UIImage(named: "chevron_up")
                            childStack.isHidden = false
                            return
                        }, viewController: self)
                    }
                    else {
                        APIManager.sharedInstance.getMyCategories(parentID: lgBtn.tag, all: false, completionHandler: { (data) in
                            
                            guard let status = data["status"]?.number, status == 1 else {
                                UIView.animate(withDuration: 0.1, animations: {
                                })
                                return
                            }
                            
                            data["data"]?.array?.forEach({ (childCategory) in
                                
                                let stack = UIStackView()
                                stack.axis = .vertical
                                self.createCategory(parentStack: stack, categories: childCategory, spacingLeading: CGFloat(self.spaceLeading))
                                childStack.addArrangedSubview(stack)
                            })
                            
                            self.spaceLeading += 10
                            
                            isChildLoaded = true
                            lgBtn.rightImageSrc = UIImage(named: "chevron_up")
                            childStack.isHidden = false
                            return
                        }, viewController: self)
                    }
                }
                else {
                    lgBtn.rightImageSrc = !childStack.isHidden ? UIImage(named: "chevron_down") : UIImage(named: "chevron_up")
                    childStack.isHidden = !childStack.isHidden
                }
            }
            
            lgBtn.leftImageSrc = lgBtn.isSelected ? UIImage(named: "check_unfill") : UIImage(named: "check_fill")
            
            if lgBtn.isSelected {
                self.selectedCategoriesBtns.removeAll { (btn) -> Bool in
                    return btn === lgBtn
                }
                
                self.selectedCategoriesIds.removeAll { (id) -> Bool in
                    return id == lgBtn.tag
                }
            }
            else {
                self.selectedCategoriesBtns.append(lgBtn)
                self.selectedCategoriesIds.append(lgBtn.tag)
            }
            
            lgBtn.isSelected = !lgBtn.isSelected
            
        }.disposed(by: self.disposedBag)
        
        parentStack.addArrangedSubview(lgBtn)
        
        if haveChild {
            parentStack.addArrangedSubview(childStack)
        }
    }
}

