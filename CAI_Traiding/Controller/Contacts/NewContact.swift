//
//  NewContact.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 02.02.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit
import JGProgressHUD

class NewContact: UIViewController {
    
    @IBOutlet weak var userName:UILabel!
    @IBOutlet weak var userPhone:UILabel!
    @IBOutlet weak var userNickaname:UILabel!
    @IBOutlet weak var userNickanameLoc:UILabel!
    @IBOutlet weak var searchBtn:UIButton!
    @IBOutlet weak var searchField:UITextField!
    @IBOutlet weak var backBtn:UIButton!

    var userID:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setTransparentNavigationBar()
        setupViewConfiguration()
    }
    
    @objc func adjustForKeyboard(notification: NSNotification) {
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }        } else {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0 {
                    self.view.frame.origin.y -= keyboardSize.height
                }
            }
        }
    }
    
    func setTransparentNavigationBar() {
        backBtn.isHidden = true
        if (self.navigationController != nil) {
            self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController!.navigationBar.shadowImage = UIImage()
            self.navigationController!.navigationBar.isTranslucent = true
        }
        else {
            backBtn.isHidden = false
        }
    }
    
    func setupViewConfiguration() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)

        self.searchField.addTarget(self, action: #selector(searchContact(value:)), for: .editingChanged )
        self.userName.text = "contact_not_faund".localized()
        self.searchBtn.setTitle("contact_add_to".localized(), for: .normal)
        self.userNickanameLoc.text = "contact_input_user_identifier".localized()
        self.searchBtn.isEnabled = false
        self.searchBtn.alpha = 0.5
        self.title = "addContact".localized()
    }
    
    @IBAction func back(_ sender:Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func previus(_ sender:Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addContact(_ sender:UIButton) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        APIManager.sharedInstance.addContact(id: userID!, completionHandler: { (data) in
            
            guard let status = data["status"]?.int, status == 1 else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["data"]?.string ?? ""
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                })
                
                hud.dismiss(afterDelay: 2.0, animated: true)
                
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = nil
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            hud.dismiss(afterDelay: 1.0, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                self.back(self)
            })
        }, viewController: self)
    }
    
    @objc func searchContact(value:UITextField) {
        
        if value.text?.count > 3, !value.text!.isEmpty {
            
            var phone = value.text!
            phone = phone.replacingOccurrences(of: ")", with: "")
            phone = phone.replacingOccurrences(of: "(", with: "")
            phone = phone.replacingOccurrences(of: " ", with: "")

            APIManager.sharedInstance.checkPhone(phoneNumber: phone, onSuccess: { (data) in
                guard let status = data["status"]?.number, status == 1, let user = data["data"]?.dictionary,
                    let name = user["name"]?.string,
                    let phone = user["phoneNumber"]?.string,
                    let nickName = user["nickName"]?.string,
                    let id = user["id"]?.number
                    else {
                        self.userName.text = "contact_not_faund".localized()
                        self.userPhone.text = ""
                        self.userNickaname.text = ""
                        self.searchBtn.isEnabled = false
                        self.searchBtn.alpha = 0.5
                        
                        return
                }
                
                self.searchBtn.alpha = 1
                self.searchBtn.isEnabled = true
                self.userID = (id as! Int)
                self.userName.text = name
                self.userPhone.text = phone
                self.userNickaname.text = nickName
            }, viewController: self)
        }
    }
}
