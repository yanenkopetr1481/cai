//
//  Seller.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 10/17/19.
//  Copyright © 2019 Petr Yanenko. All rights reserved.
//

import UIKit
import AARefreshControl
import JGProgressHUD
import RxSwift
import SwiftyJSON
import SwiftyContacts
import NotificationBannerSwift

class Seller: RootViewController {
    
    @IBOutlet weak var sellersTable:UITableView!
    @IBOutlet weak var addContactBtn:UIButton!
    @IBOutlet weak var searchBtn:UIButton!
    @IBOutlet weak var searchTextField:UITextField!
    
    var sellersData:[SellerData] = []
    var sellerstID:[NSNumber] = [NSNumber]()
    var refreshControl: AARefreshControl!
    var delegate:SelectContactDelegate?
    var isAddContactBtnHidden:Bool = false
    var isResponseTry:Bool = false
    var fetchingMore:Bool = false
    var isDelegate:Bool = false
    var isRemoveImage = false
    var contactID:Int?
    var sellerID:Int = 0
    var page:Int = 0
    var isAddContactView = false
    
    var alertTitle:String = "contact_alertTitle".localized()
    var alertTrueButton:String = "contact_selectFromPhone".localized()
    var alertFalseButton:String = "contact_selectFromApp".localized()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewConfiguration()
        configureFooter()
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkDeletedData()
        getPageData(updateTable: true)
    }
    
    func checkDeletedData() {
        sellersData = [SellerData]()
        sellerstID = [NSNumber]()
        page = 0
    }
    
    func configureFooter() {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: sellersTable.bounds.width, height: CGFloat(44))
        self.sellersTable.tableFooterView = spinner
        self.sellersTable.tableFooterView?.isHidden = true
    }
    
    @objc func refresh(_ sender: Any?) {
        if !fetchingMore {
            
            fetchingMore = true
            sellersData = [SellerData]()
            sellerstID = [NSNumber]()
            page = 0
            
            self.getPageData(updateTable: true, beginRefresh: true)
        }
        else {
            self.refreshControl.endRefreshing()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !sellersData.isEmpty {
            sellersTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
    }
    
    private func setTableViewEmptyBackground() {
        UIView.animate(withDuration: 0.5) {
            let view = Bundle.main.loadNibNamed("EmptyTableView", owner: self, options: nil)?.first as! EmptyTableView
            view.titleLabel.text = "EmptyRessult".localized()
            view.refreshBtn.setTitle("Refresh".localized(), for: .normal)
            view.delegate = self
            view.imageView.image = UIImage(named: "empty")
            
            self.sellersTable.backgroundView = view
        }
    }
    
    func dataProcessingContacts(data:[String:JSON], updateTable:Bool) {
        var count = 0
        var isChanged = false
        
        if let status = data["status"]?.number, status == 1, let response = data["data"]?.array, !response.isEmpty  {
            count = response.count
            
            response.forEach { value in
                guard let user = value.dictionary,
                      let name = user["name"]?.string,
                      let phone = user["phone"]?.string,
                      let nickName = user["nickname"]?.string,
                      let id = user["id"]?.number
                else { return }
                
                if !self.sellerstID.contains(id) {
                    isChanged = true
                    let data = SellerData(id: Int(truncating:id), name: name, phone: phone, nickName: nickName)

                    self.sellersData.append(data)
                    self.sellerstID.append(id)
                }
            }
            
            if isChanged {
                self.page += 1
            }
        }
        
        if self.sellersData.isEmpty {
            
            if (!self.isResponseTry) {
                self.isResponseTry = true
                self.getPageData()
            }
            else {
                self.isResponseTry = false
                self.setTableViewEmptyBackground()
                self.sellersData.removeAll()
                self.sellerstID.removeAll()
                self.sellersTable.reloadData()
            }
        }
        else {
            
            if (count < 15 && self.page > 0) {
                self.page -= 1
            }
            
            if (isChanged) {
                self.sellersTable.reloadData()
            }
            
            self.isResponseTry = false
        }
        
        self.sellersTable.tableFooterView?.isHidden = true
        self.sellersTable.isUserInteractionEnabled = true
        self.refreshControl.endRefreshing()
        self.fetchingMore = false
    }
    
    func getPageData(updateTable:Bool = false, beginRefresh: Bool = true) {
        
        fetchingMore = true
        self.sellersTable.isUserInteractionEnabled = false
        
        if (beginRefresh) {
            self.refreshControl.beginRefreshing()
        }
        
        self.sellersTable.backgroundView = UIView()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            APIManager.sharedInstance.getSellers(page: self.page, onSuccess: { (data) in
                self.dataProcessingContacts(data: data, updateTable: updateTable)
            }, viewController: self)
        }
    }
    
    func setupViewConfiguration() {
        addContactBtn.isHidden = isAddContactBtnHidden
        refreshControl = AARefreshControl(scrollView: sellersTable)
        refreshControl.tintColor = UIColor.black
        refreshControl.activityIndicatorViewColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        sellersTable.addSubview(refreshControl)
        
        sellersTable.register(UINib(nibName: "SellerCell", bundle: nil), forCellReuseIdentifier: "seller")
        self.sellersTable.rowHeight = UITableView.automaticDimension
        self.sellersTable.estimatedRowHeight = 90
        self.sellersTable.layer.cornerRadius = 10
        
        searchBtn.rx.tap.subscribe(onNext: {
            _ in
            
            guard let name = self.searchTextField.text else {
                return
            }
            self.searchContactByName(name: name)
        }).disposed(by: disposeBag)
        
        addContactBtn.rx.tap.subscribe(onNext: {
            _ in
            self.selectContactType()
        }).disposed(by: self.disposeBag)
    }
    
    func searchContactByName(name:String) {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        APIManager.sharedInstance.searchContact(value: name, completionHandler: { (data) in
            
            guard let status = data["status"]?.int, status == 1, let contacts = data["data"]?.array, !contacts.isEmpty else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["status"]?.string ?? ""
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                })
                
                hud.dismiss(afterDelay: 0.5, animated: true)
                
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = nil
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            hud.dismiss(afterDelay: 0.5, animated: true)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                self.sellersData.removeAll()
                self.sellerstID.removeAll()
                self.dataProcessingContacts(data: data, updateTable: true)
            }
            
        }, viewController: self)
    }
    
    @objc func editUser(_ sender:UIButton) {
        contactID = sender.tag
        performSegue(withIdentifier: "editUser", sender: self)
    }
    
    @objc func removeUserAlert(_ sender:UIButton) {
        sellerID = sender.tag
        isRemoveImage = true
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.config = AlertCustomData(title: "contact_remove_contact".localized(), firstSegment: "Yes".localized(), secondSegment: "No".localized(), amount:1)
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func removeContact() {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        APIManager.sharedInstance.removeContact(id: sellerID, completionHandler: { (data) in
            guard let status = data["status"]?.int, status == 1 else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["status"]?.string ?? ""
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                
                return
            }
            
            self.sellerstID.removeAll(NSNumber(value: self.sellerID))
            
            self.sellersData.removeAll { (data) -> Bool in
                return data.id == self.sellerID
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = nil
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            hud.dismiss(afterDelay: 1.0, animated: true)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) {
                self.sellersTable.reloadData()
                
                if self.sellersData.isEmpty {
                    self.setTableViewEmptyBackground()
                }
            }
            
        }, viewController: self)
    }
    
    func selectContactType() {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert") as! CustomAlertViewPurhcases
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.config = AlertCustomData(title: alertTitle, firstSegment: alertTrueButton, secondSegment: alertFalseButton,amount: 1)
        self.present(customAlert, animated: true, completion: nil)
        self.isAddContactView = true
    }
}

extension Seller: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.sellersData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "seller", for: indexPath) as! SellerCell
        
        if indexPath.row < self.sellersData.count {
            cell.removeBtn.tag = self.sellersData[indexPath.row].id
            cell.removeBtn.addTarget(self, action: #selector(removeUserAlert(_:)), for: .touchUpInside)
            cell.editBtn.addTarget(self, action: #selector(editUser(_:)), for: .touchUpInside)
            cell.editBtn.tag = self.sellersData[indexPath.row].id
            cell.setupView(data: self.sellersData[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.setUser(user: self.sellersData[indexPath.row])
        if (isDelegate) {
            self.dismiss(animated: true, completion: nil)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (indexPath.row == sellersData.count - 1) {
            if !fetchingMore {
                if (indexPath.row > 4) {
                    self.sellersTable.tableFooterView?.isHidden = false
                    getPageData(updateTable: false, beginRefresh: false)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? EditUser {
            vc.contactID = contactID
        }
    }
}

extension Seller:EmptyTableViewDelegate {
    func refreshData() {
        if !fetchingMore {
            self.getPageData(updateTable: true)
        }
    }
}

extension Seller: CustomAlertViewDelegate {
    
    func okButtonTapped(count: String) {
        
        if isAddContactView {
            
            requestAccess { (responce) in
                if responce {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(style: .alert)
                        alert.addContactsPicker { contacts in
                            
                            var contactIDs = [Int]()
                            
                            if !(contacts?.isEmpty ?? true) {
                                contacts?.forEach({ (contact) in
                                    contactIDs.append(contact.appID)
                                })
                                
                                let hud = JGProgressHUD(style: .dark)
                                hud.textLabel.text = "Sending".localized()
                                hud.vibrancyEnabled = true
                                hud.show(in: self.view)
                                
                                APIManager.sharedInstance.addContactsV2(ids: contactIDs, completionHandler: { (data) in
                                    guard let status = data["status"]?.int, status == 1 else {
                                        UIView.animate(withDuration: 0.1, animations: {
                                            hud.textLabel.text = "Error".localized()
                                            hud.detailTextLabel.text = data["status"]?.string ?? ""
                                            hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                        })
                                        
                                        hud.dismiss(afterDelay: 1.0, animated: true)
                                        
                                        return
                                    }
                                    
                                    UIView.animate(withDuration: 0.1, animations: {
                                        hud.textLabel.text = "Success".localized()
                                        hud.detailTextLabel.text = data["data"]?.string ?? ""
                                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                                    })
                                    
                                    hud.dismiss(afterDelay: 1.0, animated: true)
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) {
                                        self.refresh(self)
                                    }
                                    
                                }, viewController: self)
                            }
                        }
                        alert.addAction(title: "Cancel".localized(), style: .cancel)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    self.isAddContactView = false
                } else {
                    let banner = NotificationBanner(title: "ContactsAcessDenied".localized(), subtitle: "GrantAccess".localized(), style: .info)
                    banner.show()
                }
            }
        }
        else {
            removeContact()
        }
    }
    
    func cancelButtonTapped() {
        
        if isAddContactView {
            self.performSegue(withIdentifier: "showContactVC", sender: self)
            isAddContactView = false
        }
        else {
            removeContact()
        }
    }
}
