//
//  EditUser.swift
//  CAI_Traiding
//
//  Created by Petr Yanenko on 23.04.2020.
//  Copyright © 2020 Petr Yanenko. All rights reserved.
//

import UIKit

import JGProgressHUD

class EditUser: UIViewController {
    
    @IBOutlet weak var userName:UILabel!
    @IBOutlet weak var applyBtn:UIButton!
    @IBOutlet weak var searchField:UITextField!
    @IBOutlet weak var backBtn:UIButton!

    var contactID:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setTransparentNavigationBar()
        setupViewConfiguration()
    }
    
    @objc func adjustForKeyboard(notification: NSNotification) {
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }        } else {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0 {
                    self.view.frame.origin.y -= keyboardSize.height
                }
            }
        }
    }
    
    func setTransparentNavigationBar() {
        backBtn.isHidden = true

        if (self.navigationController != nil) {
            self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController!.navigationBar.shadowImage = UIImage()
            self.navigationController!.navigationBar.isTranslucent = true
        }
        else {
            backBtn.isHidden = false
        }
    }
    
    @IBAction func previus(_ sender:Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupViewConfiguration() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.userName.text = "contact_user_name".localized()
        self.applyBtn.setTitle("Save".localized(), for: .normal)
        self.applyBtn.isEnabled = false
        self.applyBtn.alpha = 0.5
        self.searchField.addTarget(self, action: #selector(checkField(value:)), for: .editingChanged)
        self.title = "Save".localized()
    }
    
    @IBAction func back(_ sender:Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editContact(_ sender:UIButton) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Sending".localized()
        hud.vibrancyEnabled = true
        hud.show(in: self.view)
        
        APIManager.sharedInstance.editContact(id: contactID!, value: searchField.text!, completionHandler: { (data) in
            
            guard let status = data["status"]?.int, status == 1 else {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Error".localized()
                    hud.detailTextLabel.text = data["status"]?.string ?? ""
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0, animated: true)
                
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                hud.textLabel.text = "Success".localized()
                hud.detailTextLabel.text = nil
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            
            hud.dismiss(afterDelay: 1.0, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                self.back(self)
            })
        }, viewController: self)
    }
    
    @objc func checkField(value:UITextField) {
        
        if value.text?.count > 1, !value.text!.isEmpty {
            self.applyBtn.isEnabled = true
            self.applyBtn.alpha = 1
        }
        else {
            self.applyBtn.isEnabled = false
            self.applyBtn.alpha = 0.5
        }
    }
}

extension EditUser:UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return !string.isSingleEmoji
    }
}
